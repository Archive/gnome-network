/* sync.h - Main header file for synchronizer.

   Copyright (C) 1998 Tom Tromey
   Copyright (C) 2002 Rodrigo Moya

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#ifndef SYNC_H
#define SYNC_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>

/* Configuration functions (config.c) */
void          config_init (void);
void          config_shutdown (void);
GtkTreeModel *config_get_entries_tree_model (void);

/* Entry list widget (entrylist.c) */
GtkWidget *entry_list_new (void);

/* Entry configuration dialog (entryconfigdialog.c) */
void entry_config_dialog_edit_new (void);

/* Global variables (main.c) */
extern GnomeApp *main_window;
extern GtkWidget *entries_list;

#endif /* SYNC_H */
