/* entrylist.c - Entry list widget

   Copyright (C) 1998 Tom Tromey
   Copyright (C) 2002 Rodrigo Moya

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#include "sync.h"

static void
name_set_func (GtkTreeViewColumn *tree_column,
	       GtkCellRenderer *cell_renderer,
	       GtkTreeModel *model,
	       GtkTreeIter *iter,
	       gpointer user_data)
{
}

static void
local_set_func (GtkTreeViewColumn *tree_column,
		GtkCellRenderer *cell_renderer,
		GtkTreeModel *model,
		GtkTreeIter *iter,
		gpointer user_data)
{
}

static void
remote_set_func (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer *cell_renderer,
		 GtkTreeModel *model,
		 GtkTreeIter *iter,
		 gpointer user_data)
{
}

static void
last_sync_set_func (GtkTreeViewColumn *tree_column,
		    GtkCellRenderer *cell_renderer,
		    GtkTreeModel *model,
		    GtkTreeIter *iter,
		    gpointer user_data)
{
}

GtkWidget *
entry_list_new (void)
{
	GtkWidget *list;

	list = gtk_tree_view_new_with_model (config_get_entries_tree_model ());

	/* adds columns */
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (list), 0, _("Name"),
						    gtk_cell_renderer_text_new (),
						    name_set_func, NULL, NULL);
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (list), 1, _("Local"),
						    gtk_cell_renderer_text_new (),
						    local_set_func, NULL, NULL);
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (list), 2, _("Remote"),
						    gtk_cell_renderer_text_new (),
						    remote_set_func, NULL, NULL);
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (list), 3, _("Last sync"),
						    gtk_cell_renderer_text_new (),
						    last_sync_set_func, NULL, NULL);

	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (list), TRUE);

	return list;
}
