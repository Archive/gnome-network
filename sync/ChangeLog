2002-12-14  Rodrigo Moya <rodrigo@gnome-db.org>

	* main.c (main): set the size of the window, or it gets too small.

2002-12-14  Rodrigo Moya <rodrigo@gnome-db.org>

	* main.c (setup_menu): create the toolbar by hand.

	* entryconfigdialog.c (load_dialog_from_glade): get pointers to all
	widgets in the dialog.

	* entryconfigdialog.glade: added content to dialog.

2002-12-14  Rodrigo Moya <rodrigo@gnome-db.org>

	Started rewrite to allow more backends (rsync, unison, etc), use GConf
	instead of Berkeley DB and use GNOME 2 look&feel.

	* main.c: added extra separator in File menu.
	(setup_contents): use a GtkTreeView instead of GtkCList.
	(setup_menu): added toolbar to main window.

	* sync.h: cleaned up.

	* entrylist.c: added implementation of entries list widget.

	* config.c: added configuration access functions.

	* entryconfigdialog.glade:
	* entryconfigdialog.c: added configuration dialog for entries.

2002-12-13  German Poo-Caaman~o <gpoo@ubiobio.cl>

	* main.c:
	* new.c: Fixed data types and casting to compile 
	  without warnings.

2002-12-07  Rodrigo Moya <rodrigo@gnome-db.org>

	* new.c (create_dialog): use GtkDialog instead of GnomeDialog.
	(dialog_response_cb): new function, being the result of merging
	ok_button and cancel_button callbacks.

	* sync.h: added "/apps/gnome-sync" prefix to configuration entries.

	* main.c (main):
	* property.c (apply_properties): use GConf for configuration.

2000-05-15  Andreas Hyden  <a.hyden@cyberpoint.se>

	* gnome-sync.desktop: Added Swedish translation.

2000-04-16  Karl EICHWALDER  <ke@suse.de>

	* gnome-sync.desktop: Add de.

1999-08-14  Matthias Warkus  <mawa@iname.com>

	* Makefile.am: Install gnome-sync.png
	* gnome-sync.desktop: Use gnome-sync.png.
	* gnome-sync.png: Added icon by Ben Frantzdale.

1999-02-22  Sergey Panov  <sipan@mit.edu>

        * Makefile.am: new path to install desktop file in;

1999-01-31  Miguel de Icaza  <miguel@nuclecu.unam.mx>

	* main.c (file_menu): Standarize the menus in gnome-sync

1998-12-08  Tom Tromey  <tromey@cygnus.com>

	* errors.c (db_posix_error): Use gnome_dialog_run_and_close.
	* main.c (setup_contents): Removed `const' to track gtk.  Use
	gtk_scrolled_window_add_with_viewport.

1998-12-04  Herbert Valerio Riedel  <hvr@hvrlab.ml.org>

	* main.c (remove_callback): GPOINTER_TO_INT instead of a mere (gint)

1998-11-23  Miguel de Icaza  <miguel@nuclecu.unam.mx>

	* main.c (setup_contents): Fix GtkCList usage.

Tue Sep 22 18:24:03 1998  Tom Tromey  <tromey@cygnus.com>

	* sync.h: Include <db_185.h> if it exists.
	* Makefile.am (gnome_sync_LDADD): Don't link in -ldb.

Sun Aug 16 17:15:59 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (column_clicked): New function.
	(setup_contents): Arrange for clist to be sorted, and for column
	clicks to sort the column.

Sat Jul 25 16:49:39 1998  Tom Tromey  <tromey@cygnus.com>

	* new.c (create_dialog): Set parent of dialog.
	* main.c (main): Initialize `top' member of database.
	(new_sync_app): Set `top' member.
	* sync.h (struct database): Added `top' member.

Thu Jul 23 01:24:59 1998  Tom Tromey  <tromey@cygnus.com>

	* property.c (make_label_entry): Use GNOME_PAD and
	GNOME_PAD_SMALL.
	(show_properties): Likewise.

1998-07-23  Nuno Ferreira  <nmrf@rnl.ist.utl.pt>

	* gnome-sync.desktop: Added Portuguese translation.

Sun Jul 19 22:57:42 1998  Tom Tromey  <tromey@cygnus.com>

	* property.c (show_properties): Use gtk_notebook_append_page.
	(apply_properties): Just return if applying a page other than 0.

Sat Jul 18 16:36:15 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (about): Added cast to avoid warning.

Wed Jul 15 23:08:47 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (main): Application id is "Synchronize", not
	"gnome-sync".
	* new.c (drop_on_entry): Removed.
	(n_drop_types): Likewise.
	(drop_types): Likewise.
	(make_entry): Added `id' argument.  Use gnome_file_entry widget.
	* sync.h: Don't mention make_label_entry.
	* property.c (make_label_entry): Moved from new.c.  Now static.

Tue Jul  7 00:39:23 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (about): Minor text changes.

Sun Jul  5 01:36:56 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (main): Set max_file_size and concurrent_transfers.
	(config_callback): New function.
	(file_menu): Added Properties item.
	* sync.h (KEY_FILE_SIZE): New macro.
	(KEY_TRANSFERS): Likewise.

	* sync.c (read_chunk): If malloc fails, revert to "unfull"
	operation.

	* sync.c (MAX_SIZE): Redefined to use max_file_size.
	(finish): Correctly handle case where multiple transfers taking
	place in parallel.
	(synchronize): Use concurrent_transfers to launch several
	operations in parallel.
	* sync.h: Declare new globals.
	* main.c (concurrent_transfers, max_file_size): New globals.
	* Makefile.am (gnome_sync_SOURCES): Added property.c.
	* property.c: New file.
	* new.c (make_label_entry): New function.
	(make_entry): Use it.

	* All files: Added copyright information.

Sat Jul  4 12:28:51 1998  Tom Tromey  <tromey@cygnus.com>

	* md5.c (md5_stream): Removed unused variable.

	* copy.c (copy): Display copying message at start of copy.

	* db.c (remove_entry): New function.
	* main.c (row_selected): Only create edit window on double click.
	(remove_callback): New function.
	(sync_menu): Add function for Remove entry.

	* new.c (cancel_button): Dialog now passed in as client data.
	(drop_types): New array.
	(n_drop_types): New macro.
	(make_entry): Enable drop support on entry.  Added `text'
	argument.
	(drop_on_entry): New function.

	* main.c (setup_contents): Set selection mode of clist.
	(find_menu_widget): New function.
	(setup_menu): Disable all Synchronize menu items.
	(row_unselected): Disable menu item when selection cleared.
	(row_selected): Enable menu item when selection set.

Fri Jul  3 00:04:08 1998  Tom Tromey  <tromey@cygnus.com>

	* sync.c (start_file_synchronization): Call display_message.
	(synchronize_one_file): Likewise.
	* copy.c (write_chunk): Call display_message.
	* errors.c (display_message): New function.
	(add_display_entry): Use it.
	(display_error): Likewise.

	* copy.c (write_chunk): Corrected assertion.
	(copy): Correctly handle FLAG_MAPPED.
	* sync.c (file_done): New function.
	(read_chunk): Use it.
	(start_file_synchronization): Likewise.
	(synchronize_when_idle): New function.
	(start_file_synchronization): Use it.

Thu Jul  2 20:32:59 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (main): Choose a better default file name.
	* sync.c (start_reading): If open failed because file doesn't
	exist, set state to STATE_MISSING.
	(start_file_synchronization): If both files missing, display error
	and continue.
	(synchronize_one_file): Correctly handle STATE_MISSING.
	(read_chunk): Likewise.
	* new.c (ok_button): Use CHECKSUM_NEVER.
	* sync.h (STATE_MISSING): New define.
	(CHECKSUM_NEVER): Likewise.

	* errors.c (add_display_entry): New function.
	(find_entry): New function.
	(display_error): Use it.
	* new.c (ok_button): New function.
	(new_entry): Wrote.  Changed type of `database' argument.
	(create_dialog): New function.
	(edit_entry): Wrote.
	(cancel_button): New function.
	(delete_event): Likewise.
	(make_entry): New function.
	* main.c (setup_contents): Set column widths and window size.
	(sync_menu): Added Remove and Retry entries.
	(new_entry_callback): New function.
	(sync_menu): Use it.

	* sync.c (start_file_synchronization): Prepend reader to `ios'.
	(free_reader): Remove reader from `ios'.
	(shut_down): New function.
	(synchronize): Shut down every reader and free `ios' before
	starting new synchronization.
	* main.c (main): Initialize `ios' member.
	* sync.h (struct database): Added `ios' member.

	* db.c (database_fill_list): Changed name and type of `database'
	argument.
	* copy.c (write_chunk): Call db_posix_error.
	* sync.c (finish): Call db_posix_error.
	(schedule_next_file): Likewise.
	(synchronize): Likewise.
	* main.c (main): Initialize filename member.
	(setup_contents): Changed type and name of `database' argument.
	(saved_db): Removed.
	* sync.h (struct database): Added `filename' member.
	* errors.c (db_posix_error): New function.

	* db.c (free_entry): New function.
	* copy.c (copy): Put `to' into STATE_WRITING.
	(write_chunk): Let free_reader halt i/o.
	* sync.h (STATE_WRITING): New macro.
	* sync.c (start_reading): Put file into STATE_READING.
	(cancel_read): Handle STATE_WRITING.
	(cancel_io): Renamed from cancel_read.
	(free_reader): Wrote.
	(free_file): Wrote.
	(read_chunk): Don't call cancel_read.
	(start_file_synchronization): Likewise.

Wed Jul  1 00:01:01 1998  Tom Tromey  <tromey@cygnus.com>

	* copy.c (write_chunk): Call update_entry.
	* db.c (update_entry): New function.

	* sync.c (finish): New function.
	(synchronize): Use it.
	(schedule_next_file): Likewise.
	* errors.c (clear_errors): Clear error flag.
	(display_error): Set error flag.
	* main.c (main): Initialize list, flags elements of database.
	(force_quit): New function.
	(quit_flag): New global.
	(main): Respect it.
	(quit): Just call force_quit.
	(session_die): Likewise.
	* sync.h (struct database): Added `flags'.
	(FLAG_DB_SYNC): New macro.
	(FLAG_DB_ERROR): Likewise.

	* sync.c (start_file_synchronization): Changed return value, type
	of database argument.
	(schedule_next_file): Changed type of argument.  Loop until a file
	read succeeds.
	(synchronize): Likewise.
	* errors.c (display_error): First arg now a `struct database *'.
	(display_posix_error): Likewise.
	(clear_errors): Likewise.  Wrote.
	(display_error): Wrote.
	* sync.h (struct database): New structure.
	(struct reader): `database' element now a `struct database *'.
	* db.c (database_fill_list): Fill new column.
	* main.c (setup_contents): Added `Status' column.
	(new_sync_app): Changed type of argument; no longer static.
	* sync.c (synchronize): Argument now a `DB *'.
	* main.c (sync_callback): New function.
	(quit): Added arguments.
	(file_menu): Use sync_callback.

	* sync.c (read_chunk): Use display_posix_error.
	(new_file): Added `name' argument.
	(start_file_synchronization): Use display_posix_error.
	* copy.c (write_chunk): Use display_posix_error.
	(copy): Likewise.
	* errors.c (display_posix_error): New function.

	* db.c (database_open): New function.
	* main.c (options): New array.
	(parse_an_arg): New function.
	(just_sync): New global.
	(parser): New global structure.
	(main): Pass parser to gnome_init.  Open database here.  Handle
	--resync command-line option.
	(new_sync_app): Added database argument.

	* new.c (new_entry): Added database argument.

	* copy.c (write_chunk): New function.
	(copy): Wrote.

	* sync.c (new_file): Initialize mode, flags.
	(start_reading): Set mode in file structure.
	(free_reader): No longer static.
	(synchronize_one_file): Only call free_reader() when not calling
	copy().
	(read_chunk): Clear full flag when required.
	(start_reading): Set full flag.

	* sync.h (struct file): Added mode member.  Removed mapped, added
	flags.
	(FLAG_MAPPED): New define.
	(FLAG_FULL): Likewise.

Tue Jun 30 10:13:10 1998  Tom Tromey  <tromey@cygnus.com>

	* sync.c (fillbuf): Removed.
	(SWAP): Removed.
	(read_chunk): Call md5_final_block.

	* md5.c (md5_init_ctx): Initialize length.
	(md5_final_block): New function.
	(md5_buffer): Use it.
	(md5_stream): Likewise.
	(md5_process_block): Update length element of context.
	* md5.h (struct md5_ctx): Added `length' member.
	(md5_final_block): Declare.

	* Makefile.am (gnome_sync_SOURCES): Added errors.c.
	* errors.c: New file.
	* sync.c (synchronize_one_file): Call display_error, not conflict.
	(synchronize): Call clear_errors.

Sun Jun 28 00:04:49 1998  Tom Tromey  <tromey@cygnus.com>

	* sync.c (synchronize_one_file): Call free_reader.
	(read_chunk): Likewise.
	(start_file_synchronization): Likewise.

	* sync.c (cancel_read): Handle mmap()ed files.
	(start_reading): Correct mmap and schedule reads.
	(read_chunk): New function.
	(MAX_SIZE): New define.
	(fillbuf): New global.
	(schedule_next_file): New function.
	(free_reader): Likewise.
	* new.c: New file.
	* md5.c, md5.h: New files from GNU sharutils.
	* db.c (database_fill_list): New function.
	(lookup): Likewise.
	* main.c (quit): New function.
	(session_die): Likewise.
	(session_save): Likewise.

Sat Jun 27 21:24:35 1998  Tom Tromey  <tromey@cygnus.com>

	* main.c (file_menu): New array.
	(help_menu): Likewise.
	(sync_menu): Likewise.
	(menubar): Likewise.
	(setup_menu): New function.

Tue Jun 23 20:25:15 1998  Tom Tromey  <tromey@cygnus.com>

	* gnome-sync.desktop: New file.
	* main.c: New file.

