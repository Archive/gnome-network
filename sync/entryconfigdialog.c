/* entryconfigdialog.c - Dialog for configuring sync entries

   Copyright (C) 1998 Tom Tromey
   Copyright (C) 2002 Rodrigo Moya

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#include "sync.h"
#include <glade/glade-xml.h>

typedef struct {
	GtkWidget *dialog;
	GtkWidget *entry_name;
	GtkWidget *local_path;
	GtkWidget *remote_host;
	GtkWidget *remote_path;
	GtkWidget *rsync_method;
	GtkWidget *unison_method;
	GtkWidget *method_notebook;

	GtkWidget *rsync_recurse;
	GtkWidget *rsync_relative_paths;
	GtkWidget *rsync_update_only;
	GtkWidget *rsync_preserve_perms;
	GtkWidget *rsync_preserve_group;
	GtkWidget *rsync_preserve_times;

	GladeXML *xml;
} DialogPrivateData;

static void
rsync_toggled_cb (GtkToggleButton *button, gpointer user_data)
{
	DialogPrivateData *priv_data = user_data;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv_data->rsync_method)))
		gtk_notebook_set_current_page (GTK_NOTEBOOK (priv_data->method_notebook), 0);
}

static void
unison_toggled_cb (GtkToggleButton *button, gpointer user_data)
{
	DialogPrivateData *priv_data = user_data;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv_data->unison_method)))
		gtk_notebook_set_current_page (GTK_NOTEBOOK (priv_data->method_notebook), 1);
}

static DialogPrivateData *
load_dialog_from_glade (void)
{
	DialogPrivateData *priv_data;

	priv_data = g_new0 (DialogPrivateData, 1);
	priv_data->xml = glade_xml_new (GLADEDIR "/entryconfigdialog.glade", "entry-config-dialog", NULL);
	if (!priv_data->xml) {
		gnome_error_dialog (_("Could not load UI for entry configuration dialog"));
		g_free (priv_data);
		return;
	}

	priv_data->dialog = glade_xml_get_widget (priv_data->xml, "entry-config-dialog");
	gtk_dialog_set_default_response (GTK_DIALOG (priv_data->dialog), GTK_RESPONSE_OK);

	priv_data->entry_name = glade_xml_get_widget (priv_data->xml, "entry-name");
	priv_data->local_path = glade_xml_get_widget (priv_data->xml, "entry-local-path");
	priv_data->remote_host = glade_xml_get_widget (priv_data->xml, "entry-remote-host");
	priv_data->remote_path = glade_xml_get_widget (priv_data->xml, "entry-remote-path");

	priv_data->rsync_method = glade_xml_get_widget (priv_data->xml, "entry-rsync");
	g_signal_connect (G_OBJECT (priv_data->rsync_method), "toggled",
			  G_CALLBACK (rsync_toggled_cb), priv_data);

	priv_data->unison_method = glade_xml_get_widget (priv_data->xml, "entry-unison");
	g_signal_connect (G_OBJECT (priv_data->unison_method), "toggled",
			  G_CALLBACK (unison_toggled_cb), priv_data);

	priv_data->method_notebook = glade_xml_get_widget (priv_data->xml, "entry-method-notebook");
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (priv_data->method_notebook), FALSE);

	priv_data->rsync_recurse = glade_xml_get_widget (priv_data->xml, "entry-rsync-recurse");
	priv_data->rsync_relative_paths = glade_xml_get_widget (priv_data->xml, "entry-rsync-relative-paths");
	priv_data->rsync_update_only = glade_xml_get_widget (priv_data->xml, "entry-update-only");
	priv_data->rsync_preserve_perms = glade_xml_get_widget (priv_data->xml, "entry-rsync-preserve-perms");
	priv_data->rsync_preserve_group = glade_xml_get_widget (priv_data->xml, "entry-rsync-preserve-group");
	priv_data->rsync_preserve_times = glade_xml_get_widget (priv_data->xml, "entry-rsync-preserve-times");

	return priv_data;
}

void
entry_config_dialog_edit_new (void)
{
	DialogPrivateData *priv_data;

	priv_data = load_dialog_from_glade ();
	if (!priv_data)
		return;

	if (gtk_dialog_run (GTK_DIALOG (priv_data->dialog)) == GTK_RESPONSE_OK) {
	}

	gtk_widget_destroy (priv_data->dialog);

	g_free (priv_data);
}
