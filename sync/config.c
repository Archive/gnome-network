/* config.c - Configuration access functions

   Copyright (C) 1998 Tom Tromey
   Copyright (C) 2002 Rodrigo Moya

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#include "sync.h"
#include <gconf/gconf-client.h>

#define KEY_ENTRY_SECTION "/apps/gnome-sync/Entries"
#define KEY_ENTRY_NAME    KEY_ENTRY_SECTION "%s/Name"

static GConfClient *conf_client = NULL;

void
config_init (void)
{
	if (!conf_client)
		conf_client = gconf_client_get_default ();
}

void
config_shutdown (void)
{
	g_object_unref (G_OBJECT (conf_client));
	conf_client = NULL;
}

GtkTreeModel *
config_get_entries_tree_model (void)
{
	GtkListStore *model;

	model = gtk_list_store_new (4, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

	/* FIXME: fill in the model with the entries from the configuration */

	return GTK_TREE_MODEL (model);
}
