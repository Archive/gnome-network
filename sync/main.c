/* main.c - Main program for gnome-sync.

   Copyright (C) 1998 Tom Tromey
   Copyright (C) 2002 Rodrigo Moya

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#include "sync.h"

static void about_callback (GtkWidget *widget, gpointer ignore);
static void sync_callback (GtkWidget *widget, gpointer client_data);
static void quit_callback (GtkWidget *widget, gpointer client_data);
static void new_entry_callback (GtkWidget *widget, gpointer client_data);
static void remove_callback (GtkWidget *widget, gpointer client_data);
static void config_callback (GtkWidget *widget, gpointer client_data);

GnomeApp *main_window = NULL;
GtkWidget *entries_list = NULL;

static GtkWidget *tb_sync_btn;
static GtkWidget *tb_new_btn;
static GtkWidget *tb_edit_btn;
static GtkWidget *tb_delete_btn;

/*
 * This section deals with the menubar and simple helper functions.
 */

/* The File menu.  */
static GnomeUIInfo file_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Synchronize"), NULL, sync_callback, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GTK_STOCK_EXECUTE, 0, 0, NULL },
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(config_callback, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM(quit_callback, NULL),
	GNOMEUIINFO_END
};

/* The Synchronize menu.  */
static GnomeUIInfo sync_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("New entry..."), NULL, new_entry_callback,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GTK_STOCK_NEW, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("Edit..."), NULL, NULL,
	  NULL, NULL, GNOME_APP_PIXMAP_STOCK, GTK_STOCK_PROPERTIES, 0, 0, NULL },
	GNOMEUIINFO_SEPARATOR,
	{ GNOME_APP_UI_ITEM, N_("Remove"), NULL, remove_callback, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GTK_STOCK_DELETE, 0, 0, NULL },
	GNOMEUIINFO_END
};

/* The Help menu.  */
static GnomeUIInfo help_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("About..."), NULL, about_callback, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_ABOUT, 0, 0, NULL },
	GNOMEUIINFO_END
};

/* The menubar.  */
static GnomeUIInfo menubar[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_SUBTREE (N_("Synchronize"), &sync_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};

static GtkWidget *
find_menu_widget (const GnomeUIInfo *uiinfo, const char *label)
{
	int i;

	for (i = 0; uiinfo[i].type != GNOME_APP_UI_ENDOFINFO; ++i) {
		if (uiinfo[i].label && ! strcmp (uiinfo[i].label, label))
			return uiinfo[i].widget;
	}
	return NULL;
}

/* Pop up about box for application.  */
static void
about_callback (GtkWidget *widget, gpointer ignore)
{
	GtkWidget *about;
	const gchar *authors[] = {
		"Tom Tromey <tromey@cygnus.com>",
		"Rodrigo Moya <rodrigo@gnome-db.org>",
		NULL
	};

	about = gnome_about_new (_("Gnome Synchronize"), VERSION,
				 "(C) 1998-2002 the Free Software Foundation",
				 _("The GNOME file synchronization program."),
				 authors, NULL, NULL,
				 NULL);
	gtk_widget_show (about);
}

/* Callback function to quit.  */
static void
quit_callback (GtkWidget *widget, gpointer client_data)
{
	gtk_widget_destroy (main_window);
}

/* This is called to start synchronization.  */
static void
sync_callback (GtkWidget *widget, gpointer client_data)
{
}

/* Let user create a new entry.  */
static void
new_entry_callback (GtkWidget *widget, gpointer client_data)
{
	entry_config_dialog_edit_new ();
}

/* Configure the properties.  */
static void
config_callback (GtkWidget *widget, gpointer client_data)
{
}

/* Remove selected entries.  */
static void
remove_callback (GtkWidget *widget, gpointer client_data)
{
}

/* Create contents that go in main window.  */
static void
setup_contents (GnomeApp *app)
{
	GtkWidget *sw;

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	entries_list = entry_list_new ();
	gtk_container_add (GTK_CONTAINER (sw), entries_list);

	gtk_widget_show_all (sw);
	gnome_app_set_contents (app, sw);
}

/* Add menubar to the application.  */
static void
setup_menu (GnomeApp *app, gpointer client_data)
{
	GtkToolbar *toolbar;
	GtkIconSize size = GTK_ICON_SIZE_LARGE_TOOLBAR;

	/* create menu bar */
	gnome_app_create_menus_with_data (app, menubar, client_data);
	gtk_widget_set_sensitive (find_menu_widget (sync_menu, _("Remove")), FALSE);
	gtk_widget_set_sensitive (find_menu_widget (sync_menu, _("Edit...")), FALSE);

	/* create the toolbar */
	toolbar = gtk_toolbar_new ();

	tb_new_btn = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), _("New"),
					      _("Create new synchronization entry"),
					      NULL, gtk_image_new_from_stock (GTK_STOCK_NEW, size),
					      GTK_SIGNAL_FUNC (new_entry_callback), NULL);
	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
	tb_sync_btn = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), _("Synchronize"),
					       _("Start synchronization for selected entries"),
					       NULL, gtk_image_new_from_stock (GTK_STOCK_EXECUTE, size),
					       GTK_SIGNAL_FUNC (sync_callback), NULL);
	tb_edit_btn = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), _("Edit"),
					       _("Edit selected entry"),
					       NULL, gtk_image_new_from_stock (GTK_STOCK_PROPERTIES, size),
					       NULL, NULL);
	tb_delete_btn = gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), _("Delete"),
						 _("Delete selected entries"),
						 NULL, gtk_image_new_from_stock (GTK_STOCK_DELETE, size),
						 GTK_SIGNAL_FUNC (remove_callback), NULL);

	gtk_widget_set_sensitive (tb_edit_btn, FALSE);
	gtk_widget_set_sensitive (tb_delete_btn, FALSE);

	gnome_app_set_toolbar (app, toolbar);
	gtk_toolbar_set_orientation (GTK_TOOLBAR (toolbar), GTK_ORIENTATION_HORIZONTAL);
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_tooltips (GTK_TOOLBAR (toolbar), TRUE);
}

static void
window_destroyed_cb (GtkWidget *w, gpointer user_data)
{
	gtk_main_quit ();
}



/*
 * Session management.
 */

static int
session_save (GnomeClient *client, int phase, GnomeSaveStyle save_style,
	      int is_shutdown, GnomeInteractStyle interact_style,
	      int is_fast, gpointer client_data)
{
	char *args[1];

	args[0] = (char *) client_data;
	gnome_client_set_restart_command (client, 1, args);

	/* FIXME: complete this.  */

	return TRUE;
}

static void
session_die (GnomeClient *client, gpointer client_data)
{
	gtk_main_quit ();
}

int
main (int argc, char *argv[])
{
	GnomeClient *client;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gnome_program_init ("gnome-sync", VERSION, LIBGNOMEUI_MODULE, argc, argv, NULL);

	/* setup session management */
	client = gnome_master_client ();
	if (client) {
		g_signal_connect (G_OBJECT (client), "save_yourself",
				  G_CALLBACK (session_save), argv[0]);
		g_signal_connect (G_OBJECT (client), "die",
				  G_CALLBACK (session_die), (gpointer) NULL);
	}

	config_init ();

	/* create the main window */
	main_window = gnome_app_new ("gnome-sync", "GNOME Synchronization tool");
	gtk_widget_set_usize (GTK_WIDGET (main_window), 300, 350);
	g_signal_connect (G_OBJECT (main_window), "destroy",
			  G_CALLBACK (window_destroyed_cb), NULL);
	setup_menu (main_window, NULL);
	setup_contents (main_window);
	gtk_widget_show (main_window);

	gtk_main ();

	/* FIXME: make sure we close all ongoing operations */
	config_shutdown ();

	return 0;
}
