/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __constants_h__
#define __constants_h__


#define GNOME_PPP_NAME             "gnome-ppp"
#define GNOME_PPP_TITLE            "Gnome PPP"
#define GNOME_PPP_VERSION          "0.99.1"

/* you can't change author/copyright strings in the
 * source code; but you can change them in the about
 * box of the executeable if you wish (or disable the about box)
 */
#define GNOME_PPP_AUTHOR           "Jay Painter"
#define GNOME_COPYRIGHT            "(C) 1998,1999 Jay Painter"

/* look/feel constants */
#define BUTTON_HEIGHT          30
#define BUTTON_WIDTH           70
#define TABLE_ROW_SPACINGS     10
#define TABLE_COL_SPACINGS      5
#define BOX_SPACING            10
#define CONTAINER_BORDER        5


#endif /* __constants_h__ */
