/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <string.h>
#include <stdio.h>
#include <gnome.h>
#include "account-window.h"
#include "modem-window.h"
#include "global.h"
#include "misc.h"


struct
{
  int speed;
  gchar *text;
} modem_speeds[] = {
  { 115200, "115200" },
  { 57600, "57600" },
  { 38400, "38400" },
  { 19200, "19200" },
  { 9600, "9600" },
  { 2400, "2400" },
  { 0, NULL }
};


struct
{
  gchar *device;
  gchar *text;
} modem_devices[] = {
  { "/dev/modem", "/dev/modem" },
  { "/dev/ttyS0", "/dev/ttyS0 (COM1)" },
  { "/dev/ttyS1", "/dev/ttyS1 (COM2)" },
  { "/dev/ttyS2", "/dev/ttyS2 (COM3)" },
  { "/dev/ttyS3", "/dev/ttyS3 (COM4)" },
  { NULL, NULL }
};


struct
{
  gchar *flow;
  gchar *text;
} modem_flows[] = {
  { "crtscts", "CRTSCTS" },
  { "xonxoff", "XON/XOFF" },
  { "none", "None" },
  { NULL, NULL }
};


struct
{
  gchar *term;
  gchar *text;
} modem_terms[] = {
  { "\r\n", "CR/LF" },
  { "\r", "CR" },
  { "\n", "LF" },
  { NULL, NULL }
};


struct
{
  int vol;
  gchar *text;
} modem_vols[] = {
  {  0, "MUTE" },
  {  1, "LOW" },
  {  2, "HIGH" },
  {  0, NULL }
};


typedef struct _AccountWindow AccountWindow;
struct _AccountWindow
{
  Account   *account;
  gboolean   free_account;

  GtkWidget *property_box;

  /* dial */
  GtkWidget *name;
  GtkWidget *phone_redial_spin_button;
  GtkWidget *phone_dial_timeout_button;
  GtkWidget *phone_clist;
  GtkWidget *phone_entry;

  /* authentication */
  GtkWidget *user;
  GtkWidget *passwd;
  GtkWidget *remotename;

  /* ip */
  GtkWidget *ip;
  GtkWidget *ip_dynamic_button;
  GtkWidget *ip_static_button;
  GtkWidget *mask;
  GtkWidget *mask_dynamic_button;
  GtkWidget *mask_static_button;
  GtkWidget *remote;
  GtkWidget *remote_dynamic_button;
  GtkWidget *remote_static_button;

  /* dns */
  GtkWidget *dns_server_entry;
  GtkWidget *dns_server_clist;
  GtkWidget *dns_search_entry;
  GtkWidget *dns_search_clist;

  /* script */
  GtkWidget *script_type_option_menu;
  GtkWidget *script_entry_label;
  GtkWidget *script_entry;
  GtkWidget *script_clist;

  /* system(ppp) */
  GtkWidget *default_route_button;
  GtkWidget *lock_device_button;
  GtkWidget *mtu_spin_button;
  GtkWidget *mru_spin_button;
  GtkWidget *connect_program_entry;
  GtkWidget *disconnect_program_entry;

  /* modem (general) */
  GtkWidget *connection_speed;
  GtkWidget *modem_device;
  GtkWidget *modem_flow;
  GtkWidget *modem_term;
  GtkObject *modem_volume;
  
};


/* text descriptions of script types */
struct _script_stuff
{
  gchar *option_text;
  gchar *label_text;
} script_type_string[] = {
  { 
    N_("Receive"),
    N_("Enter search string") 
  },
  { 
    N_("Send"),
    N_("Enter transmission string")
  },
  {
    N_("Send User Name"),
    N_("Username taken from Authentication")
  },
  {
    N_("Send User Password"),
    N_("Password taken from Authentication")
  },
  {
    N_("Dialog Box Input"),
    N_("Enter text to be used as a dialog message when prompted for input") 
  },

  { NULL, NULL }
};


/* data refreshing for the account window */
static void refresh_data(AccountWindow *aw);
static void apply_changes_cb(GtkWidget *widget, gint page_number);
static gint delete_account_window_cb(GtkWidget *widget);
static void destroy_account_window_cb(GtkWidget *widget);


/* notebook pages */
GtkWidget *create_dial_page(AccountWindow * aw);
GtkWidget *create_authentication_page(AccountWindow * aw);
GtkWidget *create_ip_page(AccountWindow * aw);
GtkWidget *create_dns_page(AccountWindow * aw);
GtkWidget *create_script_page(AccountWindow * aw);
GtkWidget *create_ppp_page(AccountWindow * aw);
GtkWidget *create_modem_page(AccountWindow * aw, Account *account);

/* dial page callbacks */
static void name_changed_cb(GtkWidget *widget);
static void modem_device_cb(GtkWidget * widget);
static void modem_flow_cb(GtkWidget * widget);
static void modem_term_cb(GtkWidget * widget);
static void connection_speed_cb(GtkWidget * widget);
static void phone_number_select_cb(GtkWidget * widget,  gint row);
static void phone_number_unselect_cb(GtkWidget * widget, gint row);
static void add_phone_number_cb(GtkWidget * widget);
static void remove_phone_number_cb(GtkWidget * widget);


/* authentication callbacks */
static void user_changed_cb(GtkWidget *widget);
static void passwd_changed_cb(GtkWidget *widget);
static void remotename_changed_cb(GtkWidget *widget);


/* ip address callbacks */
static void ip_address_dynamic_cb(GtkWidget * widget);
static void ip_address_static_cb(GtkWidget * widget);
static void netmask_dynamic_cb(GtkWidget * widget);
static void netmask_static_cb(GtkWidget * widget);
static void remote_dynamic_cb(GtkWidget * widget);
static void remote_static_cb(GtkWidget * widget);


/* dns page callbacks */
static void dns_server_select_cb(GtkWidget * widget,  gint row);
static void dns_server_unselect_cb(GtkWidget * widget, gint row);
static void add_dns_server_cb(GtkWidget * widget);
static void remove_dns_server_cb(GtkWidget * widget);
static void dns_search_select_cb(GtkWidget * widget,  gint row);
static void dns_search_unselect_cb(GtkWidget * widget, gint row);
static void add_dns_search_cb(GtkWidget * widget);
static void remove_dns_search_cb(GtkWidget * widget);


/* script page callbacks */
static void script_type_cb(GtkWidget * widget);
static void add_script_cb(GtkWidget * widget);
static void insert_script_cb(GtkWidget * widget); 
static void remove_script_cb(GtkWidget * widget);
static void script_select_cb(GtkWidget * widget,  gint row);
static void script_unselect_cb(GtkWidget * widget, gint row);


/* system page callbacks */
static void default_route_clicked_cb(GtkWidget *widget);
static void lock_device_clicked_cb(GtkWidget *widget);


/* modem page callbacks */
static void edit_modem_commands_cb(GtkWidget *widget, Account *account);


/* macros to set and get the class data structure */
#define ACCOUNT_WINDOW_DATA_STR "account_window_data"
#define get_account_context(object)\
(gtk_object_get_data(GTK_OBJECT((object)), ACCOUNT_WINDOW_DATA_STR))
#define set_account_context(object, aw)\
(gtk_object_set_data(GTK_OBJECT((object)), ACCOUNT_WINDOW_DATA_STR, (gpointer)(aw)))



void 
open_account_window (Account *account, gboolean clone)
{
  gboolean free_account;
  Account *new_account;
  AccountWindow *aw;

  /* this is fucked up */
  if (!account && clone)
    {
      g_error(_("open_account_window(): account=NULL with clone flag"));
    }

  /* clone the account which was passed in */
  else if (account && clone)
    {
      new_account = account_clone(account);
      free_account = TRUE;
    }

  /* bail if the account is already open */
  else if (account && !clone)
    {
      free_account = FALSE;
      new_account = account;
    }

  /* new account */
  else if (!account && !clone)
    {
      new_account = account_new();
      free_account = TRUE;
    }
  
  else
    {
      g_error(_("open_account_window(): calling arguments invalid"));
    }

  account_lock(new_account);

  aw = g_malloc(sizeof(AccountWindow));
  aw->account = new_account;
  aw->free_account = free_account;
  
  /* GNOME property box */
  aw->property_box = gnome_property_box_new();
  set_account_context(aw->property_box, aw);
  gtk_window_set_title(GTK_WINDOW(aw->property_box), "PPP Account");
  
  gtk_signal_connect(
      GTK_OBJECT(aw->property_box),
      "apply",
      (GtkSignalFunc) apply_changes_cb,
      NULL);
  
  gtk_signal_connect(
      GTK_OBJECT(aw->property_box),
      "destroy",
      (GtkSignalFunc) destroy_account_window_cb,
      NULL);
 
  gtk_signal_connect(
      GTK_OBJECT(aw->property_box),
      "delete_event",
      (GtkSignalFunc) delete_account_window_cb,
      NULL);
  
  /* hide unused buttons in the property box */
  gtk_widget_hide(GNOME_PROPERTY_BOX(aw->property_box)->help_button);
  gtk_widget_hide(GNOME_PROPERTY_BOX(aw->property_box)->apply_button);
  
  /* dial notbook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_dial_page(aw),
      gtk_label_new(_("Dial")));
      
  /* authentication notbook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_authentication_page(aw),
      gtk_label_new(_("Authentication")));

  /* ip notbook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_ip_page(aw),
      gtk_label_new(_("IP Address")));

  /* dns notbook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_dns_page(aw),
      gtk_label_new(_("DNS")));

  /* script notbook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_script_page(aw),
      gtk_label_new(_("Script")));

  /* system notebook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_ppp_page(aw),
      gtk_label_new(_("PPP")));
   
  /* system modem page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(aw->property_box),
      create_modem_page(aw, account),
      gtk_label_new(_("Modem")));    

  /* set the account window's data */
  refresh_data(aw);
  gtk_widget_show(aw->property_box);
  
}


static void
refresh_data(AccountWindow *aw)
{
  int i, row;
  gchar *text[2];
  GList *list;
  GString *gstr;
  Account *account = aw->account;
  ScriptEntry *script_entry;

  g_assert(account != NULL);

  /* set information on dial window */
  gtk_entry_set_text(GTK_ENTRY(aw->name), account->name->str);

  /* redial spinner */
  gtk_spin_button_set_value(
      GTK_SPIN_BUTTON(aw->phone_redial_spin_button),
      account->redial_attempts);
      
  /* dial timeout spinner */
  gtk_spin_button_set_value(
      GTK_SPIN_BUTTON(aw->phone_dial_timeout_button),
      account->dial_timeout);

  /* popluate the phone list */
  gtk_clist_clear(GTK_CLIST(aw->phone_clist));
  list = account->phone_list;
  while (list)
    {
      gstr = (GString *) list->data;
      list = list->next;
      gtk_clist_append(GTK_CLIST(aw->phone_clist), &gstr->str);
    }

  /* set information on authentication window */
  gtk_entry_set_text(GTK_ENTRY(aw->user), account->user->str);
  gtk_entry_set_text(GTK_ENTRY(aw->passwd), account->passwd->str);
  gtk_entry_set_text(GTK_ENTRY(aw->remotename), account->remotename->str);

  /* IP, Remote, Netmask */
  if (account->ip->len > 0)
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->ip_static_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->ip), TRUE);
      gtk_entry_set_text(GTK_ENTRY(aw->ip), account->ip->str);
    }
  else
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->ip_dynamic_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->ip), FALSE);
      gtk_entry_set_text(GTK_ENTRY(aw->ip), "");
    }

  if (account->mask->len > 0)
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->mask_static_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->mask), TRUE);
      gtk_entry_set_text(GTK_ENTRY(aw->mask), account->mask->str);
    }
  else
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->mask_dynamic_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->mask), FALSE);
      gtk_entry_set_text(GTK_ENTRY(aw->mask), "");
    }

  if (account->remote->len > 0)
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->remote_static_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->remote), TRUE);
      gtk_entry_set_text(GTK_ENTRY(aw->remote), account->remote->str);
    }
  else
    {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(aw->remote_dynamic_button), TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(aw->remote), FALSE);
      gtk_entry_set_text(GTK_ENTRY(aw->remote), "");
    }

  /* dns/search domains */
  gtk_clist_clear(GTK_CLIST(aw->dns_server_clist));
  list = account->dns_list;
  while (list)
    {
      gstr = (GString *) list->data;
      list = list->next;
      gtk_clist_append(GTK_CLIST(aw->dns_server_clist), &gstr->str);
    }

  gtk_clist_clear(GTK_CLIST(aw->dns_search_clist));
  list = account->domain_list;
  while (list)
    {
      gstr = (GString *) list->data;
      list = list->next;
      gtk_clist_append(GTK_CLIST(aw->dns_search_clist), &gstr->str);
    }

  /* script */
  gtk_clist_clear(GTK_CLIST(aw->script_clist));
  list = account->script_list;
  while (list)
    {
      script_entry = (ScriptEntry *) list->data;
      list = list->next;

      text[0] = _(script_type_string[script_entry->type].option_text);
      text[1] = script_entry->text->str;

      row = gtk_clist_append(GTK_CLIST(aw->script_clist), text);
      gtk_clist_set_row_data(
          GTK_CLIST(aw->script_clist),
	  row, 
	  GINT_TO_POINTER(script_entry->type));
    }

  /* system information */
  gtk_toggle_button_set_active(
      GTK_TOGGLE_BUTTON(aw->default_route_button),
      account->default_route);

  gtk_toggle_button_set_active(
      GTK_TOGGLE_BUTTON(aw->lock_device_button),
      account->lock_device);

  /* mtu, mru */
  gtk_spin_button_set_value(
      GTK_SPIN_BUTTON(aw->mtu_spin_button),
      account->mtu);

  gtk_spin_button_set_value(
      GTK_SPIN_BUTTON(aw->mru_spin_button),
      account->mru);

  gtk_entry_set_text(
      GTK_ENTRY(aw->connect_program_entry), 
      account->connect_program->str);

  gtk_entry_set_text(
      GTK_ENTRY(aw->disconnect_program_entry), 
      account->disconnect_program->str);

  /* modem device -- set default then override it */
  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_device), 0);
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_device), (gpointer) modem_devices[0].text);
  for (i = 0; modem_devices[i].text != NULL; i++)
    {
      if (strcmp(modem_devices[i].device, account->modem_device->str) == 0)
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_device), i);
	  gtk_object_set_user_data(
              GTK_OBJECT(aw->modem_device), 
	      (gpointer) modem_devices[i].text);
	  break;
	}
    }

  /* modem speed -- set default then override it */
  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->connection_speed), 0);
  gtk_object_set_user_data(GTK_OBJECT(aw->connection_speed), (gpointer) modem_speeds[0].text);
  for (i = 0; modem_speeds[i].text != NULL; i++)
    {
      if (modem_speeds[i].speed == account->speed)
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->connection_speed), i);
	  gtk_object_set_user_data(
              GTK_OBJECT(aw->connection_speed), 
	      (gpointer) modem_speeds[i].text);
	  break;
	}
    }

  /* modem flow control -- set default then override it */
  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_flow), 0);
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_flow), (gpointer) modem_flows[0].text);
  for (i = 0; modem_flows[i].text != NULL; i++)
    {
      if (strcmp(modem_flows[i].flow, account->modem_flow->str) == 0)
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_flow), i);
	  gtk_object_set_user_data(
              GTK_OBJECT(aw->modem_flow), 
	      (gpointer) modem_flows[i].text);
	  break;
	}
    }

  /* modem line termination -- set default then override it */
  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_term), 0);
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_term), (gpointer) modem_terms[0].text);
  for (i = 0; modem_terms[i].text != NULL; i++)
    {
      if (strcmp(modem_terms[i].term, account->modem_term->str) == 0)
	{
	  gtk_option_menu_set_history(GTK_OPTION_MENU(aw->modem_term), i);
	  gtk_object_set_user_data(
              GTK_OBJECT(aw->modem_term), 
	      (gpointer) modem_terms[i].text);
	  break;
	}
    }
  
  /* modem speaker volume */
  GTK_ADJUSTMENT(aw->modem_volume)->value = account->modem_volume;    
  
}


/*
 * apply, ok, cancel
 */
static void
apply_changes_cb(GtkWidget *widget, gint page_number)
{
  gint i;
  GList *list;
  gchar *text;
  AccountWindow *aw;
  Account *account;

  aw = get_account_context(widget);
  account = aw->account;

  /* save information from dial window */
  g_string_assign(account->name, gtk_entry_get_text(GTK_ENTRY(aw->name)));

  /* redial attempts */
  account->redial_attempts = 
    gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(aw->phone_redial_spin_button));

  /* dial timeout */
  account->dial_timeout = 
    gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(aw->phone_dial_timeout_button));

  /* clear and re-poplulate the phone number list */
  account_phone_list_clear(account);
  for (i = 0; i < GTK_CLIST(aw->phone_clist)->rows; i++)
    {
      gtk_clist_get_text(GTK_CLIST(aw->phone_clist), i, 0, &text);
      account_phone_list_append(account, text);
    }

  /* save information from authentication window */
  g_string_assign(account->user, gtk_entry_get_text(GTK_ENTRY(aw->user)));
  g_string_assign(account->passwd, gtk_entry_get_text(GTK_ENTRY(aw->passwd)));
  g_string_assign(account->remotename, gtk_entry_get_text(GTK_ENTRY(aw->remotename)));

  /* ip, mask, remote */
  g_string_assign(account->ip, gtk_entry_get_text(GTK_ENTRY(aw->ip)));
  g_string_assign(account->mask, gtk_entry_get_text(GTK_ENTRY(aw->mask)));
  g_string_assign(account->remote, gtk_entry_get_text(GTK_ENTRY(aw->remote)));

  /* dns/search domains */
  account_dns_list_clear(account);
  for (i = 0; i < GTK_CLIST(aw->dns_server_clist)->rows; i++)
    {
      gtk_clist_get_text(GTK_CLIST(aw->dns_server_clist), i, 0, &text);
      account_dns_list_append(account, text);
    }

  account_domain_list_clear(account);
  for (i = 0; i < GTK_CLIST(aw->dns_search_clist)->rows; i++)
    {
      gtk_clist_get_text(GTK_CLIST(aw->dns_search_clist), i, 0, &text);
      account_domain_list_append(account, text);
    }

  /* save the script list */
  account_script_list_clear(account);
  for (i = 0; i < GTK_CLIST(aw->script_clist)->rows; i++)
    {
      ScriptEntryType script_type;
      gchar *text_string;

      script_type = 
	GPOINTER_TO_INT(gtk_clist_get_row_data(GTK_CLIST(aw->script_clist), i));

      gtk_clist_get_text(GTK_CLIST(aw->script_clist), i, 1, &text_string);

      account_script_list_append(account, script_type, text_string);
    }

  /* system */
  account->default_route = 
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(aw->default_route_button));

  account->lock_device =
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(aw->lock_device_button));

  /* mtu, mru */
  account->mtu = 
    gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(aw->mtu_spin_button));

  account->mru = 
    gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(aw->mru_spin_button));

  g_string_assign(
      account->connect_program, 
      gtk_entry_get_text(GTK_ENTRY(aw->connect_program_entry)));

  g_string_assign(
      account->disconnect_program, 
      gtk_entry_get_text(GTK_ENTRY(aw->disconnect_program_entry)));

  /* modem options */
  for (i = 0; modem_devices[i].text != NULL; i++)
    {
      gpointer data;

      data = gtk_object_get_user_data(GTK_OBJECT(aw->modem_device));
      if (modem_devices[i].text == (gchar *) data)
	{
	  g_string_assign(account->modem_device, modem_devices[i].device);
	  break;
	}
    }

  for (i = 0; modem_speeds[i].text != NULL; i++)
    {
      gpointer data;

      data = gtk_object_get_user_data(GTK_OBJECT(aw->connection_speed));
      if (modem_speeds[i].text == (gchar *) data)
	{
	  account->speed = modem_speeds[i].speed;
	  break;
	}
    }

  for (i = 0; modem_flows[i].text != NULL; i++)
    {
      gpointer data;

      data = gtk_object_get_user_data(GTK_OBJECT(aw->modem_flow));
      if (modem_flows[i].text == (gchar *) data)
	{
	  g_string_assign(account->modem_flow, modem_flows[i].flow);
	  break;
	}
    }
    
  for (i = 0; modem_terms[i].text != NULL; i++)
    {
      gpointer data;

      data = gtk_object_get_user_data(GTK_OBJECT(aw->modem_term));
      if (modem_terms[i].text == (gchar *) data)
	{
	  g_string_assign(account->modem_term, modem_terms[i].term);
	  break;
	}
    }
    
  /* modem speaker volume */
  account->modem_volume = GTK_ADJUSTMENT(aw->modem_volume)->value;
      
      
  /* cleanup */
  if (aw->free_account)
    {
      aw->free_account = FALSE;
      account_add(aw->account);
    }

  account_save();
  account_broadcast_watchers();
}


static gint
delete_account_window_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  return FALSE;
}


static void
destroy_account_window_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);

  account_unlock(aw->account);

  /* to free or not to free this account */
  if (aw->free_account)
    {
      account_free(aw->account);
      aw->account = NULL;
    }

  g_free(aw);
}


/*
 * notebook pages
 */
GtkWidget *
create_dial_page(AccountWindow *aw)
{
  gint i;
  GtkWidget *vbox, *vbox2, *table, *table2, *label, *frame, 
    *button, *alignment, *menu, *menu_item;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  table = gtk_table_new(3, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  label = gtk_label_new(_("Account Name"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);

  aw->name = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), aw->name, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(aw->name);

  /* redial spin button */
  label = gtk_label_new(_("Redial Maximum"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  alignment = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(table), alignment, 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(alignment);

  aw->phone_redial_spin_button = 
    gtk_spin_button_new(
        (GtkAdjustment *) gtk_adjustment_new(1.0, 1.0, 100.0, 1.0, 10.0, 0.0),
	0, 0);
  set_account_context(aw->phone_redial_spin_button, aw);
  gtk_container_add(GTK_CONTAINER(alignment), aw->phone_redial_spin_button);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(aw->phone_redial_spin_button), TRUE);
  gtk_widget_show(aw->phone_redial_spin_button);
  
  /* dial timeout spin button */
  label = gtk_label_new(_("Dial Timeout (sec)"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  alignment = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(table), alignment, 1, 2, 2, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(alignment);

  aw->phone_dial_timeout_button = 
    gtk_spin_button_new(
        (GtkAdjustment *) gtk_adjustment_new(60.0, 5.0, 500.0, 1.0, 10.0, 0.0),
	0, 0);
  set_account_context(aw->phone_dial_timeout_button, aw);
  gtk_container_add(GTK_CONTAINER(alignment), aw->phone_dial_timeout_button);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(aw->phone_dial_timeout_button), TRUE);
  gtk_widget_show(aw->phone_dial_timeout_button);
  
  /* phone number frame */
  frame = gtk_frame_new(_("Phone Numbers"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table2 = gtk_table_new(3, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table2);
  gtk_container_border_width(GTK_CONTAINER(table2), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table2), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table2), TABLE_COL_SPACINGS);
  gtk_widget_show(table2);

  /* Phone number */
  label = gtk_label_new(_("#"));
  gtk_table_attach(GTK_TABLE(table2), label, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  /* phone number entry */
  aw->phone_entry = gtk_entry_new(); 
  set_account_context(aw->phone_entry, aw);
  gtk_table_attach(GTK_TABLE(table2), aw->phone_entry, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->phone_entry),
      "activate",
      (GtkSignalFunc) add_phone_number_cb,
      aw);

  gtk_widget_show(aw->phone_entry);

  /* add/remove buttons */
  button = gtk_button_new_with_label(_("Add")); 
  set_account_context(button, aw);
  gtk_table_attach(GTK_TABLE(table2), button, 0, 1, 1, 2, 
		   0, 0, 0, 0);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) add_phone_number_cb,
      NULL);

  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Remove")); 
  set_account_context(button, aw);
  gtk_table_attach(GTK_TABLE(table2), button, 0, 1, 2, 3, 
		   0, 0, 0, 0);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);

  gtk_signal_connect(
      GTK_OBJECT (button),
      "clicked",
      (GtkSignalFunc) remove_phone_number_cb,
      NULL);

  gtk_widget_show(button);

  /* phone number list */
  aw->phone_clist = gtk_clist_new(1); 
  set_account_context(aw->phone_clist, aw);
  gtk_table_attach(GTK_TABLE(table2), aw->phone_clist, 1, 2, 1, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_clist_set_selection_mode(GTK_CLIST(aw->phone_clist), 
			       GTK_SELECTION_SINGLE);
  gtk_clist_set_column_width(GTK_CLIST(aw->phone_clist), 0, 150);

  gtk_signal_connect(
      GTK_OBJECT(aw->phone_clist),
      "select_row",
      (GtkSignalFunc) phone_number_select_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(aw->phone_clist),
      "unselect_row",
      (GtkSignalFunc) phone_number_unselect_cb,
      NULL);

  gtk_widget_show(aw->phone_clist);

  return vbox;
}


GtkWidget *
create_authentication_page(AccountWindow *aw)
{
  GtkWidget *vbox, *table, *label, *frame;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  frame = gtk_frame_new(_("PAP/CHAP"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(2, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);


  label = gtk_label_new(_("User Name"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->user = gtk_entry_new();
  set_account_context(aw->user, aw);
  gtk_table_attach(GTK_TABLE(table), aw->user, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->user),
      "changed",
      GTK_SIGNAL_FUNC(user_changed_cb),
      NULL);

  gtk_widget_show(aw->user);


  label = gtk_label_new(_("Remote Name"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->remotename = gtk_entry_new();
  set_account_context(aw->remotename, aw);
  gtk_table_attach(GTK_TABLE(table), aw->remotename, 1, 2, 1, 2,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->remotename),
      "changed",
      GTK_SIGNAL_FUNC(remotename_changed_cb),
      NULL);

  gtk_widget_show(aw->remotename);


  /* password */
  frame = gtk_frame_new(_("Hidden Password"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(1, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);


  label = gtk_label_new(_("Password"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->passwd = gtk_entry_new();
  set_account_context(aw->passwd, aw);
  gtk_table_attach(GTK_TABLE(table), aw->passwd, 1, 2, 0, 1,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_entry_set_visibility(GTK_ENTRY(aw->passwd), FALSE);

  gtk_signal_connect(
      GTK_OBJECT(aw->passwd),
      "changed",
      GTK_SIGNAL_FUNC(passwd_changed_cb),
      NULL);

  gtk_widget_show(aw->passwd);

  return vbox;
}


GtkWidget *
create_ip_page(AccountWindow * aw)
{
  gint i;
  GtkWidget *vbox, *frame, *table, *label;
  GSList *radio_group;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  /* local ip address */
  frame = gtk_frame_new(_("Local IP Address"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(2, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  aw->ip_dynamic_button = gtk_radio_button_new_with_label(NULL, _("Dynamic"));
  set_account_context(aw->ip_dynamic_button, aw);
  radio_group = gtk_radio_button_group(GTK_RADIO_BUTTON(aw->ip_dynamic_button));
  gtk_table_attach(GTK_TABLE(table), aw->ip_dynamic_button, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->ip_dynamic_button),
      "clicked",
      (GtkSignalFunc) ip_address_dynamic_cb,
      NULL);

  gtk_widget_show(aw->ip_dynamic_button);

  aw->ip_static_button = gtk_radio_button_new_with_label(radio_group, _("Static")); 
  set_account_context(aw->ip_static_button, aw);
  gtk_table_attach(GTK_TABLE(table), aw->ip_static_button, 0, 1, 1, 2, 
		   GTK_FILL, GTK_FILL|GTK_EXPAND,  0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->ip_static_button),
      "clicked",
      (GtkSignalFunc) ip_address_static_cb,
      NULL);

  gtk_widget_show(aw->ip_static_button);

  aw->ip = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), aw->ip , 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(aw->ip);


  /* netmask */
  frame = gtk_frame_new(_("Netmask"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(2, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  aw->mask_dynamic_button = gtk_radio_button_new_with_label(NULL, _("Dynamic")); 
  set_account_context(aw->mask_dynamic_button, aw);
  radio_group = gtk_radio_button_group(GTK_RADIO_BUTTON(aw->mask_dynamic_button));
  gtk_table_attach(GTK_TABLE(table), aw->mask_dynamic_button, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->mask_dynamic_button),
      "clicked",
      (GtkSignalFunc) netmask_dynamic_cb,
      NULL);

  gtk_widget_show(aw->mask_dynamic_button);

  aw->mask_static_button = gtk_radio_button_new_with_label(radio_group, _("Static"));
  set_account_context(aw->mask_static_button, aw);
  gtk_table_attach(GTK_TABLE(table), aw->mask_static_button, 0, 1, 1, 2,
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->mask_static_button),
      "clicked",
      (GtkSignalFunc) netmask_static_cb,
      NULL);

  gtk_widget_show(aw->mask_static_button);

  aw->mask = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), aw->mask, 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(aw->mask);


  /* remote ip address */
  frame = gtk_frame_new(_("Remote IP Address"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(2, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  aw->remote_dynamic_button = gtk_radio_button_new_with_label(NULL, _("Dynamic"));
  set_account_context(aw->remote_dynamic_button, aw);
  radio_group = gtk_radio_button_group(GTK_RADIO_BUTTON(aw->remote_dynamic_button));
  gtk_table_attach(GTK_TABLE(table), aw->remote_dynamic_button, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->remote_dynamic_button),
      "clicked",
      (GtkSignalFunc) remote_dynamic_cb,
      NULL);

  gtk_widget_show(aw->remote_dynamic_button);
  
  aw->remote_static_button = gtk_radio_button_new_with_label(radio_group, _("Static"));
  set_account_context(aw->remote_static_button, aw);
  gtk_table_attach(GTK_TABLE(table), aw->remote_static_button, 0, 1, 1, 2,
		   GTK_FILL, GTK_FILL | GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->remote_static_button),
      "clicked",
      (GtkSignalFunc) remote_static_cb,
      NULL);

  gtk_widget_show(aw->remote_static_button);

  aw->remote = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), aw->remote, 1, 2, 1, 2,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
  gtk_widget_show(aw->remote);

  return vbox;
}


GtkWidget *
create_dns_page(AccountWindow *aw)
{
  GtkWidget *vbox, *table, *label, *frame, *button;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  /* dns server list */
  frame = gtk_frame_new(_("DNS Servers"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(3, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);


  /* dns server entry */
  label = gtk_label_new(_("IP Address"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);

  aw->dns_server_entry = gtk_entry_new();
  set_account_context(aw->dns_server_entry, aw);
  gtk_table_attach(GTK_TABLE(table), aw->dns_server_entry, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_server_entry),
      "activate",
      (GtkSignalFunc) add_dns_server_cb,
      aw);

  gtk_widget_show(aw->dns_server_entry);

  button = gtk_button_new_with_label(_("Add"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_table_attach(GTK_TABLE(table), button, 0, 1, 1, 2, 
		   0, 0, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) add_dns_server_cb,
      NULL);

  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Remove"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3,
		   0, 0, 0, 0);  

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) remove_dns_server_cb,
      NULL);

  gtk_widget_show(button);

  /* dns server list */
  aw->dns_server_clist = gtk_clist_new(1);
  set_account_context(aw->dns_server_clist, aw);
  gtk_clist_set_selection_mode(GTK_CLIST(aw->dns_server_clist),
			       GTK_SELECTION_SINGLE);
  gtk_clist_set_column_width(GTK_CLIST(aw->dns_server_clist), 0, 150);
  gtk_table_attach(GTK_TABLE(table), aw->dns_server_clist, 1, 2, 1, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_server_clist),
      "select_row",
      (GtkSignalFunc) dns_server_select_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_server_clist),
      "unselect_row",
      (GtkSignalFunc) dns_server_unselect_cb,
      NULL);

  gtk_widget_show(aw->dns_server_clist);


  /* search domains list */
  frame = gtk_frame_new(_("Search Domains"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  table = gtk_table_new(3, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), CONTAINER_BORDER);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  label = gtk_label_new(_("Domain Name"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);

  aw->dns_search_entry = gtk_entry_new();
  set_account_context(aw->dns_search_entry, aw);
  gtk_table_attach(GTK_TABLE(table), aw->dns_search_entry, 1, 2, 0, 1,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_search_entry),
      "activate",
      (GtkSignalFunc) add_dns_search_cb,
      aw);

  gtk_widget_show(aw->dns_search_entry);

  /* add/remove buttons */
  button = gtk_button_new_with_label(_("Add"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_table_attach(GTK_TABLE(table), button, 0, 1, 1, 2,
		   0, 0, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) add_dns_search_cb,
      NULL);

  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Remove"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3,
		   0, 0, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) remove_dns_search_cb,
      NULL);

  gtk_widget_show(button);

  /* dns search list */
  aw->dns_search_clist = gtk_clist_new(1);
  set_account_context(aw->dns_search_clist, aw);
  gtk_clist_set_selection_mode(GTK_CLIST(aw->dns_search_clist),
			       GTK_SELECTION_SINGLE);
  gtk_clist_set_column_width(GTK_CLIST(aw->dns_search_clist), 0, 150);
  gtk_table_attach(GTK_TABLE(table), aw->dns_search_clist, 1, 2, 1, 3,
		   GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_search_clist),
      "select_row",
      (GtkSignalFunc) dns_search_select_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(aw->dns_search_clist),
      "unselect_row",
      (GtkSignalFunc) dns_search_unselect_cb,
      NULL);

  gtk_widget_show(aw->dns_search_clist);

  return vbox;
}


GtkWidget *
create_script_page(AccountWindow * aw)
{
  gint i;
  GtkWidget *vbox, *vbox2, *hbox, *table, *label, *frame, *button, *menu;
  static char *titles[2];

  titles[0] = _("Action");
  titles[1] = _("Text");

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_widget_show(vbox);

  /* script list */
  frame = gtk_frame_new(_("Login Script"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
  gtk_widget_show(frame);

  vbox2 = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox2), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox2), BOX_SPACING);
  gtk_container_add(GTK_CONTAINER(frame), vbox2);
  gtk_widget_show(vbox2);

  /* script type option menu */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, TRUE, 0);
  gtk_widget_show(hbox);

  aw->script_type_option_menu = gtk_option_menu_new();
  set_account_context(aw->script_type_option_menu, aw);
  gtk_box_pack_start(GTK_BOX(hbox), aw->script_type_option_menu, FALSE, TRUE, 0);
  gtk_widget_set_usize(aw->script_type_option_menu, 0, BUTTON_HEIGHT);

  menu = gtk_menu_new();
  for (i = 0; script_type_string[i].option_text; i++)
    {
      GtkWidget *menu_item;

      menu_item = append_menuitem_connect(
          GTK_MENU(menu),
	  _(script_type_string[i].option_text),
	  (GtkSignalFunc) script_type_cb,
	  NULL,
	  GINT_TO_POINTER(i));

      set_account_context(menu_item, aw);
    }

  /* we've got to set the inital user_data for the option menu */
  gtk_object_set_user_data(
      GTK_OBJECT(aw->script_type_option_menu),
      GINT_TO_POINTER(0));
  gtk_option_menu_set_menu(GTK_OPTION_MENU(aw->script_type_option_menu), menu);
  gtk_widget_show(aw->script_type_option_menu);

  /* script entry label */
  aw->script_entry_label = gtk_label_new(_(script_type_string[0].label_text));
  gtk_box_pack_start(GTK_BOX(vbox2), aw->script_entry_label, FALSE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(aw->script_entry_label), 0.0, 0.5);
  gtk_widget_show(aw->script_entry_label);

  /* script text entry */
  aw->script_entry = gtk_entry_new();
  set_account_context(aw->script_entry, aw);
  gtk_box_pack_start(GTK_BOX(vbox2), aw->script_entry, FALSE, TRUE, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->script_entry),
      "activate",
      (GtkSignalFunc) add_script_cb,
      aw);

  gtk_widget_show(aw->script_entry);

  /* add/insert/remove buttons */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, TRUE, 0);
  gtk_box_set_spacing(GTK_BOX(hbox), BOX_SPACING);
  gtk_widget_show(hbox);

  button = gtk_button_new_with_label(_("Add"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) add_script_cb,
      NULL);

  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Insert"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) insert_script_cb,
      NULL);

  gtk_widget_show(button);

  button = gtk_button_new_with_label(_("Remove"));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, BUTTON_WIDTH, BUTTON_HEIGHT);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

  gtk_signal_connect(
      GTK_OBJECT(button),
      "clicked",
      (GtkSignalFunc) remove_script_cb,
      NULL);

  gtk_widget_show(button);

  /* script list */
  aw->script_clist = gtk_clist_new_with_titles(2, titles);
  set_account_context(aw->script_clist, aw);
  gtk_clist_set_selection_mode(GTK_CLIST(aw->script_clist), GTK_SELECTION_SINGLE);
  gtk_clist_set_column_width(GTK_CLIST(aw->script_clist), 0, 150);
  gtk_box_pack_start(GTK_BOX(vbox2), aw->script_clist, TRUE, TRUE, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->script_clist),
      "select_row",
      (GtkSignalFunc) script_select_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(aw->script_clist),
      "unselect_row",
      (GtkSignalFunc) script_unselect_cb,
      NULL);

  gtk_widget_show(aw->script_clist);

  return vbox;
}



GtkWidget *
create_ppp_page(AccountWindow *aw)
{
  gint i;
  GtkWidget *vbox, *label, *table, *alignment;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  /* pppd options */
  table = gtk_table_new(6, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(vbox), table);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);


  /* default route button */
  aw->default_route_button = 
    gtk_check_button_new_with_label(_("Add Default Route"));
  set_account_context(aw->default_route_button, aw);
  gtk_table_attach(GTK_TABLE(table), aw->default_route_button, 0, 2, 0, 1, 
		   GTK_FILL, GTK_FILL, 0, 0);
  
  gtk_signal_connect(
      GTK_OBJECT(aw->default_route_button),
      "clicked",
      GTK_SIGNAL_FUNC(default_route_clicked_cb),
      NULL);

  gtk_widget_show(aw->default_route_button);


  /* lock device button */
  aw->lock_device_button = 
    gtk_check_button_new_with_label(_("Lock Dialout Device"));
  set_account_context(aw->lock_device_button, aw);
  gtk_table_attach(GTK_TABLE(table), aw->lock_device_button, 0, 2, 1, 2, 
		   GTK_FILL, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(aw->lock_device_button),
      "clicked",
      GTK_SIGNAL_FUNC(lock_device_clicked_cb),
      NULL);

  gtk_widget_show(aw->lock_device_button);


  /* mtu, mru spin buttons */
  label = gtk_label_new(_("Maximum Transmission Unit"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  alignment = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(table), alignment, 1, 2, 2, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(alignment);

  aw->mtu_spin_button = 
    gtk_spin_button_new(
        (GtkAdjustment *) gtk_adjustment_new(296.0, 128.0, 1500.0, 1.0, 10.0, 0.0),
	0, 0);
  set_account_context(aw->mtu_spin_button, aw);
  gtk_widget_set_usize(aw->mtu_spin_button, 50, 0);
  gtk_container_add(GTK_CONTAINER(alignment), aw->mtu_spin_button);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(aw->mtu_spin_button), TRUE);
  gtk_widget_show(aw->mtu_spin_button);


  label = gtk_label_new(_("Maximum Receive Unit"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  alignment = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(table), alignment, 1, 2, 3, 4, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(alignment);

  aw->mru_spin_button = 
    gtk_spin_button_new(
        (GtkAdjustment *) gtk_adjustment_new(296.0, 128.0, 1500.0, 1.0, 10.0, 0.0),
	0, 0);
  set_account_context(aw->mru_spin_button, aw);
  gtk_widget_set_usize(aw->mru_spin_button, 50, 0);
  gtk_container_add(GTK_CONTAINER(alignment), aw->mru_spin_button);
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(aw->mru_spin_button), TRUE);
  gtk_widget_show(aw->mru_spin_button);

  label = gtk_label_new(_("Run Program After Connect"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->connect_program_entry = gtk_entry_new();
  set_account_context(aw->connect_program_entry, aw);
  gtk_table_attach(GTK_TABLE(table), aw->connect_program_entry, 1, 2, 4, 5,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(aw->connect_program_entry);

  label = gtk_label_new(_("Run Program After Disconnect"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 5, 6,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->disconnect_program_entry = gtk_entry_new();
  set_account_context(aw->disconnect_program_entry, aw);
  gtk_table_attach(GTK_TABLE(table), aw->disconnect_program_entry, 1, 2, 5, 6,
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show(aw->disconnect_program_entry); 

  return vbox;
}


GtkWidget *
create_modem_page(AccountWindow * aw, Account *account)
{
  gint i;
  GtkWidget *vbox, *hbox, *table, *table2, *label, *frame, *menu, *menu_item, *scale, *button;
  
  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);
  
  table = gtk_table_new(8, 2, FALSE);
  gtk_container_add(GTK_CONTAINER(vbox), table);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table); 
  
  /* modem device */
  label = gtk_label_new(_("Modem Device"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->modem_device = gtk_option_menu_new();
  menu = gtk_menu_new();
  for (i = 0; modem_devices[i].text != NULL; i++)
    {
      menu_item = append_menuitem_connect(
          GTK_MENU(menu), 
	  modem_devices[i].text, 
	  (GtkSignalFunc) modem_device_cb, 
	  NULL,
	  (gpointer) modem_devices[i].text);
      set_account_context(menu_item, aw);
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(aw->modem_device), menu);
  gtk_table_attach(GTK_TABLE(table), aw->modem_device, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_set_usize(aw->modem_device, 0, BUTTON_HEIGHT);
  gtk_widget_show(aw->modem_device);

  /* modem speed */ 
  label = gtk_label_new(_("Connection Speed"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show (label);

  aw->connection_speed = gtk_option_menu_new ();
  menu = gtk_menu_new();
  for (i = 0; modem_speeds[i].text != NULL; i++)
    {
      menu_item = append_menuitem_connect(
          GTK_MENU(menu), 
	  modem_speeds[i].text, 
	  (GtkSignalFunc) connection_speed_cb, 
	  NULL,
	  (gpointer) modem_speeds[i].text);
      set_account_context(menu_item, aw);
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(aw->connection_speed), menu);
  gtk_table_attach(GTK_TABLE(table), aw->connection_speed, 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_set_usize(aw->connection_speed, 0, BUTTON_HEIGHT);
  gtk_widget_show(aw->connection_speed); 

  /* modem flow control */
  label = gtk_label_new(_("Flow Control"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->modem_flow = gtk_option_menu_new();
  menu = gtk_menu_new();
  for (i = 0; modem_flows[i].text != NULL; i++)
    {
      menu_item = append_menuitem_connect(
          GTK_MENU(menu), 
	  modem_flows[i].text, 
	  (GtkSignalFunc) modem_flow_cb, 
	  NULL,
	  (gpointer) modem_flows[i].text);
      set_account_context(menu_item, aw);
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(aw->modem_flow), menu);
  gtk_table_attach(GTK_TABLE(table), aw->modem_flow, 1, 2, 2, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_set_usize(aw->modem_flow, 0, BUTTON_HEIGHT);
  gtk_widget_show(aw->modem_flow);
  
  /* modem line termination */
  label = gtk_label_new(_("Line Termination"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_widget_show(label);

  aw->modem_term = gtk_option_menu_new();
  menu = gtk_menu_new();
  for (i = 0; modem_terms[i].text != NULL; i++)
    {
      menu_item = append_menuitem_connect(
          GTK_MENU(menu), 
	  modem_terms[i].text, 
	  (GtkSignalFunc) modem_term_cb, 
	  NULL,
	  (gpointer) modem_terms[i].text);
      set_account_context(menu_item, aw);
    }

  gtk_option_menu_set_menu(GTK_OPTION_MENU(aw->modem_term), menu);
  gtk_table_attach(GTK_TABLE(table), aw->modem_term, 1, 2, 3, 4, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_set_usize(aw->modem_term, 0, BUTTON_HEIGHT);
  gtk_widget_show(aw->modem_term);
 
  /* modem speaker volume */
  label = gtk_label_new(_("Modem Speaker Volume"));
  gtk_table_attach(GTK_TABLE(table), label, 1, 2, 4, 5, 
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0.5, 0.5);
  gtk_widget_show(label);
  
  aw->modem_volume = gtk_adjustment_new(0, 0, 2, 1, 1, 0);
  set_account_context(aw->modem_volume, aw); 
 
  scale = gtk_hscale_new((GtkAdjustment *) aw->modem_volume );  
  gtk_table_attach(GTK_TABLE(table), scale, 1, 2, 5, 6, 
		   GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_scale_set_digits( GTK_SCALE(scale), 0 );
  gtk_scale_set_value_pos( GTK_SCALE(scale), GTK_POS_RIGHT );
  gtk_widget_show(scale);
  
  /* modem edit commands */
  button = gtk_button_new_with_label(_(" Edit Modem Commands "));
  set_account_context(button, aw);
  gtk_widget_set_usize(button, 0, BUTTON_HEIGHT);
  gtk_table_attach(GTK_TABLE(table), button, 1, 2, 7, 8, 
		   GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
       GTK_OBJECT(button),
       "clicked",
       (GtkSignalFunc) edit_modem_commands_cb,
       aw->account);
  gtk_widget_show(button);
 
  return vbox;
}


/*
 * dial page callbacks
 */
static void
name_changed_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
phone_number_select_cb(GtkWidget * widget, gint row)
{
  gchar *text;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_clist_get_text(GTK_CLIST(aw->phone_clist), row, 0, &text);
  gtk_entry_set_text(GTK_ENTRY(aw->phone_entry), text);
}


static void
phone_number_unselect_cb(GtkWidget * widget, gint row)
{
  gchar *text[1];
  AccountWindow *aw = get_account_context(widget);
  
  g_assert(aw != NULL);

  gtk_entry_set_text(GTK_ENTRY(aw->phone_entry), "");
}


static void 
add_phone_number_cb(GtkWidget * widget)
{
  gchar *text[1];
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* check to see if we're editing a selection */
  if (GTK_CLIST(aw->phone_clist)->selection)
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->phone_clist)->selection->data);
    }
  else
    {
      row = -1;
    }

  text[0] = gtk_entry_get_text(GTK_ENTRY(aw->phone_entry));
  if (strcmp(text[0], "") == 0)
    {
      return;
    }

  /* we edit the phone number in place if they user hits 
   * "enter" from the phone number entry, we add it to the list
   * if the user clicks the "add" button -- both widgets use 
   * this callback, so the choice is made here
   */
  if (row >= 0 && widget == aw->phone_entry)
    {
      gtk_clist_set_text(GTK_CLIST(aw->phone_clist), row, 0, text[0]);
    }
  else
    {
      gtk_clist_append(GTK_CLIST(aw->phone_clist), text);
    }

  if (row >= 0)
    {
      gtk_clist_unselect_row(GTK_CLIST(aw->phone_clist), row, -1);
    }

  gtk_entry_set_text(GTK_ENTRY(aw->phone_entry), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
remove_phone_number_cb(GtkWidget * widget)
{
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* make sure there is somthing selected */
  if (!GTK_CLIST(aw->phone_clist)->selection)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->phone_clist)->selection->data);
    }

  gtk_clist_remove(GTK_CLIST(aw->phone_clist), row);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


/*
 * authentication page callbacks
 */
static void
user_changed_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
passwd_changed_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
remotename_changed_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


/*
 * ip address page callbacks
 */
static void 
ip_address_dynamic_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->ip), FALSE);
  gtk_entry_set_text(GTK_ENTRY(aw->ip), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
ip_address_static_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->ip), TRUE);
  gtk_entry_set_text(GTK_ENTRY(aw->ip), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void 
netmask_dynamic_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->mask), FALSE);
  gtk_entry_set_text(GTK_ENTRY(aw->mask), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
netmask_static_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->mask), TRUE);
  gtk_entry_set_text(GTK_ENTRY(aw->mask), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void 
remote_dynamic_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->remote), FALSE);
  gtk_entry_set_text(GTK_ENTRY(aw->remote), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
remote_static_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_widget_set_sensitive(GTK_WIDGET(aw->remote), TRUE);
  gtk_entry_set_text(GTK_ENTRY(aw->remote), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


/* 
 * dns page callbacks 
 */
static void 
dns_server_select_cb(GtkWidget * widget,  gint row)
{
  AccountWindow *aw = get_account_context(widget);
  gchar *text;

  g_assert(aw != NULL);

  gtk_clist_get_text(GTK_CLIST(aw->dns_server_clist), row, 0, &text);
  gtk_entry_set_text(GTK_ENTRY(aw->dns_server_entry), text);
}


static void
dns_server_unselect_cb(GtkWidget * widget, gint row)
{
  AccountWindow *aw = get_account_context(widget);
  gchar *text[1];
  gtk_entry_set_text(GTK_ENTRY(aw->dns_server_entry), "");
}


static void 
add_dns_server_cb(GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);
  gchar *text[1];
  gint row;

  g_assert(aw != NULL);

  /* check to see if we're editing a selection */
  if (GTK_CLIST(aw->dns_server_clist)->selection)
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->dns_server_clist)->selection->data);
    }
  else
    {
      row = -1;
    }

  text[0] = gtk_entry_get_text(GTK_ENTRY(aw->dns_server_entry));
  if (strcmp(text[0], "") == 0)
    {
      return;
    }

  if (row >= 0 && widget == aw->dns_server_entry)
    {
      gtk_clist_set_text(GTK_CLIST(aw->dns_server_clist), row, 0, text[0]);
    }
  else
    {
      gtk_clist_append(GTK_CLIST(aw->dns_server_clist), text);
    }

  if (row >= 0)
    {
      gtk_clist_unselect_row(GTK_CLIST(aw->dns_server_clist), row, -1);
    }

  gtk_entry_set_text(GTK_ENTRY(aw->dns_server_entry), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
remove_dns_server_cb(GtkWidget * widget)
{
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* make sure there is somthing selected */
  if (!GTK_CLIST(aw->dns_server_clist)->selection)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->dns_server_clist)->selection->data);
    }

  gtk_clist_remove(GTK_CLIST(aw->dns_server_clist), row);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void 
dns_search_select_cb(GtkWidget * widget,  gint row)
{
  gchar *text;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_clist_get_text(GTK_CLIST(aw->dns_search_clist), row, 0, &text);
  gtk_entry_set_text(GTK_ENTRY(aw->dns_search_entry), text);
}


static void 
dns_search_unselect_cb(GtkWidget * widget, gint row)
{
  gchar *text[1];
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  gtk_entry_set_text(GTK_ENTRY(aw->dns_search_entry), "");
}


static void
add_dns_search_cb(GtkWidget * widget)
{
  gchar *text[1];
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* check to see if we're editing a selection */
  if (GTK_CLIST(aw->dns_search_clist)->selection)
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->dns_search_clist)->selection->data);
    }
  else
    {
      row = -1;
    }

  text[0] = gtk_entry_get_text(GTK_ENTRY(aw->dns_search_entry));
  if (strcmp(text[0], "") == 0)
    {
      return;
    }

  if (row >= 0 && widget == aw->dns_search_entry)
    {
      gtk_clist_set_text(GTK_CLIST(aw->dns_search_clist), row, 0, text[0]);
    }
  else
    {
      gtk_clist_append(GTK_CLIST(aw->dns_search_clist), text);
    }

  if (row >= 0)
    {
      gtk_clist_unselect_row(GTK_CLIST(aw->dns_search_clist), row, -1);
    }

  gtk_entry_set_text(GTK_ENTRY(aw->dns_search_entry), "");

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
remove_dns_search_cb(GtkWidget * widget)
{
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* make sure there is somthing selected */
  if (!GTK_CLIST(aw->dns_search_clist)->selection)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(GTK_CLIST(aw->dns_search_clist)->selection->data);
    }

  gtk_clist_remove(GTK_CLIST(aw->dns_search_clist), row);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


/*
 * script page callbacks
 */
static void
script_type_cb (GtkWidget * widget)
{
  gint i;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  i = GPOINTER_TO_INT(gtk_object_get_user_data(GTK_OBJECT(widget)));
  switch (i)
    {
    case 0:
    case 1:
    case 4:
      gtk_widget_set_sensitive(GTK_WIDGET(aw->script_entry), TRUE);
      gtk_entry_set_text(GTK_ENTRY(aw->script_entry), "");
      break;
      
    case 2:
      gtk_widget_set_sensitive(GTK_WIDGET(aw->script_entry), FALSE);
      gtk_entry_set_text(
          GTK_ENTRY(aw->script_entry), 
	  gtk_entry_get_text(GTK_ENTRY(aw->user)));
      break;
      
    case 3:
      gtk_widget_set_sensitive(GTK_WIDGET(aw->script_entry), FALSE);
      gtk_entry_set_text(GTK_ENTRY(aw->script_entry), _("**PASSWORD**"));
      break;
      
    default:
      g_error(_("script_type_cb(): optionmenu data bad"));
      break;
    }
  
  /* set entry label */
  gtk_label_set_text(
      GTK_LABEL(aw->script_entry_label), 
      _(script_type_string[i].label_text));

  /* set the user data of the option menu to the type index so it can
   * be recovered in the add_script_cb
   */
  gtk_object_set_user_data(
      GTK_OBJECT(aw->script_type_option_menu), 
      GINT_TO_POINTER(i));
}


static void 
insert_script (GtkWidget *widget, gint row)
{
  gint i;
  AccountWindow *aw = get_account_context(widget);
  gchar *text_string;
  gchar *text[2];

  g_assert(aw != NULL);

  i = GPOINTER_TO_INT(
          gtk_object_get_user_data(GTK_OBJECT(aw->script_type_option_menu)));

  text_string = gtk_entry_get_text(GTK_ENTRY(aw->script_entry));

  /* append to clist */
  text[0] = _(script_type_string[i].option_text);
  text[1] = text_string;

  if (row < 0)
    {
      row = gtk_clist_append(GTK_CLIST(aw->script_clist), text);
    }
  else
    {
      gtk_clist_insert(GTK_CLIST(aw->script_clist), row, text);
    }
  gtk_clist_set_row_data(GTK_CLIST(aw->script_clist), row, GINT_TO_POINTER(i));

  switch (i)
    {
    case 0:
    case 1:
      gtk_widget_set_sensitive(GTK_WIDGET(aw->script_entry), TRUE);
      gtk_entry_set_text(GTK_ENTRY(aw->script_entry), "");
      break;

    default:
      break;
    }

}


static void 
add_script_cb(GtkWidget *widget)
{
  insert_script(widget, -1);
}


static void
insert_script_cb(GtkWidget *widget)
{
  gint row;
  AccountWindow *aw = get_account_context(widget);

  g_assert(aw != NULL);

  /* make sure there is a row selected */
  if (!GTK_CLIST(aw->script_clist)->selection)
    {
      return;
    }

  row = GPOINTER_TO_INT(GTK_CLIST(aw->script_clist)->selection->data);
  insert_script(widget, row);
}


static void
remove_script_cb (GtkWidget * widget)
{
  AccountWindow *aw = get_account_context(widget);
  gint row;

  g_assert(aw != NULL);

  /* make sure there is a row selected */
  if (!GTK_CLIST(aw->script_clist)->selection)
    {
      return;
    }

  row = GPOINTER_TO_INT(GTK_CLIST(aw->script_clist)->selection->data);
  gtk_clist_remove(GTK_CLIST(aw->script_clist), row);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
script_select_cb (GtkWidget * widget,  gint row)
{
  AccountWindow *aw = get_account_context(widget);
  g_assert(aw != NULL);
}


static void
script_unselect_cb (GtkWidget * widget, gint row)
{
  AccountWindow *aw = get_account_context(widget);
  g_assert(aw != NULL);
}


/*
 * system page callbacks
 */
static void
default_route_clicked_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
lock_device_clicked_cb(GtkWidget *widget)
{
  AccountWindow *aw = get_account_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


/* 
 * modem page callbacks 
 */
static void
modem_device_cb(GtkWidget * widget)
{
  gpointer data;
  AccountWindow *aw = get_account_context(widget);

  data = gtk_object_get_user_data(GTK_OBJECT(widget));
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_device), data);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
connection_speed_cb(GtkWidget * widget)
{
  gpointer data;
  AccountWindow *aw = get_account_context(widget);
  
  data = gtk_object_get_user_data(GTK_OBJECT(widget));
  gtk_object_set_user_data(GTK_OBJECT(aw->connection_speed), data);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
modem_flow_cb(GtkWidget * widget)
{
  gpointer data;
  AccountWindow *aw = get_account_context(widget);

  data = gtk_object_get_user_data(GTK_OBJECT(widget));
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_flow), data);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));
}


static void
modem_term_cb(GtkWidget * widget)
{
  gpointer data;
  AccountWindow *aw = get_account_context(widget);

  data = gtk_object_get_user_data(GTK_OBJECT(widget));
  gtk_object_set_user_data(GTK_OBJECT(aw->modem_term), data);

  gnome_property_box_changed(GNOME_PROPERTY_BOX(aw->property_box));  
}

  
static void 
edit_modem_commands_cb(GtkWidget *widget, Account *account)
{
  /* open the modem properties window */
  open_modem_window(account);
} 
