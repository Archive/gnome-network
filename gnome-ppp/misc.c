/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "misc.h"


/* creates one label (text) menuitem, connects the callback,
 * and adds it to the menu 
 */
GtkWidget *
append_menuitem_connect(GtkMenu *menu,
			gchar *text,
			GtkSignalFunc func,
			gpointer data,
			gpointer user_data)
{
  GtkWidget *menuitem;

  menuitem = gtk_menu_item_new_with_label(text);
  gtk_menu_append(menu, menuitem);
  gtk_widget_show(menuitem);

  gtk_signal_connect(
      GTK_OBJECT(menuitem),
      "activate",
      (GtkSignalFunc) func,
      data);

  if (user_data)
    {
      gtk_object_set_user_data(GTK_OBJECT(menuitem), user_data);
    }

  return menuitem;
}
