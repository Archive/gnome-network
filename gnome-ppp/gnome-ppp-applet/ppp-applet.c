/*
 * PPP dialer applet for the GNOME panel
 *
 *  Copyright (c) 1999 by Cody A. Russell
 *  Written by Cody Russell (bratsche@dfw.net or car0969@gamma2.uta.edu)
 *  Graphics by Dietmar Heil (Dietmar.Heil@unidui.uni-duisburg.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTCIULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <applet-widget.h>
#include <signal.h>
#include "libgnome_ppp/gnome-ppp.h"
#include "ppp-applet.h"

#ifndef GNOMELOCALEDIR
#define GNOMELOCALEDIR "/usr/share/locale"
#endif /* GNOMELOCALEDIR */

void start_animation(Dialer *dialer)
{
   dialer->stop = FALSE;
   gnome_animator_start(GNOME_ANIMATOR(dialer->animator));
   gtk_clock_set_seconds(GTK_CLOCK(dialer->clock), 0);
}

void stop_animation(Dialer *dialer)
{
   dialer->stop = TRUE;
   gnome_animator_stop(GNOME_ANIMATOR(dialer->animator));
   gtk_clock_stop(GTK_CLOCK(dialer->clock));
}

static void refresh_account_list(Dialer *dialer)
{
   Account *saved_account, *account;

   saved_account = dialer->account;
   dialer->account_list = account_list();

   rebuild_submenu(dialer);
}

static Dialer *malloc_dialer(GtkWidget *applet)
{
   Dialer *dialer;

   dialer = g_malloc0(sizeof(Dialer));

   dialer->state          = PPP_DISCONNECTED;
   dialer->applet         = applet;
   dialer->account_list   = account_list();
   dialer->pid            = -1;

   return dialer;
}

#if 0
static void free_dialer(Dialer *dialer)
{
   gtk_widget_destroy(dialer->property_box);
   gtk_widget_destroy(dialer->about);
   gtk_widget_destroy(dialer->applet);

   g_list_free(dialer->account_list);
   g_slist_free(dialer->group);
   account_free(dialer->account);

   g_free(dialer);
}
#endif

static void write_status(Dialer *dialer, gchar *msg)
{
   gtk_label_set_text(GTK_LABEL(dialer->label), msg);
}

static GtkWidget *get_animator_with_size(GraphicType type)
{
   GtkWidget *animator;
   gchar *filename = NULL;
   gchar *s = NULL;
   gint size = (type == ANIMATION_SMALL ? 22 : 48);

   if(type == ANIMATION_NONE)
      return NULL;

   s = g_strconcat("gnome-ppp-animation-", (type == ANIMATION_SMALL ? "small" : "large"),
                   ".png",  NULL);
   filename = gnome_unconditional_pixmap_file(s);
   animator = gnome_animator_new_with_size(size, size);
   gnome_animator_set_loop_type(GNOME_ANIMATOR(animator), GNOME_ANIMATOR_LOOP_RESTART);
   gnome_animator_append_frames_from_file(GNOME_ANIMATOR(animator),
                                          filename, 0, 0, 36, size);
   gtk_widget_show(animator);
   g_free(s);
   g_free(filename);

   return animator;
}

static void create_dialer_applet(Dialer *dialer)
{
   DialerProperties *properties;
   GtkWidget        *frame;
   GtkWidget        *vbox;
   GtkWidget        *separator;
   GtkWidget        *menu_item;
   GtkWidget        *hbox;
   GtkWidget        *evtbox;
   GtkWidget        *label;
   GtkWidget        *tmpbox;
   gchar            *str;
   Account          *account;
   GList            *list;
   gint             max_width, tmp_width;

   properties = dialer->properties;

   vbox = gtk_vbox_new(FALSE, 0);
   hbox = gtk_hbox_new(FALSE, 0);
   gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
   gtk_widget_show(hbox);
   gtk_widget_show(vbox);

   dialer->animator = get_animator_with_size(properties->animation_size);

  /* Setup an event box to trap mouse press events to the animator */
   evtbox = gtk_event_box_new();
   gtk_signal_connect(GTK_OBJECT(evtbox),
                      "button_press_event",
                      GTK_SIGNAL_FUNC(button_press),
                      dialer);
   gtk_widget_set_events(evtbox, GDK_BUTTON_PRESS_MASK);
   tmpbox = gtk_hbox_new(TRUE, 0);
   gtk_container_add(GTK_CONTAINER(evtbox), tmpbox);
   gtk_box_pack_start(GTK_BOX(tmpbox), dialer->animator, FALSE, TRUE, 0);
   gtk_box_pack_start(GTK_BOX(hbox), evtbox, FALSE, FALSE, 0);
   gtk_widget_show(evtbox);
   gtk_widget_show(tmpbox);

   dialer->clock = gtk_clock_new(0);
   gtk_clock_set_seconds(GTK_CLOCK(dialer->clock), 0);
   gtk_box_pack_start(GTK_BOX(hbox), dialer->clock, TRUE, FALSE, 0);
   gtk_widget_show(dialer->clock);

   str = gnome_config_get_string("/gnome-ppp/applet/selected_account");
   if(str)
   {
      list = account_list();

      while(list)
      {
         if(strcmp(((Account *) list->data)->name->str, str) == 0)
         {
            dialer->account = account_clone(list->data);
         }

         list = list->next;
      }

      g_list_free(list);
      g_free(str);
   }

   dialer->menu = gtk_menu_new();
   menu_item = gtk_menu_item_new_with_label(_("Raise PPP Link"));
   if(dialer->account == NULL)
   {
      gtk_widget_set_sensitive(menu_item, FALSE);
   }
   gtk_signal_connect(GTK_OBJECT(menu_item), "activate",
		      GTK_SIGNAL_FUNC(link_raise_cb),
		      dialer);
   gtk_widget_show(menu_item);
   gtk_object_set_data(GTK_OBJECT(dialer->menu), "dialer", dialer);
   gtk_object_set_data(GTK_OBJECT(dialer->menu), "raise_item",
		       menu_item);
   gtk_menu_append(GTK_MENU(dialer->menu), menu_item);

   menu_item = gtk_menu_item_new_with_label("Lower PPP Link");
   if(dialer->account == NULL)
   {
      gtk_widget_set_sensitive(menu_item, FALSE);
   }
   gtk_signal_connect(GTK_OBJECT(menu_item), "activate",
		      GTK_SIGNAL_FUNC(link_lower_cb),
		      dialer);
   gtk_widget_show(menu_item);
   gtk_object_set_data(GTK_OBJECT(dialer->menu), "lower_item",
		       menu_item);
   gtk_menu_append(GTK_MENU(dialer->menu), menu_item);

   build_submenu(dialer);

   applet_widget_register_stock_callback(APPLET_WIDGET(dialer->applet),
					"about",
					GNOME_STOCK_MENU_ABOUT,
					_("About..."),
					about_cb,
					dialer);
   applet_widget_register_stock_callback(APPLET_WIDGET(dialer->applet),
                                         "properties",
                                         GNOME_STOCK_MENU_PROP,
                                         _("Properties..."),
                                         properties_cb,
                                         dialer);

   dialer->label = gtk_label_new(_("Offline"));
   gtk_widget_show(dialer->label);
   gtk_box_pack_start(GTK_BOX(vbox), dialer->label, FALSE, FALSE, 0);

   frame = gtk_frame_new(NULL);
   gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
   gtk_widget_show(frame);
   gtk_container_add(GTK_CONTAINER(frame), vbox);

   /* We want to change the width. Determine the lengths of all the strings that
      will be fitting into the frame, and use the largest one as our new width.
      This way we don't need to resize the frame when the label changes later on. */
   max_width = gdk_string_width(dialer->label->style->font,
                                DISCONNECTED_LABEL);
   tmp_width = gdk_string_width(dialer->label->style->font,
                                CONNECTING_LABEL);
   if(tmp_width > max_width)
      max_width = tmp_width;
   tmp_width = gdk_string_width(dialer->label->style->font,
                                CONNECTED_LABEL);
   if(tmp_width > max_width)
      max_width = tmp_width;
   tmp_width = gdk_string_width(dialer->label->style->font,
                                ERROR_LABEL);
   if(tmp_width > max_width)
      max_width = tmp_width;

   /* Give it four pixels of buffer space */
   max_width += 4;

   /* We want to set a new width, and leave the height alone. */
   gtk_widget_set_usize(frame, max_width, -2);

   applet_widget_add(APPLET_WIDGET(dialer->applet), frame);
   gtk_widget_show(dialer->applet);
}

static gboolean load_session()
{
}

static gint save_session(GnomeClient *client, gint phase,
                         GnomeSaveStyle save_style, gint is_shutdown,
                         GnomeInteractStyle interact_style, gint is_fast,
                         gpointer client_data)
{
}

static gint session_die(GnomeClient *client, gpointer client_data)
{
}

static void select_account_cb(GtkWidget *widget, Account *account)
{
   GtkWidget *tmp;

   Dialer *dialer = gtk_object_get_data(GTK_OBJECT(widget), "dialer");
   dialer->account = account;
   tmp = gtk_object_get_data(GTK_OBJECT(dialer->menu),
                             "raise_item");
   gtk_widget_set_sensitive(tmp, TRUE);
   gnome_config_set_string("/gnome-ppp/applet/selected_account",
                           account->name->str);
}

static void add_submenu_item(gpointer data, gpointer user_data)
{
   GtkWidget *menu_item;
   Account *account = data;
   Dialer *dialer = user_data;

   if(!dialer->group)
   {
      menu_item = gtk_radio_menu_item_new_with_label(NULL, account->name->str);
      dialer->group = gtk_radio_menu_item_group(GTK_RADIO_MENU_ITEM(menu_item));
   }
   else
   {
      menu_item = gtk_radio_menu_item_new_with_label(dialer->group,
                                                     account->name->str);
   }

   gtk_object_set_data(GTK_OBJECT(menu_item), "dialer",
                       (gpointer) dialer);
   gtk_signal_connect(GTK_OBJECT(menu_item), "activate",
		     GTK_SIGNAL_FUNC(select_account_cb),
		     account);

   gtk_widget_show(menu_item);
   gtk_menu_append(GTK_MENU(dialer->submenu), menu_item);
}

static void rebuild_submenu(Dialer *dialer)
{
   GtkWidget *submenu;
   GtkWidget *menu_item;
   gint length;

   menu_item = gtk_object_get_data(GTK_OBJECT(dialer->menu), "select_item");
   gtk_menu_item_remove_submenu(GTK_MENU_ITEM(menu_item));
   gtk_widget_destroy(dialer->submenu);

   length = g_list_length(dialer->account_list);
   if(length != 1)
   {
      submenu = gtk_menu_new();
      dialer->submenu = submenu;

      if(length > 1)
      {
         g_list_foreach(dialer->account_list, add_submenu_item, dialer);
      }
      else
      {
         menu_item = gtk_menu_item_new_with_label("No available hosts to dial");
         gtk_widget_set_sensitive(menu_item, FALSE);
         gtk_widget_show(menu_item);
         gtk_menu_append(GTK_MENU(dialer->submenu), menu_item);
      }
   }
   else if(length == 1)
   {
      dialer->account = g_list_nth_data(dialer->account_list, 0);
      menu_item = gtk_object_get_data(GTK_OBJECT(dialer->menu), "raise_item");
      gtk_widget_set_sensitive(menu_item, TRUE);
   }
}

static void build_submenu(Dialer *dialer)
{
   GtkWidget *menu;
   GtkWidget *menu_item;
   GtkWidget *submenu;
   gint length;

   length = g_list_length(dialer->account_list);
   if(length != 1)
   {
      submenu = gtk_menu_new();
      dialer->submenu = submenu;
      menu_item = gtk_menu_item_new_with_label("Select Account");
      gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), submenu);
      gtk_menu_append(GTK_MENU(dialer->menu), menu_item);
      gtk_object_set_data(GTK_OBJECT(dialer->menu), "select_item", menu_item);
      gtk_widget_show(menu_item);

      if(length > 1)
      {
         g_list_foreach(dialer->account_list, add_submenu_item, dialer);
      }
      else
      {
         menu_item = gtk_menu_item_new_with_label("No available hosts to dial");
         gtk_widget_set_sensitive(menu_item, FALSE);
         gtk_widget_show(menu_item);
         gtk_menu_append(GTK_MENU(dialer->submenu), menu_item);
      }
   }
   else if(length == 1)
   {
      dialer->account = g_list_nth_data(dialer->account_list, 0);
      menu_item = gtk_object_get_data(GTK_OBJECT(dialer->menu), "raise_item");
      gtk_widget_set_sensitive(menu_item, TRUE);
   }
}

static void dialog_destroy_cb(GtkWidget *widget, gpointer data)
{
   Dialer *dialer = data;

   dialer->input_dialog = NULL;
}

static void input_dialog_cb(gchar *str, gpointer data)
{
   Dialer *dialer = data;

   connect_send(dialer->account, str, TRUE);
}

static void set_raise_active(Dialer *dialer, gboolean active)
{
   GtkWidget *tmp;

   tmp = gtk_object_get_data(GTK_OBJECT(dialer->menu),
		             "raise_item");
   gtk_widget_set_sensitive(tmp, active);
   tmp = gtk_object_get_data(GTK_OBJECT(dialer->menu),
                             "lower_item");
   gtk_widget_set_sensitive(tmp, ! active );
}

static void ppp_message_handler(Account *account, PPPMessageType msg,
                                gchar *text, gpointer data)
{
   Dialer *dialer = data;
   gint update_tooltip = FALSE;

   switch(msg)
   {
      case PPP_DISCONNECTED:
         dialer->state = PPP_DISCONNECTED;
         update_tooltip = TRUE;
         write_status(dialer, DISCONNECTED_LABEL);
	 set_raise_active(dialer, TRUE);
        break;
      case PPP_IN_PROGRESS:
         dialer->state = PPP_IN_PROGRESS;
         update_tooltip = TRUE;
         write_status(dialer, _("Connecting..."));
         break;
      case PPP_CONNECTED:
         dialer->state = PPP_CONNECTED;
         update_tooltip = TRUE;
	 write_status(dialer, _("PPP Connected"));
         gtk_clock_start(GTK_CLOCK(dialer->clock));
         break;
      case PPP_ERROR:
         gnome_error_dialog(_(text));
	 lower_link(dialer);
         write_status(dialer, _("PPP Error!"));
         break;
      case PPP_DEBUG:
         break;
      case PPP_PROMPT_INPUT:
         if(dialer->input_dialog)
            g_error("dialer->input_dialog != NULL");

         dialer->input_dialog = gnome_request_dialog(
                                  TRUE,
                                  _(text),
                                  "",
                                  100,
                                  (GnomeStringCallback) input_dialog_cb,
                                  dialer,
                                  NULL);
         gtk_signal_connect(GTK_OBJECT(dialer->input_dialog),
                            "destroy",
                            GTK_SIGNAL_FUNC(dialog_destroy_cb),
                            dialer);
         break;
      default:
         g_error("unknown callback message");
   }

   return;
}

static void lower_link(Dialer *dialer)
{
   set_raise_active(dialer, TRUE);
   connect_stop(dialer->account);
   account_unlock(dialer->account);
   stop_animation(dialer);
}

static void raise_link(Dialer *dialer)
{
   if(account_is_locked(dialer->account))
   {
      gnome_error_dialog(_("That account is already in use.\n"));
   }
   else
   {
      account_lock(dialer->account);
      set_raise_active(dialer, FALSE);
      if(connect_start(dialer->account, ppp_message_handler, dialer))
      {
         dialer->pid = connect_pid(dialer->account);
         start_animation(dialer);
      }
   }
}

static void link_lower_cb(GtkWidget *widget, Dialer *dialer)
{
   lower_link(dialer);
}

static void link_raise_cb(GtkWidget *widget, Dialer *dialer)
{
   raise_link(dialer);
}

static void menu_position_func(GtkMenu *menu, gint *x, gint *y,
                               gpointer data)
{
   GtkWidget *button;
   Dialer *dialer;
   DialerProperties *properties;
   int wx, wy;
   int ax, ay;
   gint size = (properties->animation_size == ANIMATION_SMALL ? 22 : 48);

   button = GTK_WIDGET(data);
   dialer = gtk_object_get_data(GTK_OBJECT(menu), "dialer");
   properties = dialer->properties;
   *x = *y = 0;
   gdk_window_get_origin(button->window, &wx, &wy);

   switch(applet_widget_get_panel_orient(APPLET_WIDGET(dialer->applet)))
   {
      case ORIENT_UP:
         ay = GTK_WIDGET(menu)->requisition.height;
         *x = wx;
         *y = wy - ay;
         break;

      case ORIENT_DOWN:
         ay = GTK_WIDGET(menu)->requisition.height;
         *x = wx;
         *y = wy + size;
         break;

      case ORIENT_RIGHT:
         ax = GTK_WIDGET(menu)->requisition.width;
         *x = wx + size;
         *y = wy;
         break;

      case ORIENT_LEFT:
         ax = GTK_WIDGET(menu)->requisition.width;
         *x = wx - ax;
         *y = wy;
         break;

      default:
         g_error("Unknown panel orientation!\n");
   }
}

static void about_cb(AppletWidget *widget, gpointer data)
{
   GtkWidget *about;
   Dialer *dialer = data;

   static const gchar *authors[] = {
     "Cody Russell (car0969@gamma2.uta.edu or bratsche@dfw.net)",
     "Graphics by Dietmar Heil (Dietmar.Heil@unidui.uni-duisburg.de)",
     NULL
   };

   dialer->about =
      gnome_about_new(_("GNOME PPP Dialer Applet"), VERSION,
			"(C) 1999 Cody Russell",
			authors,
                        _("An unnecessarily nice looking applet for GNOME's "
                        "panel for easy control of PPP network links."),
			NULL);
  gtk_widget_show(dialer->about);
}

static void prop_apply_cb(GtkWidget *widget, gint button, gpointer data)
{
   Dialer *dialer = data;

   dialer->animation_speed = dialer->tmp_speed;
   gnome_config_set_int("/gnome-ppp/applet/animation_speed", dialer->animation_speed);

   return;
}

static void prop_destroy_cb(GtkWidget *widget, gpointer data)
{
   Dialer *dialer = data;

   gtk_widget_destroy(dialer->property_box);
   dialer->property_box = NULL;
}

static void speed_adjustment_cb(GtkAdjustment *adj, gpointer data)
{
   Dialer *dialer = (Dialer *) data;

   dialer->tmp_speed = (gint) adj->value;
   gnome_property_box_changed(GNOME_PROPERTY_BOX(dialer->property_box));
}

static void disconnect_toggled(GtkWidget *widget, gpointer data)
{
   DialerProperties *properties;

   properties = gtk_object_get_user_data(GTK_OBJECT(widget));
   properties->disconnect_on_logout = !properties->disconnect_on_logout;
}

static void freeze_toggled(GtkWidget *widget, gpointer data)
{
   DialerProperties *properties;

   properties = gtk_object_get_user_data(GTK_OBJECT(widget));
   properties->freeze = !properties->freeze;
}

static void timer_radio_cb(GtkWidget *widget, gpointer data)
{
   DialerProperties *properties;

   properties = gtk_object_get_user_data(GTK_OBJECT(widget));
   properties->timer_type = GPOINTER_TO_INT(data);
}

static void load_properties(DialerProperties *properties)
{
   gchar *str = NULL;

   gnome_config_push_prefix("/gnome-ppp/applet");
   if((properties->animation_speed = gnome_config_get_int("anomation_speed")) == 0)
      properties->animation_speed = 100;
   str = gnome_config_get_string("animation_size");
   if(!str)
   {
      properties->animation_size = ANIMATION_SMALL;
      gnome_config_set_string("animation_size", "small");
   }
   else
   {
      if((strcmp(str, "small") == 0))
         properties->animation_size = ANIMATION_SMALL;
      else if((strcmp(str, "large") == 0))
         properties->animation_size = ANIMATION_LARGE;
      else if((strcmp(str, "none") == 0))
         properties->animation_size = ANIMATION_NONE;
      else
      {
         properties->animation_size = ANIMATION_SMALL;
         gnome_config_set_string("animation_size", "small");
      }

      g_free(str);
   }
}

static void properties_cb(AppletWidget *applet, gpointer data)
{
   DialerProperties *properties;
   GtkWidget        *hbox;
   GtkWidget        *vbox;
   GtkWidget        *slider;
   GtkWidget        *radio;
   GtkWidget        *animation_select;
   GtkWidget        *x_size;
   GtkWidget        *y_size;
   GtkWidget        *frame;
   GtkWidget        *label;
   GtkWidget        *hscale;
   GtkWidget        *check;
   GSList           *group;
   GtkWidget        *box2;
   GtkObject        *adjust;

   static GnomeHelpMenuEntry help = {
          NULL, "ppp-applet-properties"
   };

   Dialer *dialer = data;
   properties = dialer->properties;

   help.name = gnome_app_id;

   if(dialer->property_box != NULL)
   {
      gdk_window_raise(dialer->property_box->window);
      return;
   }

   dialer->property_box = gnome_property_box_new();
   gtk_window_set_title(GTK_WINDOW(dialer->property_box),
                        _("PPP Applet Properties"));
   gtk_signal_connect(GTK_OBJECT(dialer->property_box), "apply",
                      GTK_SIGNAL_FUNC(prop_apply_cb), dialer);
   gtk_signal_connect(GTK_OBJECT(dialer->property_box), "destroy",
                      GTK_SIGNAL_FUNC(prop_destroy_cb), dialer);
   gtk_signal_connect(GTK_OBJECT(dialer->property_box), "help",
                      GTK_SIGNAL_FUNC(gnome_help_pbox_display), &help);
   gtk_widget_show(dialer->property_box);

   vbox = gtk_vbox_new(FALSE, 0);
   gtk_widget_show(vbox);
   hbox = gtk_hbox_new(FALSE, 0);
   gtk_widget_show(hbox);
   gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

   frame = gtk_frame_new(_("Animation"));
   gtk_widget_show(frame);
   box2 = gtk_vbox_new(FALSE, 0);
   gtk_widget_show(box2);
   gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);
   gtk_container_add(GTK_CONTAINER(frame), box2);
   radio = gtk_radio_button_new_with_label(NULL, _("Small"));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);
   group = gtk_radio_button_group(GTK_RADIO_BUTTON(radio));
   radio = gtk_radio_button_new_with_label(group, _("Large"));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);
   group = gtk_radio_button_group(GTK_RADIO_BUTTON(radio));
   radio = gtk_radio_button_new_with_label(group, _("None"));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);

   frame = gtk_frame_new(_("Options"));
   gtk_widget_show(frame);
   gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);
   box2 = gtk_vbox_new(FALSE, 0);
   gtk_widget_show(box2);

   check = gtk_check_button_new_with_label(_("Freeze Animation on Connect"));
   gtk_object_set_user_data(GTK_OBJECT(check), properties);
   gtk_signal_connect(GTK_OBJECT(check), "toggled",
                      GTK_SIGNAL_FUNC(freeze_toggled), dialer);
   gtk_widget_show(check);
   gtk_box_pack_start(GTK_BOX(box2), check, TRUE, TRUE, 0);

   check = gtk_check_button_new_with_label(_("Disconnect PPP on GNOME logout"));
   gtk_object_set_user_data(GTK_OBJECT(check), properties);
   gtk_signal_connect(GTK_OBJECT(check), "toggled",
                      GTK_SIGNAL_FUNC(disconnect_toggled), dialer);
   gtk_widget_show(check);
   gtk_box_pack_start(GTK_BOX(box2), check, TRUE, TRUE, 0);
   gtk_container_add(GTK_CONTAINER(frame), box2);

   frame = gtk_frame_new(_("Timer"));
   gtk_widget_show(frame);
   gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);
   box2 = gtk_vbox_new(FALSE, 0);
   gtk_widget_show(box2);

   radio = gtk_radio_button_new_with_label(NULL, _("Label"));
   gtk_object_set_user_data(GTK_OBJECT(radio), properties);
   gtk_signal_connect(GTK_OBJECT(radio), "toggled",
                      GTK_SIGNAL_FUNC(timer_radio_cb),
                      GINT_TO_POINTER(TIMER_LABEL));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);
   group = gtk_radio_button_group(GTK_RADIO_BUTTON(radio));

   radio = gtk_radio_button_new_with_label(group, _("Tooltip"));
   gtk_object_set_user_data(GTK_OBJECT(radio), properties);
   gtk_signal_connect(GTK_OBJECT(radio), "toggled",
                      GTK_SIGNAL_FUNC(timer_radio_cb),
                      GINT_TO_POINTER(TIMER_TOOLTIPS));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);
   group = gtk_radio_button_group(GTK_RADIO_BUTTON(radio));

   radio = gtk_radio_button_new_with_label(group, _("None"));
   gtk_object_set_user_data(GTK_OBJECT(radio), properties);
   gtk_signal_connect(GTK_OBJECT(radio), "toggled",
                      GTK_SIGNAL_FUNC(timer_radio_cb),
                      GINT_TO_POINTER(TIMER_NONE));
   gtk_widget_show(radio);
   gtk_box_pack_start(GTK_BOX(box2), radio, TRUE, TRUE, 0);
   gtk_container_add(GTK_CONTAINER(frame), box2);

   frame = gtk_frame_new(_("Animation Speed"));
   adjust = gtk_adjustment_new(50.0, 1.0, 300.0, 1.0, 1.0, 0.0);
   gtk_signal_connect(adjust, "value_changed",
                      GTK_SIGNAL_FUNC(speed_adjustment_cb), dialer);
   hscale = gtk_hscale_new(GTK_ADJUSTMENT(adjust));
   gtk_widget_show(hscale);
   gtk_container_add(GTK_CONTAINER(frame), hscale);
   gtk_scale_set_digits(GTK_SCALE(hscale), 0);
   gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
   gtk_widget_show_all(frame);

   gtk_widget_show(vbox);

   label = gtk_label_new(_("PPP Applet"));
   gnome_property_box_append_page(GNOME_PROPERTY_BOX(dialer->property_box),
                                  vbox, label);
}

static gint button_press(GtkWidget *widget, GdkEventButton *evt, Dialer *dialer)
{
   if(evt->button == 1)
      gtk_menu_popup(GTK_MENU(dialer->menu), 0, 0,
                     (GtkMenuPositionFunc) menu_position_func,
                     GTK_OBJECT(dialer->animator),
                     evt->button, evt->time);

   return TRUE;
}

gint main(gint argc, gchar **argv)
{
   GnomeClient *client;
   DialerProperties *properties;
   GtkWidget *applet;
   Dialer *dialer;
   gchar *goad_id;

   bindtextdomain(PACKAGE, GNOMELOCALEDIR);
   textdomain(PACKAGE);

   applet_widget_init ("PPP_Applet", VERSION, argc, argv, NULL, 0, NULL);
   gnomeppplib_init();
   account_load();

   properties = g_malloc0(sizeof(DialerProperties));
   load_properties(properties);

   client = gnome_master_client();
   gtk_signal_connect(GTK_OBJECT(client), "save_yourself",
                      GTK_SIGNAL_FUNC(save_session), properties);
   gtk_signal_connect(GTK_OBJECT(client), "die",
                      GTK_SIGNAL_FUNC(session_die), properties);

   goad_id = (gchar *) goad_server_activation_id();
   if(goad_id && !strcmp(goad_id, "PPP_Applet"))
   {
      applet = applet_widget_new("PPP_Applet");
      if(!applet)
         g_error("Cannot create applet!\n");

      gtk_widget_realize(applet);
      dialer = malloc_dialer(applet);
      dialer->account_list = account_list();
      dialer->properties = properties;
      create_dialer_applet(dialer);
   }

   applet_widget_gtk_main();
   /* free_dialer(dialer); */

   return 0;
}
