 /*
 * ppp-applet.h: PPP Dialer for GNOME panel
 *    (c) Copyright 1999 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTCIULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DIALER_H
#define DIALER_H

/* Some of these need to configurable from the properties box */
#define TOTAL_FRAMES   36
#define WIDGET_HEIGHT  22
#define WIDGET_WIDTH   22

#define DISCONNECTED_LABEL  _("PPP Offline")
#define CONNECTING_LABEL    _("Connecting...")
#define CONNECTED_LABEL     _("PPP Connected")
#define ERROR_LABEL         _("PPP Error!")

typedef enum
{
   ANIMATION_SMALL = 1,
   ANIMATION_LARGE = 2,
   ANIMATION_NONE  = 3
} GraphicType;

typedef enum
{
   TIMER_LABEL    = 1,
   TIMER_TOOLTIPS = 2,
   TIMER_NONE     = 3
} TimerType;

typedef struct _Dialer           Dialer;
typedef struct _DialerProperties DialerProperties;

struct _Dialer
{
   Account          *account;
   GList            *account_list;
   GSList           *group;
   DialerProperties *properties;
   GtkWidget        *menu;
   GtkWidget        *applet;
   GtkWidget        *clock;
   GtkWidget        *submenu;
   GtkWidget        *button;
   GtkWidget        *input_dialog;
   GtkWidget        *about;
   GtkWidget        *property_box;
   GtkWidget        *animator;
   GtkWidget        *label;
   gboolean         stop;
   gint             state;
   gint             pid;
   gint             timeouttime;
   gint             clock_timeout;
   gint             tmp_speed;
   gint             freeze;
   gint             animation_speed;
};

struct _DialerProperties
{
   gint        animation_speed;
   GraphicType animation_size;
   TimerType   timer_type;
   gboolean    freeze;
   gboolean    disconnect_on_logout;
};

/* Callbacks and utility function prototypes */
static void ppp_message_handler(Account *account, PPPMessageType msg,
                                gchar *text, gpointer data);
static void lower_link(Dialer *dialer);
static void raise_link(Dialer *dialer);
static void rebuild_submenu(Dialer *dialer);
static void menu_position_func(GtkMenu *menu, gint *x, gint *y, gpointer data);
static void hide_widgets(GtkWidget *widget, GtkWidget *toggle);
static void link_raise_cb(GtkWidget *widget, Dialer *dial);
static void link_lower_cb(GtkWidget *widget, Dialer *dial);
static void about_cb(AppletWidget *widget, gpointer data);
static gint button_press(GtkWidget *widget, GdkEventButton *evt, Dialer *dialer);
static void add_submenu_item(gpointer data, gpointer user_data);
static GtkWidget *setup_submenu(void);
static void build_submenu(Dialer *dialer);
static void properties_cb(AppletWidget *applet, gpointer data);

#endif /* DIALER_H */
