/* gnome-ppp-cmd - The GNOME PPP Dialer
 * Copyright (C) 1997, 1998 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <unistd.h>
#include <gnome.h>
#include "gnome-ppp.h"


static gint done              = 0;
static gint error_occured     = 0;
static gint verbose_flag      = 0;
static gint pppd_pid          = 0;


/* prototypes */
static void usage               (char *argv[]);
static void connect_message_cb  (Account         *account, 
			         PPPMessageType   message, 
			         gchar           *text,
			         gpointer         data);

int
main(int argc, char *argv[])
{
  gint arg;
  gchar *account_name;
  GList *list;
  Account *account;

  /* require at least 2 arguments */
  if (argc < 2)
    {
      usage(argv);
    }

  /* parse arguments */
  while ((arg = getopt(argc, argv, "hv?")) != EOF)
    {
      switch (arg)
	{
	case 'v':
	  verbose_flag = 1;
	  break;

	case 'h':
	case '?':
	default:
	  usage(argv);
	    break;
	}
    }

  /* account name */
  account = argv[argc-1];

  /* initalize libraries */
  gnomelib_init("gnome-ppp-cmd", "0.5");
  gnomeppplib_init();

  /* load account information */
  account_load();

  /* lookup account */
  account = NULL;
  list = account_list();
  while (list)
    {
      account = (Account *) list->data;
      if (strcmp(account_name, account->name->str) == 0)
	{
	  break;
	}
      list = list->next;
    }
  
  if (!account)
    {
      fprintf(stdout, "%s not found\n", account_name);
      exit(1);
    }

  /* dial/connect */
  if (pppd_pid = connect_start(account, connect_message_cb, NULL))
    {
      while (!done)
	{
	  usleep(50000);
	}
    }

  if (error_occured)
    {
      exit(1);
    }
 
  exit(0);
}



static void
usage(char *argv[])
{
  fprintf(stdout, "Usage: %s [-v] account\n", argv[0]);
  fprintf(stdout, "  -v\tverbose output\n");
  exit(1);
}


static void 
connect_message_cb(Account         *account, 
		   PPPMessageType   message, 
		   gchar           *text,
		   gpointer         data)
{
  g_assert(account != NULL);

  /* map messages from the connect engine to internal
   * states of the dial window
   */
  switch (message)
    {
    case PPP_DISCONNECTED:
      done = 1;
      break;


    case PPP_IN_PROGRESS:
      if (text && verbose_flag)
	{
	  fprintf(stdout, "%s\n", text);
	}
      break;


    case PPP_CONNECTED:
      fprintf(stdout, "%d\n", pppd_pid);
      break;


    case PPP_ERROR:
      error_occured = 1;
      if (text)
	{
	  fprintf(stdout, "ERROR: %s\n", text);
	}
      break;


    default:
      break;
    }
}

