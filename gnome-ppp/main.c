/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997, 1998 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <unistd.h>
#include <gnome.h>
#include "gnome-ppp.h"
#include "dial-window.h"
#include "global.h"

#ifndef GNOMELOCALEDIR
#define GNOMELOCALEDIR "/usr/share/locale"
#endif

int
main(int argc, char *argv[])
{
  gnome_init(GNOME_PPP_NAME, GNOME_PPP_VERSION, argc, argv);
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);
  gnomeppplib_init();
  account_load();
  new_dial_app();
  gtk_main();
  exit(0);
}
