/* gnome-ppp-chat
 * Copyright (C) 1997, 1998 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>

#include "config.h"

#if HAVE_SOCKETBITS_H
#include <socketbits.h>
#include <iovec.h>
#endif


/* linux libc5 ancillary data object manipulation macros */
#ifndef CMSG_DATA
#if !defined __STRICT_ANSI__ && defined __GNUC__ && __GNUC__ >= 2
# define CMSG_DATA(cmsg) ((cmsg)->cmsg_data)
#else
# define CMSG_DATA(cmsg) ((unsigned char *) ((struct cmsghdr *) (cmsg) + 1))
#endif
#endif /* CMSG_DATA */

#define CONTROLLEN (sizeof(struct cmsghdr) + sizeof(int))

/* define a random byte of data to send along with the file descriptor */
#define RANDOM_BYTE "Z"

/* max number of connection retries to the domain socket and the pause
 * time between connection attempts
 */
#define MAX_CONNECT_RETRIES 5
#define SEC_BETWEEN_RETRIES 2


/* prototypes */
int recieve_fd(int fd, int *recvfd);
int send_fd(int fd, int sendfd);


int
main(int argc, char *argv[])
{
  int fd, len, conn_stat, retry = 0;
  struct timeval tv;
  struct sockaddr_un name;
  fd_set rset;


  /* gnome-ppp-chat only takes one argument */
  if (argc != 2)
    {
      exit(1);
    }

  if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
      exit(1);
    }

  /* set address/path of the server */
  memset(&name, 0, sizeof(struct sockaddr_un));
  name.sun_family = AF_UNIX;
  strcpy(name.sun_path, argv[1]);
  len = sizeof(name.sun_family) + strlen(name.sun_path);

  /* connect to the server, wait, if necessary */
  while (1)
    {
      conn_stat = connect(fd, (struct sockaddr *) &name, len);
      if (conn_stat < 0 && errno == ECONNREFUSED)
	{
	  /* exit after the maximum tries */
	  if (++retry > MAX_CONNECT_RETRIES)
	    {
	      exit(1);
	    }

	  /* pause before tring to connect */
	  sleep(SEC_BETWEEN_RETRIES);
	  continue;
	}
      else if (conn_stat < 0 && errno != ECONNREFUSED)
	{
	  exit(1);
	}

      break;
    }

  /* now we want to pass the file descriptor of the modem
   * to the server; we can either pass 0 or 1 since they
   * are both the same
   */
  if (!send_fd(fd, 0))
    {
      exit(1);
    }

  /* the server will send a byte of data when finished */
  FD_ZERO(&rset);
  FD_SET(fd, &rset);

  /* set a 5 min timeout before dieing */
  tv.tv_sec = 300;
  tv.tv_usec = 0;

  select(fd + 1, &rset, NULL, NULL, &tv);
  exit(0);
}


int
send_fd(int fd, int sendfd)
{
  struct msghdr msg;
  struct iovec iov[1];
  struct cmsghdr *cmptr;


  if (!(cmptr = malloc(CONTROLLEN)))
    {
      exit(1);
    }

  iov[0].iov_base     = RANDOM_BYTE;
  iov[0].iov_len      = 1;

  msg.msg_name        = NULL;
  msg.msg_namelen     = 0;
  msg.msg_control     = cmptr;
  msg.msg_controllen  = CONTROLLEN;
  msg.msg_iov         = iov;
  msg.msg_iovlen      = 1;
  msg.msg_flags       = 0;

  cmptr->cmsg_len     = CONTROLLEN;
  cmptr->cmsg_level   = SOL_SOCKET;
  cmptr->cmsg_type    = SCM_RIGHTS;
  
  *((int *) CMSG_DATA(cmptr)) = sendfd;

  if (sendmsg(fd, &msg, 0) <= 0)
    {
      goto error;
    }

  free(cmptr);
  return 1;

 error:
  free(cmptr);
  return 0;
}
