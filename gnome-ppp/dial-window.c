/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <gnome.h>
#include "gnome-ppp.h"
#include "account-window.h"
#include "dial-window.h"
#include "global.h"
#include "misc.h"

/* for zvtterm */
#include <libzvt/libzvt.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkprivate.h>
#include <gdk/gdkkeysyms.h>

#define FONT "-misc-fixed-medium-r-normal--12-200-75-75-c-100-iso8859-1"
#define SCROLLBACK_LINES 50


enum
{
  _DISCONNECTED,
  _IN_PROGRESS,
  _CONNECTED
};


/* dial application context structure */
typedef struct _Dial
{
  GtkWidget *app;
  GtkWidget *account_clist;
  GtkWidget *appbar;
  GtkWidget *connect_button_label;
  GtkWidget *connect;
  GtkWidget *debug_terminal_check_menu_item;
  GtkWidget *term_app;
  GtkWidget *term;

  GList     *active_list;
} Dial;


typedef struct _Active
{
  Dial      *dial;
  Account   *account;
  gint       state;
  gint       pppd_pid;
  gint       ppp_device;
  GtkWidget *input_dialog;
} Active;



static void refresh_dial_windows();
static void refresh_dial_app(Dial *dial);
static void refresh_dial_account_list(Dial *dial);
static void refresh_dial_button(Dial *dial);
static gint close_dial_app(GtkWidget *widget, Dial *dial);


/* app callbacks */
static void account_clist_select_cb(GtkWidget *widget);
static void destroy_dial_app_cb(GtkWidget *widget);
static void connect_button_cb(GtkWidget *widget);


/* dial engine callbacks */
static void connect_message_cb(Account         *account, 
			       PPPMessageType   message,
			       gchar           *text, 
			       gpointer         data);


/* menu callbacks */
static void menu_file_new_dialer_cb(GtkWidget *widget, Dial *dial);
static void menu_file_exit_cb(GtkWidget *widget, Dial *dial);
static void menu_view_debug_terminal_cb(GtkWidget *widget, Dial *dial);
static void menu_account_new_cb(GtkWidget *widget, Dial *dial);
static void menu_account_duplicate_cb(GtkWidget *widget, Dial *dial);
static void menu_account_edit_cb(GtkWidget *widget, Dial *dial);
static void menu_account_delete_cb(GtkWidget *widget, Dial *dial);
static void menu_help_about_cb(GtkWidget *widget, Dial *dial);


/* dial application context */
static Dial *malloc_dial();
static void free_dial(Dial *dial);
static Dial *get_dial_context(GtkObject *object);
static void set_dial_context(GtkObject *object, Dial *dial);
static Account *get_selected_account(Dial *dial);
static Active *get_active_account(Dial *dial, Account *account);
static Active *malloc_active();
static void free_active(Active *active);
static Active *get_active_context(GtkObject *object);
static void set_active_context(GtkObject *object, Active *active);


/* terminal stuff */
static void new_terminal_app(Dial *dial);
static void terminal_show(Dial *dial);
static void terminal_hide(Dial *dial);
static void terminal_write(Dial *dial, gchar *buff);

static void terminal_size_allocate_cb(GtkWidget *widget);
static gint terminal_delete_event_cb(GtkWidget *widget);
static void terminal_menu_close_cb(GtkWidget *widget, Dial *dial);


/* input dialog */
static void input_dialog_destroy_cb(GtkWidget *widget);
static void input_dialog_cb(gchar *string, gpointer data);


/* menus */
static GnomeUIInfo file_menu[] = 
{
  GNOMEUIINFO_MENU_EXIT_ITEM(menu_file_exit_cb, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo view_menu[] =
{
  GNOMEUIINFO_TOGGLEITEM(N_("_Debug Terminal"), NULL, menu_view_debug_terminal_cb, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo account_menu[] = 
{
  GNOMEUIINFO_ITEM_NONE(N_("_New..."), NULL, menu_account_new_cb),
  GNOMEUIINFO_ITEM_NONE(N_("D_uplicate..."), NULL, menu_account_duplicate_cb),
  GNOMEUIINFO_ITEM_NONE(N_("_Edit..."), NULL, menu_account_edit_cb),
  GNOMEUIINFO_ITEM_NONE(N_("_Delete..."), NULL, menu_account_delete_cb),
  GNOMEUIINFO_END
};


static GnomeUIInfo help_menu[] =
{
  GNOMEUIINFO_HELP ("gnome-ppp"),
  GNOMEUIINFO_MENU_ABOUT_ITEM(menu_help_about_cb, NULL),
  GNOMEUIINFO_END
};


/* The menu definitions: File/Exit and Help/About are mandatory */
static GnomeUIInfo main_menu[] =
{
  GNOMEUIINFO_MENU_FILE_TREE(file_menu),
  GNOMEUIINFO_MENU_VIEW_TREE(view_menu),

  { GNOME_APP_UI_SUBTREE, N_("_Account"), NULL, account_menu,
    NULL, NULL, (GnomeUIPixmapType) 0, NULL, 0, (GdkModifierType) 0,
    NULL },

  GNOMEUIINFO_MENU_HELP_TREE(help_menu),

  GNOMEUIINFO_END
};


/* keep track of the number of dial applications */
static GList *__dial_app_list = NULL;


void
new_dial_app()
{
  static gboolean watcher_init = FALSE;
  Dial *dial;
  GtkWidget *vbox, *hbox;

  gchar *titles[] = {
    "Name", "Status", "PID", "Device"
  };


  /* initalize account watcher to update the clist of all dial
   * application windows
   */
  if (!watcher_init)
    {
      watcher_init = TRUE;
      account_add_watcher(refresh_dial_windows);
    }

  dial = malloc_dial();

  /* set up the main application */
  dial->app = gnome_app_new(GNOME_PPP_NAME, _("gnome-ppp"));
  set_dial_context(GTK_OBJECT(dial->app), dial);

  /* menus & status bar are imbedded into the gnome-app */
  gnome_app_create_menus_with_data(GNOME_APP(dial->app), main_menu, dial);
  dial->debug_terminal_check_menu_item = view_menu[0].widget;

  dial->appbar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_USER);
  gnome_app_set_statusbar(GNOME_APP(dial->app), GTK_WIDGET(dial->appbar));

  gtk_signal_connect(
      GTK_OBJECT(dial->app),
      "destroy",
      GTK_SIGNAL_FUNC(destroy_dial_app_cb),
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(dial->app),
      "delete_event",
      GTK_SIGNAL_FUNC(close_dial_app),
      NULL);

  /* hbox */
  hbox =  gtk_hbox_new(FALSE, 0);
  gnome_app_set_contents(GNOME_APP(dial->app), hbox);
  gtk_container_border_width(GTK_CONTAINER(hbox), 3);
  gtk_widget_show(hbox);

  /* boxes for widget layout */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
  gtk_box_set_spacing(GTK_BOX(vbox), 5);
  gtk_widget_show(vbox);

  /* account clist */
  dial->account_clist = gtk_clist_new_with_titles(4, titles);
  set_dial_context(GTK_OBJECT(dial->account_clist), dial);
  gtk_box_pack_start(GTK_BOX(vbox), dial->account_clist, TRUE, TRUE, 0);

  gtk_clist_set_column_auto_resize(GTK_CLIST(dial->account_clist), 0, 1);
  gtk_clist_set_column_auto_resize(GTK_CLIST(dial->account_clist), 1, 1);
  gtk_clist_set_column_auto_resize(GTK_CLIST(dial->account_clist), 2, 1);
  gtk_clist_set_column_auto_resize(GTK_CLIST(dial->account_clist), 3, 1);

  gtk_clist_column_titles_passive(GTK_CLIST(dial->account_clist));
  gtk_clist_set_selection_mode(GTK_CLIST(dial->account_clist), GTK_SELECTION_BROWSE);

  gtk_signal_connect(
      GTK_OBJECT(dial->account_clist),
      "select_row",
      GTK_SIGNAL_FUNC(account_clist_select_cb),
      NULL);

  gtk_widget_show(dial->account_clist);

  /* connect button */
  dial->connect_button_label = gtk_label_new("");
  set_dial_context(GTK_OBJECT(dial->connect_button_label), dial);
  gtk_widget_show(dial->connect_button_label);

  dial->connect = gtk_button_new();
  set_dial_context(GTK_OBJECT(dial->connect), dial);
  gtk_box_pack_start(GTK_BOX(vbox), dial->connect, FALSE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(dial->connect), dial->connect_button_label);
  gtk_widget_set_usize(dial->connect, 100, BUTTON_HEIGHT);

  gtk_signal_connect(
      GTK_OBJECT(dial->connect),
      "clicked",
      GTK_SIGNAL_FUNC(connect_button_cb),
      NULL);

  gtk_widget_show(dial->connect);

  /* create terminal */
  new_terminal_app(dial);

  /* set data in the dial application */
  refresh_dial_account_list(dial);
  refresh_dial_app(dial);

  /* Show it */
  gtk_widget_show(dial->app);
}


void
refresh_dial_windows()
{
  GList *list;
  Dial *dial;

  /* refresh all the dial applications */
  list = __dial_app_list;
  while (list)
    {
      dial = (Dial *) list->data;
      list = list->next;
      refresh_dial_app(dial);
    } 
}


static void
refresh_dial_app(Dial *dial)
{
  refresh_dial_account_list(dial);
  refresh_dial_button(dial);
}


static void
refresh_dial_account_list(Dial *dial)
{
  GList *list;
  Active *active;
  Account *saved_account, *account;


  /* save selected account */
  saved_account = get_selected_account(dial);

  /* refresh the accounts clist */
  gtk_clist_freeze(GTK_CLIST(dial->account_clist));
  gtk_clist_clear(GTK_CLIST(dial->account_clist));

  list = account_list();
  while (list)
    {
      gint row;
      gchar *text[4], pid_buff[32], device_buff[32];

      account = (Account *) list->data;
      list = list->next;

      text[0] = account->name->str;
      text[1] = NULL;
      text[2] = NULL;
      text[3] = NULL;

      /* active is the data structure associated with a actively used account */
      active = get_active_account(dial, account);
      if (active)
	{
	  switch (active->state)
	    {
	    case _IN_PROGRESS:
	      text[1] = _("Dialing");
	      break;
	      
	    case _CONNECTED:
	      text[1] = _("Connected");
	      break;
	      
	    default:
	      break;
	    }

	  if (active->pppd_pid >= 0)
	    {
	      snprintf(pid_buff, 31, "%d", active->pppd_pid);
	      text[2] = pid_buff;
	    }

	  if (active->ppp_device >= 0)
	    {
	      snprintf(device_buff, 31, "ppp%d", active->ppp_device);
	      text[3] = device_buff;
	    } 
	}

      row = gtk_clist_append(GTK_CLIST(dial->account_clist), text);
      if (row < 0)
	{
	  g_error(_("refresh_dial_app(): gtk_clist_append failed."));
	}
      gtk_clist_set_row_data(GTK_CLIST(dial->account_clist), row, account);

      /* select this row if it is the default account */
      if (account == saved_account)
	{
	  gtk_clist_select_row(GTK_CLIST(dial->account_clist), row, 0);
	}
    }

  gtk_clist_thaw(GTK_CLIST(dial->account_clist));
}


static void
refresh_dial_button(Dial *dial)
{
  Active *active;


  /* disable button if there are no accounts */
  if (!account_list())
    {
      gtk_label_set_text(GTK_LABEL(dial->connect_button_label), _("Connect"));
      gtk_widget_set_sensitive(GTK_WIDGET(dial->connect), FALSE);
      return;
    }

  active = get_active_account(dial, get_selected_account(dial));
  if (!active)
    {
      gtk_label_set_text(GTK_LABEL(dial->connect_button_label), _("Connect"));
    }
  else
    {
      /* set the active state of the connection button and the
       * account-selection option menu based on the connection state
       */
      switch (active->state)
	{
	case _IN_PROGRESS:
	  gtk_label_set_text(GTK_LABEL(dial->connect_button_label), _("Cancel"));
	  break;
	  
	case _CONNECTED:
	  gtk_label_set_text(GTK_LABEL(dial->connect_button_label), _("Disconnect"));
	  break;

	default:
	  g_error("refresh_dial_button(): unknown state");
	  break;
	}
    }

  gtk_widget_set_sensitive(GTK_WIDGET(dial->connect), TRUE);
}


static gint
close_dial_app(GtkWidget *widget, Dial *dial)
{
  dial = get_dial_context(GTK_OBJECT(widget));

  /* don't close if the connection is active */
  if (dial->active_list)
    {
      gnome_error_dialog(_("You must disconnect before closing."));
      return TRUE;
    }

  return FALSE;
}


static void
destroy_dial_app_cb(GtkWidget *widget)
{ 
  Dial *dial;

  dial = get_dial_context(GTK_OBJECT(widget));

  gtk_widget_destroy(dial->term_app);
  free_dial(dial);

  /* quit gnome-ppp when there are no moer dial applications */
  if (__dial_app_list == NULL)
    {
      account_remove_watcher(refresh_dial_windows);
      gtk_main_quit();
    }
}


static void
account_clist_select_cb(GtkWidget *widget)
{
  Dial *dial = get_dial_context(GTK_OBJECT(widget));
  refresh_dial_button(dial);
}


static void
connect_button_cb(GtkWidget *widget)
{
  Dial *dial;
  Account *account;
  Active *active;


  dial = get_dial_context(GTK_OBJECT(widget));
  account = get_selected_account(dial);
  if (!account)
    {
      g_error("connect_button_cb(): account == NULL");
    }

  active = get_active_account(dial, account);
  if (!active)
    {
      active = malloc_active();
      active->dial = dial;
      active->account = account;
      dial->active_list = g_list_append(dial->active_list, active);
    }

  switch (active->state)
    {
    case _DISCONNECTED:
      if (connect_start(active->account, connect_message_cb, active))
	{
	  active->pppd_pid = connect_pid(active->account);
	  refresh_dial_account_list(active->dial);
	}
    break;


    case _IN_PROGRESS:
    case _CONNECTED:
      connect_stop(active->account);
    break;
    }
}


/* message callback from connect */
static void
disconnected_cb(Active *active, gchar *text)
{
  Dial *dial = active->dial;


  /* get rid of any remaining input dialog */
  if (active->input_dialog)
    {
      gtk_widget_destroy(active->input_dialog);
    }
  
  /* remove the active account from the dial app's active list */
  dial->active_list = g_list_remove(dial->active_list, active);
  free_active(active);

  refresh_dial_app(dial);
}


static void
in_progress_cb(Active *active, gchar *text)
{
  active->state = _IN_PROGRESS;

  if (text)
    {
      gnome_appbar_set_status(GNOME_APPBAR(active->dial->appbar), _(text));
    }
  else
    {
      gnome_appbar_set_status(GNOME_APPBAR(active->dial->appbar), "");
    }

  refresh_dial_app(active->dial);
}


static void
connected_cb(Active *active, gchar *text)
{
  if (active->state != _CONNECTED)
    {
      active->state = _CONNECTED;
      active->ppp_device = connect_device(active->account);

      if (text)
	{
	  gnome_appbar_set_status(GNOME_APPBAR(active->dial->appbar), _(text));
	}
      else
	{
	  gnome_appbar_set_status(GNOME_APPBAR(active->dial->appbar), "");
	}

      refresh_dial_app(active->dial);
    }
}


static void
error_cb(Active *active, gchar *text)
{
  gnome_error_dialog(_(text));
}


debug_cb(Active *active, gchar *text)
{
  terminal_write(active->dial, _(text));
}


static void
prompt_input_cb(Active *active, gchar *text)
{
  active->input_dialog = 
    gnome_request_dialog(TRUE, _(text), "", 100, (GnomeStringCallback) input_dialog_cb, 
			 active, NULL);
      
  set_active_context(GTK_OBJECT(active->input_dialog), active);

  gtk_signal_connect(
      GTK_OBJECT(active->input_dialog),
      "destroy",
      GTK_SIGNAL_FUNC(input_dialog_destroy_cb),
      NULL);
}


static void
connect_message_cb(Account *account, PPPMessageType message, gchar *text, gpointer data)
{
  Active *active = (Active *) data;
  Dial *dial = active->dial;


  /* map messages from the connect engine to internal states of the dial window */
  switch (message)
    {
    case PPP_DISCONNECTED:
      disconnected_cb(active, text);
      break;

    case PPP_IN_PROGRESS:
      in_progress_cb(active, text);
      break;

    case PPP_CONNECTED:
      connected_cb(active, text);
      break;

    case PPP_ERROR:
      error_cb(active, text);
      break;

    case PPP_DEBUG:
      debug_cb(active, text);
      break;

    case PPP_PROMPT_INPUT:
      prompt_input_cb(active, text);
      break;

    default:
      g_error("connect_message_cb(): unknown callback message");
      break;
    }
}


/* menu callbacks need to be different */
static void
menu_file_exit_cb(GtkWidget *widget, Dial *dial)
{
  g_assert(dial != NULL);

  if (close_dial_app(dial->app, dial) == FALSE)
    {
      gtk_widget_destroy(dial->app);
    }
}


static void
menu_view_debug_terminal_cb(GtkWidget *widget, Dial *dial)
{
  if (GTK_CHECK_MENU_ITEM(widget)->active)
    {
      terminal_show(dial);
    }
  else
    {
      terminal_hide(dial);
    }
}


static void
menu_account_new_cb(GtkWidget *widget, Dial *dial)
{
  g_assert(dial != NULL);
  open_account_window(NULL, FALSE);
}


static void
menu_account_duplicate_cb(GtkWidget *widget, Dial *dial)
{
  Account *account;

  g_assert(dial != NULL);

  if (!(account = get_selected_account(dial)))
    {
      return;
    }
  open_account_window(account, TRUE);
}


static void
menu_account_edit_cb(GtkWidget *widget, Dial *dial)
{
  Account *account;

  g_assert(dial != NULL);

  if (!(account = get_selected_account(dial)))
    {
      return;
    }
  open_account_window(account, FALSE);
}


static void
menu_account_delete_cb(GtkWidget *widget, Dial *dial)
{
  Account     *account;
  Active       *active;
  GtkWidget *Dialogbox;
  
  g_assert(dial != NULL);

  if (!(account = get_selected_account(dial)))
    {
      return;
    }
    
  Dialogbox = gnome_question_dialog
		  (_("Are you sure you want to delete this account?\n"
		     ""), NULL, NULL);
  
  if (!(gnome_dialog_run_and_close (GNOME_DIALOG (Dialogbox))))
    {
      active = get_active_account(dial, account); 
      if (active == NULL)
        {
          if (!account_delete(account))
            {
              gnome_error_dialog(_("The account cannot be deleted."));
              return;
            }
        }
      else
        {
	  gnome_error_dialog(_("The account is in use and cannot be deleted."));
          return;
	}	
    }   
  
  account_save(); 
}


static void
menu_help_about_cb(GtkWidget *widget, Dial *dial)
{
  GtkWidget *about;
  const gchar *author[] = 
  {
    GNOME_PPP_AUTHOR,
    NULL
  };
	
  about = gnome_about_new(
      _(GNOME_PPP_NAME), 
      GNOME_PPP_VERSION,
      _(GNOME_COPYRIGHT),
      _("GNOME Internet Dialer"),
      author,
      NULL,
      NULL,
      NULL);

  gtk_widget_show(about);
}


/* dial application context */
static Dial*
malloc_dial()
{
  Dial *dial = g_malloc(sizeof(Dial));
  dial->term_app = NULL;
  dial->term = NULL;
  dial->active_list = NULL;
  __dial_app_list = g_list_append(__dial_app_list, dial);  
  return dial;
}


static void
free_dial(Dial *dial)
{
  __dial_app_list = g_list_remove(__dial_app_list, dial);
  g_free(dial);
}


static Dial* 
get_dial_context(GtkObject *object)
{
  return gtk_object_get_data(object, "dial_context");
}


static void
set_dial_context(GtkObject *object, Dial *dial)
{
  gtk_object_set_data(object, "dial_context", dial);
}


static Account *
get_selected_account(Dial *dial)
{
  Account *account;

  if (GTK_CLIST(dial->account_clist)->selection)
    {
      return (Account *)
	gtk_clist_get_row_data(
	    GTK_CLIST(dial->account_clist),
	    GPOINTER_TO_INT(GTK_CLIST(dial->account_clist)->selection->data));
    }
  
  return NULL;
}


static Active*
get_active_account(Dial *dial, Account *account)
{
  Active *active;
  GList *list = dial->active_list;
  
  while (list)
    {
      active = list->data;
      list = list->next;
      
      if (active->account == account)
	{
	  return active;
	}
    }

  return NULL;
}


static Active*
malloc_active()
{
  Active *active = g_malloc(sizeof(Active));
  active->dial = NULL;
  active->account = NULL;
  active->state = _DISCONNECTED;
  active->pppd_pid = -1;
  active->ppp_device = -1;
  active->input_dialog = NULL;
  return active;
}


static void
free_active(Active *active) 
{
  g_free(active);
}


static Active* 
get_active_context(GtkObject *object)
{
  return gtk_object_get_data(object, "active_context");
}


static void
set_active_context(GtkObject *object, Active *active)
{
  gtk_object_set_data(object, "active_context", active);
}



/*** terminal ***/
static void
new_terminal_app(Dial *dial)
{
  GtkWidget *hbox, *scrollbar;

  dial->term_app = gnome_app_new(GNOME_PPP_NAME, "gnome-ppp");
  set_dial_context(GTK_OBJECT(dial->term_app), dial);

  gtk_signal_connect(
      GTK_OBJECT(dial->term_app),
      "delete_event",
      GTK_SIGNAL_FUNC(terminal_delete_event_cb),
      NULL);

  /* create hbox */
  hbox = gtk_hbox_new(FALSE, 0);
  gnome_app_set_contents(GNOME_APP(dial->term_app), hbox);
  gtk_box_set_spacing(GTK_BOX(hbox), 2);
  gtk_container_border_width(GTK_CONTAINER(hbox), 2);
  gtk_widget_show(hbox);

  /* create terminal */
  dial->term = zvt_term_new();
  set_dial_context(GTK_OBJECT(dial->term), dial);
  gtk_box_pack_start(GTK_BOX(hbox), dial->term, 1, 1, 0);
  zvt_term_set_shadow_type(ZVT_TERM(dial->term), GTK_SHADOW_IN);
  zvt_term_set_size(ZVT_TERM(dial->term), 80, 25);
  zvt_term_set_font_name(ZVT_TERM(dial->term), FONT);
  zvt_term_set_blink(ZVT_TERM(dial->term), FALSE);
  zvt_term_set_bell(ZVT_TERM(dial->term), TRUE);
  zvt_term_set_scrollback(ZVT_TERM(dial->term), SCROLLBACK_LINES);
  zvt_term_set_scroll_on_keystroke(ZVT_TERM(dial->term), TRUE);
  zvt_term_set_scroll_on_output(ZVT_TERM(dial->term), FALSE);
  zvt_term_set_background(ZVT_TERM(dial->term), NULL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(dial->term),
      "key_press_event",
      GTK_SIGNAL_FUNC(NULL),
      NULL);

  gtk_signal_connect_after(
      GTK_OBJECT(dial->term),
      "size_allocate",
      GTK_SIGNAL_FUNC(terminal_size_allocate_cb),
      NULL);

  gtk_widget_show(dial->term);

  /* scrollbar */
  scrollbar = 
    gtk_vscrollbar_new(GTK_ADJUSTMENT(ZVT_TERM(dial->term)->adjustment));
  GTK_WIDGET_UNSET_FLAGS(scrollbar, GTK_CAN_FOCUS);
  gtk_box_pack_start(GTK_BOX(hbox), scrollbar, FALSE, TRUE, 0);
  gtk_widget_show(scrollbar);

  /* realize the GNOME terminal app */
  gtk_widget_realize(dial->term_app);
}


static void
terminal_show(Dial *dial)
{
  gtk_widget_show(dial->term_app);
}


static void
terminal_hide(Dial *dial)
{
  gtk_widget_hide(dial->term_app);
}


static void
terminal_write(Dial *dial, gchar *buff)
{
  zvt_term_feed(ZVT_TERM(dial->term), buff, strlen(buff));
}


static void
terminal_size_allocate_cb(GtkWidget *widget)
{
  ZvtTerm *term;
  XSizeHints sizehints;
  Dial *dial;

  dial = get_dial_context(GTK_OBJECT(widget));
  term = ZVT_TERM(widget);
  
  sizehints.base_width = 
    (GTK_WIDGET(dial->term_app)->allocation.width) +
    (GTK_WIDGET(term)->style->xthickness * 2) -
    (GTK_WIDGET(term)->allocation.width);
  
  sizehints.base_height =
    (GTK_WIDGET(dial->term_app)->allocation.height) +
    (GTK_WIDGET(term)->style->ythickness * 2) -
    (GTK_WIDGET(term)->allocation.height);
  
  sizehints.width_inc = term->charwidth;
  sizehints.height_inc = term->charheight;
  sizehints.min_width = sizehints.base_width + sizehints.width_inc;
  sizehints.min_height = sizehints.base_height + sizehints.height_inc;
  
  sizehints.flags = (PBaseSize|PMinSize|PResizeInc);
  
  XSetWMNormalHints(GDK_DISPLAY(),
		    GDK_WINDOW_XWINDOW (GTK_WIDGET(dial->term_app)->window),
		    &sizehints);
  gdk_flush();
}


static gint
terminal_delete_event_cb(GtkWidget *widget)
{
  Dial *dial;

  dial = get_dial_context(GTK_OBJECT(widget));
  terminal_hide(dial);

  /* update the view/debug window check menu item */
  gtk_check_menu_item_set_active(
      GTK_CHECK_MENU_ITEM(dial->debug_terminal_check_menu_item),
      FALSE);

  return TRUE;
}


static void
terminal_menu_close_cb(GtkWidget *widget, Dial *dial)
{
  terminal_hide(dial);
}


/*** input dialog ***/
static void
input_dialog_destroy_cb(GtkWidget *widget)
{
  Active *active = get_active_context(GTK_OBJECT(widget));
  active->input_dialog = NULL;
}


static void 
input_dialog_cb(gchar *string, gpointer data)
{
  Active *active = (Active *) data;
  connect_send(active->account, string, TRUE);
}
