/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __connect_h__
#define __connect_h__

#include <gnome.h>
#include "modem.h"

typedef struct _Connect
{
  Account *orig_account, *account;

  ConnectCBFunc func;
  gpointer data;
  gint pppd_pid;
  gint ppp_device;

  Modem *modem;
  GString *modem_buffer;
  time_t timeout;
  time_t connect_time;

  gint hanup_attempts;

  /* state keepers */
  gint state;
  gint get_state;
  gint detect_state;
  gint adjust_speaker_state;
  gint dial_state;
  gint script_state;
  gint pppd_state;

  /* keep track of how many dial attempts there has been */
  gint dial_attempts;

  /* dial phone number list node */
  GList *phone_number_node;

  /* current script entry */
  GList *script_entry_node;
  gboolean send_buffer_set;
  GString *send_buffer;

  gboolean close;
  gboolean dead;
  gboolean user_closed;
} Connect;


void connect_inform_pppd_died(Connect *connect);

#endif /* __connect_h__ */
