/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __pppd_h__
#define __pppd_h__

#include <gnome.h>
#include <signal.h>
#include "gnome-ppp.h"
#include "connect.h"


enum
{
  PPPD_OK,
  PPPD_LOOKUP_FAIL,
  PPPD_PENDING,
  PPPD_MODEM_LOCKED,
  PPPD_NO_EXEC_PERMISSION,
};


extern gint pppd_errno;


gint          pppd_exec(Connect *connect, Account *account);
gint          pppd_modem_fd(Account *account, int *modem_fd);
gint          pppd_end_chat(Account *account);
gint          pppd_device_number(Account *account, int *device_number);
pid_t         pppd_pid(Account *account);
gint          pppd_kill(Account *account);
void          pppd_sigchld(pid_t pid, int status);

#endif /* __pppd_h__ */
