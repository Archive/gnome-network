/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <unistd.h>
#include <gnome.h>
#include "program.h"
#include "sighndl.h"
#include "pppd.h"


static pid_t fork_program(Account *account, gchar *path);


gint
program_exec(Account *account, gchar *path)
{
  pid_t pid;

  g_assert(account != NULL);
  g_assert(path != NULL);

  /* check for execute permission */
  if (access(path, X_OK) < 0)
    {
      return 0;
    }
  
  pid = fork_program(account, path);
  if (pid <= 0)
    {
      return 0;
    }

  return 1;
}


static pid_t
fork_program(Account *account, gchar *path)
{
  pid_t pid;

  pid = fork();
  if (pid == 0)
    {
      int fd, arg_index;
      char *args[2];
      GString *gstr;

      gstr = g_string_new("");

      /* args */
      arg_index = 0;
      args[arg_index++] = path;
      args[arg_index] = NULL;

      /* environment */
      g_string_sprintf(gstr, "ACCOUNT_NAME=%s", account->name->str);
      putenv(gstr->str);

      g_string_sprintf(gstr, "PPPD_PID=%d", pppd_pid(account));
      putenv(gstr->str);

      /* become head of process group */
      if (setpgrp() == -1)
	{
	  _exit(0);
	}

      /* close all file descriptors */
      for (fd = 0; fd <= 64; fd++)
	{
	  close(fd);
	}

      /* if the execv fails, exit with _exit so no
       * atexit handlers get called
       */
      execv(path, args);
      _exit(0);
    }

  return pid;
}


