/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __ppp_misc_h__
#define __ppp_misc_h__

#include <glib.h>

gint      g_string_find(GString *string, gchar *find, gboolean case_insensitive);
GList    *j_read_lines(gchar *path);
gboolean  j_write_lines(gchar *path, GList *line_list);
void      j_free_lines(GList *line_list);

#endif /* __ppp_misc_h__ */
