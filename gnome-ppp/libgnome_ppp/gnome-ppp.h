/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __gnome_ppp_h__
#define __gnome_ppp_h__

#include <gnome.h>


/* enumerated types */
typedef enum
{
  SCRIPT_ENTRY_EXPECT,
  SCRIPT_ENTRY_SEND,
  SCRIPT_ENTRY_SEND_USER,
  SCRIPT_ENTRY_SEND_PASSWORD,
  SCRIPT_ENTRY_PROMPT_INPUT
}
ScriptEntryType;

typedef enum
{
  PPP_DISCONNECTED,
  PPP_IN_PROGRESS,
  PPP_CONNECTED,
  PPP_ERROR,
  PPP_DEBUG,
  PPP_PROMPT_INPUT
} PPPMessageType;


/* structures */
typedef struct _ScriptEntry ScriptEntry;
typedef struct _Account Account;


struct _ScriptEntry
{
  ScriptEntryType type;
  GString *text;
};

struct _Account 
{
  GString *name;             /* account name */
  gint redial_attempts;      /* number of times to redial before failing */
  gint dial_timeout;         /* number of seconds until next redial */
  GList *phone_list;         /* phone number list */

  /* for scripting, PAP, CHAP and other uses */
  GString *user;
  GString *passwd;
  GString *remotename;

  /* for static IP's */
  GString *ip;
  GString *remote;
  GString *mask;
  
  /* DNS servers and search domains
   * list data type: string
   */
  GList *dns_list;
  GList *domain_list;

  /* the list of script entries forming the login script
   * list data type: ScriptEntry structures
   */
  GList *script_list;

  /* system settings */
  gboolean default_route;
  gboolean lock_device;
  gint mtu;
  gint mru;
  GString *connect_program;
  GString *disconnect_program;

  /* modem general settings */
  GString *modem_device;
  GString *modem_flow;
  GString *modem_term;
  gint modem_volume;
  gint speed;
 
  /* modem command settings */
  GString *modem_init;
  GString *modem_dial;
  GString *modem_hang;
  GString *modem_mute;
  GString *modem_low;
  GString *modem_high;
  
  /* modem response settings */
  GString *init_resp;
  GString *hang_resp;
  GString *connect_resp;
  GString *busy_resp;
  GString *noanswer_resp;
  GString *nocarrier_resp;
  GString *nodialtone_resp;

  /* PRIVATE */
  gboolean locked;
};


/* typedefs */
typedef void (*AccountWatcherCBFunc)();
typedef void (*ConnectCBFunc)(Account         *account, 
			      PPPMessageType   message,
			      gchar           *text,
			      gpointer         data);


/* initalize library -- a must before using! */
void gnomeppplib_init();


/* save/restore accounts */
void          account_load();
void          account_save();

/* list of all accounts */
GList        *account_list();

/* account locking */
gint          account_is_locked(Account *account);
gint          account_lock(Account *account);
void          account_unlock(Account *account);

/* add watcher functions for changes/updates to accounts */
void          account_add_watcher(AccountWatcherCBFunc func);
void          account_remove_watcher(AccountWatcherCBFunc func);
void          account_broadcast_watchers();

/* allocate account structures */
Account      *account_new();
gint          account_free(Account *account);
Account      *account_clone(Account *account);

/* add/remove accounts from the account list */
void          account_add(Account *account);
gint          account_delete(Account *account);

/* change settings/information in a account */
void          account_phone_list_clear(Account *account);
void          account_phone_list_append(Account *account, gchar *str);
void          account_dns_list_clear(Account *account);
void          account_dns_list_append(Account *account, gchar *str);
void          account_domain_list_clear(Account *account);
void          account_domain_list_append(Account *account, gchar *str);
void          account_script_list_clear(Account *account);
void          account_script_list_append(Account *account, 
					 ScriptEntryType type, 
					 gchar *text);

/* begin a connection, messages about this connection are
 * sent back though the callback
 */
gint          connect_start(Account *account, ConnectCBFunc func, gpointer data);

/* this must be called into at least once every second during the entire
 * connection; I call into it once every 1/2 second; returns 0 when
 * dead
 */
gint          connect_engine_iteration(Account *account);

/* signal the end of the connection; it's not reall gone until the
 * callback signals that the connection has ended
 */
void          connect_stop(Account *account);

/* send output after the connect engine prompts for user input */
void          connect_send(Account *account, gchar *text, gboolean append_cr);

/* get the pppd pid of the connection */
gint          connect_pid(Account *account);

/* get the pppX device number of the connection */
gint          connect_device(Account *account);


#endif /* __gnome_ppp_h__ */
