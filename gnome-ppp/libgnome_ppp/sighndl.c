/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <signal.h>
#include "pppd.h"


static struct sigaction         sact;
static gint                     block_sigchld_refcount = 0;


static void handle_sigchld(int signum);


void
__init_sighndl()
{
  static gboolean done_init = FALSE;

  if (!done_init)
    {
      done_init = TRUE;

      /* catch SIGCHLD to detect dead PPP connections */
      sact.sa_handler = handle_sigchld;
      sigemptyset(&sact.sa_mask);
      sigaddset(&sact.sa_mask, SIGCHLD);
      sigaction(SIGCHLD, &sact, (struct sigaction *) NULL);

    }
}


void
block_sigchld()
{
  block_sigchld_refcount++;

  g_assert(block_sigchld_refcount > 0);

  if (block_sigchld_refcount == 1)
    {
      sigprocmask(SIG_BLOCK, &sact.sa_mask, (sigset_t *) NULL);
    }
}


void
unblock_sigchld()
{
  block_sigchld_refcount--;

  g_assert(block_sigchld_refcount >= 0);

  if (block_sigchld_refcount == 0)
    {
      sigprocmask(SIG_UNBLOCK, &sact.sa_mask, (sigset_t *) NULL);
    }
}



static void 
handle_sigchld(int signum)
{
  pid_t pid;
  int status;

  g_assert(block_sigchld_refcount == 0);

  pid = wait(&status);
  pppd_sigchld(pid, status);

  /* reset the signal */
  sigaction(SIGCHLD, &sact, (struct sigaction *) NULL);
}

