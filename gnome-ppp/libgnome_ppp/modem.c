/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * Based loosely on WvDial
 * Copyright (C) 1997, 1998 Worldvisions Computer Technology, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <time.h>
#include <assert.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/errno.h>

#include "gnome-ppp.h"
#include "modem.h"


struct _SpeedLookup 
{
  int baud;
  speed_t speedt;
} speeds[] = {
  {230400, B230400},
  {115200, B115200},
  { 57600, B57600},
  { 38400, B38400},
  { 19200, B19200},
  {  9600, B9600},
  {  4800, B4800},
  {  2400, B2400},
  {  1200, B1200},
  {   300, B300}
};



/* private prototypes */
static gboolean modem_setup(Modem *modem, char *term);
static int      modem_get_status(Modem *modem);
static gboolean modem_select(Modem *modem, gboolean readable, 
			     gboolean writable, gboolean isexception);


Modem*
modem_new(int fd, gint baud, char *term)
{
  int flags;
  Modem *modem;

  g_assert(fd >= 0);

  modem = g_malloc(sizeof(Modem));
  modem->fd = fd;
  modem->baud = baud;

  if (!modem_setup(modem, term))
    {
      goto error;
    }

  return modem;


 error:
  if (modem->fd >= 0)
    {
      close(modem->fd);
    }
  g_free(modem);

  return NULL;
}


void
modem_destroy(Modem *modem)
{
  tcsetattr(modem->fd, TCSANOW, &modem->old_tios);
  tcflush(modem->fd, TCIOFLUSH);
  close(modem->fd);

  /* free memory */
  g_free(modem);
}


void
modem_hangup(Modem * modem, Account * account)
{
  int i, oldbaud = modem->baud;
  GString *gstr = g_string_new("");

  /* politely abort any dial in progress, to avoid locking USR modems.
   * we should only do this if we have received any response from the modem,
   * so that WvModemScan can run faster.
   */
  modem_drain(modem);

  modem_write(modem, account->modem_term->str, 1);
  for (i = 0; !modem_select(modem, TRUE, FALSE, FALSE) && i < 10; i++)
    {
      modem_write(modem, account->modem_term->str, 1);
    }

  modem_drain(modem);
  
  /* drop DTR for a while, if we're still online */
  if (modem_carrier(modem))
    {
      cfsetospeed(&modem->tios, B0);
      tcsetattr(modem->fd, TCSANOW, &modem->tios);
      for (i = 0; modem_carrier(modem) && i < 10; i++)
	{
	  usleep(100 * 1000);
	}

      /* raise DTR again, restoring the old baud rate */
      modem_set_speed(modem, oldbaud);
    }
  
  if (modem_carrier(modem))
    {
      /* need to do +++ manual-disconnect stuff */
      g_string_sprintf(gstr, "%s%s", account->modem_hang->str,
                       account->modem_term->str);
      modem_write(modem, gstr->str, gstr->len); 
      
      for (i = 0; modem_carrier(modem) && i < 5; i++)
	{
	  usleep(100 * 1000);
	}
    }
}


int
modem_write(Modem * modem, char * buff, int len)
{
  int n;

  n = write(modem->fd, buff, len);
  if (n < len)
    {
      g_message("modem_write short write\n");
    }

  return n;
}


int
modem_read(Modem * modem, char * buff, int len)
{
  return read(modem->fd, buff, len);
}


int
modem_read_ready(Modem * modem)
{
  return modem_select(modem, TRUE, FALSE, FALSE);
}


void 
modem_drain(Modem * modem)
{
  int len;
  char buff[1024];

  while (modem_select(modem, TRUE, FALSE, FALSE))
    {
      len = modem_read(modem, buff, 1024);
    }
}


void 
modem_set_speed(Modem * modem, int baud)
{
  int i;
  speed_t s = B0;
  
  modem->baud = 0;
  for (i = 0; i < sizeof(speeds) / sizeof(*speeds); i++)
    {
      if (speeds[i].baud <= baud)
	{
	  s = speeds[i].speedt;
	  modem->baud = speeds[i].baud;
	  break;
	}
    }
  
  /* auto-match to output speed */
  cfsetispeed(&modem->tios, B0);
  cfsetospeed(&modem->tios, s);
  tcsetattr(modem->fd, TCSANOW, &modem->tios);
}


int
modem_carrier(Modem * modem)
{
  return(modem_get_status(modem) & TIOCM_CD) ? 1 : 0;
}


/* PRIVATE */
static gboolean
modem_setup(Modem * modem, char * term)
{
  int i;
 
  if (tcgetattr(modem->fd, &modem->tios ) ||
      tcgetattr(modem->fd, &modem->old_tios))
    {
      return FALSE;
    }
  
  modem_drain(modem);
  
  /* set up the terminal characteristics */
  modem->tios.c_iflag &= ~(BRKINT|ISTRIP|IUCLC|IXON|IXANY|IXOFF|IMAXBEL);
  modem->tios.c_iflag |= (IGNBRK|IGNPAR);
  modem->tios.c_oflag &= ~(OLCUC);
  modem->tios.c_cflag &= ~(CSIZE|CSTOPB|PARENB|PARODD);
  modem->tios.c_cflag |= (CS8|CREAD|HUPCL|CRTSCTS|CLOCAL);
  modem->tios.c_lflag &= ~(ISIG|XCASE|ECHO);
  tcsetattr(modem->fd, TCSANOW, &modem->tios);
  
  /* make sure we leave the modem in CLOCAL when we exit, so normal user
   * tasks can open the modem without using nonblocking
   */
  modem->old_tios.c_cflag |= CLOCAL;
  
  /* Send a few returns to make sure the modem is "good and zonked" */
  if (cfgetospeed(&modem->tios) != B0)
    {
      for(i = 0; i < 5; i++)
	{
	  modem_write(modem, term, strlen(term));
	  usleep(10 * 1000);
	}
    }
  
  /* Set the baud rate to 0 for half a second to drop DTR */
  cfsetispeed(&modem->tios, B0);
  cfsetospeed(&modem->tios, B0);
  cfmakeraw(&modem->tios);
  tcsetattr(modem->fd, TCSANOW, &modem->tios );

  if (modem_carrier(modem))
    {
      usleep(500 * 1000);
    }

  modem_set_speed(modem, modem->baud);
  usleep(10 * 1000);
  modem_drain(modem);

  return TRUE;
}


static int
modem_get_status(Modem * modem)
{
  int status = 0;
  
  ioctl(modem->fd, TIOCMGET, &status);
  return status;
}


static gboolean
modem_select(Modem * modem, gboolean readable, gboolean writable, gboolean isexception)
{
  int maxfd, nreads;
  fd_set rset, wset, xset;
  struct timeval tv;
  
  if (!readable && !writable && !isexception)
    {
      return FALSE;
    }

  tv.tv_sec = 0;
  tv.tv_usec = 200;

  maxfd = modem->fd;
  FD_ZERO(&rset);
  FD_ZERO(&wset);
  FD_ZERO(&xset);
  
  if (readable)
    {
      FD_SET(maxfd, &rset);
    }

  if (writable)
    {
      FD_SET(maxfd, &wset);
    }

  if (isexception)
    {
      FD_SET(maxfd, &xset);
    }

  nreads = select(maxfd + 1, &rset, &wset, &xset, &tv);
  if (nreads < 0)
    {
      return FALSE;
    }

  if (nreads == 0)
    {
      return FALSE;
    }

  return TRUE;
}
