/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __msg_h__
#define __msg_h__

#include <gnome.h>

/* message ids for text messages, these have to be kept synchronized with
 * the text message table they refer to in msg.c
 */
enum
{
  MSG_IN_USE,
  MSG_BEGIN,
  MSG_NO_PPPD_EXEC_PERMISSION,
  MSG_MODEM_LOCKED,
  MSG_PPPD_FORK,
  MSG_CLOSING,
  MSG_MODEM_FD_PASS_TIMEOUT,
  MSG_MODEM_HANUP_FAILED,
  MSG_MODEM_DETECT,
  MSG_MODEM_DETECT_FAILED,
  MSG_SPEAKER_ADJUST_FAILED,
  MSG_MAX_REDIAL,
  MSG_NO_PHONE_NUMBER,
  MSG_DIALING,
  MSG_DIAL_TIMEOUT,
  MSG_NO_DIALTONE,
  MSG_BUSY,
  MSG_NO_ANSWER,
  MSG_NO_CONNECT,
  MSG_CONNECTED,
  MSG_NO_CARRIOR,
  MSG_RUNNING_SCRIPT,
  MSG_SCRIPT_COMPLETE,
  MSG_PPP_CONNECTED,
  MSG_PPP_FAIL,
  MSG_PPP_UNKNOWN_DEATH,
  MSG_LAST_MESSAGE
};

gchar *msg(gint id);

#endif /* __msg_h__ */
