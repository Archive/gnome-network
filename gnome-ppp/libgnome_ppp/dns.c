/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <stdio.h>
#include <string.h>

#include "config.h"

#include "gnome-ppp.h"
#include "dns.h"
#include "misc.h"


#define RESOLV_CONF_PATH      "/etc/resolv.conf"


void
dns_add_entries(Account *account, pid_t pid)
{
  GList *list, *line_list;
  GString *gstr, *line, *search_line, *dns_line;

  /* check for write permission to resolv.conf, return
   * if we don't have it
   */
  if (access(RESOLV_CONF_PATH, W_OK) < 0)
    {
      return;
    }

  /* utility gstrings */
  gstr = g_string_new("");

  /* read the resolv.conf file
   * FIXME: should flock the file before reading, and unlock
   *        it after writing
   */
  line_list = j_read_lines(RESOLV_CONF_PATH);

  /* DNS servers */
  if (g_list_length(account->dns_list) > 0)
    {
      /* start from the end of the DNS list and work
       * towards the beginning so the search ends up in
       * order
       */
      list = g_list_last(account->dns_list);
      while (list)
	{
	  line = (GString *) list->data;
	  list = list->prev;

	  dns_line = g_string_new("");
	  g_string_sprintf(
              dns_line,
	      "nameserver %s # GNOME-PPP TEMP ENTRY PID: %d\n",
	      line->str,
	      pid);

	  /* append it to the line_list to be written */
	  line_list = g_list_prepend(line_list, dns_line);
	}
    }

  /* DNS search domains */
  if (g_list_length(account->domain_list) > 0)
    {
      search_line = g_string_new("search");

      list = account->domain_list;
      while (list)
	{
	  line = (GString *) list->data;
	  list = list->next;

	  g_string_append_c(search_line, ' ');
	  g_string_append(search_line, line->str);
	}

      g_string_sprintf(gstr, " # GNOME-PPP TEMP ENTRY PID: %d\n", pid);
      g_string_append(search_line, gstr->str);
      
      /* append it to the line_list to be written */
      line_list = g_list_prepend(line_list, search_line);
    }

  /* write the new resolv.conf file and free the line list */
  j_write_lines(RESOLV_CONF_PATH, line_list);
  j_free_lines(line_list);
  g_string_free(gstr, TRUE);
}


void
dns_remove_entries(Account *account, pid_t pid)
{
  GList *list, *old_line_list, *new_line_list;
  GString *search, *line;

  /* check for write permission to resolv.conf, return
   * if we don't have it
   */
  if (access(RESOLV_CONF_PATH, W_OK) < 0)
    {
      return;
    }

  /* create the search string */
  search = g_string_new(" # GNOME-PPP TEMP ENTRY PID:");
  /* g_string_sprintf(search, " # GNOME-PPP TEMP ENTRY PID: %d\n", pid); */

  /* read the resolv.conf file
   * FIXME: should flock the file before reading, and unlock
   *        it after writing
   */
  old_line_list = j_read_lines(RESOLV_CONF_PATH);
  new_line_list = NULL;

  /* iterate through all the lines in the resolv.conf file, freeing
   * the temporary entries and adding the perminate ones to
   * new_line_list to be written back to the resolv.conf file
   */
  list = old_line_list;
  while (list)
    {
      line = (GString *) list->data;
      list = list->next;

      if (g_string_find(line, search->str, FALSE) < 0)
	{
	  new_line_list = g_list_append(new_line_list, line);
	}
      else
	{
	  g_string_free(line, TRUE);
	}
    }

  /* free the old list */
  g_list_free(old_line_list);
  
  /* write the new resolv.conf file and free the line list */
  j_write_lines(RESOLV_CONF_PATH, new_line_list);
  j_free_lines(new_line_list);
  g_string_free(search, TRUE);
}
