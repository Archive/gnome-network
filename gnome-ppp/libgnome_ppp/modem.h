/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __modem_h__
#define __modem_h__

#include <glib.h>
#include <time.h>
#include <termios.h>

typedef struct _Modem Modem;
struct _Modem
{
  int             fd;
  struct termios  tios, old_tios;
  int		  baud;
};

Modem   *modem_new(int fd, gint baud, char *term);
void     modem_destroy(Modem *modem);
void     modem_hangup(Modem *modem, Account *account);
int      modem_write(Modem *modem, char *buff, int len);
int      modem_read(Modem *modem, char *buff, int len);
int      modem_read_ready(Modem *modem);
void     modem_drain(Modem *modem);
void     modem_set_speed(Modem *modem, int baud);
int      modem_carrier(Modem *modem);

#endif /* __modem_h__ */
