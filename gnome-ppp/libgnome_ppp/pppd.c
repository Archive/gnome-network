/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>

#include "config.h"
#include "misc.h"
#include "pppd.h"
#include "dns.h"


#if HAVE_SOCKETBITS_H
#include <socketbits.h>
#include <iovec.h>
#endif


#ifndef CMSG_DATA /* Linux libc5 */
/* Ancillary data object manipulation macros.  */
#if !defined __STRICT_ANSI__ && defined __GNUC__ && __GNUC__ >= 2
# define CMSG_DATA(cmsg) ((cmsg)->cmsg_data)
#else
# define CMSG_DATA(cmsg) ((unsigned char *) ((struct cmsghdr *) (cmsg) + 1))
#endif
#endif /* CMSG_DATA */

#ifndef CMSG_FIRSTHDR /* Linux libc5 */
#define CMSG_FIRSTHDR(mhdr) ((size_t) (mhdr)->msg_controllen >= sizeof(struct cmsghdr) ? (struct cmsghdr *) (mhdr)->msg_control : (struct cmsghdr *) NULL)
#endif /* CMSG_FIRSTHDR */

#define CONTROLLEN (sizeof(struct cmsghdr) + sizeof(int))


#define ISDIGIT(c)            (((c) >= '0') && ((c) <= '9'))
#define CHARTODIGIT(c)        ((c) - '0')


/* constants */
#define PPPD_PATH             "/usr/sbin/pppd"
#define PPPD_PID_PATH         "/var/run"
#define MAX_ARGS              25



typedef struct _PPP
{
  Connect    *connect;
  Account    *account;
  pid_t       pid;
  gint        device_number;

  /* domain socket */
  GString    *socket_path;
  gint        socket_fd;
  gint        client_fd;
} PPP;


static PPP*
malloc_ppp()
{
  PPP *ppp = g_malloc(sizeof(PPP));
  ppp->connect = NULL;
  ppp->account = NULL;
  ppp->pid = 0;
  ppp->device_number = -1;
  ppp->socket_path = g_string_new("");
  ppp->socket_fd = -1;
  ppp->client_fd = -1;
  return ppp;
}


static void
free_ppp(PPP *ppp)
{
  g_assert(ppp != NULL);
  g_string_free(ppp->socket_path, TRUE);
  g_free(ppp);
}


/* static globals */
gint                            pppd_errno;
static GHashTable              *__ppp_hash = NULL;
static GHashTable              *__ppp_pid_hash = NULL;


/* prototypes */
static pid_t fork_pppd(PPP *ppp);
static gint  check_device_lock(gchar *device_name);
static gint  check_pid_file(PPP *ppp);
static pid_t get_pid(gint device_number);
static gint  is_pid_filename(gchar *filename);
static int   recieve_fd(int fd, int *recvfd);



void
__init_ppp()
{
  static gboolean done_init = FALSE;

  if (!done_init)
    {
      done_init = TRUE;

      /* map Account structures to PPP structures */
      __ppp_hash = g_hash_table_new(g_direct_hash, NULL);

      /* map pppd pids to PPP structures */
      __ppp_pid_hash = g_hash_table_new(g_int_hash, g_int_equal);
    }
}


gint
pppd_exec(Connect *connect, Account *account)
{
  int len;
  struct sockaddr_un name;
  PPP *ppp;

  ppp = NULL;

  g_assert(account != NULL);
  block_sigchld();
  if (g_hash_table_lookup(__ppp_hash, account))
    {
      g_error("pppd_exec(): account connection in progress");
    }

  /* check to see if the process has permission to
   * execute the pppd daemon
   */
  if (access(PPPD_PATH, X_OK) < 0)
    {
      pppd_errno = PPPD_NO_EXEC_PERMISSION; 
      goto error;
    }

  /* check to see if the dialout device is locked */
  if (check_device_lock(account->modem_device->str))
    {
      pppd_errno = PPPD_MODEM_LOCKED;
      goto error;
    }

  ppp = malloc_ppp();
  ppp->connect = connect;
  ppp->account = account;

  g_hash_table_insert(__ppp_hash, account, ppp);

  /* create unique socket path (address) */
  g_string_sprintf(ppp->socket_path, "/tmp/gnome-ppp.%d.XXXXXX", getpid());
  if (mkstemp(ppp->socket_path->str) == -1)
    {
      g_error("pppd_exec(): mkstemp");
    }

  unlink(ppp->socket_path->str);

  memset(&name, 0, sizeof(struct sockaddr_un));
  name.sun_family = AF_UNIX;
  strcpy(name.sun_path, ppp->socket_path->str);
  len = sizeof(name.sun_family) + strlen(name.sun_path);

  if ((ppp->socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
      g_error("pppd_exec(): socket");
    }

  if (bind(ppp->socket_fd, (struct sockaddr *) &name, len) < 0)
    {
      g_error("pppd_exec(): bind");
    }

  /* fork/exec pppd
   * set the child process ID and insert the ppp
   * structure into the process id -> ppp strcture
   * lookup hash
   */
  ppp->pid = fork_pppd(ppp);
  if (ppp->pid <= 0)
    {
      g_error("pppd_exec(): fork_pppd <= 0");
    }
  g_hash_table_insert(__ppp_pid_hash, &ppp->pid, ppp);
  unblock_sigchld();
  return 1;

 error:
  if (ppp)
    {
      /* close the domain socket fd */
      if (ppp->socket_fd >= 0)
	{
	  close(ppp->socket_fd);
	}
      
      /* unlink the domain socket */
      if (ppp->socket_path->len > 0)
	{
	  unlink(ppp->socket_path->str);
	}
      
      g_hash_table_remove(__ppp_hash, account);
      g_free(ppp);
    }
  unblock_sigchld();
  return 0;
}


gint
pppd_modem_fd(Account *account, int *modem_fd)
{
  fd_set rfds;
  struct timeval tv;
  PPP *ppp;

  g_assert(account != NULL);
  block_sigchld();
  ppp = g_hash_table_lookup(__ppp_hash, account);
  if (!ppp)
    {
      pppd_errno = PPPD_LOOKUP_FAIL;
      unblock_sigchld();
      return 0;
    }

  /* set to invalid file descriptor */
  *modem_fd = -1;

  FD_ZERO(&rfds);
  FD_SET(ppp->socket_fd, &rfds);
  tv.tv_sec = 0;
  tv.tv_usec = 200;

  if (select(ppp->socket_fd + 1, &rfds, NULL, NULL, &tv) > 0)
    {
      int len;
      struct sockaddr_un name;

      memset(&name, 0, sizeof(struct sockaddr_un));
      name.sun_family = AF_UNIX;
      strcpy(name.sun_path, ppp->socket_path->str);
      len = sizeof(name.sun_family) + strlen(name.sun_path);
      
      if (listen(ppp->socket_fd, 5) < 0)
	{
	  g_error("pppd_modem_fd(): listen");
	}
      
      ppp->client_fd = accept(ppp->socket_fd, (struct sockaddr *) &name, &len);
      if (ppp->client_fd < 0)
	{
	  g_error("pppd_modem_fd(): accept");
	}
      
      if (!recieve_fd(ppp->client_fd, modem_fd))
	{
	  g_error("pppd_modem_fd(): recieve_fd");
	}
    }
  else
    {
      pppd_errno = PPPD_PENDING;
      goto error;
    }
      
  unblock_sigchld();
  return 1;

 error:
  unblock_sigchld();
  return 0;
}


gint
pppd_end_chat(Account *account) 
{
  PPP *ppp;

  g_assert(account != NULL);
  block_sigchld();
  ppp = g_hash_table_lookup(__ppp_hash, account);
  if (!ppp) 
    {
      pppd_errno = PPPD_LOOKUP_FAIL;
      unblock_sigchld();
      return 0;
    }

  /* signal gnome-ppp-chat to shut down */
  if (ppp->client_fd >= 0)
    {
      send(ppp->client_fd, "Z", 1, 0);
      close(ppp->client_fd);
      ppp->client_fd = -1;
    }

  unblock_sigchld();
  return 1;
}


gint
pppd_device_number(Account *account, gint *device_number)
{
  PPP *ppp;

  g_assert(account != NULL);
  block_sigchld();
  ppp = g_hash_table_lookup(__ppp_hash, account);
  if (!ppp) 
    {
      pppd_errno = PPPD_LOOKUP_FAIL;
      unblock_sigchld();
      return 0;
    }

  /* if the ppp device number has not been determined, then
   * attempt to determine it
   */
  if (ppp->device_number < 0)
    {
      ppp->device_number = check_pid_file(ppp);
      if (ppp->device_number >= 0)
	{
	  *device_number = ppp->device_number;
	  dns_add_entries(ppp->account, ppp->pid);

	  /* return success */
	  unblock_sigchld();
	  return 1;
	}
   } 

  /* pending */
  pppd_errno = PPPD_PENDING;
  unblock_sigchld();
  return 0;
}


gint
pppd_kill(Account *account) 
{
  PPP *ppp;

  g_assert(account != NULL);

  block_sigchld();
  ppp = g_hash_table_lookup(__ppp_hash, account);
  if (!ppp)
    {
      pppd_errno = PPPD_LOOKUP_FAIL;
      unblock_sigchld();
      return 0;
    }

  /* sanity checks */
  if (ppp->pid <= 0 || ppp->pid == getpid())
    {
      g_error("pppd_kill(): ppp->pid = %d", ppp->pid);
    }

  kill(ppp->pid, SIGTERM);
  unblock_sigchld();
  return 1;
}


pid_t
pppd_pid(Account *account)
{
  PPP *ppp;
  pid_t pid;

  g_assert(account != NULL);
  block_sigchld();
  ppp = g_hash_table_lookup(__ppp_hash, account);
  if (!ppp)
    {
      pppd_errno = PPPD_LOOKUP_FAIL;
      unblock_sigchld();
      return 0;
    }

  pid = ppp->pid;

  unblock_sigchld();
  return pid;
}


void 
pppd_sigchld(pid_t pid, int status)
{
  PPP *ppp;

  ppp = g_hash_table_lookup(__ppp_pid_hash, &pid);
  if (ppp)
    {
      dns_remove_entries(ppp->account, ppp->pid);

      g_hash_table_remove(__ppp_hash, ppp->account);
      g_hash_table_remove(__ppp_pid_hash, &pid);

      /* close the domain socket & unlink the file */
      if (ppp->socket_fd >= 0)
	{
	  if (ppp->client_fd >= 0)
	    {
	      close(ppp->client_fd);
	      ppp->client_fd = -1;
	    }

	  if (ppp->socket_fd >= 0)
	    {
	      close(ppp->socket_fd);
	      ppp->socket_fd = -1;
	    }

	  /* unlink and clear the socket path */
	  unlink(ppp->socket_path->str);
	  g_string_assign(ppp->socket_path, "");
	}

      connect_inform_pppd_died(ppp->connect);
      g_free(ppp);
    }
}


static pid_t
fork_pppd(PPP *ppp)
{
  pid_t pid;

  pid = fork();
  if (pid == 0)
    {
      int fd, arg_index;
      char *args[MAX_ARGS];
      GString *device, *speed, *user, *remotename, *chat, *netmask, *ip,
	*mtu, *mru;

      arg_index = 0;
      
      device = g_string_new("");
      speed = g_string_new("");
      user = g_string_new("");
      remotename = g_string_new("");
      chat = g_string_new("");
      netmask = g_string_new("");
      ip = g_string_new("");
      mtu = g_string_new("");
      mru = g_string_new("");
      
      /* name of the program as argv[0] */
      args[arg_index++] = "pppd";
      
      /* modem device */
      g_string_sprintf(device, "%s", ppp->account->modem_device->str);
      args[arg_index++] = device->str;
      
      /* speed */
      g_string_sprintf(speed, "%d", ppp->account->speed);
      args[arg_index++] = speed->str;
      
      /* local:remote ip address */
      if (ppp->account->ip->len == 0)
	{
	  args[arg_index++] = "noipdefault";
	}

      if (ppp->account->ip->len > 0 || ppp->account->remote->len > 0)
	{
	  if (ppp->account->ip->len > 0)
	    {
	      g_string_assign(ip, ppp->account->ip->str);
	    }
	  else
	    {
	      g_string_assign(ip, "0.0.0.0");
	    }
	  
	  g_string_append_c(ip, ':');
	  
	  if (ppp->account->remote->len > 0)
	    {
	      g_string_append(ip, ppp->account->remote->str);
	    }
	  else
	    {
	      g_string_append(ip, "0.0.0.0");
	    }
	  
	  args[arg_index++] = ip->str;
	}
      
      /* netmask */
      if (ppp->account->mask->len > 0)
	{
	  args[arg_index++] = "netmask";
	  g_string_sprintf(netmask, "%s", ppp->account->mask->str);
	  args[arg_index++] = netmask->str;
	}
      
      /* lock the device */
      if (ppp->account->lock_device)
	{
	  args[arg_index++] = "lock";
	}
      
      /* don't fork off */
      args[arg_index++] = "-detach";
      
      /* add default route */
      if (ppp->account->default_route)
	{
	  args[arg_index++] = "defaultroute";
	}
      
      /* mtu/mru (Quake optimization support) */
      args[arg_index++] = "mtu";
      g_string_sprintf(mtu, "%d", ppp->account->mtu);
      args[arg_index++] = mtu->str;
      
      args[arg_index++] = "mru";
      g_string_sprintf(mru, "%d", ppp->account->mru);
      args[arg_index++] = mru->str;
      
      /* CRTSCTS/XONXOFF flow control */
      if ((strcmp(ppp->account->modem_flow->str, "crtscts")== 0) || 
          (strcmp(ppp->account->modem_flow->str, "xonxoff") == 0))
        {
          args[arg_index++] = ppp->account->modem_flow->str;
	}
      
      /* PAP/CHAP */
      if (ppp->account->remotename->len > 0 &&
	  ppp->account->user->len > 0)
	{
	  args[arg_index++] = "user";
	  
	  g_string_sprintf(user, "%s", ppp->account->user->str);
	  args[arg_index++] = user->str;
	  
	  args[arg_index++] = "remotename";
	  
	  g_string_sprintf(remotename, "%s", ppp->account->remotename->str);
	  args[arg_index++] = remotename->str;
	}
      
      /* gnome-ppp-chat */
      args[arg_index++] = "connect";
      g_string_sprintf(chat, "gnome-ppp-chat %s", ppp->socket_path->str);
      args[arg_index++] = chat->str;
      
      /* sentinal */  
      args[arg_index] = NULL;
      
      /* become process group leader so pppd's bad habbit of
       * echoing signals to its entire fucking process group
       * doesn't clobber this program
       */
      if (setpgrp() == -1)
	{
	  _exit(0);
	}

      /* close all file descriptors */
      for (fd = 0; fd <= 64; fd++)
	{
	  close(fd);
	}

      /* if the execv fails, exit with _exit so no
       * atexit handlers get called
       */
      execv(PPPD_PATH, args);
      _exit(0);
    }

  return pid;
}


static gint
check_device_lock(gchar *device_name)
{
  int fd;
  pid_t pid;
  char inbuf[20];
  GString *filename;

  /* create lock file path */
  filename = g_string_new("");
  g_string_sprintf(filename, "/var/lock/LCK..%s", g_basename(device_name));
  fd = open(filename->str, O_RDONLY);
  g_string_free(filename, TRUE);

  if (fd >= 0)
    {
      /* prevent race condition */
      sleep(1);

      read(fd, inbuf, 19);
      close(fd);

      inbuf[19] = '\0';
      sscanf(inbuf, "%d", &pid);

      /* check the process ID stored in the lockfile to
       * see if it is stale (written by dead process)
       */
      if(kill(pid, 0) == -1 && errno == ESRCH)
	{
	  return 0;
	} 
      else
	{
	  return 1;
	}
    }

  return 0;
}


/*** PPP<X>.PID Run Files ***/

/* returns the device number of the pppd device if a coorasponding
 * pppX.pid lockfile is found which was generated by the forked
 * pppd process; returns -1 if not found
 */
static gint
check_pid_file(PPP *ppp)
{
  gint device_number;
  pid_t pid;
  DIR *dp;
  struct dirent *d;

  /* open the directory */
  if ( (dp = opendir(PPPD_PID_PATH)) == NULL)
    {
      g_error("cannot open %s", PPPD_PID_PATH);
    }

  while( (d = readdir(dp)) != NULL)
    {
      device_number = is_pid_filename(d->d_name);
      if (device_number != -1)
	{
	  pid = get_pid(device_number);
	  if (ppp->pid == pid)
	    {
	      return device_number;
	    }
	}
    }

  return -1;
}


static pid_t
get_pid(gint device_number)
{
  int fd;
  char c;
  pid_t pid;
  GString *gstr;

  gstr = g_string_new("");
  g_string_sprintf(gstr, "%s/ppp%d.pid", PPPD_PID_PATH, device_number);

  if ( (fd = open(gstr->str, O_RDONLY)) < 0)
    {
      goto error;
    }

  pid = 0;
  while (read(fd, &c, 1) == 1 && ISDIGIT(c))
    {
      pid *= 10;
      pid += CHARTODIGIT(c);
    }

 cleanup:
  g_string_free(gstr, TRUE);
  return pid;

 error:
  g_string_free(gstr, TRUE);
  return 0;
}


static gint
is_pid_filename(gchar *filename)
{
  gint i, len, device_number;

  len = strlen(filename);

  /* should be at least 8 charactors long */
  if (len < 8)
    {
      return -1;
    }

  /* look at prefix & suffix of file */
  if (strncmp("ppp", filename, 3) != 0 ||
      strncmp(".pid", filename + len - 4, 4) != 0)
    {
      return -1;
    }
  
  /* scan the ppp device number */
  device_number = 0;
  for (i = 0; i < (len - 7); i++)
    {
      device_number *= 10;

      /* all these charactors should be digits, if they
       * are not, then this is not a pppX.pid file
       */
      if (!ISDIGIT(filename[i+3]))
	{
	  return -1;
	}

      device_number += CHARTODIGIT(filename[i+3]);
    }

  return device_number;
}


/* for file descriptor passing between gnome-ppp-chat and
 * libgnome-ppp
 */
static int
recieve_fd(int fd, int *recvfd)
{
  char buff[1];
  struct msghdr msg;
  struct iovec iov[1];
  ssize_t n;
  struct cmsghdr *cmptr;

  cmptr = g_malloc(CONTROLLEN);

  iov[0].iov_base     = buff;
  iov[0].iov_len      = 1;

  msg.msg_name        = NULL;
  msg.msg_namelen     = 0;
  msg.msg_control     = cmptr;
  msg.msg_controllen  = CONTROLLEN;
  msg.msg_iov         = iov;
  msg.msg_iovlen      = 1;
  msg.msg_flags       = 0;

  if ((n = recvmsg(fd, &msg, 0)) <= 0)
    goto error;

  if (CMSG_FIRSTHDR(&msg) && msg.msg_controllen == CONTROLLEN)
    *recvfd = *((int *) CMSG_DATA(cmptr));
  else
    goto error;


  g_free(cmptr);
  return 1;

 error:
  g_free(cmptr);
  return 0;
}
