/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <glib.h>


#define TOLOWER(c) (((c) >= 'A' && (c) <= 'Z') ? ((c) - 'A' + 'a') : (c))


gint
g_string_find(GString *string, gchar *find, gboolean case_insensitive)
{
  gint i, j, match, find_len;

  g_assert(string != NULL);
  g_assert(find != NULL);

  find_len = strlen(find);
  if (find_len == 0)
    {
      return -1;
    }

  if (case_insensitive)
    {
      for (i = 0; i < string->len; i++)
	{
	  if (TOLOWER(find[0]) != TOLOWER(string->str[i]))
	    {
	      continue;
	    }
	  
	  match = 1;

	  for (j = 1; j < find_len && (j+i) < string->len; j++)
	    {
	      if (TOLOWER(find[j]) != TOLOWER(string->str[j+i]))
		{
		  match = 0;
		  break;
		}
	    }
	  
	  if (match && j == find_len)
	    {
	      return i;
	    }
	}
    }
  else
    {
      for (i = 0; i < string->len; i++)
	{
	  if (find[0] != string->str[i])
	    {
	      continue;
	    }
	  
	  match = 1;

	  for (j = 1; j < find_len && (j+i) < string->len; j++)
	    {
	      if (find[j] != string->str[j+i])
		{
		  match = 0;
		  break;  
		}
	    }
	  
	  if (match && j == find_len)
	    {
	      return i;
	    }
	}
    }

  return -1;
}


/* this is a extreamly inefficent function to read
 * the lines of a file and return them as a list
 * of GString(s)
 */
GList *
j_read_lines(gchar *path)
{
  int fd;
  char c;
  GList *line_list = NULL;
  GString *line = NULL;

  if ( (fd = open(path, O_RDONLY)) < 0)
    {
      return NULL;
    }

  /* read lines */
  line = g_string_new("");
  while (read(fd, &c, 1) == 1)
    {
      g_string_append_c(line, c);
      
      /* upon newline ... */
      if (c == '\n')
	{
	  line_list = g_list_append(line_list, line);
	  line = g_string_new("");
	}
    }

  /* append the last line to the list ... */
  line_list = g_list_append(line_list, line);

  return line_list;  
}


gboolean
j_write_lines(gchar *path, GList *line_list)
{
  int fd;
  GString *line;
  GList *list;

  if ( (fd = open(path, O_WRONLY|O_CREAT|O_TRUNC)) < 0)
    {
      return FALSE;
    }

  list = line_list;
  while (list)
    {
      line = (GString *) list->data;
      list = list->next;

      if (write(fd, line->str, line->len) != line->len)
	{
	  goto error;
	}
    }

  close(fd);
  return TRUE;

 error:
  close(fd);
  return FALSE;
}


void
j_free_lines(GList *line_list)
{
  GList *list;
  GString *gstr;

  /* free GString(s) */
  list = line_list;
  while (list)
    {
      gstr = (GString *) list->data;
      list = list->next;
      if (gstr)
	{
	  g_string_free(gstr, TRUE);
	}
    }

  /* free the list nodes */
  g_list_free(line_list);
}
