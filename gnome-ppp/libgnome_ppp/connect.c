/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <config.h>
#include <gnome.h>
#include <stdio.h>
#include <errno.h>
#include "gnome-ppp.h"
#include "connect.h"
#include "misc.h"
#include "pppd.h"
#include "program.h"
#include "msg.h"


/* XXX: timeout voodo. -JMP */
/* various timeouts in seconds for operations */
#define HANGUP_TIMEOUT          3
#define GET_TIMEOUT             5
#define DETECT_TIMEOUT          5
#define ADJUST_SPEAKER_TIMEOUT  5
#define REDIAL_PAUSE_TIMEOUT    5
#define SCRIPT_TIMEOUT         30
#define PPPD_TIMEOUT            5
#define FIRST_STATE             0
#define MAX_HANGUP_ATTEMPTS     3


/* a couple of macros to make the code more readable */
#define SET_TIMEOUT(t, s) ((t) = time(NULL) + (s))
#define TIMEOUT(t)        ((t) < time(NULL))
#define NEXT_STATE(s)     ((s)++)
#define CALLBACK(c, t, m) ((*(c)->func)((c)->orig_account, (t), (m), ((c)->data)))


/* master states */
enum
{
  _GET_MODEM,
  _DETECT_MODEM,
  _ADJUST_SPEAKER,
  _DIAL,
  _RUN_SCRIPT,
  _EXEC_PPPD,
  _CONNECTED,
};


/* state machine for the entire connection sequence */
static gint state_engine_iteration(Connect *connect);
static void state_get_modem(Connect *connect);
static void state_detect_modem(Connect *connect);
static void state_adjust_speaker(Connect *connect);
static void state_dial(Connect *connect);
static void state_run_script(Connect *connect);
static void state_exec_pppd(Connect *connect);
static void state_connected(Connect *connect);


/* callbacks */
static void pppd_die_cb(gpointer data);


/* misc */
static gboolean read_charactor(Connect *connect);
static gboolean search_modem_buffer(Connect *connect, gchar *find);
static gchar *search_modem_buffer_list(Connect *connect, gchar *find_list[]);


/* list of all active connections  */
static GHashTable *__connect_hash = NULL;



/* initalizer registered with init.c */
void
__init_connect()
{
  static gboolean done_init = FALSE;

  if (!done_init)
    {
      done_init = TRUE;
      __connect_hash = g_hash_table_new(g_direct_hash, NULL);
    }
}


static Connect*
malloc_connect()
{
  Connect *connect = g_malloc(sizeof(Connect));

  connect->orig_account = NULL;
  connect->account = NULL;
  connect->func = NULL;
  connect->data = NULL;
  connect->pppd_pid = -1;
  connect->ppp_device = -1;
  connect->modem = NULL;
  connect->modem_buffer = g_string_new("");
  connect->timeout = 0;
  connect->connect_time = 0;

  connect->hanup_attempts = 0;

  connect->state = FIRST_STATE;
  connect->get_state = FIRST_STATE;
  connect->detect_state = FIRST_STATE;
  connect->adjust_speaker_state = FIRST_STATE;
  connect->dial_state = FIRST_STATE;
  connect->script_state = FIRST_STATE;
  connect->pppd_state = FIRST_STATE;

  connect->dial_attempts = 0;

  connect->phone_number_node = NULL;
  connect->script_entry_node = NULL;

  connect->send_buffer_set = FALSE;
  connect->send_buffer = g_string_new("");

  connect->close = FALSE;
  connect->dead = FALSE;
  connect->user_closed = FALSE;

  return connect;
}


static void
free_connect(Connect *connect)
{
  g_assert(connect != NULL);

  /* free the cloned account */
  if (connect->account)
    {
      account_free(connect->account);
    }

  g_string_free(connect->modem_buffer, TRUE);
  g_string_free(connect->send_buffer, TRUE);
  g_free(connect);
}


gint
connect_start(Account *account, ConnectCBFunc func, gpointer data)
{
  Connect *connect;

  g_assert(account != NULL);

  /* don't start a connection on a account that's already in use */
  if (g_hash_table_lookup(__connect_hash, account))
    {
      CALLBACK(connect, PPP_ERROR, msg(MSG_IN_USE));
      CALLBACK(connect, PPP_DISCONNECTED, NULL);
      return 0;
    }

  /* create connect info struct, reference this structure by the origional
   * account pointer, but create a duplicate of the account for the duration
   * of this connection so the origional account doesn't have to be locked
   * aginst editing
   */
  connect = malloc_connect();
  connect->func = func;
  connect->data = data;
  connect->orig_account = account;
  connect->account = account_clone(account);


  /* add to the hash */
  g_hash_table_insert(__connect_hash, account, connect);
  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_BEGIN));

  /* for pppd -- this forks pppd with the gnome-ppp-chat */
  if (pppd_exec(connect, connect->account))
    {
      connect->pppd_pid = pppd_pid(connect->account);

      g_timeout_add(1000, (GSourceFunc) state_engine_iteration, connect);

      CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_PPPD_FORK));
      CALLBACK(connect, PPP_DEBUG, "\r\n");
      CALLBACK(connect, PPP_DEBUG, _("---NEW CONNECTION---"));
      CALLBACK(connect, PPP_DEBUG, "\r\n");
      account_lock(account);
    }
  else
    {
      switch (pppd_errno)
	{
	case PPPD_NO_EXEC_PERMISSION:
	  CALLBACK(connect, PPP_ERROR, msg(MSG_NO_PPPD_EXEC_PERMISSION));
	  goto error;

	case PPPD_MODEM_LOCKED:
	  CALLBACK(connect, PPP_ERROR, msg(MSG_MODEM_LOCKED));
	  goto error;

	default:
	  g_error("connect_start(): unknown pppd error");
	  break;
	}
    }

  return 1;

 error:
  /* remove connection from hash */
  CALLBACK(connect, PPP_DISCONNECTED, NULL);
  g_hash_table_remove(__connect_hash, connect->orig_account);
  free_connect(connect);
  return 0;
}


void
connect_stop(Account *account)
{
  Connect *connect;

  g_assert(account != NULL);
  
  connect = g_hash_table_lookup(__connect_hash, account);
  if (!connect)
    return;

  /* inform the user the connection is being closed, make sure
   * to use the proper connect state in the callback
   */
  if (connect->state == _CONNECTED)
    CALLBACK(connect, PPP_CONNECTED, msg(MSG_CLOSING));
  else
    CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_CLOSING));

  connect->close = TRUE;
  connect->user_closed = TRUE;
  account_unlock(account);
}


void
connect_send(Account *account, gchar *text, gboolean append_cr)
{
  Connect *connect;

  g_assert(account != NULL);
  
  connect = g_hash_table_lookup(__connect_hash, account);
  if (!connect)
    return;

  connect->send_buffer_set = TRUE;
  
  g_string_sprintf(connect->send_buffer, "%s%s", text,
                   account->modem_term->str);		     
		     
}


gint
connect_pid(Account *account)
{  
  Connect *connect;

  g_assert(account != NULL);
  
  connect = g_hash_table_lookup(__connect_hash, account);
  if (!connect)
    return -1;
  
  return connect->pppd_pid;
}


gint
connect_device(Account *account)
{
  Connect *connect;

  g_assert(account != NULL);
  
  connect = g_hash_table_lookup(__connect_hash, account);
  if (!connect)
    return -1;
  
  return connect->ppp_device;
}


/* called from pppd.c when the pppd daemon dies */
void
connect_inform_pppd_died(Connect *connect)
{
  connect->close = TRUE;
  connect->dead = TRUE;
}


/* 
 * this is the main state machine for the ENTIRE connection cycle 
 */
static gint
state_engine_iteration(Connect *connect)
{

  GtkWidget *Dialogbox;

  /* get the modem's file descriptor and create a modem 
   * communication class
   */
  if (!connect->close && connect->state == _GET_MODEM)
    {
      state_get_modem(connect);
    }

  /* detect the modem */
  if (!connect->close && connect->state == _DETECT_MODEM)
    {
      state_detect_modem(connect);
    }
  
  /* adjust modem's speaker volume */
  if (!connect->close && connect->state == _ADJUST_SPEAKER)
    {
      state_adjust_speaker(connect);
    }

  /* dial the number */
  if (!connect->close && connect->state == _DIAL)
    {
      state_dial(connect);
    }

  /* run the user script */
  if (!connect->close && connect->state == _RUN_SCRIPT)
    {
      state_run_script(connect);
    }

  /* run the pppd daemon */
  if (!connect->close && connect->state == _EXEC_PPPD)
    {
      state_exec_pppd(connect);
    }

  /* connected */
  if (!connect->close && connect->state == _CONNECTED)
    {
      state_connected(connect);
    }

  /* end the connection */
  if (connect->close)
    {
      if (!connect->dead)
	{
	  pppd_kill(connect->account);
	}

      /* we're really done */
      if (connect->dead)
	{
	  if (!connect->user_closed)
	    {
	      CALLBACK(connect, PPP_ERROR, msg(MSG_PPP_UNKNOWN_DEATH));         
	    }

	  /* run the disconnection program */
	  if (connect->state == _CONNECTED &&
	      connect->account->disconnect_program->len > 0)
	    {
	      program_exec(connect->account,
			   connect->account->disconnect_program->str);
	    }

	  goto destroy;
	}
    }
  return 1;


 destroy:
  if (connect->modem)
    {
      modem_hangup(connect->modem, connect->account);
      modem_destroy(connect->modem);
      connect->modem = NULL;
    }

  CALLBACK(connect, PPP_DISCONNECTED, NULL);

  /* remove connection from hash */
  g_hash_table_remove(__connect_hash, connect->orig_account);
  free_connect(connect);

  return 0;
}


static void
state_get_modem(Connect *connect)
{
  gint modem_fd;

  enum
  {
    __SET_TIMEOUT,
    __RECIEVE_FD
  };

  switch (connect->get_state)
    {
    case __SET_TIMEOUT:
      SET_TIMEOUT(connect->timeout, GET_TIMEOUT);
      NEXT_STATE(connect->get_state);
      break;


    case __RECIEVE_FD:
      if (pppd_modem_fd(connect->account, &modem_fd))
	{
	  connect->modem = modem_new(modem_fd, connect->account->speed,
	                             connect->account->modem_term->str);
	  if (!connect->modem)
	    {
	      g_error("state_get_modem(): ioctl failure on modem");
	    }

	  modem_drain(connect->modem);
	  NEXT_STATE(connect->state);
	}
      else
	{
	  switch (pppd_errno)
	    {
	    case PPPD_PENDING:
	      if (TIMEOUT(connect->timeout))
		{
		  CALLBACK(connect, PPP_ERROR, msg(MSG_MODEM_FD_PASS_TIMEOUT));
		  connect->close = TRUE;
		  connect->user_closed = TRUE;
		  goto error;
		}
	      break;

	    case PPPD_LOOKUP_FAIL:
	      goto error;

	    default:
	      g_error("state_get_modem(): unknown pppd errno");
	      break;
	    }
	}
    }

 error:
  return;
}


static void
state_detect_modem(Connect *connect)
{
  GString *gstr;

  enum
  {
    __CHECK_CARRIER,
    __START_PAUSE,
    __END_PAUSE,
    __AT_INIT,
    __OK
  };

  gstr = g_string_new("");

  switch (connect->detect_state)
    {
    case __CHECK_CARRIER:
      
      if (modem_carrier(connect->modem))
	{
	  /* if we're under MAX_HANGUP_ATTEMPTS, then try to hangup the
	   * modem again; otherwise error out and quit
	   */
	  if (connect->hanup_attempts++ < MAX_HANGUP_ATTEMPTS)
	    {
	      modem_hangup(connect->modem, connect->account);
	      NEXT_STATE(connect->detect_state);
	    }
	  else
	    {
	      CALLBACK(connect, PPP_ERROR, msg(MSG_MODEM_HANUP_FAILED));
	      connect->close = TRUE;
	      connect->user_closed = TRUE;
	    }
	}
      else
	{
	  NEXT_STATE(connect->detect_state);
	}
      break;


    case __START_PAUSE:
      
      SET_TIMEOUT(connect->timeout, HANGUP_TIMEOUT);
      NEXT_STATE(connect->detect_state);
      break;


    case __END_PAUSE:
    
      if (TIMEOUT(connect->timeout))
	{
	  if (modem_carrier(connect->modem))
	    {
	      connect->detect_state = __CHECK_CARRIER;
	    }
	  else
	    {
	      NEXT_STATE(connect->detect_state);
	    }
	}
      break;


    case __AT_INIT:
    
      CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_MODEM_DETECT));

      g_string_sprintf(gstr, "%s%s", connect->account->modem_init->str,
                       connect->account->modem_term->str);
		       
      modem_write(connect->modem, gstr->str, gstr->len); 
      
      SET_TIMEOUT(connect->timeout, DETECT_TIMEOUT);
      NEXT_STATE(connect->detect_state);
      break;
      

    case __OK:
    
      /* check for modem 'OK' response from initalization */
      if (search_modem_buffer(connect, connect->account->init_resp->str)) 
	{
	  
	  NEXT_STATE(connect->state);
	}
      else
	{
	  /* we've timed out while trying to detect the modem, inform
	   * the GUI and set the close flag for this connection
	   */
	  if (TIMEOUT(connect->timeout))
	    {
	      CALLBACK(connect, PPP_ERROR, msg(MSG_MODEM_DETECT_FAILED));
	      connect->close = TRUE;
	      connect->user_closed = TRUE;
	    }
	}
      break;
    }

 cleanup:
  g_string_free(gstr, TRUE);
}


static void
state_adjust_speaker(Connect *connect)
{
GString *gstr;

  enum
  {
    __ADJUST_SPEAKER,
    __OK
  };

  enum
  {
    ___OFF,
    ___LOW,
    ___HIGH
  };

  gstr = g_string_new("");

  switch (connect->adjust_speaker_state)
    {      
    case __ADJUST_SPEAKER:

      switch (connect->account->modem_volume)
        {
	case ___OFF:
	  
	  g_string_sprintf(gstr, "%s%s", 
	  		   connect->account->modem_mute->str,
                           connect->account->modem_term->str);
	  break;
	  
	case ___LOW:
	
	  g_string_sprintf(gstr, "%s%s",
	                   connect->account->modem_low->str,
                           connect->account->modem_term->str);
	  break;
	  
	case ___HIGH:  
	
	  g_string_sprintf(gstr, "%s%s",
	                   connect->account->modem_high->str,
                           connect->account->modem_term->str);
	  break;
	}  
      	       
      modem_write(connect->modem, gstr->str, gstr->len); 
      
      SET_TIMEOUT(connect->timeout, ADJUST_SPEAKER_TIMEOUT);
      NEXT_STATE(connect->adjust_speaker_state);
      break;


    case __OK:
    
      /* check for modem 'OK' response from volume adjustment */
      if (search_modem_buffer(connect, connect->account->init_resp->str)) 
	{
	  NEXT_STATE(connect->state);
	}
      else
	{ 
	  /* we've timed out while trying to adjust the speaker's volume, 
	     inform the GUI and set the close flag for this connection */
	  if (TIMEOUT(connect->timeout))
	    {
	      CALLBACK(connect, PPP_ERROR, msg(MSG_SPEAKER_ADJUST_FAILED));
	      connect->close = TRUE;
	      connect->user_closed = TRUE;
	    }
	 } 
      break;
    }

 cleanup:
  g_string_free(gstr, TRUE);
}


static void
state_dial(Connect *connect)
{
  gchar *result;
  GString *gstr = g_string_new(""), *phone_number;
  
  gchar *__modem_responses[] =
  {
    connect->account->nodialtone_resp->str, 
    connect->account->nocarrier_resp->str, 
    connect->account->busy_resp->str, 
    connect->account->noanswer_resp->str,
    connect->account->connect_resp->str, 
    NULL
  };
  
  enum
  {
    __CALL,
    __ANSWER,
    __START_REDIAL_PAUSE,
    __END_REDIAL_PAUSE
  };

  switch (connect->dial_state)
    {
    case __CALL:
      /* check for maximum # of redials */
      if (connect->dial_attempts >= connect->account->redial_attempts)
	{
	  CALLBACK(connect, PPP_ERROR, msg(MSG_MAX_REDIAL));
	  connect->close = TRUE;
	  connect->user_closed = TRUE;
	  goto cleanup;
	}

      /* the account HAS to have at least one phone number to dial */
      if (g_list_length(connect->account->phone_list) == 0)
	{
	  CALLBACK(connect, PPP_ERROR, msg(MSG_NO_PHONE_NUMBER));
	  connect->close = TRUE;
	  connect->user_closed = TRUE;
	  goto cleanup;
	}
      
      /* get a phone number node */
      if (connect->phone_number_node == NULL ||
	  connect->phone_number_node->next == NULL)
	{
	  connect->phone_number_node = connect->account->phone_list;
	}
      else
	{
	  connect->phone_number_node = connect->phone_number_node->next;
	}

      phone_number = (GString *) connect->phone_number_node->data;
      if (phone_number) 
	{
	
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_DIALING));

	  g_string_sprintf(gstr, "%s%s\r", connect->account->modem_dial->str,
	  		   phone_number->str);
	  modem_write(connect->modem, gstr->str, gstr->len);
	  connect->dial_attempts++;

	  SET_TIMEOUT(connect->timeout, connect->account->dial_timeout);
	  connect->dial_state = __ANSWER;
	}
      break;


    case __ANSWER:
      result = search_modem_buffer_list(connect, __modem_responses);

      /* TIMEOUT */
      if (!result && TIMEOUT(connect->timeout))
	{
	  g_string_assign(connect->modem_buffer, "");
	  connect->dial_state = __START_REDIAL_PAUSE;
	  goto cleanup;
	}

      /* no match -- continue with life */
      if (!result)
	{
	  goto cleanup;
	}

      /* no dialtone */
       if (strncmp(result, connect->account->nodialtone_resp->str, 
                   strlen(connect->account->nodialtone_resp->str)) == 0)
	{
	  CALLBACK(connect, PPP_ERROR, msg(MSG_NO_DIALTONE));
	  connect->close = TRUE;
	  connect->user_closed = TRUE;
	  goto cleanup;
	}

      /* no carrior */
      if (strncmp(result, connect->account->nocarrier_resp->str, 
                  strlen(connect->account->nocarrier_resp->str)) == 0)
	{
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_NO_CONNECT));
	  connect->dial_state = __START_REDIAL_PAUSE;
	  goto cleanup;
	}

      /* busy */
      if (strncmp(result, connect->account->busy_resp->str, 
                  strlen(connect->account->busy_resp->str)) == 0)
	{
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_BUSY));
	  connect->dial_state = __START_REDIAL_PAUSE;
	  goto cleanup;
	}
 
      /* ring, but no answer */
      if (strncmp(result, connect->account->noanswer_resp->str,
                  strlen(connect->account->noanswer_resp->str)) == 0)
	{
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_NO_ANSWER));
	  connect->dial_state = connect->dial_state = __START_REDIAL_PAUSE;
	  goto cleanup;
	}
      
      /* we've connected */
      if (strncmp(result, connect->account->connect_resp->str, 
                  strlen(connect->account->connect_resp->str)) == 0)
	{
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_CONNECTED));
	  NEXT_STATE(connect->state);
	  goto cleanup;
	}
      break;


    case __START_REDIAL_PAUSE:
      SET_TIMEOUT(connect->timeout, REDIAL_PAUSE_TIMEOUT);
      connect->dial_state = __END_REDIAL_PAUSE;
      break;


    case __END_REDIAL_PAUSE:
      if (TIMEOUT(connect->timeout))
	{
	  connect->dial_state = __CALL;
	  goto cleanup;
	}
      break;
    }

 cleanup:
  g_string_free(gstr, TRUE);
}


static void
state_run_script(Connect *connect)
{
  ScriptEntry *script_entry;
  GString *gstr;

  enum
  {
    __INITALIZE,
    __RUN_SCRIPT,
    __WAIT_EXPECT,
    __WAIT_USER_INPUT
  };


  gstr = g_string_new("");

  /* check for carrier detect */
  if (!modem_carrier(connect->modem))
    {
      CALLBACK(connect, PPP_ERROR, msg(MSG_NO_CARRIOR));
      connect->close = TRUE;
      connect->user_closed = TRUE;
      goto cleanup;
    }

  /* script entry data off of our current node */
  if (connect->script_entry_node)
    {
      script_entry = (ScriptEntry *) connect->script_entry_node->data;
      if (!script_entry)
	{
	  g_error("state_run_script(): script entry data==NULL");
	}
    }

  switch (connect->script_state)
    {
    case __INITALIZE:
      if (connect->account->script_list)
	{
	  CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_RUNNING_SCRIPT));
	  connect->script_entry_node = connect->account->script_list;
	  NEXT_STATE(connect->script_state);
	}
      else
	{
	  /* if there's no script for this account, then jump
	   * to the next connection state
	   */
	  NEXT_STATE(connect->state);
	}
      break;


    case __RUN_SCRIPT:
      switch (script_entry->type)
	{
	case SCRIPT_ENTRY_EXPECT:
	  connect->script_state = __WAIT_EXPECT;
	  break;

	case SCRIPT_ENTRY_SEND:
	  g_string_sprintf(gstr, "%s%s", script_entry->text->str,
	                   connect->account->modem_term->str);	   
	  modem_write(connect->modem, gstr->str, gstr->len);
	  goto next;
	  break;
	  
	case SCRIPT_ENTRY_SEND_USER:
	  g_string_sprintf(gstr, "%s%s", connect->account->user->str,
	                   connect->account->modem_term->str);
	  modem_write(connect->modem, gstr->str, gstr->len);
	  goto next;
	  break;
	  
	case SCRIPT_ENTRY_SEND_PASSWORD:
	  g_string_sprintf(gstr, "%s%s", connect->account->passwd->str,
	                   connect->account->modem_term->str);
	  modem_write(connect->modem, gstr->str, gstr->len);
	  goto next;
	  break;
	  
	case SCRIPT_ENTRY_PROMPT_INPUT:
	  CALLBACK(connect, PPP_PROMPT_INPUT, script_entry->text->str);
	  connect->script_state = __WAIT_USER_INPUT;
	  break;
	}
      break;


    case __WAIT_EXPECT:
      if (search_modem_buffer(connect, script_entry->text->str))
	{
	  connect->script_state = __RUN_SCRIPT;
	  goto next;
	}
      break;


    case __WAIT_USER_INPUT:
      if (connect->send_buffer_set)
	{
	  modem_write(
	      connect->modem, 
	      connect->send_buffer->str,
	      connect->send_buffer->len);

	  connect->send_buffer_set = FALSE;
	  connect->script_state = __RUN_SCRIPT;
	  goto next;
	}
      break;
    }

 cleanup:
  g_string_free(gstr, TRUE);
  return;

 next:
  /* get next script node */
  connect->script_entry_node = connect->script_entry_node->next;
  if (!connect->script_entry_node)
    {
      CALLBACK(connect, PPP_IN_PROGRESS, msg(MSG_SCRIPT_COMPLETE));
      NEXT_STATE(connect->state);
    }

  g_string_free(gstr, TRUE);
  return;
}


static void
state_exec_pppd(Connect *connect)
{
  gint device_number;

  enum
  {
    __START_PPP,
    __PAUSE,
    __DETECT_PPP_CONNECTION
  };
  

  switch (connect->pppd_state)
    {
    case __START_PPP:
      /* close the connection to the modem */
      modem_destroy(connect->modem);
      connect->modem = NULL;
      
      /* ends gnome-ppp-chat allowing pppd to attempt to 
       * establish a PPP connection
       */
      if (pppd_end_chat(connect->account))
	{
	  SET_TIMEOUT(connect->timeout, 2);
	  NEXT_STATE(connect->pppd_state);
	}
      else
	{
	  switch (pppd_errno)
	    {
	    case PPPD_LOOKUP_FAIL:
	      goto error;
 
	    default:
	      g_error("state_exec_pppd(): unknown error from pppd_end_chat");
	      break;
	    }
	}
      break;


    case __PAUSE:
      if (TIMEOUT(connect->timeout))
	{
	  SET_TIMEOUT(connect->timeout, PPPD_TIMEOUT);
	  NEXT_STATE(connect->pppd_state);
	}


    case __DETECT_PPP_CONNECTION:
      if (pppd_device_number(connect->account, &device_number))
	{
	  connect->ppp_device = device_number;
	  CALLBACK(connect, PPP_CONNECTED, msg(MSG_PPP_CONNECTED));

	  /* run connect program */
	  if (connect->account->connect_program->len > 0)
	    {
	      program_exec(connect->account,
			   connect->account->connect_program->str);
	    }

	  NEXT_STATE(connect->state);
	}
      else
	{
	  switch (pppd_errno)
	    {
	    case PPPD_PENDING:
	      if (TIMEOUT(connect->timeout))
		{
		  CALLBACK(connect, PPP_ERROR, msg(MSG_PPP_FAIL));
		  connect->close = TRUE;
		  connect->user_closed = TRUE;
		  goto error;
		}
	      break;

	    case PPPD_LOOKUP_FAIL:
	      goto error;
	      
	    default:
	      g_error("state_exec_pppd(): unknown error from pppd_device_number");
	      break;
	    }
	}
      break;
    }

 error:
}


static void
state_connected(Connect *connect)
{
}




/** modem buffer reading ***/
static gboolean
read_charactor(Connect *connect)
{
  gint len;
  gchar buff[2];

  if (modem_read_ready(connect->modem))
    {
      len = modem_read(connect->modem, buff, 1);
      if (len == 1)
	{
	  buff[1] = '\0';
	  CALLBACK(connect, PPP_DEBUG, buff);
	  g_string_append_c(connect->modem_buffer, buff[0]);
	}
      else
	{
	  connect->close = TRUE;
	  connect->user_closed = TRUE;
	  return FALSE;
	}

      return TRUE;
    }
  else
    {
      return FALSE;
    }
}


static gboolean
search_modem_buffer(Connect *connect, gchar *find)
{
  while (read_charactor(connect))
    {
      if (g_string_find(connect->modem_buffer, find, FALSE) >= 0)
	{
	  g_string_assign(connect->modem_buffer, "");
	  return TRUE;
	}
    }

  /* just in case there was nothing to read, but we need to search
   * the recieve buffer anyways
   */
  if (g_string_find(connect->modem_buffer, find, FALSE) >= 0)
    {
      g_string_assign(connect->modem_buffer, "");
      return TRUE;
    }

  return FALSE;
}


static gchar*
search_modem_buffer_list(Connect *connect, gchar *find_list[])
{
  gint i;
  gchar *find;

  while (read_charactor(connect))
    {
      for (i = 0; find_list[i]; i++)
	{
	  find = find_list[i];
	  
	  if (g_string_find(connect->modem_buffer, find, FALSE) >= 0)
	    {
	      g_string_assign(connect->modem_buffer, "");
	      return find;
	    }
	}
    }

  /* just in case there was nothing to read, but we need to search
   * the recieve buffer anyways
   */
  for (i = 0; find_list[i]; i++)
    {
      find = find_list[i];
      
      if (g_string_find(connect->modem_buffer, find, FALSE) >= 0)
	{
	  g_string_assign(connect->modem_buffer, "");
	  return find;
	}
    }
  
  return NULL;
}
