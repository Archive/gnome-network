/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "msg.h"


struct {
  gint index;
  gchar *text;
} message_array[] = {
  { MSG_IN_USE,                  N_("Account already connected.") },
  { MSG_BEGIN,                   N_("Starting PPP Connection.") },
  { MSG_NO_PPPD_EXEC_PERMISSION, N_("You don't have permission to execute pppd.") },
  { MSG_MODEM_LOCKED,            N_("The modem is in use by another program.") },
  { MSG_PPPD_FORK,               N_("Running pppd.") },
  { MSG_CLOSING,                 N_("Closing connection.") },
  { MSG_MODEM_FD_PASS_TIMEOUT,   N_("The pppd program failed to run gnome-ppp-chat.") },
  { MSG_MODEM_HANUP_FAILED,      N_("The modem could not be hung up.") },
  { MSG_MODEM_DETECT,            N_("Initalizing modem.") },
  { MSG_MODEM_DETECT_FAILED,     N_("Your modem is not responding to initalization.") },
  { MSG_SPEAKER_ADJUST_FAILED,	 N_("The command to set the volume on your modem's speaker failed.") },
  { MSG_MAX_REDIAL,              N_("No answer after maximum number of re-dial attempts.") },
  { MSG_NO_PHONE_NUMBER,         N_("No phone numbers specified for this account.") },
  { MSG_DIALING,                 N_("Dialing.") },
  { MSG_DIAL_TIMEOUT,            N_("Dialing timeout.") },
  { MSG_NO_DIALTONE,             N_("No dialtone.") },
  { MSG_BUSY,                    N_("Number busy.") },
  { MSG_NO_ANSWER,               N_("Ring, but no answer.") },
  { MSG_NO_CONNECT,              N_("Connection failure, reason unknown.") },
  { MSG_CONNECTED,               N_("Connected.") },
  { MSG_NO_CARRIOR,              N_("The modem lost connection with remote host.") },
  { MSG_RUNNING_SCRIPT,          N_("Running Script.") },
  { MSG_SCRIPT_COMPLETE,         N_("Script Complete.") },
  { MSG_PPP_CONNECTED,           N_("PPP connection established.") },
  { MSG_PPP_FAIL,                N_("Failed to establish a PPP connection.") },
  { MSG_PPP_UNKNOWN_DEATH,       N_("The pppd daemon died unexpectedly.") },

  /* sentinal */
  { -1, NULL }
};


gchar *
msg(gint id)
{
  gint i;

  for (i = 0; message_array[i].index >= 0; i++)
    {
      if (message_array[i].index == id)
	{
	  return message_array[i].text;
	}
    }

  return NULL;
}
