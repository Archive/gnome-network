/* libgnome-ppp - The GNOME PPP Dialer Library
 * Copyright (C) 1997 Jay Painter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gnome-ppp.h"

/* built-in default parameters */
#define INIT_CMD	    "ATZ"
#define DIAL_CMD            "ATDT"
#define HANGUP_CMD          "+++ATH"
#define MUTE_CMD            "ATM0L0"
#define LOW_VOLUME_CMD      "ATM1L1"
#define HIGH_VOLUME_CMD     "ATM1L3"
#define INIT_RESP           "OK"
#define HANGUP_RESP         "OK"
#define CONNECT_RESP        "CONNECT"
#define BUSY_RESP           "BUSY"
#define NOANSWER_RESP       "NO ANSWER"
#define NOCARRIER_RESP      "NO CARRIER"
#define NODIALTONE_RESP     "NO DIALTONE"
#define DIALTIMEOUT         60


/* globals */
GList              *__account_list = NULL;
static GList       *__account_list_watchers = NULL;


/* load/save */
static void          save_account(Account *account);
static void          load_account();


/* script */
static ScriptEntry  *script_entry_new(ScriptEntryType type, gchar *text);
static ScriptEntry  *script_entry_clone(ScriptEntry *script_entry);
static void          script_entry_free(ScriptEntry *script_entry);


/* utility */
static GList        *duplicate_g_string_list(GList *list);
static void          free_g_string_list(GList **list);
static gchar        *get_string_use_default(gchar *key, gchar *gstr);



void
__init_account()
{
  static gboolean done_init = FALSE;

  if (!done_init)
    {
      done_init = TRUE;
    }
}


Account *
account_new()
{
  Account *account;

  account = g_malloc(sizeof(Account));

  /* gint types */
  account->speed                = 115200;
  account->redial_attempts      = 5;
  account->dial_timeout         = DIALTIMEOUT;
  account->mtu                  = 296;
  account->mru                  = 296;
  account->modem_volume		= 1;

  /* gboolean types */
  account->default_route        = TRUE;
  account->lock_device          = TRUE;
  account->locked               = FALSE;

  /* GString types */
  account->name                 = g_string_new("");
  account->modem_device         = g_string_new("/dev/modem");
  account->modem_flow		= g_string_new("crtscts");
  account->modem_term		= g_string_new("\r");
  account->user                 = g_string_new("");
  account->passwd               = g_string_new("");
  account->remotename           = g_string_new("");
  account->ip                   = g_string_new("");
  account->mask                 = g_string_new("");
  account->remote               = g_string_new("");
  account->modem_init           = g_string_new(INIT_CMD);
  account->modem_dial		= g_string_new(DIAL_CMD);
  account->modem_hang		= g_string_new(HANGUP_CMD);
  account->modem_mute		= g_string_new(MUTE_CMD);
  account->modem_low		= g_string_new(LOW_VOLUME_CMD);
  account->modem_high		= g_string_new(HIGH_VOLUME_CMD);
  account->init_resp		= g_string_new(INIT_RESP);
  account->hang_resp		= g_string_new(HANGUP_RESP);
  account->connect_resp		= g_string_new(CONNECT_RESP);
  account->busy_resp		= g_string_new(BUSY_RESP);
  account->noanswer_resp	= g_string_new(NOANSWER_RESP);
  account->nocarrier_resp	= g_string_new(NOCARRIER_RESP);
  account->nodialtone_resp	= g_string_new(NODIALTONE_RESP);
  account->connect_program      = g_string_new("");
  account->disconnect_program   = g_string_new("");

  /* GList types (of GString) */
  account->phone_list           = NULL;
  account->dns_list             = NULL;
  account->domain_list          = NULL;
  account->script_list          = NULL;

  return account;
}


void
account_add(Account *account)
{
  g_assert(account != NULL);

  if (g_list_find(__account_list, account))
    {
      g_error("account_add(): re-adding existing account");
    }

  __account_list = g_list_append(__account_list, account);
  account_broadcast_watchers();
}


gint
account_free(Account *account)
{
  g_assert(account != NULL);

  if (g_list_find(__account_list, account))
    {
      g_error("account_free(): tring to free active account");
    }
  
  g_string_free(account->name, TRUE);
  g_string_free(account->user, TRUE);
  g_string_free(account->passwd, TRUE);
  g_string_free(account->remotename, TRUE);
  g_string_free(account->ip, TRUE);
  g_string_free(account->mask, TRUE);
  g_string_free(account->remote, TRUE);
  g_string_free(account->modem_init, TRUE);
  g_string_free(account->modem_dial, TRUE);
  g_string_free(account->modem_hang, TRUE);
  g_string_free(account->modem_mute, TRUE);
  g_string_free(account->modem_low,  TRUE);
  g_string_free(account->modem_high, TRUE);
  g_string_free(account->init_resp,  TRUE);
  g_string_free(account->hang_resp,  TRUE);
  g_string_free(account->connect_resp,    TRUE);
  g_string_free(account->busy_resp,       TRUE);
  g_string_free(account->noanswer_resp,   TRUE);
  g_string_free(account->nocarrier_resp,  TRUE);
  g_string_free(account->nodialtone_resp, TRUE);
  g_string_free(account->connect_program, TRUE);
  g_string_free(account->disconnect_program, TRUE);

  free_g_string_list(&account->phone_list);
  free_g_string_list(&account->dns_list);
  free_g_string_list(&account->domain_list);

  account_script_list_clear(account);

  g_free(account);
  return 1;
}



Account *
account_clone(Account *account)
{
  Account *new_account;
  GList *list;
  ScriptEntry *script_entry;

  g_assert(account != NULL);

  new_account = account_new();

  /* gint types */
  new_account->speed               = account->speed;
  new_account->redial_attempts     = 5;
  new_account->dial_timeout        = DIALTIMEOUT;
  new_account->mtu                 = 296;
  new_account->mru                 = 296;
  
  /*gfloat types */
  new_account->modem_volume	   = account->modem_volume;

  /* gboolean types */
  new_account->default_route       = account->default_route;
  new_account->lock_device         = account->lock_device;

  /* GString types */
  g_string_assign(new_account->name,            account->name->str);
  g_string_assign(new_account->modem_device,    account->modem_device->str);
  g_string_assign(new_account->modem_flow,      account->modem_flow->str);
  g_string_assign(new_account->modem_term,      account->modem_term->str);
  g_string_assign(new_account->user,            account->user->str);
  g_string_assign(new_account->passwd,          account->passwd->str);
  g_string_assign(new_account->remotename,      account->remotename->str);
  g_string_assign(new_account->ip,              account->ip->str);
  g_string_assign(new_account->mask,            account->mask->str);
  g_string_assign(new_account->remote,          account->remote->str);
  g_string_assign(new_account->modem_init,      account->modem_init->str);
  g_string_assign(new_account->modem_dial,      account->modem_dial->str);
  g_string_assign(new_account->modem_hang,      account->modem_hang->str);
  g_string_assign(new_account->modem_mute,      account->modem_mute->str);
  g_string_assign(new_account->modem_low,       account->modem_low->str);
  g_string_assign(new_account->modem_high,      account->modem_high->str);
  g_string_assign(new_account->init_resp,       account->init_resp->str);
  g_string_assign(new_account->hang_resp,       account->hang_resp->str);
  g_string_assign(new_account->connect_resp,    account->connect_resp->str);
  g_string_assign(new_account->busy_resp,       account->busy_resp->str);
  g_string_assign(new_account->noanswer_resp,   account->noanswer_resp->str);
  g_string_assign(new_account->nocarrier_resp,  account->nocarrier_resp->str);
  g_string_assign(new_account->nodialtone_resp, account->nodialtone_resp->str);
  g_string_assign(new_account->connect_program, account->connect_program->str);
  g_string_assign(new_account->disconnect_program, account->disconnect_program->str);

  /* GList types (of GString) */
  new_account->phone_list    = duplicate_g_string_list(account->phone_list);
  new_account->dns_list      = duplicate_g_string_list(account->dns_list);
  new_account->domain_list   = duplicate_g_string_list(account->domain_list);

  /* SCRIPT LIST: duplicate all the script entries */
  list = account->script_list;
  while (list)
    {
     script_entry = (ScriptEntry *) list->data;
     list = list->next;

     new_account->script_list = 
       g_list_append(new_account->script_list, script_entry_clone(script_entry));
    }

  return new_account;
}


gint
account_delete(Account *account)
{
  g_assert(account != NULL);

  if (account->locked)
    {
      return 0;
    }

  if (!g_list_find(__account_list, account))
    {
      g_error("account_delete(): deleting non-added account");
    }

  /* remove account from the account list */
  __account_list = g_list_remove(__account_list, account);
  account_free(account);
  account_broadcast_watchers();
}


GList *
account_list()
{
  return __account_list;
}

gint
account_is_locked(Account *account)
{
  g_assert(account != NULL);
  return account->locked;
}


gint
account_lock(Account *account)
{
  g_assert(account != NULL);

  if (account->locked)
    {
      return 0;
    }

  account->locked = TRUE;
  return 1;
}


void
account_unlock(Account *account)
{
  g_assert(account != NULL);
  account->locked = FALSE;
}



void
account_add_watcher(AccountWatcherCBFunc func)
{
  g_assert(func != NULL);
  __account_list_watchers = g_list_append(__account_list_watchers, func);
}


void
account_remove_watcher(AccountWatcherCBFunc func)
{
  g_assert(func != NULL);
  __account_list_watchers = g_list_remove(__account_list_watchers, func);
}


void
account_broadcast_watchers()
{
  GList *list;
  AccountWatcherCBFunc func;

  list = __account_list_watchers;
  while (list)
    {
      func = (AccountWatcherCBFunc) list->data;
      list = list->next;
      (*func)();
    }
}


void
account_phone_list_clear(Account *account)
{
  g_assert(account != NULL);
  free_g_string_list(&account->phone_list);
}


void
account_phone_list_append(Account *account, gchar *str)
{
  g_assert(account != NULL);
  account->phone_list = g_list_append(account->phone_list, g_string_new(str));
}


void
account_dns_list_clear(Account *account)
{
  g_assert(account != NULL);
  free_g_string_list(&account->dns_list);
}


void
account_dns_list_append(Account *account, gchar *str)
{
  g_assert(account != NULL);
  account->dns_list =
    g_list_append(account->dns_list, g_string_new(str));
}


void
account_domain_list_clear(Account *account)
{
  g_assert(account != NULL);
  free_g_string_list(&account->domain_list);
}


void
account_domain_list_append(Account *account, gchar *str)
{
  g_assert(account != NULL);
  account->domain_list = 
    g_list_append(account->domain_list, g_string_new(str));
}


void
account_script_list_clear(Account *account)
{
  GList *list;
  ScriptEntry *script_entry;
  
  g_assert(account != NULL);

  list = account->script_list;
  while (list)
    {
      script_entry = (ScriptEntry *) list->data;
      list = list->next;

      script_entry_free(script_entry);
    }

  g_list_free(account->script_list);
  account->script_list = NULL;
}


void
account_script_list_append(Account *account, ScriptEntryType type, gchar *text)
{
  ScriptEntry *script_entry;

  g_assert(account != NULL);

  script_entry = script_entry_new(type, text);
  account->script_list = g_list_append(account->script_list, script_entry);
}


void 
account_save()
{
  gint i;
  void *iter;
  gchar *some_key;
  GList *list;
  GString *key = g_string_new(NULL);
  GString *value = g_string_new(NULL);
  Account *account;

  /* drop all account sections */
  iter = gnome_config_init_iterator_sections("/gnome-ppp");
  while (gnome_config_iterator_next(iter, &some_key, NULL))
    {
      int len;

      len = strlen(some_key);
      if (len >= 9 && strcmp(some_key + len - 8, ",Account") == 0)
	{
	  g_string_sprintf(key, "/gnome-ppp/%s", some_key);
	  gnome_config_clean_section(key->str);
	}
    }

  list = account_list();
  for (i = 0; list; i++)
    {
      account = (Account *) list->data;
      list = list->next;

      /* form the section string */
      g_string_sprintf(key, "/gnome-ppp/%d,Account/", i);
      gnome_config_push_prefix(key->str);
      save_account(account);
      gnome_config_pop_prefix();
    }

  /* sync the configuration file */
  gnome_config_sync();

 cleanup:
  g_string_free(key, TRUE);
  g_string_free(value, TRUE);
}


void
account_load()
{
  gint i;
  GString *key = g_string_new(NULL);
  GString *value = g_string_new(NULL);

  for (i = 0;; i++)
    {
      /* form the section string */
      g_string_sprintf(key, "/gnome-ppp/%d,Account", i);
      if (!gnome_config_has_section(key->str))
	{
	  break;
	}

      g_string_sprintf(key, "/gnome-ppp/%d,Account/", i);

      gnome_config_push_prefix(key->str);
      load_account();
      gnome_config_pop_prefix();
    }

 cleanup:
  g_string_free(key, TRUE);
  g_string_free(value, TRUE);

}


static void
save_string_list(gchar *base_key, GList *list)
{
  int i;
  GString *key, *value;

  key = g_string_new("");

  for (i = 0; list; i++)
    {
      value = (GString *) list->data;
      list = list->next;

      g_string_sprintf(key, "%d,%s", i, base_key);
      gnome_config_set_string(key->str, value->str);
    }

  g_string_free(key, TRUE);
}


static void
save_account(Account *account)
{
  gint i;
  GList *list;
  GString *key;

  key = g_string_new("");

  gnome_config_set_string("name", account->name->str);
  gnome_config_set_int("redial_attempts", account->redial_attempts);
  gnome_config_set_int("dial_timeout", account->dial_timeout);
  save_string_list("phone_number", account->phone_list);

  gnome_config_set_string("user", account->user->str);
  gnome_config_set_string("passwd", account->passwd->str);
  gnome_config_set_string("remotename", account->remotename->str);

  gnome_config_set_string("ip", account->ip->str);
  gnome_config_set_string("remote", account->remote->str);
  gnome_config_set_string("mask", account->mask->str);

  save_string_list("dns_server", account->dns_list);
  save_string_list("domain", account->domain_list);

  /* save script! */
  list = account->script_list;
  for (i = 0; list; i++)
    {
      ScriptEntry *script_entry;

      script_entry = (ScriptEntry *) list->data;
      list = list->next;

      switch (script_entry->type)
	{
	case SCRIPT_ENTRY_EXPECT:
	  g_string_sprintf(key, "%d,SE_expect", i);
	  gnome_config_set_string(key->str, script_entry->text->str);
	  break;

	case SCRIPT_ENTRY_SEND:
	  g_string_sprintf(key, "%d,SE_send", i);
	  gnome_config_set_string(key->str, script_entry->text->str);
	  break;

	case SCRIPT_ENTRY_SEND_USER:
	  g_string_sprintf(key, "%d,SE_send_user", i);
	  gnome_config_set_string(key->str, "");
	  break;

	case SCRIPT_ENTRY_SEND_PASSWORD:
	  g_string_sprintf(key, "%d,SE_send_password", i);
	  gnome_config_set_string(key->str, "");
	  break;

	case SCRIPT_ENTRY_PROMPT_INPUT:
	  g_string_sprintf(key, "%d,SE_user_input", i);
	  gnome_config_set_string(key->str, script_entry->text->str);
	  break;
	}
    }

  gnome_config_set_bool("default_route", account->default_route);
  gnome_config_set_bool("lock_device", account->lock_device);
  gnome_config_set_int("mtu", account->mtu);
  gnome_config_set_int("mru", account->mru);
  gnome_config_set_string("connect_program", 
			  account->connect_program->str);
  gnome_config_set_string("disconnect_program", 
			  account->disconnect_program->str);
  
  gnome_config_set_int("speed", account->speed);
  
  gnome_config_set_float("volume_setting", account->modem_volume);

  gnome_config_set_string("modem_device",    account->modem_device->str);
  gnome_config_set_string("modem_flow",      account->modem_flow->str);
  gnome_config_set_string("modem_term",      account->modem_term->str);
  gnome_config_set_string("init_cmd",        account->modem_init->str);
  gnome_config_set_string("dial_cmd",        account->modem_dial->str);
  gnome_config_set_string("hangup_cmd",      account->modem_hang->str);
  gnome_config_set_string("mute_cmd",        account->modem_mute->str);
  gnome_config_set_string("low_vol_cmd",     account->modem_low->str);
  gnome_config_set_string("max_vol_cmd",    account->modem_high->str);
  gnome_config_set_string("init_resp",       account->init_resp->str);
  gnome_config_set_string("hangup_resp",     account->hang_resp->str);
  gnome_config_set_string("connect_resp",    account->connect_resp->str);
  gnome_config_set_string("busy_resp",       account->busy_resp->str);
  gnome_config_set_string("noanswer_resp",   account->noanswer_resp->str);
  gnome_config_set_string("nocarrier_resp",  account->nocarrier_resp->str);
  gnome_config_set_string("nodialtone_resp", account->nodialtone_resp->str);

 cleanup:
  g_string_free(key, TRUE);
}


static void
load_account()
{
  gint i;
  gboolean junk;
  GList *list;
  GString *key;
  Account *account;
  gint dial_timeout_config;

  key = g_string_new("");
  account = account_new();

  g_string_assign(account->name, get_string_use_default("name", "Account"));
  account->redial_attempts = gnome_config_get_int("redial_attempts");
  dial_timeout_config = gnome_config_get_int("dial_timeout");
  
  /* check for a valid dial_timeout value */
  if (dial_timeout_config >= 5)
    account->dial_timeout = dial_timeout_config;
  else
    account->dial_timeout = DIALTIMEOUT;

  for (i = 0;; i++)
    {
      char *c;

      g_string_sprintf(key, "%d,phone_number", i);
      if ( (c = gnome_config_get_string(key->str)) == NULL)
	{
	  break;
	}
      account_phone_list_append(account, c);
    }

  g_string_assign(account->user, get_string_use_default("user", ""));
  g_string_assign(account->passwd, get_string_use_default("passwd", ""));
  g_string_assign(account->remotename, get_string_use_default("remotename", ""));

  g_string_assign(account->ip, get_string_use_default("ip", ""));
  g_string_assign(account->remote, get_string_use_default("remote", ""));
  g_string_assign(account->mask, get_string_use_default("mask", ""));

  for (i = 0;; i++)
    {
      char *c;

      g_string_sprintf(key, "%d,dns_server", i);
      if ( (c = gnome_config_get_string(key->str)) == NULL)
	{
	  break;
	}
      account_dns_list_append(account, c);
    }

  for (i = 0;; i++)
    {
      char *c;

      g_string_sprintf(key, "%d,domain", i);
      if ( (c = gnome_config_get_string(key->str)) == NULL)
	{
	  break;
	}
      account_domain_list_append(account, c);
    }

  /* load the login script */
  for (i = 0;; i++)
    {
      gchar *c;

      /* search for expect entries */
      g_string_sprintf(key, "%d,SE_expect", i);
      if ( (c = gnome_config_get_string(key->str)) != NULL)
	{
	  account_script_list_append(account, SCRIPT_ENTRY_EXPECT, c);
	  continue;
	}

      /* search for expect send */
      g_string_sprintf(key, "%d,SE_send", i);
      if ( (c = gnome_config_get_string(key->str)) != NULL)
	{
	  account_script_list_append(account, SCRIPT_ENTRY_SEND, c);
	  continue;
	}

      /* search for expect send user */
      g_string_sprintf(key, "%d,SE_send_user", i);
      if ( (c = gnome_config_get_string(key->str)) != NULL)
	{
	  account_script_list_append(account, SCRIPT_ENTRY_SEND_USER, "");
	  continue;
	}

      /* search for expect send password */
      g_string_sprintf(key, "%d,SE_send_password", i);
      if ( (c = gnome_config_get_string(key->str)) != NULL)
	{
	  account_script_list_append(account, SCRIPT_ENTRY_SEND_PASSWORD, "");
	  continue;
	}

      /* search for user input */
      g_string_sprintf(key, "%d,SE_user_input", i);
      if ( (c = gnome_config_get_string(key->str)) != NULL)
	{
	  account_script_list_append(account, SCRIPT_ENTRY_PROMPT_INPUT, c);
	  continue;
	}

      /* if we reach here then there's no more script */
      break;
    }

  account->default_route = gnome_config_get_bool("default_route=TRUE");
  account->lock_device = gnome_config_get_bool("lock_device=TRUE");
  account->mtu = gnome_config_get_int("mtu=296");
  account->mru = gnome_config_get_int("mru=296");
  g_string_assign(account->connect_program, 
		  get_string_use_default("connect_program", ""));
  g_string_assign(account->disconnect_program, 
		  get_string_use_default("disconnect_program", ""));		  
  g_string_assign(account->modem_device,    
                  get_string_use_default("modem_device", ""));
  g_string_assign(account->modem_flow,      
                  get_string_use_default("modem_flow", ""));
  g_string_assign(account->modem_term,      
                  get_string_use_default("modem_term", ""));
  g_string_assign(account->modem_init,      
                  get_string_use_default("init_cmd", INIT_CMD));
  g_string_assign(account->modem_dial,      
                  get_string_use_default("dial_cmd", DIAL_CMD));
  g_string_assign(account->modem_hang,      
                  get_string_use_default("hangup_cmd", HANGUP_CMD));
  g_string_assign(account->modem_mute,      
                  get_string_use_default("mute_cmd", MUTE_CMD));
  g_string_assign(account->modem_low,       
                  get_string_use_default("low_vol_cmd", LOW_VOLUME_CMD));
  g_string_assign(account->modem_high,      
                  get_string_use_default("max_vol_cmd", HIGH_VOLUME_CMD));
  g_string_assign(account->init_resp,       
                  get_string_use_default("init_resp", INIT_RESP));
  g_string_assign(account->hang_resp,       
                  get_string_use_default("hangup_resp", HANGUP_RESP));
  g_string_assign(account->connect_resp,    
                  get_string_use_default("connect_resp", CONNECT_RESP));
  g_string_assign(account->busy_resp,       
                  get_string_use_default("busy_resp", BUSY_RESP));
  g_string_assign(account->noanswer_resp,   
                  get_string_use_default("noanswer_resp", NOANSWER_RESP));
  g_string_assign(account->nocarrier_resp,  
                  get_string_use_default("nocarrier_resp", NOCARRIER_RESP));
  g_string_assign(account->nodialtone_resp, 
                  get_string_use_default("nodialtone_resp", NODIALTONE_RESP));
  account->modem_volume = gnome_config_get_float("volume_setting");
  account->speed  = gnome_config_get_int("speed");
 

 cleanup:
  g_string_free(key, TRUE);

  account_add(account);
}


/* script sturctures */
static ScriptEntry * 
script_entry_new(ScriptEntryType type, gchar *text)
{
  ScriptEntry *script_entry;

  script_entry = g_malloc(sizeof(ScriptEntry));
  script_entry->type = type;
  script_entry->text = g_string_new(text);
  return script_entry;
}


static ScriptEntry * 
script_entry_clone(ScriptEntry *script_entry)
{
  ScriptEntry *new_script_entry;

  new_script_entry = g_malloc(sizeof(ScriptEntry));
  new_script_entry->type = script_entry->type;
  new_script_entry->text = g_string_new(script_entry->text->str);
  return new_script_entry;
}


static void 
script_entry_free(ScriptEntry *script_entry)
{
  g_string_free(script_entry->text, TRUE);
  g_free(script_entry);
}


/* MISC Utility */
static gchar *
get_string_use_default(gchar *key, gchar *gstr)
{
  gchar *c;

  if ( (c = gnome_config_get_string(key)) == NULL)
    {
      return gstr;
    }

  return c;
}


static GList *
duplicate_g_string_list(GList *list)
{
  GList *new_list = NULL;
  GString *gstr;

  while (list)
    {
      gstr = (GString *) list->data;
      list = list->next;
      new_list = g_list_append(new_list, g_string_new(gstr->str));
    }

  return new_list;
}


static void
free_g_string_list(GList **list)
{
  GList *l;
  GString *gstr;

  l = *list;
  while (l)
    {
      gstr = (GString *) l->data;
      l = l->next;
      g_string_free(gstr, TRUE);
    }

  g_list_free(*list);
  *list = NULL;
}
