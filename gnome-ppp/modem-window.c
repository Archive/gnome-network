/* gnome-ppp - The GNOME PPP Dialer
 * Copyright (C) 1997 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <string.h>
#include <stdio.h>
#include <gnome.h>
#include "modem-window.h"
#include "global.h"
#include "misc.h"


typedef struct _ModemWindow ModemWindow;
struct _ModemWindow
{
  Account   *account;

  GtkWidget *property_box;

  /* modem commands */
  GtkWidget *init_cmd_entry; 
  GtkWidget *dial_cmd_entry;
  GtkWidget *hang_cmd_entry;
  GtkWidget *mute_cmd_entry;
  GtkWidget *min_cmd_entry;
  GtkWidget *max_cmd_entry;

  /* modem responses */
  GtkWidget *init_resp_entry;
  GtkWidget *hang_resp_entry;
  GtkWidget *connect_resp_entry;
  GtkWidget *busy_resp_entry;
  GtkWidget *noanswer_resp_entry;
  GtkWidget *nocarrier_resp_entry;
  GtkWidget *nodialtone_resp_entry;
  
};

/* data refreshing for the modem window */
static void refresh_data(ModemWindow *mw);
static void apply_changes_cb(GtkWidget *widget, gint page_number);
static void destroy_modem_window_cb(GtkWidget *widget);
static gint delete_modem_window_cb(GtkWidget *widget);

/* callbacks */
static void generic_entry_changed_cb(GtkWidget *widget);

/* notebook pages */
GtkWidget *create_commands_page (ModemWindow *mw);
GtkWidget *create_responses_page(ModemWindow *mw);

/* macros to set and get the class data structure */
#define MODEM_WINDOW_DATA_STR "modem_window_data"
#define get_modem_context(object)\
(gtk_object_get_data(GTK_OBJECT((object)), MODEM_WINDOW_DATA_STR))
#define set_modem_context(object, mw)\
(gtk_object_set_data(GTK_OBJECT((object)), MODEM_WINDOW_DATA_STR, (gpointer)(mw)))



void 
open_modem_window (Account *account)
{
  ModemWindow *mw;
  
  /* check to see we got a valid account */ 
  if (!account)
    {
      g_error(_("open_modem_window(): account=NULL"));
    } 

  mw = g_malloc(sizeof(ModemWindow)); 
  mw->account = account;
  
  /* GNOME property box */
  mw->property_box = gnome_property_box_new();
  set_modem_context(mw->property_box, mw);
  gtk_window_set_title(GTK_WINDOW(mw->property_box), "Modem Settings");
  
  gtk_signal_connect(
      GTK_OBJECT(mw->property_box),
      "apply",
      (GtkSignalFunc) apply_changes_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(mw->property_box),
      "destroy",
      (GtkSignalFunc) destroy_modem_window_cb,
      NULL);

  gtk_signal_connect(
      GTK_OBJECT(mw->property_box),
      "delete_event",
      (GtkSignalFunc) delete_modem_window_cb,
      NULL);
  
  /* hide unused buttons in the property box */ 
  gtk_widget_hide(GNOME_PROPERTY_BOX(mw->property_box)->help_button);
  gtk_widget_hide(GNOME_PROPERTY_BOX(mw->property_box)->apply_button);
  
  /* modem commands notebook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(mw->property_box),
      create_commands_page(mw),
      gtk_label_new(_("Commands")));
      
  /* modem response notebook page */
  gnome_property_box_append_page(
      GNOME_PROPERTY_BOX(mw->property_box),
      create_responses_page(mw),
      gtk_label_new(_("Responses")));
  
  /* set the account window's data */
  refresh_data(mw); 
  gtk_widget_show(mw->property_box);
}


static void
refresh_data(ModemWindow *mw)
{
  Account *account = mw->account;

  g_assert(account != NULL);

  /* set information on commands notebook */
  gtk_entry_set_text(GTK_ENTRY(mw->init_cmd_entry), 
                     account->modem_init->str);
  gtk_entry_set_text(GTK_ENTRY(mw->dial_cmd_entry), 
                     account->modem_dial->str);
  gtk_entry_set_text(GTK_ENTRY(mw->hang_cmd_entry), 
                     account->modem_hang->str);
  gtk_entry_set_text(GTK_ENTRY(mw->mute_cmd_entry), 
                     account->modem_mute->str);
  gtk_entry_set_text(GTK_ENTRY(mw->min_cmd_entry),  
                     account->modem_low->str);
  gtk_entry_set_text(GTK_ENTRY(mw->max_cmd_entry), 
                     account->modem_high->str);

  /* set information on responses notebook */
  gtk_entry_set_text(GTK_ENTRY(mw->init_resp_entry), 
                     account->init_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->hang_resp_entry), 
                     account->hang_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->connect_resp_entry), 
                     account->connect_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->busy_resp_entry), 
                     account->busy_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->noanswer_resp_entry), 
                     account->noanswer_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->nocarrier_resp_entry), 
                     account->nocarrier_resp->str);
  gtk_entry_set_text(GTK_ENTRY(mw->nodialtone_resp_entry), 
                     account->nodialtone_resp->str);
  
}


/*
 * apply, ok, cancel
 */
 static void
apply_changes_cb(GtkWidget *widget, gint page_number)
{
  ModemWindow *mw = get_modem_context(widget);
  Account *account = mw->account;

  /* save information from modem commands notebook */
  g_string_assign(
      account->modem_init, 
      gtk_entry_get_text(GTK_ENTRY(mw->init_cmd_entry)));
  g_string_assign(
      account->modem_dial, 
      gtk_entry_get_text(GTK_ENTRY(mw->dial_cmd_entry)));
  g_string_assign(
      account->modem_hang, 
      gtk_entry_get_text(GTK_ENTRY(mw->hang_cmd_entry)));
  g_string_assign(
      account->modem_mute, 
      gtk_entry_get_text(GTK_ENTRY(mw->mute_cmd_entry)));
  g_string_assign(
      account->modem_low, 
      gtk_entry_get_text(GTK_ENTRY(mw->min_cmd_entry)));
  g_string_assign(
      account->modem_high, 
      gtk_entry_get_text(GTK_ENTRY(mw->max_cmd_entry)));
      
  /* save information from modem responses notebook */
  g_string_assign(
      account->init_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->init_resp_entry)));
  g_string_assign(
      account->hang_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->hang_resp_entry)));
  g_string_assign(
      account->connect_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->connect_resp_entry)));
  g_string_assign(
      account->busy_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->busy_resp_entry)));
  g_string_assign(
      account->noanswer_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->noanswer_resp_entry)));
  g_string_assign(
      account->nocarrier_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->nocarrier_resp_entry)));
  g_string_assign(
      account->nodialtone_resp, 
      gtk_entry_get_text(GTK_ENTRY(mw->nodialtone_resp_entry)));                              
		
  account_save();
  account_broadcast_watchers();
} 
 

static gint
delete_modem_window_cb(GtkWidget *widget)
{
  ModemWindow *mw = get_modem_context(widget); 
  return FALSE;
}


static void
destroy_modem_window_cb(GtkWidget *widget)
{  
  ModemWindow *mw = get_modem_context(widget);
  g_free(mw);
} 


/*
 * notebook pages
 */
GtkWidget *
create_commands_page(ModemWindow *mw)
{
  GtkWidget *vbox, *table, *label;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  table = gtk_table_new(6, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  /* initialization command entry widgets */ 
  label = gtk_label_new(_("Initialization"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->init_cmd_entry = gtk_entry_new();
  set_modem_context(mw->init_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->init_cmd_entry, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->init_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);

  gtk_widget_show(mw->init_cmd_entry);
  
  /* dial command entry widgets */
  label = gtk_label_new(_("Dial"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->dial_cmd_entry = gtk_entry_new();
  set_modem_context(mw->dial_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->dial_cmd_entry, 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_signal_connect(
      GTK_OBJECT(mw->dial_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);

  gtk_widget_show(mw->dial_cmd_entry);
  
  /* hangup command entry widgets */
  label = gtk_label_new(_("Hangup"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->hang_cmd_entry = gtk_entry_new();
  set_modem_context(mw->hang_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->hang_cmd_entry, 1, 2, 2, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->hang_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);

  gtk_widget_show(mw->hang_cmd_entry);

  /* volume mute command entry widgets */  
  label = gtk_label_new(_("Volume Mute"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->mute_cmd_entry = gtk_entry_new();
  set_modem_context(mw->mute_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->mute_cmd_entry, 1, 2, 3, 4, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->mute_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);
      
  gtk_widget_show(mw->mute_cmd_entry);
  
  /* volume low command entry widgets */
  label = gtk_label_new(_("Volume Low"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->min_cmd_entry = gtk_entry_new();
  set_modem_context(mw->min_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->min_cmd_entry, 1, 2, 4, 5, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->min_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);
      
  gtk_widget_show(mw->min_cmd_entry);

  /* volume high command entry widgets */
  label = gtk_label_new(_("Volume High"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 5, 6,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->max_cmd_entry = gtk_entry_new();
  set_modem_context(mw->max_cmd_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->max_cmd_entry, 1, 2, 5, 6, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->max_cmd_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);

  gtk_widget_show(mw->max_cmd_entry);
    
  return vbox;
}


GtkWidget *
create_responses_page(ModemWindow *mw)
{
  GtkWidget *vbox, *table, *label;

  /* main return widget */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_border_width(GTK_CONTAINER(vbox), CONTAINER_BORDER);
  gtk_box_set_spacing(GTK_BOX(vbox), BOX_SPACING);
  gtk_widget_show(vbox);

  table = gtk_table_new(7, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);
  gtk_table_set_row_spacings(GTK_TABLE(table), TABLE_ROW_SPACINGS);
  gtk_table_set_col_spacings(GTK_TABLE(table), TABLE_COL_SPACINGS);
  gtk_widget_show(table);

  /* initialization response entry widgets */
  label = gtk_label_new(_("Initialization"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->init_resp_entry = gtk_entry_new();
  set_modem_context(mw->init_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->init_resp_entry, 1, 2, 0, 1, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->init_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->init_resp_entry);
    
  /* hangup response entry widgets */  
  label = gtk_label_new(_("Hangup"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->hang_resp_entry = gtk_entry_new();
  set_modem_context(mw->hang_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->hang_resp_entry, 1, 2, 1, 2, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->hang_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->hang_resp_entry);
  
  /* connection response entry widgets */
  label = gtk_label_new(_("Connection"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->connect_resp_entry = gtk_entry_new();
  set_modem_context(mw->connect_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->connect_resp_entry, 1, 2, 2, 3, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->connect_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL);
 
  gtk_widget_show(mw->connect_resp_entry); 
  
  /* busy response entry widgets */
  label = gtk_label_new(_("Busy"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->busy_resp_entry = gtk_entry_new();
  set_modem_context(mw->busy_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->busy_resp_entry, 1, 2, 3, 4, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->busy_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->busy_resp_entry); 
  
  /* no answer response entry widgets */
  label = gtk_label_new(_("No Answer"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->noanswer_resp_entry = gtk_entry_new();
  set_modem_context(mw->noanswer_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->noanswer_resp_entry, 1, 2, 4, 5, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->noanswer_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->noanswer_resp_entry); 
  
  /* no carrier response entry widgets */
  label = gtk_label_new(_("No Carrier"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 5, 6,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->nocarrier_resp_entry = gtk_entry_new();
  set_modem_context(mw->nocarrier_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->nocarrier_resp_entry, 1, 2, 5, 6, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->nocarrier_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->nocarrier_resp_entry); 
  
  /* no dialtone response entry widgets */
  label = gtk_label_new(_("No Dialtone"));
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 6, 7,
		   GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show(label);
  
  mw->nodialtone_resp_entry = gtk_entry_new();
  set_modem_context(mw->nodialtone_resp_entry, mw);
  gtk_table_attach(GTK_TABLE(table), mw->nodialtone_resp_entry, 1, 2, 6, 7, 
		   GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);

  gtk_signal_connect(
      GTK_OBJECT(mw->nodialtone_resp_entry),
      "changed",
      GTK_SIGNAL_FUNC(generic_entry_changed_cb),
      NULL); 

  gtk_widget_show(mw->nodialtone_resp_entry); 
 
  return vbox;
}

/*
 * callbacks
 */
static void
generic_entry_changed_cb(GtkWidget *widget)
{
  ModemWindow *mw = get_modem_context(widget);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(mw->property_box));
}
