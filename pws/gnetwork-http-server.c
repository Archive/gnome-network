/* 
 * GNOME NETWORK library
 * Http Server
 *
 * AUTHORS:
 *      �lvaro Pe�a <apg@esware.com>
 *
 * Copyright (C) 2003 The GNOME Foundation.
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gnetwork-http-server.h"
#include <cherokee.h>

#define PARENT_TYPE G_TYPE_OBJECT

struct _GNetworkHttpServerPrivate {
	GNetworkHttpServerConfig *config;
	cherokee_server_t        *cserver;
	guint                     idle_id;
};

static void     gnetwork_http_server_init        (GNetworkHttpServer *server);
static void     gnetwork_http_server_class_init  (GNetworkHttpServer *klass);
static void     gnetwork_http_server_finalize    (GObject            *object);
static gboolean gnetwork_http_server_reconfigure (GNetworkHttpServer *server);
static void     gnetwork_http_server_idle_func   (GNetworkHttpServer *server);

static GObjectClass *parent_class = NULL;

/**
 * GNetworkHttpServer class implementation
 */

GType
gnetwork_http_server_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GNetworkHttpServerClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnetwork_http_server_class_init,
			(GClassFinalizeFunc) NULL,
			(gconstpointer) NULL,
			sizeof (GNetworkHttpServer),
			(guint16) 0,
			(GInstanceInitFunc) gnetwork_http_server_init
		};
		type = g_type_register_static (PARENT_TYPE,
					       "GNetworkHttpServer",
					       &info, 0);
	}
	return type;
}

static void
gnetwork_http_server_init (GNetworkHttpServer *server)
{
	
	g_return_if_fail (GNETWORK_IS_HTTP_SERVER (server));
	
	server->priv = g_new0 (GNetworkHttpServerPrivate, 1);
}

static void
gnetwork_http_server_class_init (GNetworkHttpServer *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gnetwork_http_server_finalize;
}

static void
gnetwork_http_server_finalize (GObject *object)
{
	GNetworkHttpServer *server = GNETWORK_HTTP_SERVER (object);

	if (server->priv->idle_id)
		g_source_remove (server->priv->idle_id);
	
	if (server->priv->cserver)
		cherokee_server_free (server->priv->cserver);
	
	if (server->priv) {
		g_free (server->priv);
		server->priv = NULL;
	}

	if (G_OBJECT_CLASS (parent_class)->finalize) {
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
	}
}

static gboolean
gnetwork_http_server_reconfigure (GNetworkHttpServer *server)
{
	cherokee_server_t *cserver;
	gchar             *buf;

	g_return_val_if_fail (GNETWORK_IS_HTTP_SERVER (server), FALSE);
	g_return_val_if_fail (server->priv->config != NULL, FALSE);

	cherokee_server_new (&cserver);
	server->priv->cserver = cserver;
	buf = g_strdup_printf ("Port %d\n"
			       "DocumentRoot %s\n"
			       "KeepAlive %s\n"
			       "Timeout %d\n"
			       "Directory / { Handler common }\n",
			       server->priv->config->port,
			       server->priv->config->document_root,
			       server->priv->config->keepalive ? "On" : "Off",
			       server->priv->config->timeout);
	cherokee_server_read_config_string (server->priv->cserver, g_strdup (buf));
}

static void
gnetwork_http_server_idle_func (GNetworkHttpServer *server)
{
	g_return_if_fail (GNETWORK_IS_HTTP_SERVER (server));
	
	cherokee_server_step (server->priv->cserver);
}

/**
 * gnetwork_http_server_new
 *
 * Create a new #GNetworkHttpServer object
 *
 * Returns: a pointer to the new #GNetworkHttpServer object, or NULL on error
 */
GNetworkHttpServer*
gnetwork_http_server_new (GNetworkHttpServerConfig *config)
{
	GNetworkHttpServer *server;

	server = g_object_new (GNETWORK_TYPE_HTTP_SERVER, NULL);
	if (config) {
		gnetwork_http_server_set_config (server, config);
		gnetwork_http_server_reconfigure (server);
	}
	
	return server;
}

gboolean
gnetwork_http_server_set_config (GNetworkHttpServer       *server,
				 GNetworkHttpServerConfig *config)
{
	g_return_val_if_fail (GNETWORK_IS_HTTP_SERVER (server), FALSE);
	g_return_val_if_fail (config != NULL, FALSE);
	
	server->priv->config = config;
	return TRUE;
}

GNetworkHttpServerConfig*
gnetwork_http_server_get_config (GNetworkHttpServer *server)
{
	g_return_val_if_fail (GNETWORK_IS_HTTP_SERVER (server), NULL);
	
	return server->priv->config;
}

gboolean
gnetwork_http_server_start (GNetworkHttpServer *server)
{
	ret_t ret;
	
	g_return_val_if_fail (GNETWORK_HTTP_SERVER (server), FALSE);
	g_return_val_if_fail (server->priv->config != NULL, FALSE);
	if (!server->priv->cserver) {
		gnetwork_http_server_reconfigure (server);
	}
	cherokee_server_set_min_latency (server->priv->cserver, 50);
	ret = cherokee_server_init (server->priv->cserver);
	if (ret != ret_ok)
		return FALSE;
	else {
		server->priv->idle_id = g_idle_add ((GSourceFunc) gnetwork_http_server_idle_func, server);
		return TRUE;
	}
}

void
gnetwork_http_server_stop (GNetworkHttpServer *server)
{
	g_return_if_fail (GNETWORK_IS_HTTP_SERVER (server));
	g_return_if_fail (server->priv->cserver != NULL);

	if (server->priv->idle_id)
		g_source_remove (server->priv->idle_id);

	cherokee_server_free (server->priv->cserver);
	server->priv->cserver = NULL;
}
