/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include "gnome-db-gray-bar.h"

#define PARENT_TYPE GTK_TYPE_BIN

struct _GnomeDbGrayBarPrivate {
	GtkWidget *hbox;
	GtkWidget *icon;
	gboolean   show_icon;
	GtkWidget *label;
};

static void gnome_db_gray_bar_class_init   (GnomeDbGrayBarClass *klass);
static void gnome_db_gray_bar_init         (GnomeDbGrayBar      *bar,
					    GnomeDbGrayBarClass *klass);
static void gnome_db_gray_bar_realize      (GtkWidget           *widget);
static void gnome_db_gray_bar_size_request (GtkWidget           *widget,
					    GtkRequisition      *requisition);
static void gnome_db_gray_bar_allocate     (GtkWidget           *widget,
					    GtkAllocation       *allocation);
static void gnome_db_gray_bar_paint        (GtkWidget           *widget,
					    GdkRectangle        *area);
static gint gnome_db_gray_bar_expose       (GtkWidget           *widget,
					    GdkEventExpose      *event);
static void gnome_db_gray_bar_style_set    (GtkWidget           *w,
					    GtkStyle            *previous_style);
static void gnome_db_gray_bar_set_property (GObject             *object,
					    guint                paramid,
					    const GValue        *value,
					    GParamSpec          *pspec);
static void gnome_db_gray_bar_get_property (GObject             *object,
					    guint                param_id,
					    GValue              *value,
					    GParamSpec          *pspec);
static void gnome_db_gray_bar_finalize     (GObject             *object);

enum {
	PROP_0,
	PROP_TEXT,
	PROP_SHOW_ICON
};

static GObjectClass *parent_class = NULL;

/*
 * GnomeDbGrayBar class implementation
 */

static void
gnome_db_gray_bar_realize (GtkWidget *widget)
{
	GdkWindowAttr attributes;
	gint attributes_mask;
	gint border_width;
	
	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
	
	border_width = GTK_CONTAINER (widget)->border_width;
	
	attributes.x = widget->allocation.x + border_width;
	attributes.y = widget->allocation.y + border_width;
	attributes.width = widget->allocation.width - 2*border_width;
	attributes.height = widget->allocation.height - 2*border_width;
	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);
	attributes.event_mask = gtk_widget_get_events (widget)
		| GDK_BUTTON_MOTION_MASK
		| GDK_BUTTON_PRESS_MASK
		| GDK_BUTTON_RELEASE_MASK
		| GDK_EXPOSURE_MASK
		| GDK_ENTER_NOTIFY_MASK
		| GDK_LEAVE_NOTIFY_MASK;
	
	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
	
	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);
	
	widget->style = gtk_style_attach (widget->style, widget->window);
	gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
}

static void
gnome_db_gray_bar_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	GtkBin *bin = GTK_BIN (widget);
	GtkRequisition child_requisition;
	
	requisition->width = GTK_CONTAINER (widget)->border_width * 2;
	requisition->height = GTK_CONTAINER (widget)->border_width * 2;
	
	if (bin->child && GTK_WIDGET_VISIBLE (bin->child)) {
		gtk_widget_size_request (bin->child, &child_requisition);
		
		requisition->width += child_requisition.width;
		requisition->height += child_requisition.height;
	}
}

static void
gnome_db_gray_bar_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	GtkBin *bin;
	GtkAllocation child_allocation;
	
	widget->allocation = *allocation;
	bin = GTK_BIN (widget);
	
	child_allocation.x = 0;
	child_allocation.y = 0;
	child_allocation.width = MAX (allocation->width - GTK_CONTAINER (widget)->border_width * 2, 0);
	child_allocation.height = MAX (allocation->height - GTK_CONTAINER (widget)->border_width * 2, 0);
	
	if (GTK_WIDGET_REALIZED (widget)) {
		gdk_window_move_resize (widget->window,
					allocation->x + GTK_CONTAINER (widget)->border_width,
					allocation->y + GTK_CONTAINER (widget)->border_width,
					child_allocation.width,
					child_allocation.height);
	}
	
	if (bin->child)
		gtk_widget_size_allocate (bin->child, &child_allocation);
}

static void
gnome_db_gray_bar_style_set (GtkWidget *w, GtkStyle *previous_style)
{
	static int in_style_set = 0;
	GtkStyle   *style;

	if (in_style_set > 0)
                return;

        in_style_set ++;

	style = gtk_rc_get_style (GTK_WIDGET (w));
	gtk_widget_modify_bg (GTK_WIDGET (w), GTK_STATE_NORMAL, &style->bg[GTK_STATE_ACTIVE]);

	in_style_set --;

	GTK_WIDGET_CLASS (parent_class)->style_set (w, previous_style);
}

static void
gnome_db_gray_bar_paint (GtkWidget *widget, GdkRectangle *area)
{
	if (!GTK_WIDGET_APP_PAINTABLE (widget)) {
		gtk_paint_flat_box (widget->style, widget->window,
				    widget->state, GTK_SHADOW_NONE,
				    area, widget, "gnomedbgraybar",
				    1, 1,
				    (widget->allocation.width - 2), 
				    (widget->allocation.height - 2));
		
		gdk_draw_rectangle (widget->window,
				    widget->style->black_gc,
				    FALSE,
				    0,
				    0,
				    (widget->allocation.width - 1),
				    (widget->allocation.height - 1));
	}
	
}

static gboolean
gnome_db_gray_bar_expose (GtkWidget *widget, GdkEventExpose *event)
{   
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GNOME_DB_IS_GRAY_BAR (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);
	
	if (event->count > 0)
		return FALSE;
	
	if (GTK_WIDGET_DRAWABLE (widget)) {
		gnome_db_gray_bar_paint (widget, &event->area);
		
		(* GTK_WIDGET_CLASS (parent_class)->expose_event) (widget, event);
	}

	return FALSE;
}

static void
gnome_db_gray_bar_class_init (GnomeDbGrayBarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->set_property  = gnome_db_gray_bar_set_property;
	object_class->get_property  = gnome_db_gray_bar_get_property;
	object_class->finalize      = gnome_db_gray_bar_finalize;
	widget_class->style_set     = gnome_db_gray_bar_style_set;
	widget_class->realize       = gnome_db_gray_bar_realize;
	widget_class->size_request  = gnome_db_gray_bar_size_request;
	widget_class->size_allocate = gnome_db_gray_bar_allocate;
	widget_class->expose_event  = gnome_db_gray_bar_expose;

	/* add class properties */
	g_object_class_install_property (
		object_class, PROP_TEXT,
		g_param_spec_string ("text", NULL, NULL, NULL,
				     (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (
		object_class, PROP_SHOW_ICON,
		g_param_spec_string ("show_icon", NULL, NULL, NULL,
				     (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
gnome_db_gray_bar_init (GnomeDbGrayBar *bar, GnomeDbGrayBarClass *klass)
{
	GTK_WIDGET_UNSET_FLAGS (bar, GTK_NO_WINDOW);
	
	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));
	
	bar->priv = g_new0 (GnomeDbGrayBarPrivate, 1);
	
	bar->priv->hbox = gtk_hbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (bar->priv->hbox), 6);
	
	bar->priv->show_icon = FALSE;
	bar->priv->icon = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (bar->priv->icon), 0.5, 0.0);
	gtk_widget_hide (bar->priv->icon);
	gtk_box_pack_start (GTK_BOX (bar->priv->hbox), bar->priv->icon,
			    FALSE, TRUE, 0);
	
	bar->priv->label = gtk_label_new ("");
	gtk_label_set_selectable (GTK_LABEL (bar->priv->label), FALSE);
	gtk_misc_set_alignment (GTK_MISC (bar->priv->label), 0.00, 0.0);
	gtk_box_pack_end (GTK_BOX (bar->priv->hbox), bar->priv->label,
			  TRUE, TRUE, 0);
	gtk_widget_show (bar->priv->label);
	
	gtk_widget_show (bar->priv->hbox);
	gtk_container_add (GTK_CONTAINER (bar), bar->priv->hbox);
}

static void
gnome_db_gray_bar_set_property (GObject *object,
				guint param_id,
				const GValue *value,
				GParamSpec *pspec)
{
	GnomeDbGrayBar *bar = (GnomeDbGrayBar *) object;

	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));

	switch (param_id) {
	case PROP_TEXT :
		gnome_db_gray_bar_set_text (bar, g_value_get_string (value));
		break;
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_gray_bar_get_property (GObject *object,
				guint param_id,
				GValue *value,
				GParamSpec *pspec)
{
	GnomeDbGrayBar *bar = (GnomeDbGrayBar *) object;

	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));

	switch (param_id) {
	case PROP_TEXT :
		g_value_set_string (value, gnome_db_gray_bar_get_text (bar));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_gray_bar_finalize (GObject *object)
{
	GnomeDbGrayBar *bar = (GnomeDbGrayBar *) object;

	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));

	if (bar->priv) {
		bar->priv->label = NULL;
		bar->priv->icon = NULL;
		bar->priv->hbox = NULL;

		g_free (bar->priv);
		bar->priv = NULL;
	}

	parent_class->finalize (object);
}

GType
gnome_db_gray_bar_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GnomeDbGrayBarClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_gray_bar_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbGrayBar),
			0,
			(GInstanceInitFunc) gnome_db_gray_bar_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbGrayBar", &info, 0);
	}
	return type;
}

/**
 * gnome_db_gray_bar_new
 */
GtkWidget *
gnome_db_gray_bar_new (const gchar *label)
{
	GnomeDbGrayBar *bar;

	bar = g_object_new (GNOME_DB_TYPE_GRAY_BAR, NULL);
	gnome_db_gray_bar_set_text (bar, label);

	return GTK_WIDGET (bar);
}

/**
 * gnome_db_gray_bar_get_text
 * @bar: A #GnomeDbGrayBar widget.
 *
 * Get the text being displayed in the given gray bar widget. This
 * does not include any embedded underlines indicating mnemonics or
 * Pango markup.
 *
 * Returns: the text in the widget.
 */
const gchar *
gnome_db_gray_bar_get_text (GnomeDbGrayBar *bar)
{
	gchar *text;
	
	g_return_val_if_fail (GNOME_DB_IS_GRAY_BAR (bar), NULL);

	text = gtk_label_get_text (GTK_LABEL (bar->priv->label));

	return (const gchar *) text;
}

/**
 * gnome_db_gray_bar_set_text
 * @bar: A #GnomeDbGrayBar widget
 *
 * Set the text displayed in the given gray bar widget. This can include 
 * embedded underlines indicating mnemonics or Pango markup.
 * 
 */
void
gnome_db_gray_bar_set_text (GnomeDbGrayBar *bar, const gchar *text)
{
	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));

	gtk_label_set_markup (GTK_LABEL (bar->priv->label), text);
}

/**
 * gnome_db_gray_set_icon_from_file
 * @bar: A #GnomeDbGrayBar widget.
 * @file: filename.
 *
 * Set the icon displayed in the given gray bar widget. This can include 
 * embedded underlines indicating mnemonics or Pango markup.
 * 
 */
void
gnome_db_gray_bar_set_icon_from_file (GnomeDbGrayBar *bar, const gchar *file)
{
	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));
	
	gtk_image_set_from_file (GTK_IMAGE (bar->priv->icon), file);
}

/**
 * gnome_db_gray_set_icon_from_stock
 * @bar: a #GnomeDbGrayBar widget.
 * @stock_id: a stock icon name.
 * @size: a tock icon size.
 *
 * Set the icon using a stock icon for the given gray bar.
 */
void
gnome_db_gray_bar_set_icon_from_stock (GnomeDbGrayBar *bar, const gchar *stock_id, GtkIconSize size)
{
	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));
	
	gtk_image_set_from_stock (GTK_IMAGE (bar->priv->icon), stock_id, size);
}

/**
 * gnome_db_gray_bar_set_show_icon
 * @bar: a #GnomeDbGrayBar widget.
 * @show: whether to show the icon or not.
 *
 * Set the icon displaying mode for the given grid.
 */
void
gnome_db_gray_bar_set_show_icon (GnomeDbGrayBar *bar, gboolean show)
{
	g_return_if_fail (GNOME_DB_IS_GRAY_BAR (bar));

	if (show) {
		gtk_widget_show (bar->priv->icon);
		bar->priv->show_icon = TRUE;
	} else {
		gtk_widget_hide (bar->priv->icon);
		bar->priv->show_icon = FALSE;
	}
}

/**
 * gnome_db_gray_bar_get_show_icon
 * @bar: a #GnomeDbGrayBar widget.
 *
 * Get whether the icon is being shown for the given gray bar.
 *
 * Returns: TRUE if the icon is shown, FALSE if not.
 */
gboolean
gnome_db_gray_bar_get_show_icon (GnomeDbGrayBar *bar)
{
	g_return_val_if_fail (GNOME_DB_IS_GRAY_BAR (bar), FALSE);

	return bar->priv->show_icon;	
}
