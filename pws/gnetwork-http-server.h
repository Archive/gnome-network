/* 
 * GNOME NETWORK library
 * Http Server
 *
 * AUTHORS:
 *	�lvaro Pe�a <apg@esware.com>
 *
 * Copyright (C) 2003 The GNOME Foundation.
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNETWORK_HTTP_SERVER_H__
#define __GNETWORK_HTTP_SERVER_H__

#include <glib-object.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GNETWORK_TYPE_HTTP_SERVER            (gnetwork_http_server_get_type())
#define GNETWORK_HTTP_SERVER(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNETWORK_TYPE_HTTP_SERVER, GNetworkHttpServer))
#define GNETWORK_HTTP_SERVER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNETWORK_TYPE_HTTP_SERVER, GNetworkHttpServerClass))
#define GNETWORK_IS_HTTP_SERVER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNETWORK_TYPE_HTTP_SERVER))
#define GNETWORK_IS_HTTP_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNETWORK_TYPE_HTTP_SERVER))

typedef struct _GNetworkHttpServer        GNetworkHttpServer;
typedef struct _GNetworkHttpServerClass   GNetworkHttpServerClass;
typedef struct _GNetworkHttpServerPrivate GNetworkHttpServerPrivate;
typedef struct _GNetworkHttpServerConfig  GNetworkHttpServerConfig;

struct _GNetworkHttpServer {
	GObject parent;

	GNetworkHttpServerPrivate *priv;
};

struct _GNetworkHttpServerClass {
	GObjectClass parent_class;
};

struct _GNetworkHttpServerConfig {
	gchar    *document_root;
	gint      port;
	gboolean  keepalive;
	gint      timeout;
};

GType                     gnetwork_http_server_get_type   (void);
GNetworkHttpServer*       gnetwork_http_server_new        (GNetworkHttpServerConfig *config);
gboolean                  gnetwork_http_server_set_config (GNetworkHttpServer *server, GNetworkHttpServerConfig *config);
GNetworkHttpServerConfig* gnetwork_http_server_get_config (GNetworkHttpServer *server);
gboolean                  gnetwork_http_server_start      (GNetworkHttpServer *server);
void                      gnetwork_http_server_stop       (GNetworkHttpServer *server);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNETWORK_HTTP_SERVER_H__ */
