/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* GNOME Personal Web Server Preferences Widget
 *
 * Authors:
 *	�lvaro Pe�a <apg@esware.com>
 *
 * Copyright (C) 2003 GNOME Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __PWS_PREFERENCES_WIDGET_H__
#define __PWS_PREFERENCES_WIDGET_H__

#include <gtk/gtkvbox.h>
#include "gnetwork-http-server.h"

G_BEGIN_DECLS

#define PWS_TYPE_PREFERENCES_WIDGET            (pws_preferences_widget_get_type ())
#define PWS_PREFERENCES_WIDGET(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, PWS_TYPE_PREFERENCES_WIDGET, PwsPreferencesWidget))
#define PWS_PREFERENCES_WIDGET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, PWS_TYPE_PREFERENCES_WIDGET, PwsPreferencesWidgetClass))
#define PWS_IS_PREFERENCES_WIDGET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, PWS_TYPE_PREFERENCES_WIDGET))
#define PWS_IS_PREFERENCES_WIDGET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PWS_TYPE_PREFERENCES_WIDGET))

typedef struct _PwsPreferencesWidget        PwsPreferencesWidget;
typedef struct _PwsPreferencesWidgetClass   PwsPreferencesWidgetClass;
typedef struct _PwsPreferencesWidgetPrivate PwsPreferencesWidgetPrivate;

struct _PwsPreferencesWidget {
	GtkVBox parent;
	PwsPreferencesWidgetPrivate *priv;
};

struct _PwsPreferencesWidgetClass {
	GtkVBoxClass parent_class;
};

GType                     pws_preferences_widget_get_type            (void);
GtkWidget*                pws_preferences_widget_new                 (GNetworkHttpServerConfig *config,
								      gchar                    *gconf_path);
void                      pws_preferences_widget_set_show_additional (PwsPreferencesWidget     *pref,
								      gboolean                  show);
gboolean                  pws_preferences_widget_set_server_config   (PwsPreferencesWidget     *pref,
								      GNetworkHttpServerConfig *config);
GNetworkHttpServerConfig* pws_preferences_widget_get_server_config   (PwsPreferencesWidget     *pref);
gboolean                  pws_preferences_widget_set_gconf_config    (PwsPreferencesWidget     *pref,
								      gchar                    *gconf_path);

G_END_DECLS

#endif
