/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* GNOME Personal Web Server
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *	�lvaro Pe�a <apg@esware.com>
 *
 * Copyright (C) 2003 GNOME Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnome.h>
#include <gconf/gconf-client.h>
#include "eggtrayicon.h"
#include "gnetwork-http-server.h"
#include "pws-preferences-widget.h"
#include "gnome-db-gray-bar.h"

#define PWS_STOCK_RUNNING GTK_STOCK_YES
#define PWS_STOCK_STOPPED GTK_STOCK_NO

static gboolean docklet_create_cb      (gpointer        foo);
static void     docklet_destroyed_cb   (GtkWidget      *widget,
					void           *data);
static void     docklet_update_icon    (char           *icon_name);
static void     start_server           (void);
static void     stop_server            (void);
static void     docklet_embedded_cb    (GtkWidget      *widget,
					void           *data);
void            set_preferences_cb     (GtkButton      *button,
					GtkWidget      *window);
static void     popup_preferences_cb   (gpointer        callback_data,
					guint           action,
					GtkWidget      *widget);
static void	popup_browse_files_cb  (gpointer	callback_data,
					guint		action,
					GtkWidget      *widget);
static void     popup_exit_cb          (gpointer        callback_data,
					guint           action,
					GtkWidget      *widget);
static void     popup_menu_position_cb (GtkMenu        *menu,
					gint           *x,
					gint           *y,
					gboolean       *push_in,
					gpointer        data);
static void     docklet_clicked_cb     (GtkWidget      *button,
					GdkEventButton *event,
					void           *data);
static void     docklet_destroy        (void);
static void     docklet_create         (void);
static char*    item_factory_trans_cb  (const gchar    *path,
					gpointer        data);

GNetworkHttpServerConfig* get_config_from_gconf (void);

/*
 * globals for GTK+
 */
static EggTrayIcon *docklet = NULL;
static GtkWidget   *image   = NULL;
GtkItemFactory     *popup_factory = NULL;

#define GIF_CB(x) ((GtkItemFactoryCallback)(x))

static GtkItemFactoryEntry popup_items[] = {
	{ N_("/_Preferences"),  NULL, GIF_CB (popup_preferences_cb), 0, "<StockItem>",  GTK_STOCK_PREFERENCES },
	{ N_("/_Open Web Folder"), NULL, GIF_CB (popup_browse_files_cb), 0, "<StockItem>", GTK_STOCK_OPEN }, 
	{ "/sep1",              NULL, 0,                             0, "<Separator>",  NULL },
	{ N_("/_Quit"),         NULL, GIF_CB (popup_exit_cb),        0, "<StockItem>",  GTK_STOCK_QUIT }
};

/*
 * globals for libgnetwork
 */
static GNetworkHttpServer *server  = NULL;

/*
 * globals for configuration
 */
GConfClient *gconf_client = NULL;
guint        gconf_notify_id;

static gboolean
docklet_create_cb (gpointer foo)
{
        docklet_create ();
        return FALSE; 
}

static void
docklet_destroyed_cb(GtkWidget *widget, void *data)
{
        g_object_unref (G_OBJECT (docklet));
        docklet = NULL;
	
        g_idle_add (docklet_create_cb, NULL);
}

static void
docklet_update_icon (char *icon_name)
{
	gtk_image_set_from_stock (GTK_IMAGE (image), icon_name, GTK_ICON_SIZE_LARGE_TOOLBAR);
}

static void
start_server (void)
{
	if (gnetwork_http_server_start (server)) {
		docklet_update_icon (PWS_STOCK_RUNNING);
	} else {
		g_message ("Can't start the server");
	}
}


static void
stop_server (void)
{
	if (server) {
		gnetwork_http_server_stop (server);
		docklet_update_icon (PWS_STOCK_STOPPED);
	}
}


static void
docklet_embedded_cb(GtkWidget *widget, void *data)
{
	if (gconf_client_get_bool (gconf_client, "/apps/gnome-pws/prefs/running", NULL))
		start_server ();
	else
		stop_server ();
}

void
set_preferences_cb (GtkButton *button, GtkWidget *window)
{
 	GNetworkHttpServerConfig *config;
	
	gtk_widget_destroy (window);

	/* If it's running, restart with the gconf configuration */
	if (gconf_client_get_bool (gconf_client, "/apps/gnome-pws/prefs/running", NULL)) {
		stop_server ();
		config = get_config_from_gconf ();
		gnetwork_http_server_set_config (server, config);
		start_server ();
	}
}

static void
popup_preferences_cb (gpointer callback_data, guint action, GtkWidget *widget)
{
	GtkWidget *window, *vbox, *bar, *preferences;
	GtkWidget *button_box, *close;
	GdkPixbuf *pixbuf;
	gchar     *icon_path;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), _("Personal web server preferences"));
	gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (window), 12);
	icon_path = g_build_filename (ICONDIR, "gnome-pws.png", NULL);
	pixbuf = gdk_pixbuf_new_from_file (icon_path, NULL);
	if (pixbuf != NULL)
		gtk_window_set_icon (GTK_WINDOW (window), pixbuf);
	g_free (icon_path);
	
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	gtk_widget_show (vbox);

	
	bar = gnome_db_gray_bar_new (g_strdup_printf ("<big><b>%s</b></big>\n%s",
						      _("Personal Web Server Preferences"),
						      _("Setup the options for your personal web server.")));
	gnome_db_gray_bar_set_icon_from_file (GNOME_DB_GRAY_BAR (bar), ICONDIR "/gnome-pws.png");
	gnome_db_gray_bar_set_show_icon (GNOME_DB_GRAY_BAR (bar), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), bar, TRUE, TRUE, 0);
	gtk_widget_show (bar);

	preferences = pws_preferences_widget_new (NULL, "/apps/gnome-pws/prefs");
	gtk_box_pack_start (GTK_BOX (vbox), preferences, TRUE, TRUE, 0);
	gtk_widget_show (preferences);

	button_box = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (button_box), 6);
	gtk_box_pack_start (GTK_BOX (vbox), button_box, FALSE, FALSE, 0);
	gtk_widget_show (button_box);

	close = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
	g_signal_connect (GTK_OBJECT (close), "clicked",
			  G_CALLBACK (set_preferences_cb), window);
	gtk_box_pack_start (GTK_BOX (button_box), close, FALSE, FALSE, 0);
	gtk_widget_show (close);

	gtk_widget_show (window);
}

/* callback to open a nautilus window to the current directory */
static void
popup_browse_files_cb (gpointer callback_data, guint action, GtkWidget *widget)
{
	GNetworkHttpServerConfig *config;
	gchar *command_line;
	GError *error = NULL;

	config = get_config_from_gconf ();
	command_line = g_strdup_printf ("nautilus %s", config->document_root);
	g_spawn_command_line_async (command_line, &error);

	if (error != NULL)
		g_print ("Error opening document root\n");

	g_clear_error (&error);
}

static void
popup_exit_cb (gpointer callback_data, guint action, GtkWidget *widget)
{
	if (gconf_client_get_bool (gconf_client, "/apps/gnome-pws/prefs/running", NULL))
		stop_server ();

	docklet_destroy ();

	gtk_main_quit ();
}

static void
popup_menu_position_cb (GtkMenu  *menu,
			gint     *x,
			gint     *y,
			gboolean *push_in,
			gpointer  data)
{
	GtkWidget      *w = data;
	GtkRequisition  requisition;
	gint            wx, wy;

	g_return_if_fail (w != NULL);

	gtk_widget_size_request (GTK_WIDGET (menu), &requisition);

	gdk_window_get_origin (w->window, &wx, &wy);

	if (*x < wx)
		*x = wx;
	else if (*x > wx + w->allocation.width)
		*x = wx + w->allocation.width;

	if (*x + requisition.width > gdk_screen_width())
		*x = gdk_screen_width() - requisition.width;

	if (*y < wy)
		*y = wy;
	 else if (*y > wy + w->allocation.height)
		*y = wy + w->allocation.height;

	if (*y + requisition.height > gdk_screen_height())
		*y = gdk_screen_height() - requisition.height;

	*push_in = TRUE;
}

static void
docklet_clicked_cb (GtkWidget *button, GdkEventButton *event, void *data)
{
	GtkWidget *menu;
	gboolean   running;
	
        if (event->type != GDK_BUTTON_PRESS) {
                return;
	}

	switch (event->button) {
	case 1:
		running = gconf_client_get_bool (gconf_client, "/apps/gnome-pws/prefs/running", NULL);
		gconf_client_set_bool (gconf_client, "/apps/gnome-pws/prefs/running", !running, NULL);
		break;
	case 3:
		menu = gtk_item_factory_get_widget (popup_factory, "");
		gtk_menu_popup (GTK_MENU (menu),
				NULL,
				NULL,
				popup_menu_position_cb,
				image,
				event->button,
				event->time);
		break;
	}
}


static void
docklet_destroy (void)
{
        g_signal_handlers_disconnect_by_func (G_OBJECT (docklet), G_CALLBACK (docklet_destroyed_cb), NULL);

	gconf_client_notify_remove (gconf_client, gconf_notify_id);

        gtk_widget_destroy (GTK_WIDGET (docklet));
        g_object_unref (G_OBJECT (docklet));
        docklet = NULL;
}

static void
docklet_create (void)
{
        GtkWidget *box;

	if (docklet)
		docklet_destroy ();

	docklet = egg_tray_icon_new ("PWS");

        box   = gtk_event_box_new ();
        image = gtk_image_new ();

        g_signal_connect (G_OBJECT (docklet), "embedded", G_CALLBACK (docklet_embedded_cb), NULL);
        g_signal_connect (G_OBJECT (docklet), "destroy", G_CALLBACK (docklet_destroyed_cb), NULL);
        g_signal_connect (G_OBJECT (box), "button-press-event", G_CALLBACK (docklet_clicked_cb), NULL);

        gtk_container_add (GTK_CONTAINER (box), image);
        gtk_container_add (GTK_CONTAINER (docklet), box);
        gtk_widget_show_all (GTK_WIDGET (docklet));

        /* ref the docklet before we bandy it about the place 
	 */
        g_object_ref (G_OBJECT (docklet));
}

GNetworkHttpServerConfig*
get_config_from_gconf (void)
{
	GNetworkHttpServerConfig *config;

	config = g_new0 (GNetworkHttpServerConfig, 1);
	config->port = gconf_client_get_int (gconf_client, "/apps/gnome-pws/prefs/port", NULL);
	config->document_root = g_strdup (gconf_client_get_string (gconf_client, "/apps/gnome-pws/prefs/documentroot", NULL));
	config->keepalive = gconf_client_get_bool (gconf_client, "/apps/gnome-pws/prefs/keepalive", NULL);
	config->timeout = gconf_client_get_int (gconf_client, "/apps/gnome-pws/prefs/timeout", NULL);

	return config;
}

static char*
item_factory_trans_cb (const gchar *path, gpointer data)
{
	return (gchar*) path;
}

static void
gconf_notify_running_cb (GConfClient *client,
			 guint        cnxn_id,
			 GConfEntry  *entry,
			 gpointer     user_data)
{
	gboolean running;
	
	if (!strcmp (entry->key, "/apps/gnome-pws/prefs/running")) {
		if (entry->value->type == GCONF_VALUE_BOOL) {
			running = gconf_value_get_bool (entry->value);
			if (running)
				start_server ();
			else
				stop_server ();
		}
	}
}

int
main (int argc, char **argv)
{
	GNetworkHttpServerConfig *config;
	
	gtk_init (&argc, &argv);

	popup_factory = gtk_item_factory_new (GTK_TYPE_MENU,
					      "<main>",
					      NULL);
	gtk_item_factory_set_translate_func (popup_factory,
					     item_factory_trans_cb,
					     NULL,
					     NULL);
	gtk_item_factory_create_items (popup_factory,
				       G_N_ELEMENTS (popup_items),
				       popup_items,
				       NULL);

	gconf_client = gconf_client_get_default ();
	gconf_client_add_dir (gconf_client,
			      "/apps/gnome-pws/prefs",
			      GCONF_CLIENT_PRELOAD_NONE,
			      NULL);

	gconf_notify_id = gconf_client_notify_add (gconf_client, "/apps/gnome-pws/prefs/running",
						   gconf_notify_running_cb, NULL,
						   NULL, NULL);
	
	config = get_config_from_gconf ();
	server = gnetwork_http_server_new (config);
	docklet_create ();
	
	gtk_main ();
	
	return 0;
}
