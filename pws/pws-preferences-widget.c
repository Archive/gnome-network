/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* GNOME Personal Web Server Preferences Widget
 *
 * Authors:
 *	�lvaro Pe�a <apg@esware.com>
 *
 * Copyright (C) 2003 GNOME Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pws-preferences-widget.h"

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <libgnomeui/libgnomeui.h>
#include "gnetwork-http-server.h"
#include "pws-preferences-support.h"

#define PARENT_TYPE GTK_TYPE_VBOX

struct _PwsPreferencesWidgetPrivate {
	GConfClient              *gconf_client;
	guint                     gconf_notify_id;
	gchar                    *gconf_path;
	GtkWidget                *disclosure;
	GtkWidget                *documentroot;
	GtkWidget                *running;
	GtkWidget                *port;
	GtkWidget                *timeout;
	GtkWidget                *keepalive;
	GNetworkHttpServerConfig *config;
};

static void pws_preferences_widget_init       (PwsPreferencesWidget      *pref);
static void pws_preferences_widget_class_init (PwsPreferencesWidgetClass *klass);
static void pws_preferences_widget_finalize   (GObject                   *object);
static void running_toggled_cb                (GtkToggleButton           *rb, 
					       gpointer                   user_data);
static void port_changed_cb                   (GtkSpinButton             *p_sp,
					       gpointer                   user_data);
static void timeout_changed_cb                (GtkSpinButton             *t_sp,
					       gpointer                   user_data);
static void document_root_changed_cb          (GtkEntry                  *entry,
					       gpointer                   user_data);
static void keepalive_toggled_cb              (GtkToggleButton           *tb,
					       gpointer                   user_data);

static GObjectClass *parent_class = NULL;

/*
 * PwsPreferencesWidget class implementation
 */

GType
pws_preferences_widget_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (PwsPreferencesWidgetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pws_preferences_widget_class_init,
			NULL,
			NULL,
			sizeof (PwsPreferencesWidget),
			0,
			(GInstanceInitFunc) pws_preferences_widget_init
		};
		type = g_type_register_static (PARENT_TYPE, "PwsPreferencesWidget", &info, 0);
	}
	return type;
}


static void
pws_preferences_widget_init (PwsPreferencesWidget *pref)
{
	GtkWidget *hbox, *label, *hbox_additional, *vbox, *table;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (pref));
	
	pref->priv = g_new0 (PwsPreferencesWidgetPrivate, 1);

	gtk_box_set_spacing (GTK_BOX (pref), 6);

	/* Web folder */	
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (pref), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("Web folder:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	
	pref->priv->documentroot = gnome_file_entry_new (NULL, _("Select the folder to be published"));
	gnome_file_entry_set_directory_entry (GNOME_FILE_ENTRY (pref->priv->documentroot), TRUE);
	g_signal_connect (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (pref->priv->documentroot)),
			  "changed",
			  G_CALLBACK (document_root_changed_cb), pref);
	gtk_box_pack_start (GTK_BOX (hbox), pref->priv->documentroot, TRUE, TRUE, 0);
	gtk_widget_show (pref->priv->documentroot);

	/* Server status */
	pref->priv->running = gtk_check_button_new_with_label (_("Enable web server"));
	g_signal_connect (pref->priv->running,
			  "toggled",
			  G_CALLBACK (running_toggled_cb), pref);
	gtk_box_pack_start (GTK_BOX (pref), pref->priv->running, FALSE, FALSE, 0);
	gtk_widget_show (pref->priv->running);

	/* Additional options */
	pref->priv->disclosure = cddb_disclosure_new (_("Additional options"),
						      _("Additional options"));
	gtk_box_pack_start (GTK_BOX (pref), pref->priv->disclosure, FALSE, FALSE, 0);
	gtk_widget_show (pref->priv->disclosure);
	
	hbox_additional = gtk_hbox_new (FALSE, 0);
	cddb_disclosure_set_container (CDDB_DISCLOSURE (pref->priv->disclosure), hbox_additional);
	gtk_box_pack_start (GTK_BOX (pref), hbox_additional, FALSE, FALSE, 0);
	gtk_widget_show (hbox_additional);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox_additional), label, FALSE, FALSE, 0);
	gtk_widget_show (label);
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox_additional), vbox, TRUE, TRUE, 0);

	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	gtk_widget_show (table);

	label = gtk_label_new (_("Port number:"));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1,
			  0, 1,
			  GTK_FILL, GTK_FILL,
			  0, 0);
	gtk_widget_show (label);
	
	pref->priv->port = gtk_spin_button_new_with_range  (1025, 9999, 1);
	g_signal_connect (pref->priv->port,
			  "value-changed",
			  G_CALLBACK (port_changed_cb), pref);
	gtk_table_attach_defaults (GTK_TABLE (table), pref->priv->port,
				   1, 2,
				   0, 1);
	gtk_widget_show (pref->priv->port);

	label = gtk_label_new (_("Time out:"));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1,
			  1, 2,
			  GTK_FILL, GTK_FILL,
			  0, 0);
	gtk_widget_show (label);
	
	pref->priv->timeout = gtk_spin_button_new_with_range (100, 1000, 1);
	g_signal_connect (pref->priv->timeout,
			  "value-changed",
			  G_CALLBACK (timeout_changed_cb), pref);
	gtk_table_attach_defaults (GTK_TABLE (table), pref->priv->timeout,
				   1, 2,
				   1, 2);
	gtk_widget_show (pref->priv->timeout);
	
	pref->priv->keepalive = gtk_check_button_new_with_label (_("Keep alive"));
	g_signal_connect (pref->priv->keepalive,
			  "toggled",
			  G_CALLBACK (keepalive_toggled_cb), pref);
	gtk_box_pack_start (GTK_BOX (vbox), pref->priv->keepalive, FALSE, FALSE, 0);
	gtk_widget_show (pref->priv->keepalive);
}

static void
pws_preferences_widget_class_init (PwsPreferencesWidgetClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	object_class->finalize = pws_preferences_widget_finalize;
}

static void
pws_preferences_widget_finalize (GObject *object)
{
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (object));
	pref = PWS_PREFERENCES_WIDGET (object);
	
	if (pref->priv) {
		if (pref->priv->gconf_client)
			gconf_client_notify_remove (pref->priv->gconf_client,
						    pref->priv->gconf_notify_id);

		g_free (pref->priv);
		pref->priv = NULL;
	}
	
	parent_class->finalize (object);
}

static void
running_toggled_cb (GtkToggleButton *rb, gpointer user_data)
{
	gboolean running;
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);
	
	running = gtk_toggle_button_get_active (rb);

	if (pref->priv->gconf_client) {
		gconf_client_set_bool (pref->priv->gconf_client,
				       g_strdup_printf ("%s/running", pref->priv->gconf_path),
				       running, NULL);
	}
}

static void
port_changed_cb (GtkSpinButton *p_sp, gpointer user_data)
{
	gint port;
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);

	port = gtk_spin_button_get_value_as_int (p_sp);

	if (pref->priv->gconf_client) {
		gconf_client_set_int (pref->priv->gconf_client, 
				      g_strdup_printf ("%s/port", pref->priv->gconf_path),
				      port, NULL);
	}
	
	if (pref->priv->config)
		pref->priv->config->port = port;
}

static void
timeout_changed_cb (GtkSpinButton *t_sp, gpointer user_data)
{
	gint timeout;
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);

	timeout = gtk_spin_button_get_value_as_int (t_sp);

	if (pref->priv->gconf_client) {
		gconf_client_set_int (pref->priv->gconf_client, 
				      g_strdup_printf ("%s/timeout", pref->priv->gconf_path),
				      timeout, NULL);
	}
	
	if (pref->priv->config)
		pref->priv->config->timeout = timeout;
}

static void
document_root_changed_cb (GtkEntry *entry, gpointer user_data)
{
	const gchar *str;
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);
	
	str = gtk_entry_get_text (entry);

	if (pref->priv->gconf_client) {
		gconf_client_set_string (pref->priv->gconf_client, 
					 g_strdup_printf ("%s/documentroot", pref->priv->gconf_path),
					 g_strdup (str), NULL);
	}
	
	if (pref->priv->config)
		pref->priv->config->document_root = g_strdup (str);
}

static void
keepalive_toggled_cb (GtkToggleButton *tb, gpointer user_data)
{
	gboolean a;
	PwsPreferencesWidget *pref;
	
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);
	
	a = gtk_toggle_button_get_active (tb);

	if (pref->priv->gconf_client) {
		gconf_client_set_bool (pref->priv->gconf_client,
				       g_strdup_printf ("%s/keepalive", pref->priv->gconf_path),
				       a, NULL);
	}

	if (pref->priv->config)
		pref->priv->config->keepalive = a;
}

static void
gconf_notify_cb (GConfClient *client,
		 guint        cnxn_id,
		 GConfEntry  *entry,
		 gpointer     user_data)
{
	PwsPreferencesWidget *pref;
	gboolean              running, keepalive;
	int                   port, timeout;
	gchar                *documentroot;

	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (user_data));
	pref = PWS_PREFERENCES_WIDGET (user_data);

	if (!strcmp (entry->key, g_strdup_printf ("%s/running", pref->priv->gconf_path))) {
		if (entry->value->type == GCONF_VALUE_BOOL) {
			running = gconf_value_get_bool (entry->value);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->running), running);
		}
	}
	if (!strcmp (entry->key, g_strdup_printf ("%s/port", pref->priv->gconf_path))) {
		if (entry->value->type == GCONF_VALUE_INT) {
			port = gconf_value_get_int (entry->value);
			if (pref->priv->config)
				pref->priv->config->port = port;
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->port), port);
		}
	}
	if (!strcmp (entry->key, g_strdup_printf ("%s/timeout", pref->priv->gconf_path))) {
		if (entry->value->type == GCONF_VALUE_INT) {
			timeout = gconf_value_get_int (entry->value);
			if (pref->priv->config)
				pref->priv->config->timeout = timeout;
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->timeout), timeout);
		}
	}
	if (!strcmp (entry->key, g_strdup_printf ("%s/documentroot", pref->priv->gconf_path))) {
		if (entry->value->type == GCONF_VALUE_STRING) {
			documentroot = g_strdup (gconf_value_get_string (entry->value));
			if (pref->priv->config)
				pref->priv->config->document_root = g_strdup (documentroot);
			gnome_file_entry_set_filename (GNOME_FILE_ENTRY (pref->priv->documentroot),
						       g_strdup (documentroot));
		}
	}
	if (!strcmp (entry->key, g_strdup_printf ("%s/keepalive", pref->priv->gconf_path))) {
		if (entry->value->type == GCONF_VALUE_BOOL) {
			keepalive = gconf_value_get_bool (entry->value);
			if (pref->priv->config)
				pref->priv->config->keepalive = keepalive;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->keepalive), keepalive);
		}
	}	
			
}

/**
 * pws_preferences_widget_new
 *
 */
GtkWidget*
pws_preferences_widget_new (GNetworkHttpServerConfig *config, gchar *gconf_path)
{
	PwsPreferencesWidget *pref;
	
	pref = g_object_new (PWS_TYPE_PREFERENCES_WIDGET, NULL);

	if (config != NULL)
		pws_preferences_widget_set_server_config (pref, config);

	if (gconf_path != NULL) {
		pws_preferences_widget_set_gconf_config (pref, gconf_path);
	}
	
	return GTK_WIDGET (pref);
}

void
pws_preferences_widget_set_show_additional (PwsPreferencesWidget *pref, gboolean show)
{
	g_return_if_fail (PWS_IS_PREFERENCES_WIDGET (pref));

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->disclosure), show);
}

gboolean
pws_preferences_widget_set_server_config (PwsPreferencesWidget     *pref,
					  GNetworkHttpServerConfig *config)
{
	g_return_val_if_fail (PWS_IS_PREFERENCES_WIDGET (pref), FALSE);
	g_return_val_if_fail (config != NULL, FALSE);

	if (!pref->priv->config)
		pref->priv->config = g_new0 (GNetworkHttpServerConfig, 1);

	pref->priv->config->document_root = g_strdup (config->document_root);
	gnome_file_entry_set_filename (GNOME_FILE_ENTRY (pref->priv->documentroot),
				       g_strdup (pref->priv->config->document_root));

	pref->priv->config->port = config->port;
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->port),
				   pref->priv->config->port);
	
	pref->priv->config->timeout = config->timeout;
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->timeout),
				   pref->priv->config->timeout);

	pref->priv->config->keepalive = config->keepalive;
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->keepalive),
				      pref->priv->config->keepalive);
		

	return TRUE;
}

gboolean
pws_preferences_widget_set_gconf_config (PwsPreferencesWidget *pref,
					 gchar                *gconf_path)
{
	gchar   *documentroot;
	gint     port;
	gint     timeout;
	gboolean keepalive;
	gboolean running;
	
	g_return_val_if_fail (PWS_IS_PREFERENCES_WIDGET (pref), FALSE);
	g_return_val_if_fail (gconf_path != NULL, FALSE);
	
	pref->priv->gconf_path = g_strdup (gconf_path);
	pref->priv->gconf_client = gconf_client_get_default ();
	gconf_client_add_dir (pref->priv->gconf_client,
			      gconf_path,
			      GCONF_CLIENT_PRELOAD_NONE,
			      NULL);
	pref->priv->gconf_notify_id = gconf_client_notify_add (pref->priv->gconf_client,
							       gconf_path,
							       gconf_notify_cb,
							       pref,
							       NULL,
							       NULL);
	
	documentroot = g_strdup (gconf_client_get_string (pref->priv->gconf_client,
							  g_strdup_printf ("%s/documentroot", gconf_path), NULL));
	gnome_file_entry_set_filename (GNOME_FILE_ENTRY (pref->priv->documentroot), g_strdup (documentroot));

	port = gconf_client_get_int (pref->priv->gconf_client,
				     g_strdup_printf ("%s/port", gconf_path), NULL);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->port), port);

	timeout = gconf_client_get_int (pref->priv->gconf_client, 
					g_strdup_printf ("%s/timeout", gconf_path), NULL);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (pref->priv->timeout), timeout);

	keepalive = gconf_client_get_bool (pref->priv->gconf_client,
					   g_strdup_printf ("%s/keepalive", gconf_path), NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->keepalive), keepalive);

	running = gconf_client_get_bool (pref->priv->gconf_client,
					 g_strdup_printf ("%s/running", gconf_path), NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pref->priv->running), running);

	return TRUE;
}

GNetworkHttpServerConfig*
pws_preferences_widget_get_server_config (PwsPreferencesWidget *pref)
{
	g_return_val_if_fail (PWS_IS_PREFERENCES_WIDGET (pref), NULL);

	return pref->priv->config;
}
