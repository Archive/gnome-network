/* GNOME Desktop Sharing
 * Copyright (C) 2003 Rob Clews <robc@klearsystems.com>
 * Copyright (C) 2003 German Poo-Caaman~o <gpoo@ubiobio.cl>
 *
 * tray.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
 * USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* #include <stdio.h> */
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <gnome.h>
#include <gconf/gconf-client.h> 
#include "tray.h"
#include "eggtrayicon.h"

#ifdef HAVE_DBUS
#include "dbus.h"
#endif

#define SELECTION_NAME "_GNOME_DESKTOP_SHARING_SELECTION"
#define GIF_CB(x) ((GtkItemFactoryCallback)(x))

static GtkWidget *icon_image;
static EggTrayIcon *tray_icon;
static GtkTooltips *tray_icon_tooltip;
static GtkListStore *store;
GtkItemFactory *popup_factory = NULL;

static void show_about(GtkWidget *parent, gpointer data);
static void show_preferences (gpointer callback_data, guint action, GtkWidget *widget);
static void show_clients_dialog (gpointer callback_data, guint action, GtkWidget *widget);
static void setup_gconf(void);
static void disconnect_clients(void);
static void close_clients_dialog(GtkWidget *caller, GtkWidget *dialog);
static void quit_app (gpointer callback_data, guint action, GtkWidget *widget);
static gboolean get_lock(void);
static GtkTreeModel *create_model(void);
static gboolean tray_icon_press (GtkWidget *widget, GdkEventButton *event);
static gboolean tray_destroyed (GtkWidget *widget, GdkEvent *event, gpointer user_data);
static char *item_factory_trans_cb (const gchar *path, gpointer data);
void execute (char *cmd, gboolean  sync);
void init_tray (void);

static GtkItemFactoryEntry popup_items[] = {
	{ N_("/_Preferences"), NULL, GIF_CB (show_preferences), 0, "<StockItem>", GTK_STOCK_PREFERENCES },
	{ N_("/_Clients"), NULL, GIF_CB (show_clients_dialog), 0, "<Item>", NULL },
	{ "/sep1", NULL, 0, 0, "<Separator>", NULL },
	{ N_("/_About"), NULL, GIF_CB (show_about), 0, "<Item>", NULL},
	{ N_("/_Quit"), NULL, GIF_CB (quit_app), 0, "<StockItem>", GTK_STOCK_QUIT }
};

enum
{
  COLUMN_DISCONNECT,
  COLUMN_HOSTNAME,
  COLUMN_CLIENTPTR,
  NUM_COLUMNS
};

static gboolean
tray_icon_press (GtkWidget      *widget,
                 GdkEventButton *event)
{
	GtkWidget *menu;
	
	if (event->button == 3) {
		menu = gtk_item_factory_get_widget (popup_factory, "");
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL,
				NULL, event->button, event->time);
		return TRUE;
	}
	return FALSE;
}

static gboolean
tray_destroyed (GtkWidget *widget, 
                GdkEvent  *event,
                gpointer   user_data)
{
	init_tray ();
	
	return TRUE;
}

void init_tray (void)
{
	GtkWidget *evbox;
	icon_image = gtk_image_new();
	tray_icon = egg_tray_icon_new ("Desktop Sharing");
	gtk_image_set_from_stock(GTK_IMAGE(icon_image), GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_LARGE_TOOLBAR);

	if (get_lock() == FALSE) {
		g_print ("Daemon already running, exiting...\n");
		gtk_main_quit();
		exit(0);
	}
	
	tray_icon_tooltip = gtk_tooltips_new ();
	gtk_tooltips_set_tip (tray_icon_tooltip,
			GTK_WIDGET (tray_icon),
			_("Your desktop is viewable to others"),
			NULL);

	/* Event box */
	evbox = gtk_event_box_new ();
	g_signal_connect (G_OBJECT (evbox), "button_press_event",
			G_CALLBACK (tray_icon_press), NULL);

	popup_factory = gtk_item_factory_new (GTK_TYPE_MENU, "<main>", NULL);
	gtk_item_factory_set_translate_func (popup_factory, item_factory_trans_cb, NULL, NULL);
	gtk_item_factory_create_items (popup_factory, G_N_ELEMENTS (popup_items), popup_items, NULL);

	gtk_container_add (GTK_CONTAINER (evbox), icon_image);
	gtk_container_add (GTK_CONTAINER (tray_icon), evbox);
	gtk_widget_show_all (GTK_WIDGET (tray_icon));

	g_signal_connect (G_OBJECT (tray_icon), "destroy-event",
			G_CALLBACK (tray_destroyed), NULL);
	create_model();
	setup_gconf();
}

static char*
item_factory_trans_cb (const gchar *path, gpointer data)
{
	return (gchar *)path;
}

static void
/* show_about (gpointer callback_data, guint action, GtkWidget *widget) */
show_about (GtkWidget * parent, gpointer data)
{
         static GtkWidget *about = NULL;
         GdkPixbuf *pixbuf = NULL;
         const gchar *authors[] = { "Rob Clews <robc@klearsystems.com>", NULL };
         const gchar *documenters[] = { NULL };
		 const gchar copyright[] = { "Copyright \xc2\xa9 2003 Rob Clews"};
 
         if (about != NULL) {
                 gtk_window_present (GTK_WINDOW (about));
                 return;
         }
 
         pixbuf = gdk_pixbuf_new_from_file ("gnome-session.png", NULL);

         about = gnome_about_new(_("Desktop Sharing"), VERSION,
                         (const char *)copyright,
                         _("Remote Desktop Sharing"),
                         (const char **)authors,
                         (const char **)documenters,
						 _("This is an untranslated version of GNOME Network Information"),
						 pixbuf);

		 if (pixbuf != NULL)
			 gdk_pixbuf_unref (pixbuf);

		 gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (parent));

		 g_signal_connect (G_OBJECT (about), "destroy",
				 G_CALLBACK (gtk_widget_destroyed), &about);
/*		 g_object_add_weak_pointer (G_OBJECT (about), (void**)&(about));*/

		 gtk_widget_show(about);
}

static gchar *
lookup_ip(gchar *ip)
{
	struct hostent* hp;
	struct in_addr ipaddrnum;
	gchar *addr;

	ipaddrnum.s_addr = inet_addr(ip);
	if (ipaddrnum.s_addr > 0) {
		hp = gethostbyaddr((char*)&(ipaddrnum.s_addr), sizeof(ipaddrnum), AF_INET);
		if (hp != NULL)
			addr = g_strdup_printf(hp->h_name);
		else
			addr = g_strdup (ip);
	}
	else
		addr = g_strdup (ip);

	return addr;
}

void 
update_client_count(int client_count,
		    rfbClientPtr client) 
{
	char *msg;
	gchar *addr;
	gboolean valid;
	rfbClientPtr treeclient;
	GtkTreeIter iter;

	if(client == NULL)
		return;

	if(client->sock != -1) {
		addr = g_strdup (lookup_ip(client->host));
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				    COLUMN_DISCONNECT, FALSE,
				    COLUMN_HOSTNAME, addr,
				    COLUMN_CLIENTPTR, client,
				    -1);
		g_free(addr);
	} else {
		valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL(store), &iter);

		while (valid) {
			gtk_tree_model_get (GTK_TREE_MODEL(store), &iter, 
					    COLUMN_CLIENTPTR, &treeclient,
					    -1);
			if(treeclient != NULL && client != NULL) {
				if(strcmp(treeclient->host, client->host) == 0) {
					gtk_list_store_remove(GTK_LIST_STORE(store), &iter);
					break;
				}
			}
			valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(store), &iter);
		}
	}
	msg = g_strdup_printf((client_count == 1 ?
			"Your desktop is being viewed by %d user" : 
			"Your desktop is being viewed by %d users"),
			client_count);
	gtk_tooltips_set_tip (tray_icon_tooltip,
			GTK_WIDGET (tray_icon),
			_(msg),
			NULL);
	g_free(msg);
}

static void 
disconnect_clients(void) 
{
	gboolean valid;
	GtkTreeIter iter;
	rfbClientPtr client;
	gint disconnect;

	valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL(store), &iter);

	while (valid) {
		gtk_tree_model_get (GTK_TREE_MODEL(store), &iter, 
				    COLUMN_DISCONNECT, &disconnect,
				    COLUMN_CLIENTPTR, &client,
				    -1);
		if(disconnect && client != NULL) {
			rfbCloseClient(client);
		}
		valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(store), &iter);
	}
}
static void
gconf_notify_func (GConfClient *client,
                   guint        cnxn_id,
                   GConfEntry  *entry,
                   gpointer     user_data)
{
	GConfValue  *value;
	gboolean     enabled;
	
	value = gconf_entry_get_value (entry);
	enabled = gconf_value_get_bool (value);
	
	if(enabled == FALSE) {
		gtk_main_quit();
	}
}
static void
setup_gconf(void) 
{
	GConfClient *client;
	gboolean     value;

	client = gconf_client_get_default ();
	value = gconf_client_get_bool (client, GDS_GCONF_ENABLE, NULL);
	gconf_client_add_dir (client,
			      GDS_GCONF_PATH,
			      GCONF_CLIENT_PRELOAD_NONE,
			      NULL);
	gconf_client_set_bool (client, GDS_GCONF_ENABLE, TRUE, NULL);
	gconf_client_notify_add (client,
				 GDS_GCONF_ENABLE,
				 gconf_notify_func,
				 NULL,
				 NULL,
				 NULL);
}

void
execute (char     *cmd, 
         gboolean  sync)
{
        gboolean retval;
                                                                                                                                              
        if (sync != FALSE)
                retval = g_spawn_command_line_sync(cmd, NULL, NULL, NULL, NULL);
        else
                retval = g_spawn_command_line_async(cmd, NULL);
                                                                                                                                              
        if (retval == FALSE) {
                char *msg;                                                                                                                                              
                msg = g_strdup_printf("Couldn't execute command: %s", cmd);
                g_message (msg);
                g_free (msg);
        }
}

static GtkTreeModel *
create_model (void)
{
	store = gtk_list_store_new (NUM_COLUMNS,
				    G_TYPE_BOOLEAN,
				    G_TYPE_STRING,
				    G_TYPE_POINTER);

	return GTK_TREE_MODEL (store);
}

static void
fixed_toggled (GtkCellRendererToggle *cell,
	       gchar                 *path_str,
	       gpointer               data)
{
	GtkTreeModel *model = (GtkTreeModel *)data;
	GtkTreeIter  iter;
	GtkTreePath *path = gtk_tree_path_new_from_string (path_str);
	gboolean fixed;

	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, COLUMN_DISCONNECT, &fixed, -1);
	
	fixed ^= 1;
	gtk_list_store_set (GTK_LIST_STORE (model), &iter, COLUMN_DISCONNECT, fixed, -1);

	gtk_tree_path_free (path);
}
static void
add_columns (GtkTreeView *tv)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model (tv);

	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (renderer, "toggled", G_CALLBACK (fixed_toggled), model);

	column = gtk_tree_view_column_new_with_attributes ("Disconnect",
							   renderer,
							   "active",
							   COLUMN_DISCONNECT,
							   NULL);

	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
					 GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 80);
	gtk_tree_view_append_column (tv, column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Client",
							   renderer,
							   "text",
							   COLUMN_HOSTNAME,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_HOSTNAME);
	gtk_tree_view_append_column (tv, column);
}

static void 
show_clients_dialog (gpointer callback_data, guint action, GtkWidget *widget) 
{
	GtkWidget *window , *tv, *sw, *vbox, *hbox, *button, *label;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Connected Clients");
	gtk_window_set_default_size(GTK_WINDOW(window),300,300);

	gtk_container_set_border_width (GTK_CONTAINER (window), 12);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	label = gtk_label_new (_("Clients currently viewing your desktop"));
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);

	tv = gtk_tree_view_new_with_model (GTK_TREE_MODEL(store));
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (tv), TRUE);
	gtk_tree_view_set_search_column (GTK_TREE_VIEW (tv), COLUMN_HOSTNAME);
	add_columns (GTK_TREE_VIEW (tv));
	gtk_container_add (GTK_CONTAINER (sw), tv);

	hbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (hbox), 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	button = gtk_button_new_with_label (_("Disconnect"));
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	g_signal_connect ((gpointer) button, "clicked",
			  G_CALLBACK (disconnect_clients),
			  NULL);

	button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);;
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	g_signal_connect ((gpointer) button, "clicked",
			  G_CALLBACK (close_clients_dialog),
			  window);

	gtk_widget_show_all(window);
}
static void 
close_clients_dialog(GtkWidget *caller, GtkWidget *dialog)
{
	gtk_widget_destroy(dialog);
}

static void
show_preferences (gpointer callback_data, guint action, GtkWidget *widget) 
{
	execute("gnome-desktop-sharing-preferences", FALSE);
}

static void
selection_get_func (GtkClipboard     *clipboard, 
                    GtkSelectionData *selection_data,
                    guint             info, 
                    gpointer          user_data_or_owner)
{
}

static void
selection_clear_func (GtkClipboard *clipboard,
                      gpointer      user_data_or_owner)
{       
	return;
}
static gboolean 
get_lock(void)
{
	gboolean result = FALSE;
	GtkClipboard *clipboard;
	Atom clipboard_atom = gdk_x11_get_xatom_by_name (SELECTION_NAME);
	static const GtkTargetEntry targets[] = {
		{ SELECTION_NAME, 0, 0 }
	};

	XGrabServer (GDK_DISPLAY());

	if (XGetSelectionOwner (GDK_DISPLAY(), clipboard_atom) != None)
		goto out;

	clipboard = gtk_clipboard_get (gdk_atom_intern (SELECTION_NAME, FALSE));

	if (!gtk_clipboard_set_with_data  (clipboard, targets,
				G_N_ELEMENTS (targets),
				selection_get_func,
				selection_clear_func, NULL))
		goto out;

	result = TRUE;

out:
	XUngrabServer (GDK_DISPLAY());
	gdk_flush();

	return result;
}

static void
quit_app (gpointer callback_data, guint action, GtkWidget *widget)
{
	GConfClient *client;

	client = gconf_client_get_default ();
	gconf_client_set_bool (client, GDS_GCONF_ENABLE, FALSE, NULL);
	gtk_main_quit();
}
