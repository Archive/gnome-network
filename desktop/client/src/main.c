/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h> 
#include <sys/stat.h> 
#include <stdio.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <string.h>

#include <libintl.h>
#define _(x) gettext(x)
#include <locale.h>

#include "rdpfile.h"
#include "support.h"
#include "connect.h"

static void grd_print_help (void);
static void grd_print_version (void);

int
main (int argc, char *argv[])
{
  gint i;
  gint imode = (gint)GRD_WM_COMPACT;
  gchar *rdp_file_name = g_build_path ("/", g_get_home_dir(), ".gnome-remote-desktop", "last.grd", NULL);
  rdp_file *rdp = NULL;

  gtk_set_locale ();
  gtk_init (&argc, &argv);

  #ifdef GRD_DEBUG
  printf ("debugging messages are on\n");
  #endif

  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);

  //printf ("\n\nLanguage: %s\n\n", gtk_get_default_language ());

  add_pixmap_directory (PACKAGE_DATA_DIR "/pixmaps/gnome-network/");

  // make sure our dirs & files exist
  grd_check_files ();

  //printf ("\n\nargc: %d\n\n", argc);
  // start at 1 because 0 contains binary name
  for (i = 1; i < argc; i++) {
    if (strcmp("--help", argv[i]) == 0 || strcmp("-h", argv[i]) == 0) {
      grd_print_help ();
      return 0;
    }
    if (strcmp("--version", argv[i]) == 0 || strcmp("-v", argv[i]) == 0) {
      grd_print_version ();
      return 0;
    }
    if (strcmp("-x", argv[i]) == 0) {
      if (g_file_test (argv[i+1], G_FILE_TEST_EXISTS)) {
        rdp_file_name = argv[i+1];
        rdp = g_new0 (rdp_file, 1);
        rdp_file_init (rdp);
        rdp_file_load (rdp, rdp_file_name);
        //printf ("\n\nrdp_file_name: %s\n\n", rdp_file_name);
        if (grd_launch_remote (rdp, 0, 1) == 0) {
          g_free (rdp);
          return 0;
        }
      }
    }
    if (g_file_test (argv[i], G_FILE_TEST_EXISTS)) {
      rdp_file_name = argv[i];
      continue;
    }
    if (strcmp("--full", argv[i]) == 0 || strcmp("-f", argv[i]) == 0) {
      imode = (gint)GRD_WM_COMPLEX;
      continue;
    }
  }

  // create the main window
  //gConnect = create_frmConnect ();
  create_frmConnect ();

  // Get the last.grd for last used settings and load
  rdp = g_new0 (rdp_file, 1);
  rdp_file_init (rdp);
  rdp_file_load (rdp, rdp_file_name);
  rdp_file_set_screen (rdp, gConnect);

  // set the window mode (compact)
  grd_set_window_mode (gConnect, imode);
  gtk_widget_show (gConnect);

  g_free (rdp);
  g_free (rdp_file_name);

  gtk_main ();
  return 0;
}

static void 
grd_print_help (void) {

  printf ("\n");
  printf ("  Usage: gnome-remote-desktop [OPTION]... [FILE]...\n\n");
  printf ("  FILE           an rdp format file containing options\n");
  printf ("  -f, --full     display full mode window\n");
  printf ("  -h, --help     display this help and exit\n");
  printf ("  -v, --version  output version information and exit\n");
  printf ("  -x FILE        launch rdesktop with options specified in FILE\n");
  printf ("\n");
  return;

}

static void 
grd_print_version (void) {

  printf ("\n");
  printf ("  GNOME Remote Desktop is a frontend\n");
  printf ("  for rdesktop and other remote desktop tools\n");
  printf ("  gnome-remote-desktop version "VERSION"\n");
  printf ("\n");
  return;

}
