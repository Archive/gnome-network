/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h> 

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <libgnomeui/gnome-about.h>

#include <libintl.h>
#define _(x) gettext(x)

#include <sys/types.h> 
#include <sys/stat.h> 
#include <dirent.h> 
#include <fcntl.h> 

#include "rdpfile.h"
#include "support.h"


GtkWidget* lookup_widget (GtkWidget *widget, const gchar *widget_name) {
  GtkWidget *parent, *found_widget;

  for (;;) {
    if (GTK_IS_MENU (widget))
      parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
    else
      parent = widget->parent;
    if (!parent)
      parent = gtk_object_get_data (GTK_OBJECT (widget), "GladeParentKey");
    if (parent == NULL)
      break;
    widget = parent;
  }
  found_widget = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (widget), widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}


static GList *pixmaps_directories = NULL;

/* Use this function to set the directory containing installed pixmaps. */
void add_pixmap_directory (const gchar *directory) {
  pixmaps_directories = g_list_prepend (pixmaps_directories, g_strdup (directory));
}


/* This is an internally used function to find pixmap files. */
gchar* find_pixmap_file (const gchar *filename) {
  GList *elem;
  /* We step through each of the pixmaps directory to find it. */
  elem = pixmaps_directories;
  while (elem) {
    gchar *pathname = g_strdup_printf ("%s%s%s", (gchar*)elem->data,
                                        G_DIR_SEPARATOR_S, filename);
    if (g_file_test (pathname, G_FILE_TEST_EXISTS))
      return pathname;
    g_free (pathname);
    elem = elem->next;
  }
  return NULL;
}


/* This is an internally used function to create pixmaps. */
GtkWidget* create_pixmap (GtkWidget *widget, const gchar *filename) {
  gchar *pathname = NULL;
  GtkWidget *pixmap;

  if (!filename || !filename[0])
      return gtk_image_new ();

  pathname = find_pixmap_file (filename);
  if (!pathname) {
    g_warning ("Couldn't find pixmap file: %s", filename);
    return gtk_image_new ();
  }
  pixmap = gtk_image_new_from_file (pathname);
  g_free (pathname);
  return pixmap;
}


/* This is an internally used function to create pixmaps. */
GdkPixbuf* create_pixbuf (const gchar *filename) {
  gchar *pathname = NULL;
  GdkPixbuf *pixbuf;
  GError *error = NULL;

  if (!filename || !filename[0])
      return NULL;

  pathname = find_pixmap_file (filename);

  if (!pathname)
    {
      g_warning ("Couldn't find pixmap file: %s", filename);
      return NULL;
    }

  pixbuf = gdk_pixbuf_new_from_file (pathname, &error);
  if (!pixbuf)
    {
      fprintf (stderr, "Failed to load pixbuf file: %s: %s\n",
               pathname, error->message);
      g_error_free (error);
    }
  g_free (pathname);
  return pixbuf;
}


GtkWidget *grd_option_menu_factory (gchar**vmnemonic) {
   GtkWidget *o = gtk_option_menu_new ();
   GtkWidget *m = gtk_menu_new ();
   guint i = 0;
   gtk_option_menu_set_menu (GTK_OPTION_MENU (o), m);
   while (vmnemonic[i]) {
      GtkWidget *mi = gtk_menu_item_new_with_mnemonic (vmnemonic[i]);
      gtk_menu_shell_append (GTK_MENU_SHELL (m), mi);
      i++;
   }
   gtk_option_menu_set_history (GTK_OPTION_MENU (o), 0);
   gtk_widget_show_all (o);
   return (o);
}


/***************************************
*                                      *
*   grd_check_files                    *
*                                      *
***************************************/
 
int grd_check_files (void) {
  const gchar *path_name = (const gchar *)g_build_path ((const gchar *)"/", (const gchar *)g_get_home_dir(), (const gchar *)".gnome-remote-desktop", NULL);
  const gchar *file_name = (const gchar *)g_build_path ((const gchar *)"/", (const gchar *)g_get_home_dir(), (const gchar *)".gnome-remote-desktop", (const gchar *)"mru.grd", NULL);
  int ret = 0;

  #ifdef GRD_DEBUG
  printf ("grd_check_files\n");
  #endif

  // create .gnome-remote-desktop dir in ~/
  mkdir (path_name, 0700);

  // create mru.grd dir in ~/.gnome-remote-desktop/
  if (!g_file_test (file_name, G_FILE_TEST_EXISTS)) {
    ret = open (file_name, O_WRONLY | O_CREAT | O_TRUNC, 0600);
    if (ret > 0) close (ret);
  }

  // create last.grd dir in ~/.gnome-remote-desktop/
  if (!g_file_test (file_name, G_FILE_TEST_EXISTS)) {
    ret = open (file_name, O_WRONLY | O_CREAT | O_TRUNC, 0600);
    if (ret > 0) close (ret);
  }

  /* complete successfully */
  return 0;
}


/***************************************
*   grd_set_window_mode                *
*   window_mode                        *
*     0 == compact                     *
*     1 == complex                     *
***************************************/

void grd_set_window_mode (GtkWidget *main_window, int window_mode) {
  GtkWidget *vbxComplete, *vbxCompact;
  GtkWidget *btnConnect0, *btnConnect1;
  GtkWidget *txtComputer0, *txtComputer1;
  GtkWidget *widget;
  
  #ifdef GRD_DEBUG
  printf ("grd_set_window_mode:\t %d\n", window_mode);
  #endif

  vbxComplete = lookup_widget (main_window, "vbxComplete");
  vbxCompact = lookup_widget (main_window, "vbxCompact");
  btnConnect0 = lookup_widget (main_window, "btnConnect0");
  btnConnect1 = lookup_widget (main_window, "btnConnect1");
  txtComputer0 = lookup_widget (main_window, "txtComputer0");
  txtComputer1 = lookup_widget (main_window, "txtComputer1");

  if (window_mode == (int)GRD_WM_COMPACT) {
    gtk_widget_show_all (vbxCompact);
    gtk_widget_hide_all (vbxComplete);
    gtk_widget_grab_default (btnConnect1);
    gtk_entry_set_activates_default (GTK_ENTRY (txtComputer1), TRUE);
    gtk_widget_grab_focus (txtComputer1);
  } else {
    gtk_widget_show_all (vbxComplete);
    gtk_widget_hide_all (vbxCompact);
    gtk_widget_grab_default (btnConnect0);
    gtk_entry_set_activates_default (GTK_ENTRY (txtComputer0), TRUE);
    gtk_widget_grab_focus (txtComputer0);
  }
  widget = lookup_widget (main_window, "chkDesktopBackground");
  gtk_widget_hide (widget);
  widget = lookup_widget (main_window, "chkWindowContent");
  gtk_widget_hide (widget);
  widget = lookup_widget (main_window, "chkAnimation");
  gtk_widget_hide (widget);
  widget = lookup_widget (main_window, "chkThemes");
  gtk_widget_hide (widget);

}



int grd_launch_remote (rdp_file *rdp_in, int window_mode, int launch_async)
{
  rdp_file *rdp = NULL;
  gchar *c_argv[MAX_ARGVS];
  gchar buffer[MAX_ARGV_LEN];
  gint c_argc = 0;
  GError *err = NULL;
  GSpawnFlags sflags = 0;
  gint cnt = 0;
  gchar *cmd;
  gchar *std_out;
  gchar *std_err;
  gint exit_stat = 0;

  #ifdef GRD_DEBUG
  printf ("grd_launch_remote\n");
  #endif

  rdp = rdp_in;
  
  if (strlen(rdp->full_address)) {

    cmd = NULL;

    if (rdp->protocol == 0) {
      if (g_find_program_in_path ("rdesktop")) {
        sflags += G_SPAWN_SEARCH_PATH;
        cmd = "rdesktop";
      } else {
        grd_error_message (_("rdesktop was not found in your path.\nPlease verify your rdesktop installation."));
        return 1;
      }
      sprintf(buffer, cmd);
      c_argv[c_argc++] = g_strdup (buffer);

      sprintf(buffer, "-TGNOME Remote Desktop");
      c_argv[c_argc++] = g_strdup (buffer);
      
      if (window_mode == 0) {
        // full window mode - use all opts    
        if ( rdp->username && strlen (rdp->username) ) {
          sprintf(buffer, "-u%s", (char*)g_strescape(rdp->username, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }
        if ( rdp->password && strlen (rdp->password) ) {
          sprintf(buffer, "-p%s", (char*)g_strescape(rdp->password, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }
        if ( rdp->domain && strlen (rdp->domain) ) {
          sprintf(buffer, "-d%s", (char*)g_strescape(rdp->domain, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }

        if ( rdp->client_hostname && strlen (rdp->client_hostname) ) {
          sprintf(buffer, "-n%s", (char*)g_strescape(rdp->client_hostname, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }
  
        if (rdp->screen_mode_id == 2) {
          if (rdp->alternate_full_switch == 1) {
            sprintf(buffer, "-F");
          } else {
            sprintf(buffer, "-f");
          }
          c_argv[c_argc++] = g_strdup (buffer);
        } else {
          switch (rdp->desktopwidth) {
          case 640:
            sprintf(buffer, "-g640x480");
            c_argv[c_argc++] = g_strdup (buffer);
            break;
          case 800:
            sprintf(buffer, "-g800x600");
            c_argv[c_argc++] = g_strdup (buffer);
            break;
          case 1024:
            sprintf(buffer, "-g1024x768");
            c_argv[c_argc++] = g_strdup (buffer);
            break;
          case 1152:
            sprintf(buffer, "-g1152x864");
            c_argv[c_argc++] = g_strdup (buffer);
            break;
          case 1280:
            sprintf(buffer, "-g1280x960");
            c_argv[c_argc++] = g_strdup (buffer);
            break;
          default:
            break;
          }
        }
      
        /*
        switch (rdp->session_bpp) {
        case 8:
          sprintf(buffer, "-D8");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 15:
          sprintf(buffer, "-D15");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 16:
          sprintf(buffer, "-D16");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 24:
          sprintf(buffer, "-D24");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        default:
          break;
        }
        */
      
        if (rdp->bitmapcachepersistenable == 1) {
          sprintf(buffer, "-b");
          c_argv[c_argc++] = g_strdup (buffer);
        }
        // Extra
        if (rdp->no_motion_events == 1) {
          sprintf(buffer, "-m");
          c_argv[c_argc++] = g_strdup (buffer);
        }

        if (rdp->enable_wm_keys == 1) {
          sprintf(buffer, "-K");
          c_argv[c_argc++] = g_strdup (buffer);
        }

        if ( rdp->keyboard_language && strlen (rdp->keyboard_language) ) {
          sprintf ( buffer, "-k");
          c_argv[c_argc++] = g_strdup (buffer);
          c_argv[c_argc++] = g_strdup (rdp->keyboard_language);
        }

        if (rdp->enable_alternate_shell == 1) {
          if ( rdp->alternate_shell && strlen (rdp->alternate_shell) ) {
            sprintf ( buffer, "-s");
            c_argv[c_argc++] = g_strdup (buffer);
            c_argv[c_argc++] = g_strdup (rdp->alternate_shell);
          }

          if ( rdp->shell_working_directory && strlen (rdp->shell_working_directory) ) {
            sprintf ( buffer, "-c");
            c_argv[c_argc++] = g_strdup (buffer);
            c_argv[c_argc++] = g_strdup (rdp->shell_working_directory);
          }
        }
        // end of complete mode shit
      }

      // do this shit for all modes
      sprintf(buffer, "%s", (char*)g_strescape(rdp->full_address, NULL));
      c_argv[c_argc++] = g_strdup (buffer);

    } else if (rdp->protocol == 1) {

      // it's a vnc call

      if (g_find_program_in_path ("vncviewer")) {
        cmd = "vncviewer";
      } else if (g_find_program_in_path ("xvncviewer")) {
        cmd = "xvncviewer";
      } else if (g_find_program_in_path ("xtightvncviewer")) {
        cmd = "xtightvncviewer";
      } else if (g_find_program_in_path ("svncviewer")) {
        cmd = "svncviewer";
      } else {
        grd_error_message (_("vncviewer or xvncviewer were\n not found in your path.\nPlease verify your vnc installation."));
        return 1;
      }
      sflags += G_SPAWN_SEARCH_PATH;
      
      sprintf(buffer, cmd);
      c_argv[c_argc++] = g_strdup (buffer);

      if (rdp->screen_mode_id == 2) {
        sprintf(buffer, "-f");
        c_argv[c_argc++] = g_strdup (buffer);
      } else {
        switch (rdp->desktopwidth) {
        case 640:
          sprintf(buffer, "-g");
          c_argv[c_argc++] = g_strdup (buffer);
          sprintf(buffer, "640x480");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 800:
          sprintf(buffer, "-g");
          c_argv[c_argc++] = g_strdup (buffer);
          sprintf(buffer, "800x600");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 1024:
          sprintf(buffer, "-g");
          c_argv[c_argc++] = g_strdup (buffer);
          sprintf(buffer, "1024x768");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 1152:
          sprintf(buffer, "-g");
          c_argv[c_argc++] = g_strdup (buffer);
          sprintf(buffer, "1152x864");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        case 1280:
          sprintf(buffer, "-g");
          c_argv[c_argc++] = g_strdup (buffer);
          sprintf(buffer, "1280x960");
          c_argv[c_argc++] = g_strdup (buffer);
          break;
        default:
          break;
        }
      }

      switch (rdp->session_bpp) {
      case 8:
        sprintf(buffer, "-d");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "8");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 15:
        sprintf(buffer, "-d");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "15");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 16:
        sprintf(buffer, "-d");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "16");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 24:
        sprintf(buffer, "-d");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "24");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      default:
        break;
      }

      if (rdp->no_motion_events == 1) {
        sprintf(buffer, "-v");
        c_argv[c_argc++] = g_strdup (buffer);
      }

      if (strlen(rdp->proto_file) && g_file_test (rdp->proto_file, G_FILE_TEST_EXISTS)) {
        sprintf(buffer, "-passwd");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "%s", (char*)g_strescape(rdp->proto_file, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }

      // do this shit for all modes
      sprintf(buffer, "%s", (char*)g_strescape(rdp->full_address, NULL));
      //buffer[strlen(buffer)-4] = '\0';
      c_argv[c_argc++] = g_strdup (buffer);

    } else if (rdp->protocol == 2) {
    
      if (g_find_program_in_path ("Xnest")) {
        sflags += G_SPAWN_SEARCH_PATH;
        
        sprintf(buffer, "Xnest");
        c_argv[c_argc++] = strdup(buffer);
      } else {
        grd_error_message (_("Xnest was not found in your path.\nPlease verify your Xnest installation."));
        return 1;
      }
        
      sprintf(buffer, ":1");
      c_argv[c_argc++] = strdup(buffer);

      sprintf(buffer, "-once");
      c_argv[c_argc++] = strdup(buffer);

      switch (rdp->desktopwidth) {
      case 640:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "640x480");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 800:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "800x600");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1024:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1024x768");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1152:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1152x864");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1280:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1280x960");
        c_argv[c_argc++] = strdup(buffer);
        break;
      default:
        break;
      }

      sprintf(buffer, "-query");
      c_argv[c_argc++] = strdup(buffer);

      sprintf(buffer, "%s", (char*)g_strescape(rdp->full_address, NULL));
      buffer[strlen(buffer)-2] = '\0';
      c_argv[c_argc++] = strdup(buffer);

    } else if (rdp->protocol == 3) {
      // ICA/Citrix Connection
      if (g_find_program_in_path ("wfica")) {
        cmd = "wfica";
      } else if (g_file_test ("/usr/lib/ICAClient/wfica", G_FILE_TEST_EXISTS)) {
        cmd = "wfica";
      } else {
        grd_error_message (_("wfica was not found in your path.\nPlease verify your ICAClient installation."));
        return 1;
      }
      sprintf(buffer, cmd);
      c_argv[c_argc++] = g_strdup (buffer);

      if ( rdp->username && strlen (rdp->username) ) {
        sprintf(buffer, "-username %s", (char*)g_strescape(rdp->username, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }
      if ( rdp->password && strlen (rdp->password) ) {
        sprintf(buffer, "-password %s", (char*)g_strescape(rdp->password, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }
      if ( rdp->domain && strlen (rdp->domain) ) {
        sprintf(buffer, "-domain %s", (char*)g_strescape(rdp->domain, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }

      if ( rdp->client_hostname && strlen (rdp->client_hostname) ) {
        sprintf(buffer, "-clientname %s", (char*)g_strescape(rdp->client_hostname, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }

      switch (rdp->desktopwidth) {
      case 640:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "640x480");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 800:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "800x600");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1024:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1024x768");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1152:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1152x864");
        c_argv[c_argc++] = strdup(buffer);
        break;
      case 1280:
        sprintf(buffer, "-geometry");
        c_argv[c_argc++] = strdup(buffer);
        sprintf(buffer, "1280x960");
        c_argv[c_argc++] = strdup(buffer);
        break;
      default:
        break;
      }

      switch (rdp->session_bpp) {
      case 8:
        sprintf(buffer, "-depth");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "4");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 15:
        sprintf(buffer, "-depth");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "8");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 16:
        sprintf(buffer, "-depth");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "16");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      case 24:
        sprintf(buffer, "-depth");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "24");
        c_argv[c_argc++] = g_strdup (buffer);
        break;
      default:
        break;
      }

      if (rdp->enable_alternate_shell == 1) {
        if ( rdp->alternate_shell && strlen (rdp->alternate_shell) ) {
          sprintf(buffer, "-program %s", (char*)g_strescape(rdp->alternate_shell, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }

        if ( rdp->shell_working_directory && strlen (rdp->shell_working_directory) ) {
          sprintf(buffer, "-directory %s", (char*)g_strescape(rdp->shell_working_directory, NULL));
          c_argv[c_argc++] = g_strdup (buffer);
        }
      }

      if (strlen(rdp->proto_file) && g_file_test (rdp->proto_file, G_FILE_TEST_EXISTS)) {
        sprintf(buffer, "-passwd");
        c_argv[c_argc++] = g_strdup (buffer);
        sprintf(buffer, "%s", (char*)g_strescape(rdp->proto_file, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }

      if ( rdp->full_address && strlen (rdp->full_address) ) {
        sprintf(buffer, "-description %s", (char*)g_strescape(rdp->full_address, NULL));
        c_argv[c_argc++] = g_strdup (buffer);
      }
		
    }

    c_argv[c_argc++] = NULL;
    
    // complete events in gtk queue
    while(gtk_events_pending())
      gtk_main_iteration();

    if (launch_async == 0) {
      if (!g_spawn_sync (NULL, (gchar**)c_argv, NULL, G_SPAWN_SEARCH_PATH,
            NULL, NULL, &std_out, &std_err, &exit_stat, &err)) {
        g_warning ("failed: spawn_sync of %s\n", cmd);
        grd_error_message ( _("Failed to spawn.\nPlease verify your installation."));
      }
      if (std_err && strlen(std_err)) {
        g_warning ("\n%s\n", std_err);
        grd_error_message ((gchar*)std_err);
      }
    } else {
      if (!g_spawn_async (NULL, (gchar**)c_argv, NULL, G_SPAWN_SEARCH_PATH,
            NULL, NULL, NULL, &err)) {
        g_warning ("failed: spawn_async of %s\n", cmd);
      }
    }
    if (err) {
      g_warning ("message %s\n", err->message);
      grd_error_message (err->message);
    }

    for (cnt = 0; cnt < c_argc; cnt++) {
      #ifdef GRD_DEBUG
      printf ("arg %d:  %s\n", cnt, c_argv[cnt]);
      #endif
      free (c_argv[cnt]);
    }

  } else {
    // clean up and exit
    return 1;  
  }
  // clean up and exit
  return 0;
}


void grd_error_message (gchar *message)
{
  GtkWidget *dialog, *err_label, *label, *image, *hbox, *vbox;

  #ifdef GRD_DEBUG
  printf ("grd_error_message\n");
  #endif

  /* create the widgets */
  dialog = gtk_dialog_new_with_buttons ( _("GNOME Remote Desktop Error"),
                                        NULL,
                                        GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_STOCK_OK,
                                        GTK_RESPONSE_NONE,
                                        NULL);
  err_label = gtk_label_new ( _("An error has occurred."));
  label     = gtk_label_new (message);
  image     = gtk_image_new_from_stock (GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_DIALOG);
  hbox      = gtk_hbox_new (FALSE, 12);
  vbox      = gtk_vbox_new (FALSE, 0);
  gtk_box_set_spacing ((GtkBox*)hbox, 12);
  gtk_label_set_markup (GTK_LABEL (err_label), g_strconcat ("<span weight=\"bold\">", _("An error has occurred."), "</span>", NULL));
  gtk_label_set_justify (GTK_LABEL (err_label), GTK_JUSTIFY_LEFT);

  /* ensure that the dialog box is destroyed when the user responds. */
   
  g_signal_connect_swapped (GTK_OBJECT (dialog), 
                            "response", 
                            G_CALLBACK (gtk_widget_destroy),
                            GTK_OBJECT (dialog));

  /* add the label, and show everything we've added to the dialog. */
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 12);
  gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 12);
  gtk_box_pack_start (GTK_BOX (vbox), err_label, TRUE, TRUE, 12);
  gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 12);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox), hbox);
  gtk_box_set_spacing ((GtkBox*)GTK_DIALOG(dialog)->vbox, 12);
  gtk_widget_show_all (dialog);
}


void grd_about_dialog (void)
{
  static GtkWidget *about = NULL;
  const gchar *name      = _("GNOME Remote Desktop");
  const gchar *version   = VERSION;
  const gchar *copyright = "(C) 2003 Erick Woods\n<http://www.gnomepro.com/>";
  const gchar *authors[] = { "Erick Woods <erick@gnomepro.com>", NULL };
  const gchar *documenters[] = { NULL };
  const gchar *translator_credits = NULL;
  GdkPixbuf *pixbuf = create_pixbuf ("gnome-remote-desktop.png");

  const gchar *comments = \
    "GNOME Remote Desktop is a frontend for \nrdesktop <http://www.rdesktop.org/> \nand vncviewer.\n" \
    "\n" \
    "GNOME Remote Desktop is licensed under the \n" \
    "GNU General Public License (GPL)\n<http://www.gnu.org/licenses/gpl.html>\n" \
    "\n" \
    "Images by: Jakub \"jimmac\" Steiner and others.";
  
  if (about) {
    gtk_window_present (GTK_WINDOW (about));
    return;
  }
  
  about = gnome_about_new ( 
        name,
        version,
        copyright,
        comments, 
        authors, 
        documenters, 
        translator_credits,  
        pixbuf);
  
  //gtk_window_set_wmclass (GTK_WINDOW (about), "gnome remote desktop", "GNOME Remote Desktop");
  g_signal_connect (G_OBJECT (about), "destroy", G_CALLBACK (gtk_widget_destroyed), &about);
  gtk_widget_show (about);  
  return;
}


void grd_quick_pick_activate (GtkMenuItem *menuitem, gpointer user_data)
{
  gchar *file_name;
  rdp_file *rdp = NULL;

  #ifdef GRD_DEBUG
  printf ("grd_quick_pick_activate\n");
  #endif

  // build path to file
  file_name = g_build_path ((const gchar*)"/", g_get_home_dir(), ".gnome-remote-desktop", g_strdup ((const gchar*)user_data), NULL);
  // check for file in ~/
  if (g_file_test (file_name, G_FILE_TEST_EXISTS)) {
    // exists, load it
    rdp = g_new0 (rdp_file, 1);
    rdp_file_init (rdp);
    rdp_file_load (rdp, file_name);
    if (grd_launch_remote (rdp, 0, 1) == 0) {
      //mru_add_server (rdp->full_address);
    }
  }
  g_free (file_name);
  g_free (rdp);
  return;
}


void
grd_set_protocol_widgets (GtkWidget *main_win, gint protocol)
{
  #ifdef GRD_DEBUG
  printf ("grd_set_protocol_widgets: %d\n", protocol);
  #endif

  gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "btnProtoFile"), TRUE);
  gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProtoFile"), TRUE);
  
  gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "optSound"), TRUE);
  
  gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "chkStartProgram"), TRUE);
  if (gtk_toggle_button_get_active ((GtkToggleButton*)g_object_get_data (G_OBJECT (main_win), "chkStartProgram"))) {
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProgramPath"), TRUE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtStartFolder"), TRUE);
  } else {
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProgramPath"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtStartFolder"), FALSE);
  }

  switch (protocol) {
  case 0:  // rdp
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "optSound"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "btnProtoFile"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProtoFile"), FALSE);
    break;
  case 1:  // vnc
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "optSound"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "chkStartProgram"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProgramPath"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtStartFolder"), FALSE);
    break;
  case 2:  // xdmcp
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "btnProtoFile"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProtoFile"), FALSE);

    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "optSound"), FALSE);

    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "chkStartProgram"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtProgramPath"), FALSE);
    gtk_widget_set_sensitive ((GtkWidget*) g_object_get_data (G_OBJECT (main_win), "txtStartFolder"), FALSE);
    break;
  case 3:  // ica
    break;
  default:
    break;
  }
  
}
