/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/***************************************
*                                      *
*   MRU File Handlers                  *
*                                      *
***************************************/


#include <glib.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <dirent.h> 

#include "mrulist.h"
#include "rdpfile.h"
#include "support.h"

static int mru_file_to_list (GSList** list);
static int mru_list_to_file (GSList** list);

/***************************************
*                                      *
*   mru_file_to_list                   *
*                                      *
***************************************/

int mru_file_to_list (GSList** list)
{
  gchar *mru_filename;
  FILE* fptr;
  gchar buffer[MAX_SERVER_SIZE];
  int i;

  #ifdef GRD_DEBUG
  printf ("mru_file_to_list\n");
  #endif

  // create .gnome-remote-desktop dir in ~/
  mru_filename = g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "mru.grd", NULL);

  // open the file for reading
  if((fptr = fopen(mru_filename, "r")) == NULL) {
    return 1;   // bad path to file
  }

  // Grab the first line, it may indicate a unicode file
  buffer[0] = '\0';
  if(fgets(buffer, MAX_SERVER_SIZE, fptr) == NULL) {
    return 0; // complete successfully
  }

  i = 0;
  // the first line is already in the buffer
  do {
    if (buffer) {
      if(buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
      if(buffer[strlen(buffer) - 1] == '\r') buffer[strlen(buffer) - 1] = '\0';
      buffer[MAX_SERVER_SIZE - 1] = '\0';
      // append next structure to the list
      *list = g_slist_append(*list, g_strdup(buffer));
    }
    // read the next flat file record
    buffer[0] = '\n';
    i++;
  }
  while ((fgets(buffer, MAX_SERVER_SIZE, fptr) != NULL && i <= 10));

  // close file
  fclose (fptr);

  // clean up
  g_free (mru_filename);

  // complete successfully
  return 0;
}


/***************************************
*                                      *
*   mru_list_to_file                   *
*                                      *
***************************************/
 
int mru_list_to_file (GSList** list)
{
  GSList* lptr = NULL;
  gchar *mru_filename;
  FILE* fptr;
  char buffer[MAX_SERVER_SIZE * 10];
  int i;
  
  #ifdef GRD_DEBUG
  printf ("mru_list_to_file\n");
  #endif

  // create mru.grd dir in ~/.gnome-remote-desktop/
  mru_filename = g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "mru.grd", NULL);

  // open the file for writing
  if ((fptr = fopen(mru_filename, "wt")) == NULL) {
    return 1;   // bad fully qualified path to file
  }

  // write each record to the flat file in turn
  buffer[0] = '\0';
  lptr = *list;
  i = 0;
  while ((lptr != NULL && i <= 10)) {
    // combine the key, type, and value segments
    strncat (buffer, (char*)lptr->data, MAX_SERVER_SIZE - 2);
    strcat (buffer, "\r");
    strcat (buffer, "\n");

    /* move forward */
    lptr = lptr->next;
    i++;
  }
  //strncat(buffer, '\0', 1);
  
  // save flat file
  if (fputs (buffer, fptr) == EOF) {
    return 2;   // write failed
  }

  // close file
  fclose (fptr);
 
  g_free (mru_filename);

  // complete successfully
  return 0;
}


/***************************************
*                                      *
*   mru_add_server                     *
*                                      *
***************************************/
 
int mru_add_server (const char *server_name)
{
  GSList* list = NULL;
  GSList* list_new = NULL;

  #ifdef GRD_DEBUG
  printf ("mru_add_server\n");
  #endif

  mru_file_to_list (&list);

  while (list) {
    if (strcmp(server_name, (char*)list->data) != 0) {
      list_new = g_slist_append (list_new, (gpointer)g_strdup ((gchar*)list->data));
    }
    g_free (list->data);
    list = list->next;
  }
  g_slist_free (list);
  
  list_new = g_slist_prepend (list_new, (gpointer)g_strdup (server_name));

  mru_list_to_file (&list_new);

  while (list_new) {
    g_free (list_new->data);
    list_new = list_new->next;
  }
  g_slist_free (list_new);

  /* complete successfully */
  return 0;
}


/***************************************
*                                      *
*   mru_list_to_screen                 *
*                                      *
***************************************/
 
int mru_to_screen (GtkWidget *main_window)
{
  GList *server_items = NULL;
  GSList *server_list = NULL;
  GtkWidget *widget;
  int i;

  #ifdef GRD_DEBUG
  printf ("mru_to_screen\n");
  #endif

  // Get the mru.grd
  mru_file_to_list (&server_list);

  // load mru combos
  i = 0;
  while ((server_list != NULL && i <= 10)) {
    server_items = g_list_append (server_items, g_strdup (server_list->data));
    server_list = server_list->next;
    i++;
  }
  if (i > 0) {
    widget = lookup_widget (main_window, "cboComputer0");
    gtk_combo_set_popdown_strings (GTK_COMBO (widget), server_items);
    widget = lookup_widget (main_window, "cboComputer1");
    gtk_combo_set_popdown_strings (GTK_COMBO (widget), server_items);
  }
  g_list_free (server_items);

  return 0;
}


