/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/***************************************
*                                      *
*   RDP File Handlers                  *
*                                      *
***************************************/


#include <glib.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <dirent.h> 

#include "rdpfile.h"
#include "support.h"


/***************************************
*                                      *
*   rdp_file_init                      *
*                                      *
***************************************/

int rdp_file_init (rdp_file *rdp_in)
{
  rdp_file *rdp = NULL;
  
  #ifdef GRD_DEBUG
  printf ("rdp_file_init\n");
  #endif

  /* swap the return array */
  rdp = rdp_in;

  rdp->alternate_full_switch = 0;
  rdp->alternate_shell = "";
  rdp->audiomode = 0;
  rdp->auto_connect = 0;
  rdp->bitmapcachepersistenable = 0;
  rdp->client_hostname = "";
  rdp->compression = 0;
  rdp->desktop_size_id = 0;
  rdp->desktopheight = 0;
  rdp->desktopwidth = 0;
  rdp->disable_full_window_drag = 0;
  rdp->disable_menu_anims = 0;
  rdp->disable_themes = 0;
  rdp->disable_wallpaper = 0;
  rdp->displayconnectionbar = 0;
  rdp->domain = "";
  rdp->enable_alternate_shell = 0;
  rdp->enable_wm_keys = 0;
  rdp->full_address = "";
  rdp->keyboard_language = "";
  rdp->keyboardhook = 0;
  rdp->no_motion_events = 0;
  rdp->password = "";
  rdp->progman_group = "";
  rdp->protocol = 0;
  rdp->proto_file = "";
  rdp->redirectcomports = 0;
  rdp->redirectdrives = 0;
  rdp->redirectprinters = 0;
  rdp->redirectsmartcards = 0;
  rdp->screen_mode_id = 0;
  rdp->session_bpp = 0;
  rdp->shell_working_directory = "";
  rdp->username = "";
  rdp->winposstr = "";

  return 0;

}

/***************************************
*                                      *
*   rdp_file_load                      *
*                                      *
***************************************/

int rdp_file_load (rdp_file *rdp_in, const char *fqpath)
{
  FILE *fptr;
  int unicode;
  char buffer[MAX_BUFFER_SIZE];
  char tmpbuf[2];
  rdp_file *rdp = NULL;
  int i;
  int ch;
  
  #ifdef GRD_DEBUG
  printf ("rdp_file_load\n");
  #endif

  // swap the return array
  rdp = rdp_in;

  // open the file for reading
  if ((fptr = fopen(fqpath, "r")) == NULL) {
    return 1;  // bad fully qualified path to file
  }

  // Grab the first line, it may indicate a unicode file
  if (fgets(buffer, MAX_BUFFER_SIZE, fptr) == NULL) {
    // it's an empty file, close it
    fclose (fptr);
    // complete successfully
    return 0;
  }

  // Look for an indication of unicode
  unicode = 0;
  if ((strlen(buffer) == 2) || (strlen(buffer) == 3)) {
    // it's probably a unicode file
    if ((buffer[0] == (char)255) && (buffer[1] == (char)254)) {
      // it's unicode
      unicode = 1;
    }
  }

  // use this routine for ascii files
  if (!unicode) {
    // remember the first line is already in the buffer
    do {
      rdp_file_set_from_line (rdp, buffer);
      // read the next flat file record
    } while (fgets(buffer, MAX_BUFFER_SIZE, fptr) != NULL);
  }

  // use this routine for unicode files
  if (unicode) {
    // read each flat file record in turn
    rewind(fptr);
    ch = -1;
    buffer[0] = '\0';
    for (i=0; (i < 1000000 && feof(fptr) == 0); i++)
    {
      ch = fgetc(fptr);
      if (ch > 0 && (ch != 255 && ch != 254 && ch != 10)) {
        if (ch == 13) {
          rdp_file_set_from_line (rdp, buffer);
          buffer[0] = '\0';
        } else {
          g_unichar_to_utf8 (ch, tmpbuf);
          tmpbuf[1] = '\0';
          strcat(buffer, tmpbuf); 
          tmpbuf[0] = '\0';
          tmpbuf[1] = '\0';
        }
      }
    }
  }

  // close file
  fclose (fptr);

  // complete successfully
  return 0;
}


/***************************************
*                                      *
*   rdp_file_save                      *
*                                      *
***************************************/

int rdp_file_save (rdp_file *rdp_in, const char *fqpath)
{
  FILE* fptr;
  rdp_file *rdp = NULL;
  char *buffer;

  #ifdef GRD_DEBUG
  printf ("rdp_file_save\n");
  #endif

  // swap the return array
  rdp = rdp_in;

  // open the file for writing
  if((fptr = fopen(fqpath, "w")) == NULL) {
    return 1;  // bad fully qualified path to file
  }
  
  // combine the key, type, and value segments
  buffer = g_strconcat ("alternate full switch:i:", g_strdup_printf("%d",rdp->alternate_full_switch), "\r\n", NULL);
  buffer = g_strconcat (buffer, "alternate shell:s:", rdp->alternate_shell, "\r\n", NULL);
  buffer = g_strconcat (buffer, "audiomode:i:", g_strdup_printf("%d",rdp->audiomode), "\r\n", NULL);
  buffer = g_strconcat (buffer, "auto connect:i:", g_strdup_printf("%d",rdp->auto_connect), "\r\n", NULL);
  buffer = g_strconcat (buffer, "bitmapcachepersistenable:i:", g_strdup_printf("%d",rdp->bitmapcachepersistenable), "\r\n", NULL);
  buffer = g_strconcat (buffer, "client hostname:s:", rdp->client_hostname, "\r\n", NULL);
  buffer = g_strconcat (buffer, "compression:i:", g_strdup_printf("%d",rdp->compression), "\r\n", NULL);
  buffer = g_strconcat (buffer, "desktop size id:i:", g_strdup_printf("%d",rdp->desktop_size_id), "\r\n", NULL);
  buffer = g_strconcat (buffer, "desktopheight:i:", g_strdup_printf("%d",rdp->desktopheight), "\r\n", NULL);
  buffer = g_strconcat (buffer, "desktopwidth:i:", g_strdup_printf("%d",rdp->desktopwidth), "\r\n", NULL);
  buffer = g_strconcat (buffer, "disable full window drag:i:", g_strdup_printf("%d",rdp->disable_full_window_drag), "\r\n", NULL);
  buffer = g_strconcat (buffer, "disable menu anims:i:", g_strdup_printf("%d",rdp->disable_menu_anims), "\r\n", NULL);
  buffer = g_strconcat (buffer, "disable themes:i:", g_strdup_printf("%d",rdp->disable_themes), "\r\n", NULL);
  buffer = g_strconcat (buffer, "disable wallpaper:i:", g_strdup_printf("%d",rdp->disable_wallpaper), "\r\n", NULL);
  buffer = g_strconcat (buffer, "displayconnectionbar:i:", g_strdup_printf("%d",rdp->displayconnectionbar), "\r\n", NULL);
  buffer = g_strconcat (buffer, "domain:s:", rdp->domain, "\r\n", NULL);
  buffer = g_strconcat (buffer, "enable alternate shell:i:", g_strdup_printf("%d",rdp->enable_alternate_shell), "\r\n", NULL);
  buffer = g_strconcat (buffer, "enable wm keys:i:", g_strdup_printf("%d",rdp->enable_wm_keys), "\r\n", NULL);
  buffer = g_strconcat (buffer, "full address:s:", rdp->full_address, "\r\n", NULL);
  buffer = g_strconcat (buffer, "keyboard language:s:", rdp->keyboard_language, "\r\n", NULL);
  buffer = g_strconcat (buffer, "keyboardhook:i:", g_strdup_printf("%d",rdp->keyboardhook), "\r\n", NULL);
  buffer = g_strconcat (buffer, "no motion events:i:", g_strdup_printf("%d",rdp->no_motion_events), "\r\n", NULL);
  buffer = g_strconcat (buffer, "password:b:", rdp->password, "\r\n", NULL);
  buffer = g_strconcat (buffer, "progman group:s:", rdp->progman_group, "\r\n", NULL);
  buffer = g_strconcat (buffer, "protocol:i:", g_strdup_printf("%d",rdp->protocol), "\r\n", NULL);
  buffer = g_strconcat (buffer, "protocol file:s:", rdp->proto_file, "\r\n", NULL);
  buffer = g_strconcat (buffer, "redirectcomports:i:", g_strdup_printf("%d",rdp->redirectcomports), "\r\n", NULL);
  buffer = g_strconcat (buffer, "redirectdrives:i:", g_strdup_printf("%d",rdp->redirectdrives), "\r\n", NULL);
  buffer = g_strconcat (buffer, "redirectprinters:i:", g_strdup_printf("%d",rdp->redirectprinters), "\r\n", NULL);
  buffer = g_strconcat (buffer, "redirectsmartcards:i:", g_strdup_printf("%d",rdp->redirectsmartcards), "\r\n", NULL);
  buffer = g_strconcat (buffer, "screen mode id:i:", g_strdup_printf("%d",rdp->screen_mode_id), "\r\n", NULL);
  buffer = g_strconcat (buffer, "session bpp:i:", g_strdup_printf("%d",rdp->session_bpp), "\r\n", NULL);
  buffer = g_strconcat (buffer, "shell working directory:s:", rdp->shell_working_directory, "\r\n", NULL);
  buffer = g_strconcat (buffer, "username:s:", rdp->username, "\r\n", NULL);
  buffer = g_strconcat (buffer, "winposstr:s:", rdp->winposstr, "\r\n", NULL);

  // save one flat file record
  if(fputs(buffer, fptr) == EOF) {
    return 2;   // write failed
  }

  // clean up
  g_free (buffer);

  // close file
  fclose(fptr);
 
  // complete successfully
  return 0;
}


/***************************************
*                                      *
*   rdp_file_set_screen                *
*                                      *
***************************************/

int rdp_file_set_screen (rdp_file *rdp_in, GtkWidget *main_window)
{
  rdp_file *rdp = NULL;
  GtkWidget *widget;
  guint pos = 0;
  guint dsize = 0;
  
  #ifdef GRD_DEBUG
  printf ("rdp_file_set_screen\n");
  #endif

  /* swap the return array */
  rdp = rdp_in;

  widget = lookup_widget (main_window, "txtComputer0");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, rdp->full_address, strlen(rdp->full_address), &pos);

  widget = lookup_widget (main_window, "txtComputer1");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, rdp->full_address, strlen(rdp->full_address), &pos);

  widget = lookup_widget (main_window, "optProtocol0");
  gtk_option_menu_set_history (GTK_OPTION_MENU (widget), rdp->protocol);

  widget = lookup_widget (main_window, "optProtocol1");
  gtk_option_menu_set_history (GTK_OPTION_MENU (widget), rdp->protocol);

  widget = lookup_widget (main_window, "txtUsername");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->username, strlen(rdp->username), &pos);

  widget = lookup_widget (main_window, "txtDomain");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->domain, strlen(rdp->domain), &pos);

  widget = lookup_widget (main_window, "txtClientHostname");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->client_hostname, strlen(rdp->client_hostname), &pos);

  widget = lookup_widget (main_window, "txtProtoFile");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->proto_file, strlen(rdp->proto_file), &pos);

  widget = lookup_widget (main_window, "chkAltFullSwitch");
  if (rdp->alternate_full_switch == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  switch (rdp->desktopwidth) {
  case 640:
    dsize = 0;
    break;
  case 800:
    dsize = 1;
    break;
  case 1024:
    dsize = 2;
    break;
  case 1152:
    dsize = 3;
    break;
  case 1280:
    dsize = 4;
    break;
  default:
    dsize = 1234;
    break;
  }

  if (rdp->screen_mode_id == 2) {
    widget = lookup_widget (main_window, "optSize3");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
  } else {
    if (dsize < 1000) {
      widget = lookup_widget (main_window, "optSize2");
      gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
      widget = lookup_widget (main_window, "optSize");
      gtk_option_menu_set_history (GTK_OPTION_MENU (widget), dsize);
    } else {
      widget = lookup_widget (main_window, "optSize1");
      gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
      widget = lookup_widget (main_window, "optSize");
      gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)0);
    }
  }

  switch (rdp->session_bpp) {
  case 8:
    widget = lookup_widget (main_window, "optColor2");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
    widget = lookup_widget (main_window, "optColor");
    gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)0);
    break;
  case 15:
    widget = lookup_widget (main_window, "optColor2");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
    widget = lookup_widget (main_window, "optColor");
    gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)1);
    break;
  case 16:
    widget = lookup_widget (main_window, "optColor2");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
    widget = lookup_widget (main_window, "optColor");
    gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)2);
    break;
  case 24:
    widget = lookup_widget (main_window, "optColor2");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
    widget = lookup_widget (main_window, "optColor");
    gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)3);
    break;
  default:
    widget = lookup_widget (main_window, "optColor1");
    gtk_toggle_button_set_active ((GtkToggleButton*) widget, TRUE);
    widget = lookup_widget (main_window, "optColor");
    gtk_option_menu_set_history (GTK_OPTION_MENU (widget), (guint)2);
    break;
  }

  widget = lookup_widget (main_window, "txtKeyboardLang");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->keyboard_language, strlen(rdp->keyboard_language), &pos);


  widget = lookup_widget (main_window, "chkStartProgram");
  if (rdp->enable_alternate_shell == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "txtProgramPath");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  if (strlen(rdp->alternate_shell) > 0) {
    gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->alternate_shell, strlen(rdp->alternate_shell), &pos);
    gtk_editable_set_editable ((GtkEditable*) widget, TRUE);
    widget = lookup_widget (main_window, "chkStartProgram");
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  }

  widget = lookup_widget (main_window, "txtStartFolder");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);
  if (strlen(rdp->shell_working_directory) > 0) {
    gtk_editable_insert_text((GtkEditable*) widget, (gchar *)rdp->shell_working_directory, strlen(rdp->shell_working_directory), &pos);
    gtk_editable_set_editable ((GtkEditable*) widget, TRUE);
    widget = lookup_widget (main_window, "chkStartProgram");
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  }
  
  // the checkboxes on the performance frame
  widget = lookup_widget (main_window, "chkBitmapCache");
  if (rdp->bitmapcachepersistenable == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "chkDesktopBackground");
  if (rdp->disable_wallpaper == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "chkWindowContent");
  if (rdp->disable_full_window_drag == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "chkAnimation");
  if (rdp->disable_menu_anims == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "chkThemes");
  if (rdp->disable_themes == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);


  // the checkboxes on the performance frame (extras)
  widget = lookup_widget (main_window, "chkNoMotionEvents");
  if (rdp->no_motion_events == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  widget = lookup_widget (main_window, "chkEnableWMKeys");
  if (rdp->enable_wm_keys == 1)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);

  // the stuff on the resources frame
  widget = lookup_widget (main_window, "optSound");
  gtk_option_menu_set_history (GTK_OPTION_MENU (widget), rdp->audiomode);

  // the stuff on the resources frame
  widget = lookup_widget (main_window, "optKeyboard");
  gtk_option_menu_set_history (GTK_OPTION_MENU (widget), rdp->keyboardhook);

/*  widget = lookup_widget (main_window, "optKeyboard");
  switch (rdp->keyboardhook) {
  case 1:
    gtk_option_menu_set_history (GTK_OPTION_MENU(widget), 1);
    break;
  case 2:
    gtk_option_menu_set_history (GTK_OPTION_MENU(widget), 2);
    break;
  default:
    gtk_option_menu_set_history (GTK_OPTION_MENU(widget), 0);
    break;
  } */
      
  // clear out the password - it can not be retreived as it is encrypted in rdp files.  :-(
  widget = lookup_widget (main_window, "txtPassword");
  gtk_editable_delete_text ((GtkEditable*) widget, 0, -1);

  grd_set_protocol_widgets (main_window, rdp->protocol);
  
  // end if and drop out
  return 0;

}


/***************************************
*                                      *
*   rdp_file_get_screen                *
*                                      *
***************************************/

int rdp_file_get_screen (rdp_file *rdp_in, GtkWidget *main_window)
{
  rdp_file *rdp = NULL;
  GtkWidget *widget;
  char *value = "";
  
  #ifdef GRD_DEBUG
  printf ("rdp_file_get_screen\n");
  #endif

  /* swap the return array */
  rdp = rdp_in;
  
  widget = lookup_widget (main_window, "txtComputer0");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->full_address = g_strdup (value);

  widget = lookup_widget (main_window, "optProtocol0");
  rdp->protocol = gtk_option_menu_get_history (GTK_OPTION_MENU (widget));
  
  widget = lookup_widget (main_window, "txtUsername");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->username = g_strdup (value);
  
  widget = lookup_widget (main_window, "txtDomain");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->domain = g_strdup (value);
  
  widget = lookup_widget (main_window, "txtClientHostname");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->client_hostname = g_strdup (value);
    
  widget = lookup_widget (main_window, "txtProtoFile");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->proto_file = g_strdup (value);
  
  widget = lookup_widget (main_window, "txtKeyboardLang");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->keyboard_language = g_strdup (value);

  widget = lookup_widget (main_window, "chkAltFullSwitch");
  if (gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->alternate_full_switch = 1;
  else
    rdp->alternate_full_switch = 0;

  rdp->desktop_size_id = 1;
  rdp->screen_mode_id = 1;
  widget = lookup_widget (main_window, "optSize1");
  if (gtk_toggle_button_get_active ((GtkToggleButton *) widget)) {
    rdp->desktopwidth = 0;
    rdp->desktopheight = 0;
  }

  widget = lookup_widget (main_window, "optSize3");
  if (gtk_toggle_button_get_active ((GtkToggleButton *) widget)) {
    rdp->screen_mode_id = 2;
    rdp->desktopwidth = 0;
    rdp->desktopheight = 0;
  }

  widget = lookup_widget (main_window, "optSize2");
  if (gtk_toggle_button_get_active ((GtkToggleButton *) widget)) {
    widget = lookup_widget (main_window, "optSize");
    if ((int) gtk_option_menu_get_history (GTK_OPTION_MENU (widget)) > 0) {
      switch ((int) gtk_option_menu_get_history (GTK_OPTION_MENU (widget))) {
      case 0:
        rdp->desktopwidth = 640;
        rdp->desktopheight = 480;
        break;
      case 1:
        rdp->desktopwidth = 800;
        rdp->desktopheight = 600;
        break;
      case 2:
        rdp->desktopwidth = 1024;
        rdp->desktopheight = 768;
        break;
      case 3:
        rdp->desktopwidth = 1152;
        rdp->desktopheight = 864;
        break;
      case 4:
        rdp->desktopwidth = 1280;
        rdp->desktopheight = 960;
        break;
      default:
        rdp->desktopwidth = 0;
        rdp->desktopheight = 0;
        break;
      }
    }
  }
  
  widget = lookup_widget (main_window, "optColor1");
  if (gtk_toggle_button_get_active ((GtkToggleButton *) widget)) {
    rdp->session_bpp = 0;
  } else {
    widget = lookup_widget (main_window, "optColor");
    if ((int) gtk_option_menu_get_history (GTK_OPTION_MENU (widget)) > -1) {
      switch ((int) gtk_option_menu_get_history (GTK_OPTION_MENU (widget))) {
      case 0:
        rdp->session_bpp = 8;
        break;
      case 1:
        rdp->session_bpp = 15;
        break;
      case 2:
        rdp->session_bpp = 16;
        break;
      case 3:
        rdp->session_bpp = 24;
        break;
      default:
        rdp->session_bpp = 0;
        break;
      }
    }
  }
  
  widget = lookup_widget (main_window, "optKeyboard");
  rdp->keyboardhook = gtk_option_menu_get_history (GTK_OPTION_MENU (widget));

  widget = lookup_widget (main_window, "optSound");
  rdp->audiomode = gtk_option_menu_get_history (GTK_OPTION_MENU (widget));

  widget = lookup_widget (main_window, "chkStartProgram");
  if (gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->enable_alternate_shell = 1;
  else
    rdp->enable_alternate_shell = 0;

  widget = lookup_widget (main_window, "txtProgramPath");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->alternate_shell = g_strdup (value);
  
  widget = lookup_widget (main_window, "txtStartFolder");
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) rdp->shell_working_directory = g_strdup (value);
  

  widget = lookup_widget (main_window, "chkBitmapCache");
  if (!gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->bitmapcachepersistenable = 1;
  else
    rdp->bitmapcachepersistenable = 0;


  widget = lookup_widget (main_window, "chkDesktopBackground");
  if (!gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->disable_wallpaper = 1;
  else
    rdp->disable_wallpaper = 0;

  widget = lookup_widget (main_window, "chkWindowContent");
  if (!gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->disable_full_window_drag = 1;
  else
    rdp->disable_full_window_drag = 0;

  widget = lookup_widget (main_window, "chkAnimation");
  if (!gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->disable_menu_anims = 1;
  else
    rdp->disable_menu_anims = 0;

  widget = lookup_widget (main_window, "chkThemes");
  if (!gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->disable_themes = 1;
  else
    rdp->disable_themes = 0;

  
  // Extra
  widget = lookup_widget (main_window, "chkNoMotionEvents");
  if (gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->no_motion_events = 1;
  else
    rdp->no_motion_events = 0;

  widget = lookup_widget (main_window, "chkEnableWMKeys");
  if (gtk_toggle_button_get_active ((GtkToggleButton*)widget))
    rdp->enable_wm_keys = 1;
  else
    rdp->enable_wm_keys = 0;

  // clean up
  g_free (value);

  return 0;

}


/***************************************
*                                      *
*   rdp_file_set_from_line             *
*                                      *
***************************************/

int rdp_file_set_from_line (rdp_file *rdp_in, const char *str_in)
{
  char *str;
  char *token;
  char key[MAX_KEY_SIZE];
  char value[MAX_VALUE_SIZE];
  rdp_file *rdp = NULL;

  key[0] = '\0';
  value[0] = '\0';
  str = g_strdup (str_in);

  /* grab the key token */
  if ((token = strtok(str, ":")))
  {
    strncpy(key, token, MAX_KEY_SIZE - 1);
    key[MAX_KEY_SIZE - 1] = '\0';

    /* discard the type token */
    token = strtok(NULL, ":");

    /* grab the value token */
    if((token = strtok(NULL, "\n"))) {
      if(token[strlen(token) - 1] == '\r') token[strlen(token) - 1] = '\0';
      strncpy(value, token, MAX_VALUE_SIZE - 1);
      value[MAX_VALUE_SIZE - 1] = '\0';
    }

    rdp = rdp_in;

    if (strcmp(key, "alternate full switch") == 0) {
      rdp->alternate_full_switch = atoi(value);
    }
    if (strcmp(key, "alternate shell") == 0) {
      rdp->alternate_shell = g_strdup(value);
    }
    if (strcmp(key, "audiomode") == 0) {
      rdp->audiomode = atoi(value);
    }
    if (strcmp(key, "auto connect") == 0) {
      rdp->auto_connect = atoi(value);
    }
    if (strcmp(key, "bitmapcachepersistenable") == 0) {
      rdp->bitmapcachepersistenable = atoi(value);
    }
    if (strcmp(key, "client hostname") == 0) {
      rdp->client_hostname = g_strdup(value);
    }
    if (strcmp(key, "compression") == 0) {
      rdp->compression = atoi(value);
    }
    if (strcmp(key, "desktop size id") == 0) {
      rdp->desktop_size_id = atoi(value);
    }
    if (strcmp(key, "desktopheight") == 0) {
      rdp->desktopheight = atoi(value);
    }
    if (strcmp(key, "desktopwidth") == 0) {
      rdp->desktopwidth = atoi(value);
    }
    if (strcmp(key, "disable full window drag") == 0) {
      rdp->disable_full_window_drag = atoi(value);
    }
    if (strcmp(key, "disable menu anims") == 0) {
      rdp->disable_menu_anims = atoi(value);
    }
    if (strcmp(key, "disable themes") == 0) {
      rdp->disable_themes = atoi(value);
    }
    if (strcmp(key, "disable wallpaper") == 0) {
      rdp->disable_wallpaper = atoi(value);
    }
    if (strcmp(key, "displayconnectionbar") == 0) {
      rdp->displayconnectionbar = atoi(value);
    }
    if (strcmp(key, "domain") == 0) {
      rdp->domain = g_strdup(value);
    }
    if (strcmp(key, "enable alternate shell") == 0) {
      rdp->enable_alternate_shell = atoi(value);
    }
    if (strcmp(key, "enable wm keys") == 0) {
      rdp->enable_wm_keys = atoi(value);
    }
    if (strcmp(key, "full address") == 0) {
      rdp->full_address = g_strdup(value);
    }
    if (strcmp(key, "keyboard language") == 0) {
      rdp->keyboard_language = g_strdup(value);
    }
    if (strcmp(key, "keyboardhook") == 0) {
      rdp->keyboardhook = atoi(value);
    }
    if (strcmp(key, "no motion events") == 0) {
      rdp->no_motion_events = atoi(value);
    }
    if (strcmp(key, "password") == 0) {
      rdp->password = g_strdup(value);
    }
    if (strcmp(key, "progman group") == 0) {
      rdp->progman_group = g_strdup(value);
    }
    if (strcmp(key, "protocol") == 0) {
      rdp->protocol = atoi(value);
    }
    if (strcmp(key, "protocol file") == 0) {
      rdp->proto_file = g_strdup(value);
    }
    if (strcmp(key, "redirectcomports") == 0) {
      rdp->redirectcomports = atoi(value);
    }
    if (strcmp(key, "redirectdrives") == 0) {
      rdp->redirectdrives = atoi(value);
    }
    if (strcmp(key, "redirectprinters") == 0) {
      rdp->redirectprinters = atoi(value);
    }
    if (strcmp(key, "redirectsmartcards") == 0) {
      rdp->redirectsmartcards = atoi(value);
    }
    if (strcmp(key, "screen mode id") == 0) {
      rdp->screen_mode_id = atoi(value);
    }
    if (strcmp(key, "session bpp") == 0) {
      rdp->session_bpp = atoi(value);
    }
    if (strcmp(key, "shell working directory") == 0) {
      rdp->shell_working_directory = g_strdup(value);
    }
    if (strcmp(key, "username") == 0) {
      rdp->username = g_strdup(value);
    }
    if (strcmp(key, "winposstr") == 0) {
      rdp->winposstr = g_strdup(value);
    }

    //smode_id = atoi(value);
  }  // end if and drop out

  // clean up
  g_free (str);

  return 0;
}


/***************************************
*                                      *
*   rdp_load_profile_launcher          *
*                                      *
***************************************/
 
int rdp_load_profile_launcher (GtkWidget *main_window)
{
  GtkWidget *lbl;
  GtkWidget *opt;
  GtkWidget *mnu;
  GtkWidget *mi;
  GSList    *lptr = NULL;
  gchar     *name = NULL;
  gint cnt, ret;
  
  #ifdef GRD_DEBUG
  printf ("rdp_load_profile_launcher\n");
  #endif

  lbl = lookup_widget (main_window, "lblProfileLauncher");
  opt = lookup_widget (main_window, "optProfileLauncher");

  ret = rdp_files_to_list (&lptr);
  cnt = 0;
  mnu = gtk_menu_new ();
  while (lptr) {
    name = g_strdup (lptr->data);
    mi = gtk_menu_item_new_with_mnemonic (name);
    g_signal_connect (G_OBJECT (mi), "activate", G_CALLBACK (grd_quick_pick_activate), name);
    gtk_widget_ref (GTK_WIDGET (mi));
    gtk_container_add (GTK_CONTAINER (mnu), mi);
    //gtk_menu_shell_append ((GtkMenuShell *)mnu, mi);
    /* move forward */
    g_free (lptr->data);
    lptr = lptr->next;
    cnt++;
  }
  g_slist_free (lptr);
  g_free (name);
  g_object_ref (G_OBJECT (mnu));

  if (cnt < 1) {
    gtk_widget_hide (lbl);
    gtk_widget_hide (opt);
  } else {
    gtk_widget_show_all (mnu);
    gtk_option_menu_set_menu ((GtkOptionMenu*)opt, mnu);
  }

  /* complete successfully */
  return 0;
  
}


/***************************************
*                                      *
*   rdp_files_to_list                  *
*                                      *
***************************************/

int rdp_files_to_list (GSList** list)
{
  DIR *dptr;
  gchar *path_name;
  struct dirent *dp;
  gint i;

  #ifdef GRD_DEBUG
  printf ("rdp_files_to_list\n");
  #endif

  // create .gnome-remote-desktop dir in ~/
  path_name = g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", NULL);
  if ((dptr = opendir(path_name)) == NULL) {
    printf ("Can't open %s\n", path_name);
    return 1;
  }
  
  i = 0;
  for (dp = readdir(dptr); dp != NULL; dp = readdir(dptr)) {
    if (strlen(dp->d_name) > 4) {
      if (dp->d_name[strlen(dp->d_name) - 3] == 'r'
          && dp->d_name[strlen(dp->d_name) - 2] == 'd'
          && dp->d_name[strlen(dp->d_name) - 1] == 'p') {
        // append next structure to the list
        *list = g_slist_append (*list, (gpointer) g_strdup_printf ("%s", dp->d_name));
        i++;
      }
    }
	}
  #ifdef GRD_DEBUG
  printf ("rdp_files_to_list count: %d\n", g_slist_length (*list));
  #endif

  closedir (dptr);

  // complete successfully
  return 0;
}
