/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */


/***************************************
*                                      *
*   frmConnect Create & Events         *
*                                      *
***************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <unistd.h>

#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <libintl.h>
#define _(x) gettext(x)

#include "rdpfile.h"
#include "support.h"
#include "connect.h"
#include "mrulist.h"


/***************************************
*                                      *
*   frmConnect Create                  *
*                                      *
***************************************/

int 
create_frmConnect (void)
{
  gint i = 0;
  gchar *langlist[]={    "", "be", "bg", "br", "cf", "ch", 
    "cz", "dk", "dv", "et", "fr", "gk", 
    "gr", "hu", "is", "it", "jp", "ko", 
    "la", "nl", "no", "pl", "po", "ro",
    "ru", "sf", "sl", "sp", "su", "sv",
    "tr", "uk", "us", "usl", "usr", "yu", 
    NULL};
  gchar *size_items[] = {
    _("640 x 480 pixels"),    _("800 x 600 pixels"),
    _("1024 x 768 pixels"),
    _("1152 x 864 pixels"),
    _("1280 x 960 pixels"),
	  NULL
  };
  gchar *color_items[] = {
    _("256 Colors"),
    _("High Color (15 bit)"),
    _("High Color (16 bit)"),
    _("True Color (24 bit)"),
	  NULL
  };

  GtkWidget *frmConnect;
  
  GtkWidget *vbxComplete;
  GtkWidget *nbkComplete;
  GtkWidget *vbxCompact;

  GtkWidget *vbxMain;
  GtkWidget *vbxBanner;
  GtkWidget *lblBanner;

  // Compact Mode Widgets
  GtkWidget *tblCompact;
  GtkWidget *lblComputer1;
  GtkWidget *hbxComputer1;
  GtkWidget *cboComputer1;
  GtkWidget *txtComputer1;
  GtkWidget *optProtocol1;
  GtkWidget *mnuProtocol1;
  
    // Profile Launcher Widgets
    GtkWidget *lblProfileLauncher;
    GtkWidget *optProfileLauncher;

    // Compact Button Box Widgets
    GtkWidget *hbbAppOps1;
    GtkWidget *btnConnect1;
    GtkWidget *alnConnect1;
    GtkWidget *hbxConnect1;
    GtkWidget *imgConnect1;
    GtkWidget *lblConnect1;
    GtkWidget *btnQuit1;
    GtkWidget *btnHelp1;
    GtkWidget *btnMore;
    GtkWidget *alnMore;
    GtkWidget *hbxMore;
    GtkWidget *imgMore;
    GtkWidget *lblMore;

  // Connection Frame Widgets
  GtkWidget *lblConnectionProfile;
  GtkWidget *hbbFileOps;
  GtkWidget *btnSaveAs;
  GtkWidget *btnOpen;

  // General Tab Widgets
  GtkWidget *lblGeneralTab1;
  GtkWidget *vbxGeneralTab1;

    // Logon Frame Widgets
    GtkWidget *frameLogon;
    GtkWidget *tblLogon0;
    GtkWidget *tblLogon1;
    GtkWidget *lblLogonFrame;
    GtkWidget *lblLogonNote;
    GtkWidget *imgGeneralLogon;
    GtkWidget *lblComputer0;
    GtkWidget *cboComputer0;
    GtkWidget *txtComputer0;
    GtkWidget *lblProtocol0;
    GtkWidget *optProtocol0;
    GtkWidget *mnuProtocol0;
    GtkWidget *lblUsername;
    GtkWidget *txtUsername;
    GtkWidget *lblPassword;
    GtkWidget *txtPassword;
    GtkWidget *lblDomain;
    GtkWidget *txtDomain;
    GtkWidget *lblClientHostname;
    GtkWidget *txtClientHostname;
    GtkWidget *lblProtoFile;
    GtkWidget *hbxProtoFile;
    GtkWidget *txtProtoFile;
    GtkWidget *btnProtoFile;
    GtkWidget *lblProtoFile1;

  // Display Tab Widgets
  GtkWidget *lblDisplayTab1;
  GtkWidget *vbxDisplayTab1;

    // Size Frame Widgets
    GtkWidget *frameSize;
    GtkWidget *lblSizeFrame;
    GtkWidget *vbxSize;
    GtkWidget *imgSize;
    GtkWidget *hbxSize;
    GtkWidget *optSize1;
    GtkWidget *optSize2;
    GtkWidget *optSize3;
    GtkWidget *alnSize;
    GtkWidget *optSize;
    GtkWidget *alnAltFullSwitch;
    GtkWidget *chkAltFullSwitch;

    // Color Frame Widgets
    GtkWidget *frameColor;
    GtkWidget *lblColorFrame;
    GtkWidget *hbxColor;
    GtkWidget *imgColor;
    GtkWidget *vbxColor;
    GtkWidget *optColor1;
    GtkWidget *optColor2;
    GtkWidget *alnColor;
    GtkWidget *optColor;

  // Local Resources Tab Widgets
  GtkWidget *lblLocalTab1;
  GtkWidget *vbxLocalTab1;

    // Sound Frame Widgets
    GtkWidget *frameSound;
    GtkWidget *lblSoundFrame;
    GtkWidget *tblSound;
    GtkWidget *imgSound;
    GtkWidget *optSound;
    GtkWidget *mnuSound;
    GtkWidget *miSound;

    // Keyboard Frame Widgets
    GtkWidget *frameKeyboard;
    GtkWidget *lblKeyboardFrame;
    GtkWidget *tblKeyboard;
    GtkWidget *lblKeyboard;
    GtkWidget *imgKeyboard;
    GtkWidget *optKeyboard;
    GtkWidget *mnuKeyboard;
    GtkWidget *miKeyboard;
    GtkWidget *lblKeyboardLang;
    GtkWidget *cboKeyboardLang;
    GtkWidget *txtKeyboardLang;

  // Program Tab Widgets
  GtkWidget *lblProgramsTab1;

    // Program Frame Widgets
    GtkWidget *frameProgram;
    GtkWidget *lblProgramFrame;
    GtkWidget *tblProgram;
    GtkWidget *chkStartProgram;
    GtkWidget *imgProgram;
    GtkWidget *lblProgramPath;
    GtkWidget *txtProgramPath;
    GtkWidget *lblStartFolder;
    GtkWidget *txtStartFolder;

  // Performance Tab Widgets
  GtkWidget *lblPerformanceTab1;

    // Performance Frame Widgets
    GtkWidget *framePerform;
    GtkWidget *lblPerformFrame;
    GtkWidget *tblPerform;
    GtkWidget *imgPerform;
    GtkWidget *lblPerformanceOptions;
    GtkWidget *vbxExpChecks;
    GtkWidget *chkDesktopBackground;
    GtkWidget *chkWindowContent;
    GtkWidget *chkAnimation;
    GtkWidget *chkThemes;
    GtkWidget *chkBitmapCache;
    GtkWidget *chkNoMotionEvents;
    GtkWidget *chkEnableWMKeys;

  // Complete Button Box Widgets
  GtkWidget *hbbAppOps0;
  GtkWidget *btnConnect0;
  GtkWidget *alnConnect0;
  GtkWidget *hbxConnect0;
  GtkWidget *imgConnect0;
  GtkWidget *lblConnect0;
  GtkWidget *btnQuit0;
  GtkWidget *btnHelp0;
  GtkWidget *btnLess;
  GtkWidget *alnLess;
  GtkWidget *hbxLess;
  GtkWidget *imgLess;
  GtkWidget *lblLess;

  // Protocol Menu Item Widget
  GtkWidget *miProtocol;

  GList *cboKeyboard_items = NULL;
  GSList *optSize1_group = NULL;
  GSList *optColor1_group = NULL;

  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();

  #ifdef GRD_DEBUG
  printf ("create_frmConnect\n");
  #endif

  /*
    This is the main form
  */
  frmConnect = NULL;
  frmConnect = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (frmConnect, "frmConnect");
  gtk_window_set_title (GTK_WINDOW (frmConnect), _("GNOME Remote Desktop"));
  gtk_window_set_default_size (GTK_WINDOW (frmConnect), 405, -1);
  gtk_window_set_position (GTK_WINDOW (frmConnect), GTK_WIN_POS_CENTER);
  gtk_window_set_resizable (GTK_WINDOW (frmConnect), FALSE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (frmConnect), TRUE);
  gtk_window_set_icon (GTK_WINDOW (frmConnect), create_pixbuf ("gnome-remote-desktop.png"));
  
  /*
    This is the main container
  */
  vbxMain = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxMain, "vbxMain");
  gtk_widget_show (vbxMain);
  gtk_container_add (GTK_CONTAINER (frmConnect), vbxMain);

  /*
    This is the banner at the top
  */
  vbxBanner = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxBanner, "vbxBanner");
  gtk_box_pack_start (GTK_BOX (vbxMain), vbxBanner, TRUE, TRUE, 1);
  gtk_widget_show (vbxBanner);

  lblBanner = gtk_label_new (_("GNOME Remote Desktop"));
  gtk_widget_set_name (lblBanner, "lblBanner");
  gtk_label_set_markup (GTK_LABEL (lblBanner), g_strconcat ("<span size=\"larger\"><span weight=\"bold\">", _("GNOME Remote Desktop"), "</span></span>", NULL));
  gtk_widget_show (lblBanner);
  gtk_box_pack_start (GTK_BOX (vbxBanner), lblBanner, FALSE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (lblBanner), 0.5, 0);


  /*
    This is the complete container
  */
  vbxComplete = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxComplete, "vbxComplete");
  gtk_box_pack_start (GTK_BOX (vbxMain), vbxComplete, TRUE, TRUE, 0);

  /*
    This is the compact container
  */
  vbxCompact = gtk_vbox_new (FALSE, 6);
  gtk_widget_set_name (vbxCompact, "vbxCompact");
  gtk_box_pack_start (GTK_BOX (vbxMain), vbxCompact, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (vbxCompact), 6);

  tblCompact = gtk_table_new (2, 2, FALSE);
  gtk_widget_set_name (tblCompact, "tblCompact");
  gtk_table_set_row_spacings (GTK_TABLE (tblCompact), 6);
  gtk_table_set_col_spacings (GTK_TABLE (tblCompact), 6);
  gtk_container_add (GTK_CONTAINER (vbxCompact), tblCompact);


  /*
    This is the input for the Compact mode
  */
  
  lblComputer1 = gtk_label_new_with_mnemonic (_("Compu_ter:"));
  gtk_widget_set_name (lblComputer1, "lblComputer1");
  gtk_table_attach (GTK_TABLE (tblCompact), lblComputer1, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblComputer1), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblComputer1), 0, 0.5);

  hbxComputer1 = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxComputer1, "hbxComputer1");
  gtk_table_attach (GTK_TABLE (tblCompact), hbxComputer1, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  cboComputer1 = gtk_combo_new ();
  gtk_object_set_data (GTK_OBJECT (GTK_COMBO (cboComputer1)->popwin),
                       "GladeParentKey", cboComputer1);
  gtk_widget_set_name (cboComputer1, "cboComputer1");
  gtk_box_pack_start (GTK_BOX (hbxComputer1), cboComputer1, TRUE, TRUE, 0);
  gtk_combo_set_value_in_list (GTK_COMBO (cboComputer1), FALSE, FALSE);

  txtComputer1 = GTK_COMBO (cboComputer1)->entry;
  gtk_entry_set_activates_default (GTK_ENTRY (txtComputer1), TRUE);
  gtk_widget_set_name (txtComputer1, "txtComputer1");
  gtk_tooltips_set_tip (tooltips, txtComputer1, _("Enter the name or address of the remote system."), NULL);

  optProtocol1 = gtk_option_menu_new ();
  gtk_widget_set_name (optProtocol1, "optProtocol1");
  gtk_box_pack_start (GTK_BOX (hbxComputer1), optProtocol1, FALSE, FALSE, 0);
  gtk_tooltips_set_tip (tooltips, optProtocol1, _("Select the protocol to use for this connection."), NULL);

  mnuProtocol1 = gtk_menu_new ();
  gtk_widget_set_name (mnuProtocol1, "mnuProtocol1");

  miProtocol = gtk_menu_item_new_with_mnemonic ("RDP");
  gtk_container_add (GTK_CONTAINER (mnuProtocol1), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("RDP protocol is used to connect to Microsoft Terminal Servers and Windows XP systems with desktop sharing."), NULL);
  if (!g_find_program_in_path ("rdesktop")) { gtk_widget_set_sensitive (miProtocol, FALSE); }

  miProtocol = gtk_menu_item_new_with_mnemonic ("VNC");
  gtk_container_add (GTK_CONTAINER (mnuProtocol1), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("VNC protocol is used to connect to systems sharing the desktop by using VNC."), NULL);
  if (!g_find_program_in_path ("vncviewer") &&
      !g_find_program_in_path ("xvncviewer") &&
      !g_find_program_in_path ("xtightvncviewer") &&
      !g_find_program_in_path ("svncviewer") )
    { gtk_widget_set_sensitive (miProtocol, FALSE); }

  miProtocol = gtk_menu_item_new_with_mnemonic ("XDMCP");
  gtk_container_add (GTK_CONTAINER (mnuProtocol1), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("XDMCP protocol uses Xnest to connect to servers running X."), NULL);
  if (!g_find_program_in_path ("Xnest")) { gtk_widget_set_sensitive (miProtocol, FALSE); }

  miProtocol = gtk_menu_item_new_with_mnemonic ("ICA");
  gtk_container_add (GTK_CONTAINER (mnuProtocol1), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("ICA protocol is used to connect to Citrix servers."), NULL);
  if (!g_find_program_in_path ("wfica") &&
      !g_file_test ("/usr/lib/ICAClient/wfica", G_FILE_TEST_EXISTS) )
    { gtk_widget_set_sensitive (miProtocol, FALSE); }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optProtocol1), mnuProtocol1);


  /*
    This is the Profile Launcher
  */
  lblProfileLauncher = gtk_label_new_with_mnemonic (_("Prof_ile Launcher:"));
  gtk_widget_set_name (lblProfileLauncher, "lblProfileLauncher");
  gtk_table_attach (GTK_TABLE (tblCompact), lblProfileLauncher, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblProfileLauncher), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblProfileLauncher), 0, 0.5);

  optProfileLauncher = gtk_option_menu_new ();
  gtk_widget_set_name (optProfileLauncher, "optProfileLauncher");
  gtk_table_attach (GTK_TABLE (tblCompact), optProfileLauncher, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_tooltips_set_tip (tooltips, optProfileLauncher, _("Select a saved profile to launch it immediately."), NULL);


  /*
    This is the button box for the Compact mode
  */
  hbbAppOps1 = gtk_hbutton_box_new ();
  gtk_widget_set_name (hbbAppOps1, "hbbAppOps1");
  gtk_box_pack_start (GTK_BOX (vbxCompact), hbbAppOps1, FALSE, TRUE, 1);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbbAppOps1), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbbAppOps1), 6);

  btnHelp1 = gtk_button_new_from_stock ("gtk-help");
  gtk_widget_set_name (btnHelp1, "btnHelp1");
  gtk_container_add (GTK_CONTAINER (hbbAppOps1), btnHelp1);

  btnMore = gtk_button_new ();
  gtk_widget_set_name (btnMore, "btnMore");
  gtk_container_add (GTK_CONTAINER (hbbAppOps1), btnMore);

  alnMore = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_widget_set_name (alnMore, "alnMore");
  gtk_container_add (GTK_CONTAINER (btnMore), alnMore);

  hbxMore = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxMore, "hbxMore");
  gtk_container_add (GTK_CONTAINER (alnMore), hbxMore);

  imgMore = gtk_image_new_from_stock ("gtk-go-down", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_name (imgMore, "imgMore");
  gtk_box_pack_start (GTK_BOX (hbxMore), imgMore, FALSE, FALSE, 0);

  lblMore = gtk_label_new_with_mnemonic (_("_More"));
  gtk_widget_set_name (lblMore, "lblMore");
  gtk_box_pack_start (GTK_BOX (hbxMore), lblMore, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (lblMore), GTK_JUSTIFY_LEFT);

  btnQuit1 = gtk_button_new_from_stock ("gtk-cancel");
  gtk_widget_set_name (btnQuit1, "btnQuit1");
  gtk_container_add (GTK_CONTAINER (hbbAppOps1), btnQuit1);

  btnConnect1 = gtk_button_new ();
  gtk_widget_set_name (btnConnect1, "btnConnect1");
  gtk_container_add (GTK_CONTAINER (hbbAppOps1), btnConnect1);
  GTK_WIDGET_SET_FLAGS (btnConnect1, GTK_CAN_DEFAULT);

  alnConnect1 = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_widget_set_name (alnConnect1, "alnConnect1");
  gtk_container_add (GTK_CONTAINER (btnConnect1), alnConnect1);

  hbxConnect1 = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxConnect1, "hbxConnect1");
  gtk_container_add (GTK_CONTAINER (alnConnect1), hbxConnect1);

  imgConnect1 = gtk_image_new_from_stock ("gtk-ok", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_name (imgConnect1, "imgConnect1");
  gtk_box_pack_start (GTK_BOX (hbxConnect1), imgConnect1, FALSE, FALSE, 0);

  lblConnect1 = gtk_label_new_with_mnemonic (_("Co_nnect"));
  gtk_widget_set_name (lblConnect1, "lblConnect1");
  gtk_box_pack_start (GTK_BOX (hbxConnect1), lblConnect1, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (lblConnect1), GTK_JUSTIFY_LEFT);


  /*
    This is the label for the Connection Profile
  */
  lblConnectionProfile = gtk_label_new (_("Connection Profile"));
  gtk_widget_set_name (lblConnectionProfile, "lblConnectionProfile");
  gtk_label_set_markup (GTK_LABEL (lblConnectionProfile), g_strconcat ("<span weight=\"bold\">", _("Connection Profile"), "</span>", NULL));
  gtk_label_set_justify (GTK_LABEL (lblConnectionProfile), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblConnectionProfile), 0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbxComplete), lblConnectionProfile, TRUE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (lblConnectionProfile), 6, 3);


  /*
    This is the notebook for the Complete mode
  */
  nbkComplete = gtk_notebook_new ();
  gtk_widget_set_name (nbkComplete, "nbkComplete");
  gtk_box_pack_start (GTK_BOX (vbxComplete), nbkComplete, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (nbkComplete), 6);
  gtk_notebook_set_show_border (GTK_NOTEBOOK (nbkComplete), FALSE);

  vbxGeneralTab1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxGeneralTab1, "vbxGeneralTab1");
  gtk_container_add (GTK_CONTAINER (nbkComplete), vbxGeneralTab1);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (nbkComplete), vbxGeneralTab1,
                                      TRUE, TRUE, GTK_PACK_START);
                                      

  frameLogon = gtk_frame_new (NULL);
  gtk_widget_set_name (frameLogon, "frameLogon");
  gtk_box_pack_start (GTK_BOX (vbxGeneralTab1), frameLogon, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frameLogon), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameLogon), GTK_SHADOW_NONE);

  lblLogonFrame = gtk_label_new (_("Logon Settings"));
  gtk_widget_set_name (lblLogonFrame, "lblLogonFrame");
  gtk_label_set_markup (GTK_LABEL (lblLogonFrame), g_strconcat ("<span weight=\"bold\">", _("Logon Settings"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameLogon), lblLogonFrame);
  gtk_label_set_justify (GTK_LABEL (lblLogonFrame), GTK_JUSTIFY_LEFT);

  tblLogon0 = gtk_table_new (2, 2, FALSE);
  gtk_widget_set_name (tblLogon0, "tblLogon0");
  gtk_container_add (GTK_CONTAINER (frameLogon), tblLogon0);

  lblLogonNote = gtk_label_new (_("Type the name of the computer or choose a computer from the drop-down list."));
  gtk_widget_set_name (lblLogonNote, "lblLogonNote");
  gtk_table_attach (GTK_TABLE (tblLogon0), lblLogonNote, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblLogonNote), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (lblLogonNote), TRUE);
  gtk_misc_set_alignment (GTK_MISC (lblLogonNote), 0, 0.5);

  imgGeneralLogon = create_pixmap (frmConnect, "computer.png");
  gtk_widget_set_name (imgGeneralLogon, "imgGeneralLogon");
  gtk_table_attach (GTK_TABLE (tblLogon0), imgGeneralLogon, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgGeneralLogon), 3, 3);

  tblLogon1 = gtk_table_new (6, 2, FALSE);
  gtk_widget_set_name (tblLogon1, "tblLogon1");
  gtk_table_attach (GTK_TABLE (tblLogon0), tblLogon1, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_table_set_row_spacings (GTK_TABLE (tblLogon1), 4);
  gtk_table_set_col_spacings (GTK_TABLE (tblLogon1), 4);

  lblComputer0 = gtk_label_new_with_mnemonic (_("Compu_ter:"));
  gtk_widget_set_name (lblComputer0, "lblComputer0");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblComputer0, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblComputer0), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblComputer0), 0, 0.5);

  lblProtocol0 = gtk_label_new_with_mnemonic (_("Pro_tocol:"));
  gtk_widget_set_name (lblProtocol0, "lblProtocol0");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblProtocol0, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblProtocol0), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblProtocol0), 0, 0.5);

  lblUsername = gtk_label_new_with_mnemonic (_("_User Name:"));
  gtk_widget_set_name (lblUsername, "lblUsername");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblUsername, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblUsername), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblUsername), 0, 0.5);

  lblPassword = gtk_label_new_with_mnemonic (_("Pass_word:"));
  gtk_widget_set_name (lblPassword, "lblPassword");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblPassword, 0, 1, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblPassword), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblPassword), 0, 0.5);

  lblDomain = gtk_label_new_with_mnemonic (_("Do_main:"));
  gtk_widget_set_name (lblDomain, "lblDomain");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblDomain, 0, 1, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblDomain), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblDomain), 0, 0.5);

  lblClientHostname = gtk_label_new_with_mnemonic (_("C_lient Hostname:"));
  gtk_widget_set_name (lblClientHostname, "lblClientHostname");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblClientHostname, 0, 1, 5, 6,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblClientHostname), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblClientHostname), 0, 0.5);

  lblProtoFile = gtk_label_new_with_mnemonic (_("Prot_ocol File:"));
  gtk_widget_set_name (lblProtoFile, "lblProtoFile");
  gtk_table_attach (GTK_TABLE (tblLogon1), lblProtoFile, 0, 1, 6, 7,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 6, 6);
  gtk_label_set_justify (GTK_LABEL (lblProtoFile), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblProtoFile), 0, 0.5);

  cboComputer0 = gtk_combo_new ();
  gtk_object_set_data (GTK_OBJECT (GTK_COMBO (cboComputer0)->popwin),
                       "GladeParentKey", cboComputer0);
  gtk_widget_set_name (cboComputer0, "cboComputer0");
  gtk_combo_set_value_in_list (GTK_COMBO (cboComputer0), FALSE, FALSE);
  gtk_table_attach (GTK_TABLE (tblLogon1), cboComputer0, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  txtComputer0 = GTK_COMBO (cboComputer0)->entry;
  gtk_widget_set_name (txtComputer0, "txtComputer0");
  gtk_entry_set_activates_default (GTK_ENTRY (txtComputer0), TRUE);
  gtk_tooltips_set_tip (tooltips, txtComputer0, _("Enter the name or address of the remote system."), NULL);

  optProtocol0 = gtk_option_menu_new ();
  gtk_widget_set_name (optProtocol0, "optProtocol0");
  gtk_table_attach (GTK_TABLE (tblLogon1), optProtocol0, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, optProtocol0, _("Select the protocol to use for this connection."), NULL);

  mnuProtocol0 = gtk_menu_new ();
  gtk_widget_set_name (mnuProtocol0, "mnuProtocol0");

  miProtocol = gtk_menu_item_new_with_mnemonic ("RDP");
  gtk_container_add (GTK_CONTAINER (mnuProtocol0), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("RDP protocol is used to connect to Microsoft Terminal Servers and Windows XP systems with desktop sharing."), NULL);
  g_signal_connect (G_OBJECT (miProtocol), "activate", G_CALLBACK (on_protocol_changed), (int*)0);
  gtk_widget_ref (miProtocol);
  if (!g_find_program_in_path ("rdesktop")) { gtk_widget_set_sensitive (miProtocol, FALSE); }

  miProtocol = gtk_menu_item_new_with_mnemonic ("VNC");
  gtk_container_add (GTK_CONTAINER (mnuProtocol0), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("VNC protocol is used to connect to systems sharing the desktop by using VNC."), NULL);
  g_signal_connect (G_OBJECT (miProtocol), "activate", G_CALLBACK (on_protocol_changed), (int*)1);
  gtk_widget_ref (miProtocol);
  if (!g_find_program_in_path ("vncviewer") &&
      !g_find_program_in_path ("xvncviewer") &&
      !g_find_program_in_path ("xtightvncviewer") &&
      !g_find_program_in_path ("svncviewer") )
    { gtk_widget_set_sensitive (miProtocol, FALSE); }
    
  miProtocol = gtk_menu_item_new_with_mnemonic ("XDMCP");
  gtk_container_add (GTK_CONTAINER (mnuProtocol0), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("XDMCP protocol uses Xnest to connect to servers running X."), NULL);
  g_signal_connect (G_OBJECT (miProtocol), "activate", G_CALLBACK (on_protocol_changed), (int*)2);
  gtk_widget_ref (miProtocol);
  if (!g_find_program_in_path ("Xnest")) { gtk_widget_set_sensitive (miProtocol, FALSE); }

  miProtocol = gtk_menu_item_new_with_mnemonic ("ICA");
  gtk_container_add (GTK_CONTAINER (mnuProtocol0), miProtocol);
  gtk_tooltips_set_tip (tooltips, miProtocol, _("ICA protocol is used to connect to Citrix servers."), NULL);
  g_signal_connect (G_OBJECT (miProtocol), "activate", G_CALLBACK (on_protocol_changed), (int*)3);
  gtk_widget_ref (miProtocol);
  if (!g_find_program_in_path ("wfica") &&
      !g_file_test ("/usr/lib/ICAClient/wfica", G_FILE_TEST_EXISTS) )
    { gtk_widget_set_sensitive (miProtocol, FALSE); }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optProtocol0), mnuProtocol0);


  txtUsername = gtk_entry_new ();
  gtk_widget_set_name (txtUsername, "txtUsername");
  gtk_entry_set_activates_default (GTK_ENTRY (txtUsername), TRUE);
  gtk_table_attach (GTK_TABLE (tblLogon1), txtUsername, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, txtUsername, _("Enter the username for the remote system.\nFor VNC, enter the path to your saved vnc password file."), NULL);

  txtPassword = gtk_entry_new ();
  gtk_widget_set_name (txtPassword, "txtPassword");
  gtk_entry_set_activates_default (GTK_ENTRY (txtPassword), TRUE);
  gtk_table_attach (GTK_TABLE (tblLogon1), txtPassword, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_visibility (GTK_ENTRY (txtPassword), FALSE);
  gtk_tooltips_set_tip (tooltips, txtPassword, _("Enter the password for the remote system."), NULL);

  txtDomain = gtk_entry_new ();
  gtk_widget_set_name (txtDomain, "txtDomain");
  gtk_entry_set_activates_default (GTK_ENTRY (txtDomain), TRUE);
  gtk_table_attach (GTK_TABLE (tblLogon1), txtDomain, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, txtDomain, _("Enter the domain for the remote system."), NULL);

  txtClientHostname = gtk_entry_new ();
  gtk_widget_set_name (txtClientHostname, "txtClientHostname");
  gtk_entry_set_activates_default (GTK_ENTRY (txtClientHostname), TRUE);
  gtk_table_attach (GTK_TABLE (tblLogon1), txtClientHostname, 1, 2, 5, 6,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_tooltips_set_tip (tooltips, txtClientHostname, _("Enter the local hostname for this system."), NULL);

  hbxProtoFile = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxProtoFile, "hbxProtoFile");
  gtk_table_attach (GTK_TABLE (tblLogon1), hbxProtoFile, 1, 2, 6, 7,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  txtProtoFile = gtk_entry_new ();
  gtk_widget_set_name (txtProtoFile, "txtProtoFile");
  gtk_entry_set_activates_default (GTK_ENTRY (txtProtoFile), TRUE);
  gtk_box_pack_start (GTK_BOX (hbxProtoFile), txtProtoFile, TRUE, TRUE, 0);
  gtk_tooltips_set_tip (tooltips, txtProtoFile, _("Some protocols require a file containing settings. If required, enter the path to the file here."), NULL);

  btnProtoFile = gtk_button_new ();
  gtk_widget_set_name (btnProtoFile, "btnProtoFile");
  gtk_container_add (GTK_CONTAINER (hbxProtoFile), btnProtoFile);

  lblProtoFile1 = gtk_label_new_with_mnemonic ("...");
  gtk_widget_set_name (lblProtoFile1, "lblProtoFile1");
  gtk_container_add (GTK_CONTAINER (btnProtoFile), lblProtoFile1);

  vbxDisplayTab1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxDisplayTab1, "vbxDisplayTab1");
  gtk_container_add (GTK_CONTAINER (nbkComplete), vbxDisplayTab1);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (nbkComplete), vbxDisplayTab1,
                                      TRUE, TRUE, GTK_PACK_START);

  frameSize = gtk_frame_new (NULL);
  gtk_widget_set_name (frameSize, "frameSize");
  gtk_box_pack_start (GTK_BOX (vbxDisplayTab1), frameSize, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frameSize), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameSize), GTK_SHADOW_NONE);

  lblSizeFrame = gtk_label_new (_("Remote Desktop Size"));
  gtk_widget_set_name (lblSizeFrame, "lblSizeFrame");
  gtk_label_set_markup (GTK_LABEL (lblSizeFrame), g_strconcat ("<span weight=\"bold\">", _("Remote Desktop Size"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameSize), lblSizeFrame);
  gtk_label_set_justify (GTK_LABEL (lblSizeFrame), GTK_JUSTIFY_LEFT);

  hbxSize = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbxSize, "hbxSize");
  gtk_container_add (GTK_CONTAINER (frameSize), hbxSize);

  imgSize = create_pixmap (frmConnect, "size.png");
  gtk_widget_set_name (imgSize, "imgSize");
  gtk_box_pack_start (GTK_BOX (hbxSize), imgSize, FALSE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (imgSize), 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgSize), 3, 3);

  vbxSize = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxSize, "vbxSize");
  gtk_box_pack_start (GTK_BOX (hbxSize), vbxSize, TRUE, TRUE, 0);

  optSize1 = gtk_radio_button_new_with_mnemonic (NULL, _("Use default screen size"));
  gtk_widget_set_name (optSize1, "optSize1");
  gtk_box_pack_start (GTK_BOX (vbxSize), optSize1, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (optSize1), 3);
  gtk_tooltips_set_tip (tooltips, optSize1, _("Use the default screen size."), NULL);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (optSize1), optSize1_group);
  optSize1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (optSize1));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (optSize1), TRUE);

  optSize2 = gtk_radio_button_new_with_mnemonic (NULL, _("Use specified screen size"));
  gtk_widget_set_name (optSize2, "optSize2");
  gtk_box_pack_start (GTK_BOX (vbxSize), optSize2, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (optSize2), 3);
  gtk_tooltips_set_tip (tooltips, optSize2, _("Use the list to choose the screen size to use."), NULL);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (optSize2), optSize1_group);
  optSize1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (optSize2));

  alnSize = gtk_alignment_new (0.5, 0.5, 0.88, 1);
  gtk_widget_set_name (alnSize, "alnSize");
  gtk_box_pack_start (GTK_BOX (vbxSize), alnSize, FALSE, TRUE, 0);

  optSize = grd_option_menu_factory (size_items);
  gtk_widget_set_name (optSize, "optSize");
  gtk_container_add (GTK_CONTAINER (alnSize), optSize);

  optSize3 = gtk_radio_button_new_with_mnemonic (NULL, _("Operate in full screen mode"));
  gtk_widget_set_name (optSize3, "optSize3");
  gtk_box_pack_start (GTK_BOX (vbxSize), optSize3, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (optSize3), 3);
  gtk_tooltips_set_tip (tooltips, optSize3, _("Work in full screen mode."), NULL);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (optSize3), optSize1_group);
  optSize1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (optSize3));

  alnAltFullSwitch = gtk_alignment_new (0.5, 0.5, 0.88, 1);
  gtk_widget_set_name (alnAltFullSwitch, "alnAltFullSwitch");
  gtk_box_pack_start (GTK_BOX (vbxSize), alnAltFullSwitch, FALSE, FALSE, 0);
  gtk_widget_set_sensitive (alnAltFullSwitch, FALSE);

  chkAltFullSwitch = gtk_check_button_new_with_mnemonic (_("Use alternate full screen switch (-F)"));
  gtk_widget_set_name (chkAltFullSwitch, "chkAltFullSwitch");
  gtk_container_add (GTK_CONTAINER (alnAltFullSwitch), chkAltFullSwitch);
  gtk_container_set_border_width (GTK_CONTAINER (chkAltFullSwitch), 3);
  gtk_tooltips_set_tip (tooltips, chkAltFullSwitch, _("Check this if you use rdesktop with the unified patches."), NULL);


  frameColor = gtk_frame_new (NULL);
  gtk_widget_set_name (frameColor, "frameColor");
  gtk_box_pack_start (GTK_BOX (vbxDisplayTab1), frameColor, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frameColor), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameColor), GTK_SHADOW_NONE);

  lblColorFrame = gtk_label_new (_("Colors"));
  gtk_widget_set_name (lblColorFrame, "lblColorFrame");
  gtk_label_set_markup (GTK_LABEL (lblColorFrame), g_strconcat ("<span weight=\"bold\">", _("Colors"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameColor), lblColorFrame);
  gtk_label_set_justify (GTK_LABEL (lblColorFrame), GTK_JUSTIFY_LEFT);

  hbxColor = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbxColor, "hbxColor");
  gtk_container_add (GTK_CONTAINER (frameColor), hbxColor);

  imgColor = create_pixmap (frmConnect, "colors.png");
  gtk_widget_set_name (imgColor, "imgColor");
  gtk_box_pack_start (GTK_BOX (hbxColor), imgColor, FALSE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (imgColor), 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgColor), 3, 3);

  vbxColor = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxColor, "vbxColor");
  gtk_box_pack_start (GTK_BOX (hbxColor), vbxColor, TRUE, TRUE, 0);

  optColor1 = gtk_radio_button_new_with_mnemonic (NULL, _("Use default color depth"));
  gtk_widget_set_name (optColor1, "optColor1");
  gtk_box_pack_start (GTK_BOX (vbxColor), optColor1, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (optColor1), 3);
  gtk_tooltips_set_tip (tooltips, optColor1, _("Use the default color depth."), NULL);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (optColor1), optColor1_group);
  optColor1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (optColor1));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (optColor1), TRUE);

  optColor2 = gtk_radio_button_new_with_mnemonic (NULL, _("Use specified color depth"));
  gtk_widget_set_name (optColor2, "optColor2");
  gtk_box_pack_start (GTK_BOX (vbxColor), optColor2, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (optColor2), 3);
  gtk_tooltips_set_tip (tooltips, optColor2, _("Use the list to choose the color depth to use."), NULL);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (optColor2), optColor1_group);
  optColor1_group = gtk_radio_button_group (GTK_RADIO_BUTTON (optColor2));

  alnColor = gtk_alignment_new (0.5, 0.5, 0.88, 1);
  gtk_widget_set_name (alnColor, "alnColor");
  gtk_box_pack_start (GTK_BOX (vbxColor), alnColor, FALSE, TRUE, 0);

  optColor = grd_option_menu_factory (color_items);
  gtk_widget_set_name (optColor, "optColor");
  gtk_container_add (GTK_CONTAINER (alnColor), optColor);

  vbxLocalTab1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxLocalTab1, "vbxLocalTab1");
  gtk_container_add (GTK_CONTAINER (nbkComplete), vbxLocalTab1);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (nbkComplete), vbxLocalTab1,
                                      TRUE, TRUE, GTK_PACK_START);

  frameSound = gtk_frame_new (NULL);
  gtk_widget_set_name (frameSound, "frameSound");
  gtk_box_pack_start (GTK_BOX (vbxLocalTab1), frameSound, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frameSound), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameSound), GTK_SHADOW_NONE);

  lblSoundFrame = gtk_label_new_with_mnemonic (_("Remote Computer _Sound"));
  gtk_widget_set_name (lblSoundFrame, "lblSoundFrame");
  gtk_label_set_markup (GTK_LABEL (lblSoundFrame), g_strconcat ("<span weight=\"bold\">", _("Remote Computer Sound"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameSound), lblSoundFrame);
  gtk_label_set_justify (GTK_LABEL (lblSoundFrame), GTK_JUSTIFY_LEFT);

  tblSound = gtk_table_new (1, 2, FALSE);
  gtk_widget_set_name (tblSound, "tblSound");
  gtk_container_add (GTK_CONTAINER (frameSound), tblSound);
  gtk_table_set_col_spacings (GTK_TABLE (tblSound), 6);

  imgSound = create_pixmap (frmConnect, "sound.png");
  gtk_widget_set_name (imgSound, "imgSound");
  gtk_misc_set_padding (GTK_MISC (imgSound), 3, 3);
  gtk_table_attach (GTK_TABLE (tblSound), imgSound, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) 0, 0, 0);

  optSound = gtk_option_menu_new ();
  gtk_widget_set_name (optSound, "optSound");
  gtk_table_attach (GTK_TABLE (tblSound), optSound, 1, 2, 0, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  mnuSound = gtk_menu_new ();

  miSound = gtk_menu_item_new_with_mnemonic (_("On the local computer"));
  gtk_container_add (GTK_CONTAINER (mnuSound), miSound);

  miSound = gtk_menu_item_new_with_mnemonic (_("On the remote computer"));
  gtk_container_add (GTK_CONTAINER (mnuSound), miSound);

  miSound = gtk_menu_item_new_with_mnemonic (_("Do not play"));
  gtk_container_add (GTK_CONTAINER (mnuSound), miSound);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optSound), mnuSound);

  frameKeyboard = gtk_frame_new (NULL);
  gtk_widget_set_name (frameKeyboard, "frameKeyboard");
  gtk_box_pack_start (GTK_BOX (vbxLocalTab1), frameKeyboard, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (frameKeyboard), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameKeyboard), GTK_SHADOW_NONE);

  lblKeyboardFrame = gtk_label_new_with_mnemonic (_("_Keyboard"));
  gtk_widget_set_name (lblKeyboardFrame, "lblKeyboardFrame");
  gtk_label_set_markup (GTK_LABEL (lblKeyboardFrame), g_strconcat ("<span weight=\"bold\">", _("Keyboard"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameKeyboard), lblKeyboardFrame);
  gtk_label_set_justify (GTK_LABEL (lblKeyboardFrame), GTK_JUSTIFY_LEFT);

  tblKeyboard = gtk_table_new (2, 2, FALSE);
  gtk_widget_set_name (tblKeyboard, "tblKeyboard");
  gtk_container_add (GTK_CONTAINER (frameKeyboard), tblKeyboard);
  gtk_table_set_col_spacings (GTK_TABLE (tblKeyboard), 6);

  optKeyboard = gtk_option_menu_new ();
  gtk_widget_set_name (optKeyboard, "optKeyboard");
  gtk_table_attach (GTK_TABLE (tblKeyboard), optKeyboard, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  mnuKeyboard = gtk_menu_new ();

  miKeyboard = gtk_menu_item_new_with_mnemonic (_("On the local computer"));
  gtk_container_add (GTK_CONTAINER (mnuKeyboard), miKeyboard);

  miKeyboard = gtk_menu_item_new_with_mnemonic (_("On the remote computer"));
  gtk_container_add (GTK_CONTAINER (mnuKeyboard), miKeyboard);

  miKeyboard = gtk_menu_item_new_with_mnemonic (_("In full screen mode only"));
  gtk_container_add (GTK_CONTAINER (mnuKeyboard), miKeyboard);

  gtk_option_menu_set_menu (GTK_OPTION_MENU (optKeyboard), mnuKeyboard);


  lblKeyboardLang = gtk_label_new (_("Use the following keyboard language\n(2 char keycode)"));
  gtk_widget_set_name (lblKeyboardLang, "lblKeyboardLang");
  gtk_table_attach (GTK_TABLE (tblKeyboard), lblKeyboardLang, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblKeyboardLang), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblKeyboardLang), 0, 0.5);

  cboKeyboardLang = gtk_combo_new ();
  gtk_object_set_data ( GTK_OBJECT ( GTK_COMBO ( cboKeyboardLang)->popwin ),
			"GladeParentLangKey", cboKeyboardLang );
  gtk_widget_set_name ( cboKeyboardLang, "cboKeyboardLang" );
  gtk_table_attach (GTK_TABLE (tblKeyboard), cboKeyboardLang, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  cboKeyboard_items = NULL;
  i = 0;
  while(langlist[i]){
    cboKeyboard_items = g_list_append (cboKeyboard_items, (gpointer) langlist[i]);
	  i++;
  }
  gtk_combo_set_popdown_strings (GTK_COMBO (cboKeyboardLang), cboKeyboard_items);
  g_list_free (cboKeyboard_items);  

  txtKeyboardLang = GTK_COMBO (cboKeyboardLang)->entry;
  gtk_widget_set_name (txtKeyboardLang, "txtKeyboardLang");

  lblKeyboard = gtk_label_new (_("Apply Windows key combinations\n(for example ALT+TAB) (unsupported)"));
  gtk_widget_set_name (lblKeyboard, "lblKeyboard");
  gtk_table_attach (GTK_TABLE (tblKeyboard), lblKeyboard, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblKeyboard), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (lblKeyboard), 0, 0.5);

  imgKeyboard = create_pixmap (frmConnect, "keyboard.png");
  gtk_widget_set_name (imgKeyboard, "imgKeyboard");
  gtk_table_attach (GTK_TABLE (tblKeyboard), imgKeyboard, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) 0, 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgKeyboard), 3, 3);

  frameProgram = gtk_frame_new (NULL);
  gtk_widget_set_name (frameProgram, "frameProgram");
  gtk_container_add (GTK_CONTAINER (nbkComplete), frameProgram);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (nbkComplete), frameProgram,
                                      TRUE, TRUE, GTK_PACK_START);
  gtk_container_set_border_width (GTK_CONTAINER (frameProgram), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (frameProgram), GTK_SHADOW_NONE);

  lblProgramFrame = gtk_label_new (_("Start a Program"));
  gtk_widget_set_name (lblProgramFrame, "lblProgramFrame");
  gtk_label_set_markup (GTK_LABEL (lblProgramFrame), g_strconcat ("<span weight=\"bold\">", _("Start a Program"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (frameProgram), lblProgramFrame);
  gtk_label_set_justify (GTK_LABEL (lblProgramFrame), GTK_JUSTIFY_LEFT);

  tblProgram = gtk_table_new (2, 5, FALSE);
  gtk_widget_set_name (tblProgram, "tblProgram");
  gtk_container_add (GTK_CONTAINER (frameProgram), tblProgram);

  imgProgram = create_pixmap (frmConnect, "program.png");
  gtk_widget_set_name (imgProgram, "imgProgram");
  gtk_table_attach (GTK_TABLE (tblProgram), imgProgram, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgProgram), 3, 3);

  chkStartProgram = gtk_check_button_new_with_mnemonic (_("Start the following program on connection"));
  gtk_widget_set_name (chkStartProgram, "chkStartProgram");
  gtk_table_attach (GTK_TABLE (tblProgram), chkStartProgram, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_container_set_border_width (GTK_CONTAINER (chkStartProgram), 3);

  lblProgramPath = gtk_label_new (_("Program path and filename"));
  gtk_widget_set_name (lblProgramPath, "lblProgramPath");
  gtk_table_attach (GTK_TABLE (tblProgram), lblProgramPath, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblProgramPath), GTK_JUSTIFY_LEFT);

  txtProgramPath = gtk_entry_new ();
  gtk_widget_set_name (txtProgramPath, "txtProgramPath");
  gtk_table_attach (GTK_TABLE (tblProgram), txtProgramPath, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  lblStartFolder = gtk_label_new (_("Start in the following folder"));
  gtk_widget_set_name (lblStartFolder, "lblStartFolder");
  gtk_table_attach (GTK_TABLE (tblProgram), lblStartFolder, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblStartFolder), GTK_JUSTIFY_LEFT);

  txtStartFolder = gtk_entry_new ();
  gtk_widget_set_name (txtStartFolder, "txtStartFolder");
  gtk_table_attach (GTK_TABLE (tblProgram), txtStartFolder, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);


  framePerform = gtk_frame_new (NULL);
  gtk_widget_set_name (framePerform, "framePerform");
  gtk_container_add (GTK_CONTAINER (nbkComplete), framePerform);
  gtk_notebook_set_tab_label_packing (GTK_NOTEBOOK (nbkComplete), framePerform,
                                      TRUE, TRUE, GTK_PACK_START);
  gtk_container_set_border_width (GTK_CONTAINER (framePerform), 3);
  gtk_frame_set_shadow_type (GTK_FRAME (framePerform), GTK_SHADOW_NONE);

  lblPerformFrame = gtk_label_new (_("Optimize Performance"));
  gtk_widget_set_name (lblPerformFrame, "lblPerformFrame");
  gtk_label_set_markup (GTK_LABEL (lblPerformFrame), g_strconcat ("<span weight=\"bold\">", _("Optimize Performance"), "</span>", NULL));
  gtk_frame_set_label_widget (GTK_FRAME (framePerform), lblPerformFrame);
  gtk_label_set_justify (GTK_LABEL (lblPerformFrame), GTK_JUSTIFY_LEFT);

  tblPerform = gtk_table_new (2, 2, FALSE);
  gtk_widget_set_name (tblPerform, "tblPerform");
  gtk_container_add (GTK_CONTAINER (framePerform), tblPerform);

  imgPerform = create_pixmap (frmConnect, "perform.png");
  gtk_widget_set_name (imgPerform, "imgPerform");
  gtk_table_attach (GTK_TABLE (tblPerform), imgPerform, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_misc_set_padding (GTK_MISC (imgPerform), 3, 3);

  vbxExpChecks = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbxExpChecks, "vbxExpChecks");
  gtk_table_attach (GTK_TABLE (tblPerform), vbxExpChecks, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

  chkDesktopBackground = gtk_check_button_new_with_mnemonic (_("Desktop background (u)"));
  gtk_widget_set_name (chkDesktopBackground, "chkDesktopBackground");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkDesktopBackground, FALSE, FALSE, 0);

  chkWindowContent = gtk_check_button_new_with_mnemonic (_("Show content of window while dragging (u)"));
  gtk_widget_set_name (chkWindowContent, "chkWindowContent");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkWindowContent, FALSE, FALSE, 0);

  chkAnimation = gtk_check_button_new_with_mnemonic (_("Menu and window animation (u)"));
  gtk_widget_set_name (chkAnimation, "chkAnimation");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkAnimation, FALSE, FALSE, 0);

  chkThemes = gtk_check_button_new_with_mnemonic (_("Themes (u)"));
  gtk_widget_set_name (chkThemes, "chkThemes");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkThemes, FALSE, FALSE, 0);

  chkBitmapCache = gtk_check_button_new_with_mnemonic (_("Enable bitmap caching"));
  gtk_widget_set_name (chkBitmapCache, "chkBitmapCache");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkBitmapCache, FALSE, FALSE, 0);

  chkNoMotionEvents = gtk_check_button_new_with_mnemonic (_("Do not send motion events"));
  gtk_widget_set_name (chkNoMotionEvents, "chkNoMotionEvents");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkNoMotionEvents, FALSE, FALSE, 0);

  chkEnableWMKeys = gtk_check_button_new_with_mnemonic (_("Enable window manager's key bindings"));
  gtk_widget_set_name (chkEnableWMKeys, "chkEnableWMKeys");
  gtk_box_pack_start (GTK_BOX (vbxExpChecks), chkEnableWMKeys, FALSE, FALSE, 0);


  lblPerformanceOptions = gtk_label_new (_("Options available for optimizing performance"));
  gtk_widget_set_name (lblPerformanceOptions, "lblPerformanceOptions");
  gtk_table_attach (GTK_TABLE (tblPerform), lblPerformanceOptions, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_label_set_justify (GTK_LABEL (lblPerformanceOptions), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (lblPerformanceOptions), TRUE);
  gtk_misc_set_alignment (GTK_MISC (lblPerformanceOptions), 0, 0.5);

  /*
    These are the labels for the notebook control
  */

  lblGeneralTab1 = gtk_label_new_with_mnemonic (_("_General"));
  gtk_widget_set_name (lblGeneralTab1, "lblGeneralTab1");
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (nbkComplete), gtk_notebook_get_nth_page (GTK_NOTEBOOK (nbkComplete), 0), lblGeneralTab1);
  gtk_label_set_justify (GTK_LABEL (lblGeneralTab1), GTK_JUSTIFY_LEFT);

  lblDisplayTab1 = gtk_label_new_with_mnemonic (_("_Display"));
  gtk_widget_set_name (lblDisplayTab1, "lblDisplayTab1");
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (nbkComplete), gtk_notebook_get_nth_page (GTK_NOTEBOOK (nbkComplete), 1), lblDisplayTab1);
  gtk_label_set_justify (GTK_LABEL (lblDisplayTab1), GTK_JUSTIFY_LEFT);

  lblLocalTab1 = gtk_label_new_with_mnemonic (_("Local _Resources"));
  gtk_widget_set_name (lblLocalTab1, "lblLocalTab1");
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (nbkComplete), gtk_notebook_get_nth_page (GTK_NOTEBOOK (nbkComplete), 2), lblLocalTab1);
  gtk_label_set_justify (GTK_LABEL (lblLocalTab1), GTK_JUSTIFY_LEFT);

  lblProgramsTab1 = gtk_label_new_with_mnemonic (_("_Programs"));
  gtk_widget_set_name (lblProgramsTab1, "lblProgramsTab1");
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (nbkComplete), gtk_notebook_get_nth_page (GTK_NOTEBOOK (nbkComplete), 3), lblProgramsTab1);
  gtk_label_set_justify (GTK_LABEL (lblProgramsTab1), GTK_JUSTIFY_LEFT);

  lblPerformanceTab1 = gtk_label_new_with_mnemonic (_("Perf_ormance"));
  gtk_widget_set_name (lblPerformanceTab1, "lblPerformanceTab1");
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (nbkComplete), gtk_notebook_get_nth_page (GTK_NOTEBOOK (nbkComplete), 4), lblPerformanceTab1);
  gtk_label_set_justify (GTK_LABEL (lblPerformanceTab1), GTK_JUSTIFY_LEFT);

  gtk_label_set_mnemonic_widget (GTK_LABEL (lblComputer0), txtComputer1);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblComputer1), txtComputer0);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblProtocol0), optProtocol0);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblUsername), txtUsername);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblPassword), txtPassword);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblDomain), txtDomain);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblClientHostname), txtClientHostname);
  gtk_label_set_mnemonic_widget (GTK_LABEL (lblProtoFile), txtProtoFile);

  /*
    This is the frame for the Connection widgets
  */
  hbbFileOps = gtk_hbutton_box_new ();
  gtk_widget_set_name (hbbFileOps, "hbbFileOps");
  gtk_container_set_border_width (GTK_CONTAINER (hbbFileOps), 6);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbbFileOps), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbbFileOps), 6);
  gtk_box_pack_start (GTK_BOX (vbxComplete), hbbFileOps, TRUE, TRUE, 0);

  btnOpen = gtk_button_new_from_stock ("gtk-open");
  gtk_widget_set_name (btnOpen, "btnOpen");
  gtk_container_add (GTK_CONTAINER (hbbFileOps), btnOpen);

  btnSaveAs = gtk_button_new_from_stock ("gtk-save-as");
  gtk_widget_set_name (btnSaveAs, "btnSaveAs");
  gtk_container_add (GTK_CONTAINER (hbbFileOps), btnSaveAs);


  /*
    This is the button box for the Complete mode
  */

  hbbAppOps0 = gtk_hbutton_box_new ();
  gtk_widget_set_name (hbbAppOps0, "hbbAppOps0");
  gtk_box_pack_start (GTK_BOX (vbxComplete), hbbAppOps0, FALSE, TRUE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbbAppOps0), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbbAppOps0), 6);
  gtk_container_set_border_width (GTK_CONTAINER (hbbAppOps0), 6);

  btnHelp0 = gtk_button_new_from_stock ("gtk-help");
  gtk_widget_set_name (btnHelp0, "btnHelp0");
  gtk_container_add (GTK_CONTAINER (hbbAppOps0), btnHelp0);

  btnLess = gtk_button_new ();
  gtk_widget_set_name (btnLess, "btnLess");
  gtk_container_add (GTK_CONTAINER (hbbAppOps0), btnLess);

  alnLess = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_widget_set_name (alnLess, "alnLess");
  gtk_container_add (GTK_CONTAINER (btnLess), alnLess);

  hbxLess = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxLess, "hbxLess");
  gtk_container_add (GTK_CONTAINER (alnLess), hbxLess);

  imgLess = gtk_image_new_from_stock ("gtk-go-up", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_name (imgLess, "imgLess");
  gtk_box_pack_start (GTK_BOX (hbxLess), imgLess, FALSE, FALSE, 0);

  lblLess = gtk_label_new_with_mnemonic (_("_Less"));
  gtk_widget_set_name (lblLess, "lblLess");
  gtk_box_pack_start (GTK_BOX (hbxLess), lblLess, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (lblLess), GTK_JUSTIFY_LEFT);

  btnQuit0 = gtk_button_new_from_stock ("gtk-cancel");
  gtk_widget_set_name (btnQuit0, "btnQuit0");
  gtk_container_add (GTK_CONTAINER (hbbAppOps0), btnQuit0);

  btnConnect0 = gtk_button_new ();
  gtk_widget_set_name (btnConnect0, "btnConnect0");
  gtk_container_add (GTK_CONTAINER (hbbAppOps0), btnConnect0);
  GTK_WIDGET_SET_FLAGS (btnConnect0, GTK_CAN_DEFAULT);


  alnConnect0 = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_widget_set_name (alnConnect0, "alnConnect0");
  gtk_container_add (GTK_CONTAINER (btnConnect0), alnConnect0);

  hbxConnect0 = gtk_hbox_new (FALSE, 2);
  gtk_widget_set_name (hbxConnect0, "hbxConnect0");
  gtk_container_add (GTK_CONTAINER (alnConnect0), hbxConnect0);

  imgConnect0 = gtk_image_new_from_stock ("gtk-ok", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_name (imgConnect0, "imgConnect0");
  gtk_box_pack_start (GTK_BOX (hbxConnect0), imgConnect0, FALSE, FALSE, 0);

  lblConnect0 = gtk_label_new_with_mnemonic (_("Co_nnect"));
  gtk_widget_set_name (lblConnect0, "lblConnect0");
  gtk_box_pack_start (GTK_BOX (hbxConnect0), lblConnect0, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (lblConnect0), GTK_JUSTIFY_LEFT);


  gtk_signal_connect (GTK_OBJECT (frmConnect), "destroy_event",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (frmConnect), "delete_event",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnConnect0), "clicked",
                      GTK_SIGNAL_FUNC (on_btnConnect0_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnConnect1), "clicked",
                      GTK_SIGNAL_FUNC (on_btnConnect1_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnQuit0), "clicked",
                      GTK_SIGNAL_FUNC (on_btnQuit_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnQuit1), "clicked",
                      GTK_SIGNAL_FUNC (on_btnQuit_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnHelp0), "clicked",
                      GTK_SIGNAL_FUNC (on_btnHelp_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnHelp1), "clicked",
                      GTK_SIGNAL_FUNC (on_btnHelp_clicked),
                      NULL);

  gtk_signal_connect (GTK_OBJECT (optSize1), "clicked",
                      GTK_SIGNAL_FUNC (on_optSize1_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (optSize2), "clicked",
                      GTK_SIGNAL_FUNC (on_optSize2_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (optSize3), "clicked",
                      GTK_SIGNAL_FUNC (on_optSize3_clicked),
                      NULL);

  gtk_signal_connect (GTK_OBJECT (optColor1), "clicked",
                      GTK_SIGNAL_FUNC (on_optColor1_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (optColor2), "clicked",
                      GTK_SIGNAL_FUNC (on_optColor2_clicked),
                      NULL);

  gtk_signal_connect (GTK_OBJECT (btnSaveAs), "clicked",
                      GTK_SIGNAL_FUNC (on_btnSaveAs_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnOpen), "clicked",
                      GTK_SIGNAL_FUNC (on_btnOpen_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnProtoFile), "clicked",
                      GTK_SIGNAL_FUNC (on_btnProtoFile_clicked),
                      NULL);

  /*
  gtk_signal_connect (GTK_OBJECT (btnAdvanced), "clicked",
                      GTK_SIGNAL_FUNC (on_btnMore_clicked),
                      NULL);
  */

  gtk_signal_connect (GTK_OBJECT (btnMore), "clicked",
                      GTK_SIGNAL_FUNC (on_btnMore_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (btnLess), "clicked",
                      GTK_SIGNAL_FUNC (on_btnLess_clicked),
                      NULL);
  gtk_signal_connect ((gpointer) chkStartProgram, "toggled",
                      G_CALLBACK (on_chkStartProgram_toggled),
                      NULL);

  /* Store pointers to all widgets, for use by lookup_widget(). */
  gtk_object_set_data (GTK_OBJECT (frmConnect), "frmConnect", frmConnect);

  HOOKUP_OBJECT (frmConnect, vbxComplete, "vbxComplete");
  HOOKUP_OBJECT (frmConnect, nbkComplete, "nbkComplete");
  HOOKUP_OBJECT (frmConnect, vbxCompact, "vbxCompact");

  HOOKUP_OBJECT (frmConnect, vbxMain, "vbxMain");
  HOOKUP_OBJECT (frmConnect, vbxBanner, "vbxBanner");
  HOOKUP_OBJECT (frmConnect, lblBanner, "lblBanner");

  // Compact Mode Widgets
  HOOKUP_OBJECT (frmConnect, tblCompact, "tblCompact");
  HOOKUP_OBJECT (frmConnect, lblComputer1, "lblComputer1");
  HOOKUP_OBJECT (frmConnect, cboComputer1, "cboComputer1");
  HOOKUP_OBJECT (frmConnect, txtComputer1, "txtComputer1");
  HOOKUP_OBJECT (frmConnect, optProtocol1, "optProtocol1");

    // Profile Launcher Widgets
    HOOKUP_OBJECT (frmConnect, lblProfileLauncher, "lblProfileLauncher");
    HOOKUP_OBJECT (frmConnect, optProfileLauncher, "optProfileLauncher");

    // Compact Button Box Widgets
    HOOKUP_OBJECT (frmConnect, hbbAppOps1, "hbbAppOps1");
    HOOKUP_OBJECT (frmConnect, btnConnect1, "btnConnect1");
    HOOKUP_OBJECT (frmConnect, alnConnect1, "alnConnect1");
    HOOKUP_OBJECT (frmConnect, hbxConnect1, "hbxConnect1");
    HOOKUP_OBJECT (frmConnect, imgConnect1, "imgConnect1");
    HOOKUP_OBJECT (frmConnect, lblConnect1, "lblConnect1");
    HOOKUP_OBJECT (frmConnect, btnQuit1, "btnQuit1");
    HOOKUP_OBJECT (frmConnect, btnHelp1, "btnHelp1");
    HOOKUP_OBJECT (frmConnect, btnMore, "btnMore");
    HOOKUP_OBJECT (frmConnect, alnMore, "alnMore");
    HOOKUP_OBJECT (frmConnect, hbxMore, "hbxMore");
    HOOKUP_OBJECT (frmConnect, imgMore, "imgMore");
    HOOKUP_OBJECT (frmConnect, lblMore, "lblMore");


  // General Tab Widgets
  HOOKUP_OBJECT (frmConnect, lblGeneralTab1, "lblGeneralTab1");
  HOOKUP_OBJECT (frmConnect, vbxGeneralTab1, "vbxGeneralTab1");

    // Logon Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameLogon, "frameLogon");
    HOOKUP_OBJECT (frmConnect, lblLogonFrame, "lblLogonFrame");
    HOOKUP_OBJECT (frmConnect, tblLogon0, "tblLogon0");
    HOOKUP_OBJECT (frmConnect, tblLogon1, "tblLogon1");
    HOOKUP_OBJECT (frmConnect, lblLogonNote, "lblLogonNote");
    HOOKUP_OBJECT (frmConnect, imgGeneralLogon, "imgGeneralLogon");
    HOOKUP_OBJECT (frmConnect, lblComputer0, "lblComputer0");
    HOOKUP_OBJECT (frmConnect, cboComputer0, "cboComputer0");
    HOOKUP_OBJECT (frmConnect, txtComputer0, "txtComputer0");
    HOOKUP_OBJECT (frmConnect, lblProtocol0, "lblProtocol0");
    HOOKUP_OBJECT (frmConnect, optProtocol0, "optProtocol0");
    HOOKUP_OBJECT (frmConnect, mnuProtocol0, "mnuProtocol0");
    HOOKUP_OBJECT (frmConnect, lblUsername, "lblUsername");
    HOOKUP_OBJECT (frmConnect, txtUsername, "txtUsername");
    HOOKUP_OBJECT (frmConnect, lblPassword, "lblPassword");
    HOOKUP_OBJECT (frmConnect, txtPassword, "txtPassword");
    HOOKUP_OBJECT (frmConnect, lblDomain, "lblDomain");
    HOOKUP_OBJECT (frmConnect, txtDomain, "txtDomain");
    HOOKUP_OBJECT (frmConnect, lblClientHostname, "lblClientHostname");
    HOOKUP_OBJECT (frmConnect, txtClientHostname, "txtClientHostname");
    HOOKUP_OBJECT (frmConnect, lblProtoFile, "lblProtoFile");
    HOOKUP_OBJECT (frmConnect, hbxProtoFile, "hbxProtoFile");
    HOOKUP_OBJECT (frmConnect, txtProtoFile, "txtProtoFile");
    HOOKUP_OBJECT (frmConnect, btnProtoFile, "btnProtoFile");
    HOOKUP_OBJECT (frmConnect, lblProtoFile1, "lblProtoFile1");

    // Connection Frame Widgets
    HOOKUP_OBJECT (frmConnect, lblConnectionProfile, "lblConnectionProfile");
    HOOKUP_OBJECT (frmConnect, hbbFileOps, "hbbFileOps");
    HOOKUP_OBJECT (frmConnect, btnSaveAs, "btnSaveAs");
    HOOKUP_OBJECT (frmConnect, btnOpen, "btnOpen");

  // Display Tab Widgets
  HOOKUP_OBJECT (frmConnect, lblDisplayTab1, "lblDisplayTab1");
  HOOKUP_OBJECT (frmConnect, vbxDisplayTab1, "vbxDisplayTab1");

    // Size Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameSize, "frameSize");
    HOOKUP_OBJECT (frmConnect, lblSizeFrame, "lblSizeFrame");
    HOOKUP_OBJECT (frmConnect, hbxSize, "hbxSize");
    HOOKUP_OBJECT (frmConnect, imgSize, "imgSize");
    HOOKUP_OBJECT (frmConnect, vbxSize, "vbxSize");
    HOOKUP_OBJECT (frmConnect, optSize1, "optSize1");
    HOOKUP_OBJECT (frmConnect, optSize2, "optSize2");
    HOOKUP_OBJECT (frmConnect, alnSize, "alnSize");
    HOOKUP_OBJECT (frmConnect, optSize, "optSize");
    HOOKUP_OBJECT (frmConnect, optSize3, "optSize3");
    HOOKUP_OBJECT (frmConnect, alnAltFullSwitch, "alnAltFullSwitch");
    HOOKUP_OBJECT (frmConnect, chkAltFullSwitch, "chkAltFullSwitch");

    // Color Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameColor, "frameColor");
    HOOKUP_OBJECT (frmConnect, lblColorFrame, "lblColorFrame");
    HOOKUP_OBJECT (frmConnect, hbxColor, "hbxColor");
    HOOKUP_OBJECT (frmConnect, imgColor, "imgColor");
    HOOKUP_OBJECT (frmConnect, vbxColor, "vbxColor");
    HOOKUP_OBJECT (frmConnect, optColor1, "optColor1");
    HOOKUP_OBJECT (frmConnect, optColor2, "optColor2");
    HOOKUP_OBJECT (frmConnect, alnColor, "alnColor");
    HOOKUP_OBJECT (frmConnect, optColor, "optColor");

  // Local Resources Tab Widgets
  HOOKUP_OBJECT (frmConnect, lblLocalTab1, "lblLocalTab1");
  HOOKUP_OBJECT (frmConnect, vbxLocalTab1, "vbxLocalTab1");

    // Sound Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameSound, "frameSound");
    HOOKUP_OBJECT (frmConnect, lblSoundFrame, "lblSoundFrame");
    HOOKUP_OBJECT (frmConnect, tblSound, "tblSound");
    HOOKUP_OBJECT (frmConnect, imgSound, "imgSound");
    HOOKUP_OBJECT (frmConnect, optSound, "optSound");
    HOOKUP_OBJECT (frmConnect, mnuSound, "mnuSound");
    HOOKUP_OBJECT (frmConnect, miSound, "miSound");

    // Keyboard Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameKeyboard, "frameKeyboard");
    HOOKUP_OBJECT (frmConnect, lblKeyboardFrame, "lblKeyboardFrame");
    HOOKUP_OBJECT (frmConnect, tblKeyboard, "tblKeyboard");
    HOOKUP_OBJECT (frmConnect, lblKeyboard, "lblKeyboard");
    HOOKUP_OBJECT (frmConnect, optKeyboard, "optKeyboard");
    HOOKUP_OBJECT (frmConnect, mnuKeyboard, "mnuKeyboard");
    HOOKUP_OBJECT (frmConnect, miKeyboard, "miKeyboard");
    HOOKUP_OBJECT (frmConnect, lblKeyboardLang, "lblKeyboardLang");
    HOOKUP_OBJECT (frmConnect, cboKeyboardLang, "cboKeyboardLang");
    HOOKUP_OBJECT (frmConnect, txtKeyboardLang, "txtKeyboardLang");
    HOOKUP_OBJECT (frmConnect, imgKeyboard, "imgKeyboard");


  // Program Tab Widgets
  HOOKUP_OBJECT (frmConnect, lblProgramsTab1, "lblProgramsTab1");

    // Program Frame Widgets
    HOOKUP_OBJECT (frmConnect, frameProgram, "frameProgram");
    HOOKUP_OBJECT (frmConnect, lblProgramFrame, "lblProgramFrame");
    HOOKUP_OBJECT (frmConnect, tblProgram, "tblProgram");
    HOOKUP_OBJECT (frmConnect, chkStartProgram, "chkStartProgram");
    HOOKUP_OBJECT (frmConnect, imgProgram, "imgProgram");
    HOOKUP_OBJECT (frmConnect, lblProgramPath, "lblProgramPath");
    HOOKUP_OBJECT (frmConnect, txtProgramPath, "txtProgramPath");
    HOOKUP_OBJECT (frmConnect, lblStartFolder, "lblStartFolder");
    HOOKUP_OBJECT (frmConnect, txtStartFolder, "txtStartFolder");

  // Performance Tab Widgets
  HOOKUP_OBJECT (frmConnect, lblPerformanceTab1, "lblPerformanceTab1");

    // Performance Frame Widgets
    HOOKUP_OBJECT (frmConnect, framePerform, "framePerform");
    HOOKUP_OBJECT (frmConnect, lblPerformFrame, "lblPerformFrame");
    HOOKUP_OBJECT (frmConnect, tblPerform, "tblPerform");
    HOOKUP_OBJECT (frmConnect, imgPerform, "imgPerform");
    HOOKUP_OBJECT (frmConnect, lblPerformanceOptions, "lblPerformanceOptions");
    HOOKUP_OBJECT (frmConnect, vbxExpChecks, "vbxExpChecks");
    HOOKUP_OBJECT (frmConnect, chkDesktopBackground, "chkDesktopBackground");
    HOOKUP_OBJECT (frmConnect, chkWindowContent, "chkWindowContent");
    HOOKUP_OBJECT (frmConnect, chkAnimation, "chkAnimation");
    HOOKUP_OBJECT (frmConnect, chkThemes, "chkThemes");
    HOOKUP_OBJECT (frmConnect, chkBitmapCache, "chkBitmapCache");
    HOOKUP_OBJECT (frmConnect, chkNoMotionEvents, "chkNoMotionEvents");
    HOOKUP_OBJECT (frmConnect, chkEnableWMKeys, "chkEnableWMKeys");

  // Complete Button Box Widgets
  HOOKUP_OBJECT (frmConnect, hbbAppOps0, "hbbAppOps0");
  HOOKUP_OBJECT (frmConnect, btnConnect0, "btnConnect0");
  HOOKUP_OBJECT (frmConnect, alnConnect0, "alnConnect0");
  HOOKUP_OBJECT (frmConnect, hbxConnect0, "hbxConnect0");
  HOOKUP_OBJECT (frmConnect, imgConnect0, "imgConnect0");
  HOOKUP_OBJECT (frmConnect, lblConnect0, "lblConnect0");
  HOOKUP_OBJECT (frmConnect, btnQuit0, "btnQuit0");
  HOOKUP_OBJECT (frmConnect, btnHelp0, "btnHelp0");
  HOOKUP_OBJECT (frmConnect, btnLess, "btnLess");
  HOOKUP_OBJECT (frmConnect, alnLess, "alnLess");
  HOOKUP_OBJECT (frmConnect, hbxLess, "hbxLess");
  HOOKUP_OBJECT (frmConnect, imgLess, "imgLess");
  HOOKUP_OBJECT (frmConnect, lblLess, "lblLess");

  // load mru & profile launcher
  mru_to_screen (frmConnect);
  rdp_load_profile_launcher (frmConnect);

  // disable the keyboard opts until they are supported
  gtk_widget_set_sensitive (optKeyboard, FALSE);

  // toggle the start program thingy to false.  will be set after this by loading.
  gtk_toggle_button_set_active ((GtkToggleButton*) chkStartProgram, FALSE);
  gtk_widget_set_sensitive (txtProgramPath, FALSE);
  gtk_widget_set_sensitive (txtStartFolder, FALSE);
  
  gtk_widget_set_sensitive (alnSize, FALSE);
  gtk_toggle_button_set_active ((GtkToggleButton*) chkAltFullSwitch, FALSE);
  gtk_widget_set_sensitive (alnAltFullSwitch, FALSE);
  gtk_widget_set_sensitive (alnColor, FALSE);

  gtk_widget_show (frmConnect);

  gConnect = frmConnect;

  //return frmConnect;
  return 0;
}


/***************************************
*                                      *
*   Event Handlers                     *
*                                      *
***************************************/



void
on_btnConnect0_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *main_window;
  rdp_file *rdp = NULL;

  #ifdef GRD_DEBUG
  printf ("on_btnConnect0_clicked\n");
  #endif

  main_window = lookup_widget((GtkWidget*)button, "frmConnect");

  rdp = g_new0 (rdp_file, 1);
  rdp_file_init (rdp);
  rdp_file_get_screen (rdp, main_window);
  // save the damn thing as last
  rdp_file_save (rdp, g_build_path ("/", g_get_home_dir(), ".gnome-remote-desktop", "last.grd", NULL));

  gtk_widget_hide (main_window);
  gtk_widget_destroy (main_window);

  if (grd_launch_remote (rdp, 0, 0) == 0) {
    mru_add_server (rdp->full_address);
  }
  // recreate the main window
  //gConnect = create_frmConnect ();
  create_frmConnect ();
  // set the window mode (complex)
  grd_set_window_mode (gConnect, (int)GRD_WM_COMPLEX);
  rdp_file_set_screen (rdp, gConnect);
  gtk_widget_show (gConnect);
  g_free (rdp);
}


void
on_btnConnect1_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *widget;
  GtkWidget *main_window;
  rdp_file *rdp = NULL;
  const gchar* value;

  #ifdef GRD_DEBUG
  printf ("on_btnConnect1_clicked\n");
  #endif
  printf ("on_btnConnect1_clicked\n");

  main_window = lookup_widget((GtkWidget*)button, "frmConnect");
  widget = lookup_widget (main_window, "txtComputer1");

  rdp = g_new0 (rdp_file, 1);
  rdp_file_init (rdp);
  value = gtk_editable_get_chars ((GtkEditable*) widget, 0, -1);
  if (value) {
    rdp->full_address = g_strdup (value);
    gtk_widget_hide_all (main_window);
    if (grd_launch_remote (rdp, 1, 1) == 0) {
      mru_add_server (rdp->full_address);
    }
  }
  // we are in compact mode, so exit.
  g_free (rdp);
  gtk_widget_destroy (main_window);
  gtk_main_quit();
}


void
on_btnQuit_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *main_window;
  rdp_file *rdp_last = NULL;

  #ifdef GRD_DEBUG
  printf ("on_btnQuit_clicked\n");
  #endif

  main_window = gConnect;
  gtk_widget_hide (main_window);

  // save the damn thing
  rdp_last = g_new0 (rdp_file, 1);
  rdp_file_init (rdp_last);
  rdp_file_get_screen (rdp_last, main_window);
  rdp_file_save (rdp_last, g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "last.grd", NULL));
  g_free (rdp_last);

  gtk_main_quit();
}


void
on_btnHelp_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  grd_about_dialog ();
}


void
on_btnMore_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *main_window;
  main_window = lookup_widget((GtkWidget*)button, "frmConnect");
  grd_set_window_mode (main_window, (int)GRD_WM_COMPLEX);
}


void
on_btnLess_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *main_window;
  main_window = lookup_widget((GtkWidget*)button, "frmConnect");
  grd_set_window_mode (main_window, (int)GRD_WM_COMPACT);
}


void
on_optSize1_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *aln_size, *aln_afss;

  aln_size = lookup_widget (gConnect, "alnSize");
  aln_afss = lookup_widget (gConnect, "alnAltFullSwitch");
  
  gtk_widget_set_sensitive (aln_size, FALSE);
  gtk_widget_set_sensitive (aln_afss, FALSE);
}


void
on_optSize2_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *aln_size, *aln_afss;

  aln_size = lookup_widget (gConnect, "alnSize");
  aln_afss = lookup_widget (gConnect, "alnAltFullSwitch");
  gtk_widget_set_sensitive (aln_size, TRUE);
  gtk_widget_set_sensitive (aln_afss, FALSE);
}


void
on_optSize3_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *aln_size, *aln_afss;

  aln_size = lookup_widget (gConnect, "alnSize");
  aln_afss = lookup_widget (gConnect, "alnAltFullSwitch");
  gtk_widget_set_sensitive (aln_size, FALSE);
  gtk_widget_set_sensitive (aln_afss, TRUE);
}


void
on_optColor1_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *aln_color;

  aln_color = lookup_widget (gConnect, "alnColor");
  gtk_widget_set_sensitive (aln_color, FALSE);
}


void
on_optColor2_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *aln_color;

  aln_color = lookup_widget (gConnect, "alnColor");
  gtk_widget_set_sensitive (aln_color, TRUE);
}

void
on_btnSaveAs_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *save_selector = NULL;
  GtkWidget *main_window;
  rdp_file *rdp = NULL;

  save_selector = gtk_file_selection_new ("Select File");
  gtk_widget_set_name (save_selector, "save_selector");
  gtk_container_set_border_width (GTK_CONTAINER (save_selector), 12);
  gtk_window_set_position (GTK_WINDOW (save_selector), GTK_WIN_POS_CENTER);
  gtk_window_set_modal (GTK_WINDOW (save_selector), TRUE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (save_selector), TRUE);
  gtk_window_set_title ((GtkWindow*) save_selector, _("Input a filename to save as..."));
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (save_selector));
  gtk_file_selection_set_select_multiple (GTK_FILE_SELECTION (save_selector), FALSE);
  gtk_window_set_icon (GTK_WINDOW (save_selector), create_pixbuf ("gnome-remote-desktop.png"));
  gtk_widget_grab_default (GTK_FILE_SELECTION (save_selector)->ok_button);
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (save_selector), g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "/", NULL));

  if (gtk_dialog_run (GTK_DIALOG (save_selector)) == GTK_RESPONSE_OK) {
    gchar *filename = (gchar*)gtk_file_selection_get_filename (GTK_FILE_SELECTION (save_selector));
    if (filename) {
      #ifdef GRD_DEBUG
      printf ("filename: %s\n", filename);
      #endif
      main_window = lookup_widget((GtkWidget*)button, "frmConnect");
      // save rdp file
      rdp = g_new0 (rdp_file, 1);
      rdp_file_init (rdp);
      rdp_file_get_screen (rdp, main_window);
      rdp_file_save (rdp, filename);
      g_free (rdp);
    }
    g_free (filename);
  }
  gtk_widget_destroy (GTK_WIDGET (save_selector));
  return;
}


void
on_btnOpen_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *open_selector = NULL;
  GtkWidget *main_window;
  rdp_file *rdp = NULL;

  open_selector = gtk_file_selection_new ("Select File");
  gtk_widget_set_name (open_selector, "open_selector");
  gtk_container_set_border_width (GTK_CONTAINER (open_selector), 12);
  gtk_window_set_position (GTK_WINDOW (open_selector), GTK_WIN_POS_CENTER);
  gtk_window_set_modal (GTK_WINDOW (open_selector), TRUE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (open_selector), TRUE);
  gtk_window_set_title ((GtkWindow*) open_selector, _("Choose an RDP file to open..."));
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (open_selector));
  gtk_file_selection_set_select_multiple (GTK_FILE_SELECTION (open_selector), FALSE);
  gtk_window_set_icon (GTK_WINDOW (open_selector), create_pixbuf ("gnome-remote-desktop.png"));
  gtk_widget_grab_default (GTK_FILE_SELECTION (open_selector)->ok_button);
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (open_selector), g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "/", NULL));

  if (gtk_dialog_run (GTK_DIALOG (open_selector)) == GTK_RESPONSE_OK) {
    gchar *filename = (gchar*)gtk_file_selection_get_filename (GTK_FILE_SELECTION (open_selector));
    if (filename) {
      #ifdef GRD_DEBUG
      printf ("filename: %s\n", filename);
      #endif
      main_window = lookup_widget((GtkWidget*)button, "frmConnect");
      // check for file in ~/
      if (g_file_test (filename, G_FILE_TEST_EXISTS)) {
        // load if exists
        rdp = g_new0 (rdp_file, 1);
        rdp_file_init (rdp);
        rdp_file_load (rdp, filename);
        rdp_file_set_screen (rdp, main_window);
      }
      g_free (rdp);
    }
    g_free (filename);
  }
  gtk_widget_destroy (GTK_WIDGET (open_selector));
  return;
}


void
on_btnProtoFile_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
  GtkWidget *open_selector = NULL;
  GtkWidget *main_window, *proto_file;
  guint pos = 0;

  open_selector = gtk_file_selection_new ("Select File");
  gtk_widget_set_name (open_selector, "open_selector");
  gtk_container_set_border_width (GTK_CONTAINER (open_selector), 12);
  gtk_window_set_position (GTK_WINDOW (open_selector), GTK_WIN_POS_CENTER);
  gtk_window_set_modal (GTK_WINDOW (open_selector), TRUE);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (open_selector), TRUE);
  gtk_window_set_title ((GtkWindow*) open_selector, _("Choose an RDP file to open..."));
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (open_selector));
  gtk_file_selection_set_select_multiple (GTK_FILE_SELECTION (open_selector), FALSE);
  gtk_window_set_icon (GTK_WINDOW (open_selector), create_pixbuf ("gnome-remote-desktop.png"));
  gtk_widget_grab_default (GTK_FILE_SELECTION (open_selector)->ok_button);
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (open_selector), g_build_path ((const gchar *)"/", g_get_home_dir(), ".gnome-remote-desktop", "/", NULL));

  if (gtk_dialog_run (GTK_DIALOG (open_selector)) == GTK_RESPONSE_OK) {
    gchar *filename = (gchar*)gtk_file_selection_get_filename (GTK_FILE_SELECTION (open_selector));
    if (filename) {
      #ifdef GRD_DEBUG
      printf ("filename: %s\n", filename);
      #endif
      main_window = lookup_widget((GtkWidget*)button, "frmConnect");
      proto_file = lookup_widget((GtkWidget*)main_window, "txtProtoFile");
      gtk_editable_delete_text ((GtkEditable*) proto_file, 0, -1);
      gtk_editable_insert_text((GtkEditable*) proto_file, filename, strlen(filename), &pos);
    }
    g_free (filename);
  }
  gtk_widget_destroy (GTK_WIDGET (open_selector));
  return;
}


void
on_chkStartProgram_toggled             (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  GtkWidget *txtProgramPath, *txtStartFolder;
  txtProgramPath = lookup_widget (GTK_WIDGET (gConnect), "txtProgramPath");
  txtStartFolder = lookup_widget (GTK_WIDGET (gConnect), "txtStartFolder");

  if (gtk_toggle_button_get_active (togglebutton)) {
    gtk_widget_set_sensitive (txtProgramPath, TRUE);
    gtk_widget_set_sensitive (txtStartFolder, TRUE);
  } else {
    gtk_widget_set_sensitive (txtProgramPath, FALSE);
    gtk_widget_set_sensitive (txtStartFolder, FALSE);
  }
}


void
on_protocol_changed (GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *main_win;
  main_win = lookup_widget (GTK_WIDGET (menuitem), "frmConnect");
  grd_set_protocol_widgets (main_win, (gint)user_data);
}
