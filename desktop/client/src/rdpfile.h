/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/***************************************
*                                      *
*   Definitions                        *
*                                      *
***************************************/

#define MAX_KEY_SIZE 128
#define MAX_TYPE_SIZE 16
#define MAX_VALUE_SIZE 1024
#define MAX_BUFFER_SIZE 4096

typedef struct
{
  char key[MAX_KEY_SIZE];
  char type[MAX_TYPE_SIZE];
  char value[MAX_VALUE_SIZE];
} rdp_rec;

typedef struct
{
  int alternate_full_switch;
  char *alternate_shell;
  int audiomode;
  int auto_connect;
  int bitmapcachepersistenable;
  char *client_hostname;
  int compression;
  int desktop_size_id;
  int desktopheight;
  int desktopwidth;
  int disable_full_window_drag;
  int disable_menu_anims;
  int disable_themes;
  int disable_wallpaper;
  int displayconnectionbar;
  char *domain;
  int enable_alternate_shell;
  int enable_wm_keys;
  char *full_address;
  char *keyboard_language;
  int keyboardhook;
  int no_motion_events;
  char *password;
  char *progman_group;
  int protocol;
  char *proto_file;
  int redirectcomports;
  int redirectdrives;
  int redirectprinters;
  int redirectsmartcards;
  int screen_mode_id;
  int session_bpp;
  char *shell_working_directory;
  char *username;
  char *winposstr;
} rdp_file;


int rdp_file_init (rdp_file *rdp_in);
int rdp_file_load (rdp_file *rdp_in, const char *fqpath);
int rdp_file_save (rdp_file *rdp_in, const char *fqpath);
int rdp_file_set_screen (rdp_file *rdp_in, GtkWidget *main_window);
int rdp_file_get_screen (rdp_file *rdp_in, GtkWidget *main_window);
int rdp_file_set_from_line (rdp_file *rdp_in, const char *str_in);
int rdp_load_profile_launcher (GtkWidget *main_window);
int rdp_files_to_list (GSList** list);
