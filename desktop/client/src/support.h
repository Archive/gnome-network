/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#ifdef GRD_DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif

#define GRD_WM_COMPLEX 0
#define GRD_WM_COMPACT 1
#define MAX_ARGVS 16
#define MAX_ARGV_LEN 255

#define HOOKUP_OBJECT(component,widget,name) \
  gtk_object_set_data_full (GTK_OBJECT (component), name, \
    gtk_widget_ref (widget), (GtkDestroyNotify) gtk_widget_unref)

/*
 * This function returns a widget in a component created by Glade.
 * Call it with the toplevel widget in the component (i.e. a window/dialog),
 * or alternatively any widget in the component, and the name of the widget
 * you want returned.
 */
GtkWidget* lookup_widget (GtkWidget *widget, const gchar *widget_name);

gchar* find_pixmap_file (const gchar *filename);

/* Use this function to set the directory containing installed pixmaps. */
void add_pixmap_directory (const gchar *directory);

/* This is used to create the pixmaps used in the interface. */
GtkWidget* create_pixmap (GtkWidget *widget, const gchar *filename);

/* This is used to create the pixbufs used in the interface. */
GdkPixbuf* create_pixbuf (const gchar *filename);

GtkWidget *grd_option_menu_factory (gchar**vmnemonic);

int grd_check_files (void);

void grd_set_window_mode (GtkWidget *main_window, int window_mode);

int grd_launch_remote (rdp_file *rdp_in, int window_mode, int launch_async);

void grd_error_message (gchar *message);

void grd_about_dialog (void);

void grd_quick_pick_activate (GtkMenuItem *menuitem, gpointer user_data);

void grd_set_protocol_widgets (GtkWidget *main_win, gint protocol);
