/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */


/***************************************
*                                      *
*   frmConnect Create & Events         *
*                                      *
***************************************/

GtkWidget *gConnect;  // used for global accessibility of main form!  :-P

int create_frmConnect (void);

void on_btnConnect0_clicked (GtkButton *button, gpointer user_data);

void on_btnConnect1_clicked (GtkButton *button, gpointer user_data);

void on_btnQuit_clicked (GtkButton *button, gpointer user_data);

void on_btnHelp_clicked (GtkButton *button, gpointer user_data);

void on_btnMore_clicked (GtkButton *button, gpointer user_data);

void on_btnLess_clicked (GtkButton *button, gpointer user_data);

void on_optSize1_clicked (GtkButton *button, gpointer user_data);

void on_optSize2_clicked (GtkButton *button, gpointer user_data);

void on_optSize3_clicked (GtkButton *button, gpointer user_data);

void on_optColor1_clicked (GtkButton *button, gpointer user_data);

void on_optColor2_clicked (GtkButton *button, gpointer user_data);

void on_btnSaveAs_clicked (GtkButton *button, gpointer user_data);

void on_btnOpen_clicked (GtkButton *button, gpointer user_data);

void on_btnProtoFile_clicked (GtkButton *button, gpointer user_data);

void on_txtPerform_changed (GtkEditable *editable, gpointer user_data);

void on_chkStartProgram_toggled (GtkToggleButton *togglebutton, gpointer user_data);

void on_protocol_changed (GtkMenuItem *menuitem, gpointer user_data);
