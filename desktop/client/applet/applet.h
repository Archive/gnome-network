/*
 * GNOME Remote Desktop
 * A frontend for rdesktop and other remote desktop tools.
 * Originally distributed as Terminal Server Client.
 *
 * Copyright (C) (2002-2003) (Erick Woods) <erick@gnomepro.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/***************************************
*                                      *
*   gnome-remote-desktop applet code   *
*                                      *
***************************************/

#define GNOMELOCALEDIR "/usr/share/locale"

#define IS_PANEL_HORIZONTAL(x) (x == PANEL_APPLET_ORIENT_UP || x == PANEL_APPLET_ORIENT_DOWN)

#define APPLET_IID         "OAFIID:GNOME_RemoteDesktopApplet"
#define APPLET_FACTORY_IID "OAFIID:GNOME_RemoteDesktopApplet_Factory"
#define APPLET_UI          "GNOME_RemoteDesktopApplet.xml"

static gchar *grd_run_cmd = NULL;

static const char applet_menu_xml [] =
        "<popup name=\"button3\">\n"
        "  <menuitem name=\"RunRemoteDesktop\" verb=\"RunRemoteDesktop\" _label=\"_Run Remote Desktop...\"/>\n"
        "  <menuitem name=\"About\" verb=\"About\" _label=\"_About ...\" pixtype=\"stock\" pixname=\"gnome-stock-about\"/>\n"
        "</popup>\n";


typedef struct {
  PanelApplet *applet;
  PanelAppletOrient orientation;
  gint timeout;

  // The menu and scale
  GtkMenu     *popup;
  GtkWidget   *frame;
  GtkWidget   *image;
  GdkPixbuf   *imgsrc;
  GtkTooltips *tooltips;
} AppletData;


static gboolean grd_applet_factory (PanelApplet *applet, const gchar *iid, gpointer data);

void grd_applet_create (PanelApplet *applet);

gboolean applet_popup_show (GtkWidget *widget, GdkEvent *event, AppletData *data);

void applet_popup_hide (AppletData *data, gboolean revert);

void applet_menu_item (GtkMenuItem *menuitem, gpointer user_data);

gboolean applet_key_press_event (GtkWidget *widget, GdkEventKey *event, AppletData *data);

void applet_destroy (GtkWidget *widget, AppletData *data);

void applet_change_background (PanelApplet *applet,
          PanelAppletBackgroundType type,
          GdkColor *color,
          const gchar *pixmap, AppletData *data);

void applet_change_size (GtkWidget *w, gint size, AppletData *data);

void applet_change_orient (GtkWidget *w, PanelAppletOrient o, AppletData *data);

void applet_launch_client (BonoboUIComponent *uic, AppletData *data, const gchar *verbname);

void applet_about (BonoboUIComponent *uic, AppletData *data, const gchar *verbname);

const BonoboUIVerb grd_applet_menu_verbs[] = {
    BONOBO_UI_UNSAFE_VERB("RunRemoteDesktop", applet_launch_client),
    BONOBO_UI_UNSAFE_VERB("About", applet_about),
    BONOBO_UI_VERB_END
};

void add_atk_namedesc (GtkWidget *widget, const gchar *name, const gchar *desc);


