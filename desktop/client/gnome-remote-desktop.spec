%define	name		gnome-remote-desktop
%define	version		0.124
%define	release		1
%define	_desktop        %{_datadir}/gnome/apps/Internet
%define	_serverdir      %{_prefix}/lib/bonobo/servers
%define	_buildroot      %{_tmppath}/%{name}-%{version}


Summary:  GNOME Remote Desktop is a frontend for rdesktop and other remote desktop tools for the GNOME platform.
Name:     %{name}
Version:  %{version}
Release:  %{release}
Copyright: GPL
Group:    User Interface/Desktops
URL:      http://%{name}.sourceforge.net/ 
Vendor:		Erick Woods <erick@gnomepro.com>
Source:		%{name}-%{version}.tar.gz
BuildRoot:	/var/tmp/%{name}-%{version}
Requires:	glib2 >= 2.0.0, gtk2 >= 2.0.0, rdesktop >= 1.2.0
BuildRequires:	glib2-devel >= 2.0.0, gtk2-devel >= 2.0.0


%description
GNOME Remote Desktop is a frontend for rdesktop and other remote desktop tools for the GNOME platform.


%prep
%setup


%build
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%_prefix
make


%install
[ -n "%{_buildroot}" -a "%{_buildroot}" != / ] && rm -rf %{_buildroot}
make prefix=%{_buildroot}%{_prefix} install
mkdir -p %{_buildroot}%{_desktop}
cp *.png %{_buildroot}%{_datadir}/pixmaps/


%clean
[ -n "%{_buildroot}" -a "%{_buildroot}" != / ] && rm -rf %{_buildroot}


%files
%defattr(-,root,root)
%doc README ChangeLog AUTHORS NEWS
%{_prefix}/bin/gnome-remote-desktop
%{_prefix}/libexec/gnome-remote-desktop-applet
%{_serverdir}/GNOME_RemoteDesktopApplet.server
%{_desktop}/gnome-remote-desktop.desktop
%{_datadir}/pixmaps/*.png
%{_datadir}/pixmaps/gnome-remote-desktop
%{_datadir}/locale/*/LC_MESSAGES/gnome-remote-desktop.mo
%{_datadir}/application-registry/gnome-remote-desktop.*
%{_datadir}/mime-info/gnome-remote-desktop.*


%changelog
* Mon Sep 30 2002 - Erick Woods <erick@gnomepro.com>
 - This file was created

