#include <gnome.h>


void
on_checkbutton1_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton2_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_button1_activate                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button3_activate                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button3_pressed                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_button3_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_window1_destroy                     (GtkObject       *object,
                                        gpointer         user_data);
