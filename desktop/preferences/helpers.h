#include <gtk/gtk.h>

#define GCONFPATH "/apps/gnome-desktop-sharing/preferences"
#define GCONFKEY GCONFPATH"/enable"
#define GCONFKEY_PASSWD GCONFPATH"/password"
#define GCONFKEY_AUTOACCEPT GCONFPATH"/autoaccept"

void execute (char *cmd, gboolean sync);

